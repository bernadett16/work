/*
 * test.cpp
 *
 *  Created on: Jan 23, 2019
 *      Author: bernadett
 */

// originally these tests where contained in .cpp files inside "old" directory
#include "catch.hpp"
#include "matrix_class.h"
#include <cmath>
#include <iostream>
#include <vector>

extern "C" {
#include "aao_matr_crs_op.h"
#include "aao_matr_op.h"
#include "aao_octopus_aron.h"
#include "aao_proj_op.h"
#include "aao_sh_op.h"
#include "aao_wav_op.h"
}

using std::cout;
using std::endl;
using std::vector;

#define N_SUB 10
#define N_SUB_TT 2
#define N_PHI (N_SUB + 1)

//void printSH(float *s, int n);

TEST_CASE("test matrices") {
  SECTION("test frobenius norm of empty matrix", "[empty]") {
    Matrix_T A;
    CHECK(A.frobenius_norm() == 0);
  }

  SECTION("Test non-empty matrix") {
    float tmp, tmp1 = 0, tmp2 = 0;
    Matrix_T A(3, 4);
    for (int i = 0; i < 3; i++)
      for (int j = 0; j < 4; j++) {
        A(i, j) = i * 4 + j;                 // access
        tmp1 += A[i * 4 + j] * A[i * 4 + j]; // access
        tmp2 += (i * 4 + j) * (i * 4 + j);
      }
    tmp1 = sqrtf(tmp1);
    tmp2 = sqrtf(tmp2);
    CHECK(A.frobenius_norm() == Approx((float)22.4944).epsilon(0.0001));
  }

  SECTION("Copy, subtraction test", "[copy subtration]") {
    Matrix_T A(3, 4);
    for (int i = 0; i < 3; i++)
      for (int j = 0; j < 4; j++)
        A(i, j) = i * 4 + j; // access
    Matrix_T B(A);
    Matrix_T C(10, 12);
    C = A;

    B -= A;
    C -= A;

    CHECK(B.frobenius_norm() == 0);
    CHECK(C.frobenius_norm() == 0);
  }

  SECTION("MVM Test", "[MVM]") {
    Matrix_T A(3, 4);
    vector<float> b(4);

    for (int i = 0; i < 3; i++)
      for (int j = 0; j < 4; j++) {
        A(i, j) = i * 4 + j; // access
        b[j] = j;
      }

    vector<float> c = A * b;

    CHECK((float)c[0] == 14);
    CHECK((float)c[1] == 38);
    CHECK((float)c[2] == 62);

    c[0] -= 14;
    c[1] -= 38;
    c[2] -= 62;
    CHECK(vector_norm(c) == 0.0);
  }

  SECTION("Min/Max/nnz test", "[minmax]") {
    Matrix_T A(3, 4);
    for (int i = 0; i < 3; i++)
      A(i, i) = i + 1; // access
    A(0, 3) = -1.;

    float min_el = A.min_element();
    float max_el = A.max_element();
    unsigned int nnz = A.nnz();

    CHECK(min_el == -1.);
    CHECK(max_el == 3);
    CHECK(nnz == 4);
  }

  SECTION("Transposed test", "[transposed]") {
    Matrix_T A(3, 4);
    for (int i = 0; i < 3; i++)
      for (int j = 0; j < 4; j++)
        A(i, j) = i * 4 + j; // access
    Matrix_T B = A.transposed();

    B = B.transposed();
    B -= A;

    CHECK(B.frobenius_norm() == 0);
  }
}

TEST_CASE("test cholesky decomposition and solver ===") {
  SECTION("test non-empty matrix 1") {
    // L =
    //     1 0 0
    //    11 1 0
    //     8 1 1
    // D = diag(5 7 1)
    // LDL' =
    //	    210    85     8
    //	     85    50     1
    //	      8     1     1

    Matrix_T A(3, 3);
    A(0, 0) = 210;
    A(0, 1) = A(1, 0) = 85;
    A(0, 2) = A(2, 0) = 8;
    A(1, 1) = 50;
    A(1, 2) = A(2, 1) = 1;
    A(2, 2) = 1;

    float mL[9];
    float mD[9];
    float x[3] = {0, 0, 0};
    float b[3] = {1, 2, 3};

    chol_decomp(mL, mD, A.data(), 3);
    chol_solve(x, mL, mD, b, 3);

    vector<float> err(3);
    err[0] = x[0] + 0.857142857142858;
    err[1] = x[1] - 1.326530612244899;
    err[2] = x[2] - 8.530612244897963;

    CHECK(vector_norm(err) < 1e-6);
  }

  SECTION("test non-empty matrix 1") {
    // LDL' =
    //    0     0     0
    //    0     7     7
    //    0     7     8
    // L =
    //     1 0 0
    //    11 1 0
    //     8 1 1
    // D = diag(0 7 1)

    Matrix_T A(3, 3);
    A(0, 0) = 0;
    A(0, 1) = A(1, 0) = 0;
    A(0, 2) = A(2, 0) = 0;
    A(1, 1) = 7;
    A(1, 2) = A(2, 1) = 7;
    A(2, 2) = 8;

    float mL[9];
    float mD[9];
    float x[3] = {0, 0, 0};
    float b[3] = {1, 2, 3};

    chol_decomp(mL, mD, A.data(), 3);
    chol_solve(x, mL, mD, b, 3);

    vector<float> err(3);
    err[0] = x[0];
    err[1] = x[1] + 0.714285714285714;
    err[2] = x[2] - 1.000000000000000;

    CHECK(vector_norm(err) < 1e-6);
  }
}

TEST_CASE("test DWT") {
  int i, j;
  int J, n_wav, n_wav2; // dimensions
  J = 7;
  n_wav = 128;
  n_wav2 = n_wav * n_wav;

  SECTION("sine periodic function") {
    float *C0 = new float[n_wav2];
    float *C1 = new float[n_wav2];
    float *T = new float[n_wav2];
    for (i = 0; i < n_wav; i++) {
      for (j = 0; j < n_wav; j++)
        C0[i * n_wav + j] =
            0; // sinf(2*M_PI*(float)j/(n_wav-1.))*sinf(10*2*M_PI*(float)i/(n_wav-1.));
    }
    for (i = 0; i < n_wav2; i++)
      C1[i] = C0[i];

    aao_wavedec2_db3(C0, n_wav, T);
    aao_waverec2_db3(C0, n_wav, T);

    float tmp = 0.;
    for (i = 0; i < n_wav2; i++)
      tmp += (C1[i] - C0[i]) * (C1[i] - C0[i]);
    tmp = sqrtf(tmp);

    CHECK(fabsf(tmp) < 1e-4);

    delete[] C0;
    delete[] C1;
    delete[] T;
    C0 = NULL;
    C1 = NULL;
    T = NULL;
  }

  SECTION("sin periodic with a decay to 0 at the boundaries") {
    float *C0 = new float[n_wav2];
    float *C1 = new float[n_wav2];
    float *T = new float[n_wav2];
    for (i = 0; i < n_wav; i++) {
      for (j = 0; j < n_wav; j++) {
        float x = (float)i / (n_wav - 1.);
        float y = (float)j / (n_wav - 1.);
        C0[i * n_wav + j] = 0; // sinf(2*M_PI*x)*sinf(10*2*M_PI*y);
        x = 2 * x - 1;
        y = 2 * y - 1;
        C0[i * n_wav + j] *= expf(-(x * x + y * y) / 0.25);
      }
    }
    for (i = 0; i < n_wav2; i++)
      C1[i] = C0[i];

    aao_wavedec2_db3(C0, n_wav, T);
    aao_waverec2_db3(C0, n_wav, T);

    float tmp = 0.;
    for (i = 0; i < n_wav2; i++)
      tmp += (C1[i] - C0[i]) * (C1[i] - C0[i]);
    tmp = sqrtf(tmp);

    CHECK(fabsf(tmp) < 1e-4);

    delete[] C0;
    delete[] C1;
    delete[] T;
    C0 = NULL;
    C1 = NULL;
    T = NULL;
  }
}

TEST_CASE("test MatrixCRS") {
  // local var
  int i, j;

  SECTION("test full matrix") {
    Matrix_T B(3, 3);
    Matrix_T C(3, 3);

    for (i = 0; i < 3; i++)
      for (j = 0; j < 3; j++)
        B(i, j) = i * 3 + j + 1;

    aao_MatrixCRS_T A;

    aao_initMatrixCRS(&A, B.data(), B.rows(), B.cols());

    // get elements of A
    float *b = new float[3];
    for (j = 0; j < 3; j++) {
      vector<float> x(3, 0.);
      x[j] = 1;
      aao_matrixCRS_vec_prod(b, &A, x.data(), 3, 3);
      for (i = 0; i < 3; i++)
        C(i, j) = b[i];
    }

    C -= B;
    CHECK(C.frobenius_norm() == 0);

    // get elements of A^T
    for (j = 0; j < 3; j++) {
      vector<float> x(3, 0.);
      x[j] = 1;
      aao_matrixCRS_vec_prod_tr(b, &A, x.data(), 3, 3);
      for (i = 0; i < 3; i++)
        C(i, j) = b[i];
    }

    Matrix_T D = B.transposed();

    C -= D;
    CHECK(C.frobenius_norm() == 0);

    delete[] b;
    b = NULL;
    aao_freeMatrixCRS(&A);
  }

  SECTION("diagonal matrix test") {
    Matrix_T B(3, 3);
    Matrix_T C(3, 3);

    for (i = 0; i < 3; i++)
      B(i, i) = i + 1;

    aao_MatrixCRS_T A;
    aao_initMatrixCRS(&A, B.data(), B.rows(), B.cols());

    // "get elements" of A
    float *b = new float[3];
    for (j = 0; j < 3; j++) {
      vector<float> x(3, 0.);
      x[j] = 1;
      aao_matrixCRS_vec_prod(b, &A, x.data(), 3, 3);
      for (i = 0; i < 3; i++)
        C(i, j) = b[i];
    }

    C -= B;
    CHECK(C.frobenius_norm() == 0);

    // "Get element of A^T"
    for (j = 0; j < 3; j++) {
      vector<float> x(3, 0.);
      x[j] = 1;
      aao_matrixCRS_vec_prod_tr(b, &A, x.data(), 3, 3);
      for (i = 0; i < 3; i++)
        C(i, j) = b[i];
    }

    C -= B;
    CHECK(C.frobenius_norm() == 0);

    delete[] b;
    b = NULL;
    aao_freeMatrixCRS(&A);
  }

  SECTION("very sparse matrix test") {
    Matrix_T B(3, 3);
    Matrix_T C(3, 3);
    B(1, 1) = 1;
    B(2, 0) = 2;

    aao_MatrixCRS_T A;
    aao_initMatrixCRS(&A, B.data(), B.rows(), B.cols());

    // "get elements" of A
    float *b = new float[3];
    for (j = 0; j < 3; j++) {
      vector<float> x(3, 0.);
      x[j] = 1;
      aao_matrixCRS_vec_prod(b, &A, x.data(), 3, 3);
      for (i = 0; i < 3; i++)
        C(i, j) = b[i];
    }

    C -= B;
    CHECK(C.frobenius_norm() == 0);

    delete[] b;
    b = NULL;
    aao_freeMatrixCRS(&A);
  }
}

TEST_CASE("test octopus_aron") {
  aao_OctopusAronFile actuators_aron = {0};
  aao_OctopusAronFile subapertures_aron = {0};

  aao_OctopusAronBlock wfs5 = {0};
  aao_OctopusAronBlock wfs6 = {0};
  aao_OctopusAronBlock wfs8 = {0};
  aao_OctopusAronBlock dm0 = {0};
  aao_OctopusAronBlock dm2 = {0};

  actuators_aron =
      aao_readOctopusAronFile("../tests/input", "actuators_aron.txt");
  subapertures_aron =
      aao_readOctopusAronFile("../tests/input", "subapertures_aron.txt");
  REQUIRE(actuators_aron.read_status == 0);
  REQUIRE(subapertures_aron.read_status == 0);

  SECTION("read WFS 5 block") {
    wfs5 = aao_deriveOctopusAronBlock(&subapertures_aron, 5, 42.);
    CHECK(wfs5.n_act == 85);
    CHECK(aao_numActSubap(wfs5.i_sub, wfs5.n_act - 1) == 5040);
  }

  SECTION("read WFS 6 block") {
    wfs6 = aao_deriveOctopusAronBlock(&subapertures_aron, 6, 42.);
    CHECK(wfs6.n_act == 3);
    CHECK(aao_numActSubap(wfs6.i_sub, wfs6.n_act - 1) == 4);
  }

  SECTION("read WFS 8 block") {
    wfs8 = aao_deriveOctopusAronBlock(&subapertures_aron, 8, 42.);
    CHECK(wfs8.n_act == 2);
    CHECK(aao_numActSubap(wfs8.i_sub, wfs8.n_act - 1) == 1);
  }

  SECTION("read DM 0 block") {
    dm0 = aao_deriveOctopusAronBlock(&actuators_aron, 0, 42.);
    CHECK(dm0.n_act == 85);
    CHECK(aao_numActSubap(dm0.i_act, dm0.n_act) == 5402);
  }

  SECTION("read DM 2 block") {
    dm2 = aao_deriveOctopusAronBlock(&actuators_aron, 2, 42.);
    CHECK(dm2.n_act == 53);
    CHECK(aao_numActSubap(dm2.i_act, dm2.n_act) == 2225);
  }

  // free aron files
  aao_freeOctopusAronFile(&actuators_aron);
  aao_freeOctopusAronFile(&subapertures_aron);
  // free aron blocks
  aao_freeOctopusAronBlock(&wfs5);
  aao_freeOctopusAronBlock(&wfs6);
  aao_freeOctopusAronBlock(&wfs8);
  aao_freeOctopusAronBlock(&dm0);
  aao_freeOctopusAronBlock(&dm2);
}

TEST_CASE("test projection") {
  // local var
  int i, j;
  int ret;
  int n_lay, n_phi; // dimensions
  int n_lay2, n_phi2;
  float d_lay, d_phi;
  float h, H;
  double angle_x, angle_y;

  // Allocated P and its transposed
  Matrix_T P;
  Matrix_T Ptr;
  Matrix_T Ptr_test;

  SECTION("projection functions") {
    n_lay = 128;
    n_phi = 85;
    d_lay = 0.5;
    d_phi = 0.5;
    h = 4000;
    H = 90000; // NGS

    angle_x = 1.; // 1 arcmin
    angle_y = 0.; // 0 arcmin

    aao_Interp2 I;
    aao_initInterp2(&I, angle_x, angle_y, h, H, n_phi, d_phi, n_lay, d_lay,
                    -n_lay * d_lay / 2.);
    aao_freeInterp2(&I);
  }

  SECTION("transposed comparison") {
    n_lay = 4;
    n_phi = 2;
    n_lay2 = n_lay * n_lay;
    n_phi2 = n_phi * n_phi;
    d_lay = 1.;
    d_phi = 1.;
    h = 400;
    H = 0; // NGS

    angle_x = 0.; // 0 arcmin
    angle_y = 0.; // 0 arcmin

    aao_Interp2 I;
    aao_initInterp2(&I, angle_x, angle_y, h, H, n_phi, d_phi, n_lay, d_lay,
                    -n_lay * d_lay / 2.);

    {
      // Generate a simple Projection matrix

      P = Matrix_T(n_phi2, n_lay2);
      Ptr = Matrix_T(n_lay2, n_phi2);

      float *phi_ptr = NULL; // lay pointer

      phi_ptr = new float[n_phi2];

      for (j = 0; j < n_lay2; j++) {
        // set phi_ptr to 0
        for (i = 0; i < n_phi2; i++)
          phi_ptr[i] = 0;

        // phi = e_j
        vector<float> lay(n_lay2, 0.);
        lay[j] = 1.0;

        // Perform P-computation phi = P(lay)
        aao_interp2_ac(phi_ptr, I, lay.data(), n_phi, n_lay);

        // copy column of P
        for (i = 0; i < n_phi2; i++)
          P(i, j) = phi_ptr[i];
      }

      delete[] phi_ptr;
      phi_ptr = NULL;
    }

    {
      // Compare P and its transposed
      Ptr_test = P.transposed();

      float *lay_ptr = NULL;

      lay_ptr = new float[n_lay2];

      for (j = 0; j < n_phi2; j++) {
        // s = e_j
        vector<float> phi(n_phi2, 0.);
        phi[j] = 1.0;

        // Perform P^T-computation lay = P^T(phi)
        aao_interp2_tr(lay_ptr, I, phi.data(), n_phi, n_lay);

        // copy column of Ptr
        for (i = 0; i < n_lay2; i++)
          Ptr(i, j) = lay_ptr[i];
      }

      // free local variables
      delete[] lay_ptr;
      lay_ptr = NULL;
    }

    Ptr_test -= Ptr;
    float tmp = Ptr_test.frobenius_norm();
    CHECK(Ptr_test.frobenius_norm() == 0);
    aao_freeInterp2(&I);
  }
}

TEST_CASE("SH") {
  int i, j;
  Matrix_T SH;                      // Shack-Hartmann matrix
  Matrix_T SHtr;                    // Shack-Hartmann transposed
  int n_sub, n_phi, n_sub2, n_phi2; // dimensions

  // Dimensions
  n_sub = 2;
  n_phi = n_sub + 1;
  n_sub2 = n_sub * n_sub;
  n_phi2 = n_phi * n_phi;

  // Allocated SH and its transposed, s and tmp memory
  SH = Matrix_T(2 * n_sub2, n_phi2);
  SHtr = Matrix_T(n_phi2, 2 * n_sub2);

  // setup SH matrix
  float *s_ptr = NULL;   // SH-measurements
  float *tmp_ptr = NULL; // temporary memory
  s_ptr = new float[2 * n_sub2];
  tmp_ptr = new float[n_sub * n_phi];

  for (j = 0; j < n_phi2; j++) {
    // phi = e_j
    vector<float> e(n_phi2, 0.);
    e[j] = 1.0;

    // Perform SH-computation s = Gamma(e)
    aao_sh(s_ptr, e.data(), n_sub, tmp_ptr);

    // copy column of SH
    for (i = 0; i < 2 * n_sub2; i++)
      SH(i, j) = s_ptr[i];
  }

  // free local variables
  delete[] s_ptr;
  delete[] tmp_ptr;
  s_ptr = NULL;
  tmp_ptr = NULL;

  SECTION("min/max/nnz") {
    float min_el = SH.min_element();
    float max_el = SH.max_element();
    unsigned int nnz = SH.nnz();

    CHECK(min_el == -0.5);
    CHECK(max_el == 0.5);
    CHECK(nnz == (unsigned int)(4 * 2 * n_sub2));
  }

  SECTION("frob. norm of SH") {
    float tmp;
    float tmp1 = SH.frobenius_norm();

    // for(i=0; i<2*n_sub2; i++)
    //	tmp += 4 * 0.5*0.5; // 4 (1/2) entries per row
    tmp = 2 * n_sub2 * 4 * 0.5 * 0.5; // 4 (1/2) entries per row
    tmp = sqrtf(tmp);

    CHECK(tmp1 == tmp);
  }

  SECTION("SH transposed") {
    Matrix_T SHtr_test = SH.transposed();

    float *phi_ptr = NULL; // phi

    phi_ptr = new float[n_phi2];

    for (j = 0; j < 2 * n_sub2; j++) {
      // s = e_j
      vector<float> e(2 * n_sub2, 0.);
      e[j] = 1.0;

      // Perform SH-computation phi = Gamma^T(e)
      aao_sh_tr(phi_ptr, e.data(), n_sub);

      // copy column of SH
      for (i = 0; i < n_phi2; i++)
        SHtr(i, j) = phi_ptr[i];
    }
    // free local variables
    delete[] phi_ptr;
    phi_ptr = NULL;

    SHtr_test -= SHtr;
    CHECK(SHtr_test.frobenius_norm() == 0);
  }
}

// TODO just copied from test_SH_tt.cpp no CHECK's or other Catch features
// included
/*TEST_CASE("SH TT") {
  int i;
  int n_sub2 = N_SUB * N_SUB;
  int n_sub_tt2 = N_SUB_TT * N_SUB_TT;
  int n_phi2 = N_PHI * N_PHI;
  float val = 1;
  float phi[N_PHI * N_PHI];
  float s[2 * N_SUB * N_SUB];
  float s_tt[2 * N_SUB_TT * N_SUB_TT];
  unsigned char i_sub[N_SUB * N_SUB];
  float tmp[N_PHI * N_SUB];

  for (i = 0; i < n_phi2; i++) {
    phi[i] = ((i % 8) - 4) * 0.5; //(200*i)/(i+1)*val;
    val = val * (-1);
  }

  printf("phi:\n");
  printSH(phi, N_PHI);
  for (i = 0; i < n_sub2; i++)
    i_sub[i] = 1;

  aao_sh(s, phi, N_SUB, tmp);
  printf("SH x:\n");
  printSH(s, N_SUB);
  printf("SH y:\n");
  printSH(s + n_sub2, N_SUB);
  aao_sh_tt(s_tt, phi, N_SUB_TT, N_SUB, i_sub, tmp);
  printf("SH_tt x:\n");
  printSH(s_tt, N_SUB_TT);
  printf("SH_tt y:\n");
  printSH(s_tt + n_sub_tt2, N_SUB_TT);

  printf("******** transposed ********\n");
  aao_sh_tr(phi, s, N_SUB);
  printf("phi:\n");
  printSH(phi, N_PHI);
  aao_sh_tt_tr(phi, s_tt, N_SUB_TT, N_SUB, i_sub);
  printf("phi_tt:\n");
  printSH(phi, N_PHI);
}

void printSH(float *s, int n) {
  int i, j;
  for (i = 0; i < n; i++) {
    for (j = 0; j < n; j++)
      printf("  %10.3f  ", s[i * n + j]);
    printf("\n");
  }
}*/
/*
 * debug_functions.c
 *
 *  Created on: Jul 31, 2014
 *      Author: misha
 */

#include <stdlib.h>
#include <stdio.h>

void print_vec_to_file(char const * filename, float const * x, int n)
{
	// local var
	FILE * file = NULL;
	int i;

	// code
	file = fopen(filename, "w");
	if(file == NULL)
	{
		printf("File %s could not be opened", filename);
		return;
	}

	for(i=0; i<n-1; i++)
		fprintf(file, "%.7e\n", x[i]);
	fprintf(file, "%.7e", x[n-1]);

	fclose(file);
}
void print_vec_double_to_file(char const * filename, double const * x, int n)
{
	// local var
	FILE * file = NULL;
	int i;

	// code
	file = fopen(filename, "w");
	if(file == NULL)
	{
		printf("File %s could not be opened", filename);
		return;
	}

	for(i=0; i<n-1; i++)
		fprintf(file, "%.7e\n", x[i]);
	fprintf(file, "%.7e", x[n-1]);

	fclose(file);
}



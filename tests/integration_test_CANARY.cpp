/*
 * integration_tests_aao_control.c
 *
 *  Created on: Jan 31, 2019
 *      Author: bernadett
 */
#include "catch.hpp"

extern "C" {
#include "aao_atmosphere.h"
#include "aao_dm.h"
#include "aao_star.h"
#include "aao_system.h"
#include "aao_wfs.h"
#include "aao_FEWHA.h"
#include "aao_FEWHA_parallel.h"
#include "aao_FEWHA_params.h"
#include "aao_print.h"
};

#define NLGS 4
#define NNGS 3 // ignoring the on-axis truth sensor.
#define NWFS 7
#define NSUBX 7
#define NACTX 8
#define TELDIAM 4.2
#define NLAY 3

TEST_CASE("Test CANARY") {
    // region Variable declaration
    aao_System LTAO_on_CANARY;
    aao_DM dm;
    aao_Star star[NWFS];
    aao_WFS wfs[NWFS];
    aao_Atmosphere atm;
    aao_Star target;
    aao_FEWHA fewha = {0};
    aao_FEWHA_Params fewha_param = {0};
    aao_FEWHA_Parallel fewha_parall = {0};
    float *slopes = NULL, *dmshape = NULL;
    int i;
    int status[NWFS];
    float actpos_x[NACTX];
    unsigned char i_act[NACTX * NACTX] = {
            0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1,
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
            1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0};
    unsigned char i_sub[NSUBX * NSUBX] = {
            0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0};
    float cn2[NLAY];
    float height_layer[NLAY];
    float d_lay[NLAY] = {0.6, 0.6, 0.6};
    int J_lay[NLAY] = {5, 5, 5}; // number of wavelet scales on each layer
    // corresponds to 2^5x2^5 = 32x32 point grids
    float alpha = 1; // regularization tuning parameter
    float alpha_eta = 0.4; // spot elongation tuning parameter (=0 NGS model, =1 full LGS model)
    float gain = 0.4;   // loop gain
    float leaky_int = 0.99; // leaky integrator value
    int max_iter = 5;       // number of CG iterations
    int useGLMS = 1;    // GLMS method, turned off
    int usePrecond = 0; // preconditioner, turned off
    char *createPrecondFromFile = NULL; // use precond from a txt file, if not null (heavy computation)
    float alpha_J = 0;  // preconditioner parameter, turned off for now
    int J_glcs = 4; // GLMS (ground layer multi-scale) parameter, turned off for now
    int global_par = 0;      // global parallelization on
    int max_thr_system = 12; // number of threads
    //endregion


    // region Init FEWHA
    // Set acutator position and layer heights
    for (i = 0; i < NACTX; i++)
        actpos_x[i] = i * (TELDIAM / NSUBX) - TELDIAM / 2;
    cn2[0] = 0.7;
    cn2[1] = 0.2;
    cn2[2] = 0.1;
    height_layer[0] = 0;
    height_layer[1] = 4000;
    height_layer[2] = 12700;

    // Set DM
    REQUIRE(aao_setSquareDM(&dm, NACTX, 0, i_act, actpos_x, actpos_x) == 0);

    // Set guide stars
    REQUIRE(aao_setLGS(&star[0], -0.23566666667, 0.23566666667, 10000, 0.9, 0., 0.) == 0);
    REQUIRE(aao_setLGS(&star[1], -0.23566666667, -0.23566666667, 10000, 0.9, 0., 0.) == 0);
    REQUIRE(aao_setLGS(&star[2], 0.23566666667, 0.23566666667, 10000, 0.9, 0., 0.) == 0);
    REQUIRE(aao_setLGS(&star[3], 0.23566666667, -0.23566666667, 10000, 0.9, 0., 0.) == 0);
    REQUIRE(aao_setNGS(&star[4], 0., -1., 10000, 0.9) == 0);
    REQUIRE(aao_setNGS(&star[5], 0.86600000000, 0.50000000000, 10000, 0.9) == 0);
    REQUIRE(aao_setNGS(&star[6], -0.86600000000, 0.50000000000, 10000, 0.9) == 0);

    // Set WFS
    for (i = 0; i < NLGS; i++)
        REQUIRE(aao_setWFS(&wfs[i], ShackHartmann, NSUBX, i_sub, 0.6, 532., &star[i]) == 0);
    for (i = NLGS; i < NWFS; i++)
        REQUIRE(aao_setWFS(&wfs[i], ShackHartmann, NSUBX, i_sub, 0.6, 800, &star[i]) == 0);

    // Set Atmosphere
    float r0 = 0.12;
    float l0 = 30.;
    float lgsheight = 22000.;
    float lgsfwhm = 1000.;
    REQUIRE(aao_setAtmosphere(&atm, 3, cn2, height_layer, r0, l0, lgsheight, lgsfwhm) == 0);

    // Set Probestar
    REQUIRE(aao_setTarget(&target, 0., 0.) == 0);

    // Init system
    REQUIRE(aao_initSystem(&LTAO_on_CANARY, LTAO, TELDIAM, ClosedLoop,
                           1 / 150., NWFS, wfs, 1, &dm, &atm, 1, &target) == 0);

    // Init FEWHA params
    REQUIRE(aao_setFEWHAparams(&fewha_param, NLAY, d_lay, J_lay, J_glcs,
                               alpha, alpha_eta, alpha_J, gain, leaky_int,
                               max_iter, useGLMS, usePrecond, createPrecondFromFile) == 0);

    // Init FEWHA Parallelization configuration
    REQUIRE(aao_setFEWHAparall(&fewha_parall, global_par, max_thr_system) == 0);

    // Init FEWHA
    aao_initFEWHA(&fewha, &LTAO_on_CANARY, &fewha_param, &fewha_parall);

    // Init sample measurements
    slopes = (float *) calloc(fewha.n_tot_meas, sizeof(float));
    for (i = 0; i < fewha.n_tot_meas; i++)
        slopes[i] = 1.0;
    dmshape = (float *) calloc(fewha.n_tot_act, sizeof(float));
    // endregion


    // region Run FEWHA
    aao_runFEWHA(dmshape, slopes, &fewha);
    aao_print_vec_float("dmshape", dmshape, fewha.n_tot_act);
    CHECK(dmshape[0] == Approx(-4.37124443054199218750).epsilon(0.001));
    CHECK(dmshape[1] == Approx(-3.97123789787292480469).epsilon(0.001));
    CHECK(dmshape[10] == Approx(-3.17118144035339355469).epsilon(0.001));
    CHECK(dmshape[fewha.n_tot_act - 1] == Approx(1.26420080661773681641).epsilon(0.001));

    // Open loop
    aao_resetFEWHAloop(dmshape, &fewha);

    // Inverse FEWHA (DM shapes to slopes)
    aao_invFEWHA(slopes, dmshape, &fewha);
    {
        int i1, i2;
        for (i1 = 0; i1 < NACTX; i1++)
            for (i2 = 0; i2 < NACTX; i2++)
                dmshape[NACTX * i1 + i2] = actpos_x[i1];
        aao_invFEWHA(slopes, dmshape, &fewha);
        //aao_print_vec_float("dmshape", dmshape, fewha.n_tot_act);
        aao_print_vec_float("slopes", slopes, fewha.n_tot_meas);
        CHECK(slopes[0] == Approx(0.59999990463256835938).epsilon(0.001));
        CHECK(slopes[10] == Approx(0.60000002384185791016).epsilon(0.001));
    }
    // endregion

    // region Destroy FEWHA
    aao_freeFEWHAparams(&fewha_param);
    aao_freeFEWHA(&fewha);

    // Destroy system
    aao_freeSystem(&LTAO_on_CANARY);

    // free local variables
    free(slopes);
    slopes = NULL;
    free(dmshape);
    dmshape = NULL;

    aao_freeDM(&dm);
    for (i = 0; i < NWFS; i++)
        aao_freeWFS(&wfs[i]);
    aao_freeAtmosphere(&atm);
    // endregion
}
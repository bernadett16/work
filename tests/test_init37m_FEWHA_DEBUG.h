/*
 * test_init37m_FEWHA_DEBUG.h
 *
 *  Created on: Jun 9, 2015
 *      Author: sraffetseder
 */

#ifndef TEST_INIT37M_FEWHA_DEBUG_H_
#define TEST_INIT37M_FEWHA_DEBUG_H_

#include <stdlib.h>
#include <stdio.h>


#include "aao_atmosphere.h"
#include "aao_dm.h"
#include "aao_star.h"
#include "aao_wfs.h"
#include "aao_system.h"

#include "aao_FEWHA.h"
#include "aao_FEWHA_params.h"
#include "aao_FEWHA_parallel.h"

#include "aao_print.h"

#define USE_LGS 0
#define USE_NGS 1
#define USE_TTS 0

#define NLGS 0
#define NNGS 1 //ignoring the on-axis truth sensor.
#define NTTS 0
//#define NWFS USE_LGS*NLGS+USE_NGS*NNGS+USE_TTS*NTTS
#define NWFS NLGS+NNGS+NTTS
#define NSUBX 74
#define NSUBX_TT_1 1
#define NSUBX_TT_2 2
#define NACTX 75
#define NACTX_TT_1 2
#define NACTX_TT_2 3
#define TELDIAM 37.
#define NLAY 1

int test_init37m_FEWHA_DEBUG(aao_FEWHA *fewha);


#endif /* TEST_INIT37M_FEWHA_DEBUG_H_ */

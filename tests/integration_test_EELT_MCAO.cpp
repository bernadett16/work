/*
 * integration_test_EELT_MCAO.c
 *
 *  Created on: Jan 31, 2019
 *      Author: bernadett
 */

#include "catch.hpp"
extern "C" {
#include "aao_atmosphere.h"
#include "aao_dm.h"
#include "aao_star.h"
#include "aao_system.h"
#include "aao_wfs.h"
#include "aao_print.h"
#include "aao_FEWHA.h"
#include "aao_FEWHA_parallel.h"
#include "aao_FEWHA_params.h"
#include "aao_vec_op.h"
}

#define NLGS 6
#define NNGS 3
#define NWFS 9
#define NSUBX 80
#define NACTX 81
#define TELDIAM 39.
#define NLAY 9
#define NDM 4
#define FOV 2.3

// TODO create test for MCAO case: find I_sub, I_act for MCAO; assumption square DM; nLay > nDM - Fitting
TEST_CASE("Test EELT MCAO") {
    // region Variable declaration
/*    aao_System MCAO_on_EELT;
    aao_DM dm;
    aao_Star star[NWFS];
    aao_WFS wfs[NWFS];
    aao_Atmosphere atm;
    aao_Star target;
    aao_FEWHA fewha = {0};
    aao_FEWHA_Params fewha_param = {0};
    aao_FEWHA_Parallel fewha_parall = {0};
    float *slopes = NULL, *dmshape = NULL;
    int i;
    float actpos_x[NACTX];
    unsigned char i_act[NACTX * NACTX] = {0};
    unsigned char i_sub[NSUBX * NSUBX] = {0};
    float cn2[NLAY] = {0.5224, 0.0260, 0.0444, 0.1160, 0.0989,
                       0.0295, 0.0598, 0.0430, 0.0600};
    float height_layer[NLAY] = {0, 140, 281, 562, 1125, 2250, 4500, 9000, 18000};
    float d_lay[NLAY] = {
            0.5, 0.5, 0.5, 0.5, 0.5,
            0.5, 0.5, 1, 1}; // discretization spacing on each layer, in meters
    // discretization of the *first* layer must be the same
    // as of the ground DM!!
    int J_lay[NLAY] = {7, 7, 7, 7, 7,
                       7, 7, 7, 7}; // number of wavelet scales on each layer
    // corresponds to 2^5x2^5 = 32x32 point grids
    float alpha = 1;                // regularization tuning parameter
    float alpha_eta = 0.4; // spot elongation tuning parameter (=0 NGS model, =1 full LGS model)
    float gain = 0.4;      // loop gain
    float leaky_int = 1.0; // leaky integrator value
    int max_iter = 10;     // number of CG iterations
    int useGLMS = 0;      // GLMS method, turned off
    int usePrecond = 1;   // preconditioner, turned off
    float alpha_J = 10e6; // preconditioner parameter, turned off for now
    int J_glcs = 0; // GLMS (ground layer multi-scale) parameter, turned off for now
    int global_par = 0;      // global parallelization on
    int max_thr_system = 12; // number of threads
    // endregion

    // region Init FEWHA
    read_vec_from_file_cast_uint8("../src/matlab_mex/MOAO_on_EELT_I_act.txt", i_act, NACTX * NACTX);
    read_vec_from_file_cast_uint8("../src/matlab_mex/MOAO_on_EELT_I_sub.txt", i_sub, NSUBX * NSUBX);

    for (i = 0; i < NACTX; i++)
        actpos_x[i] = i * (TELDIAM / NSUBX) - TELDIAM / 2;

    // Set DM
   REQUIRE(aao_setSquareDM(&dm, NACTX, 0, i_act, actpos_x, actpos_x) == 0);

    // Set guide stars
    float lgssep = 3.75;
    float n_photons = 100;
    float spot_fwhm = 1.1;
    REQUIRE(aao_setLGS(&star[0], lgssep, 0, n_photons, spot_fwhm, 16.26, -16.26) == 0);
    REQUIRE(aao_setLGS(&star[1], lgssep * 1 / 2, lgssep * sqrt(3) / 2, n_photons, spot_fwhm, 16.26, 16.26) == 0);
    REQUIRE(aao_setLGS(&star[2], -lgssep * 1 / 2, lgssep * sqrt(3) / 2, n_photons, spot_fwhm, -16.26, 16.26) == 0);
    REQUIRE(aao_setLGS(&star[3], -lgssep, 0, n_photons, spot_fwhm, -16.26, 16.26) == 0);
    REQUIRE(aao_setLGS(&star[4], -lgssep * 1 / 2, -lgssep * sqrt(3) / 2, n_photons, spot_fwhm, -16.26, -16.26) == 0);
    REQUIRE(aao_setLGS(&star[5], lgssep * 1 / 2, -lgssep * sqrt(3) / 2, n_photons, spot_fwhm, 16.26, -16.26) == 0);
    lgssep = 5;
    n_photons = 100;
    spot_fwhm = 1.1;
    REQUIRE(aao_setNGS(&star[6], -lgssep, 0, n_photons, spot_fwhm) == 0);
    REQUIRE(aao_setNGS(&star[7], lgssep * 1 / 2, lgssep * sqrt(3) / 2, n_photons, spot_fwhm) == 0);
    REQUIRE(aao_setNGS(&star[8], lgssep * 1 / 2, -lgssep * sqrt(3) / 2, n_photons, spot_fwhm) == 0);

    // Set WFS
    for (i = 0; i < NLGS; i++)
        REQUIRE(aao_setWFS(&wfs[i], ShackHartmann, NSUBX, i_sub, 0.5, 589, &star[i]) == 0);
    for (i = NLGS; i < NWFS; i++)
        REQUIRE(aao_setWFS(&wfs[i], ShackHartmann, NSUBX, i_sub, 0.5, 500, &star[i]) == 0);

    // Set Atmosphere
    float r0 = 0.129;
    float l0 = 25.;
    float lgsheight = 90000.;
    float lgsfwhm = 11400.;
    REQUIRE(aao_setAtmosphere(&atm, NLAY, cn2, height_layer, r0, l0, lgsheight, lgsfwhm) == 0);

    // Set Probestar
    REQUIRE(aao_setTargetMCAO(&target, FOV) == 0);

    // Init system
    REQUIRE(aao_initSystem(&MCAO_on_EELT, MCAO, TELDIAM, ClosedLoop, 0.002, NWFS, wfs, 1, &dm, &atm, 1, &target) == 0);

    // Init FEWHA params
    REQUIRE(aao_setFEWHAparams(&fewha_param, NLAY, d_lay, J_lay, J_glcs, alpha, alpha_eta, alpha_J, gain, leaky_int, max_iter, useGLMS, usePrecond) == 0);

    // Init FEWHA Parallelization configuration
    REQUIRE(aao_setFEWHAparall(&fewha_parall, global_par, max_thr_system) == 0);

    // Init FEWHA
    aao_initFEWHA(&fewha, &MCAO_on_EELT, &fewha_param, &fewha_parall);

    // Init sample measurements
    slopes = (float *) calloc(fewha.n_tot_meas, sizeof(float));
    for (i = 0; i < fewha.n_tot_meas; i++)
        slopes[i] = 1.0;
    dmshape = (float *) calloc(fewha.n_tot_act, sizeof(float));
    //endregion

    // region Run FEWHA
    aao_runFEWHA(dmshape, slopes, &fewha);
    aao_print_vec_float("dmshape", dmshape, fewha.n_tot_act);
    CHECK(dmshape[0] == Approx(-4.37124443054199218750).epsilon(0.001));
    CHECK(dmshape[1] == Approx(-3.97123789787292480469).epsilon(0.001));
    CHECK(dmshape[10] == Approx(-3.17118144035339355469).epsilon(0.001));
    CHECK(dmshape[fewha.n_tot_act - 1] == Approx(1.26420080661773681641).epsilon(0.001));

    // Inverse FEWHA (DM shapes to slopes)
    aao_invFEWHA(slopes, dmshape, &fewha);
    {
        int i1, i2;
        for (i1 = 0; i1 < NACTX; i1++)
            for (i2 = 0; i2 < NACTX; i2++)
                dmshape[NACTX * i1 + i2] = actpos_x[i1];

        aao_invFEWHA(slopes, dmshape, &fewha);
        aao_print_vec_float("slopes", slopes, fewha.n_tot_meas);
        CHECK(slopes[0] == Approx(1.0000).epsilon(0.001));
    }
    //endregion

    // region Destroy FEWHA
    aao_freeFEWHAparams(&fewha_param);
    aao_freeFEWHA(&fewha);

    // Destroy system
    aao_freeSystem(&MCAO_on_EELT);

    // free local variables
    free(slopes);
    slopes = NULL;
    free(dmshape);
    dmshape = NULL;

    aao_freeDM(&dm);
    for (i = 0; i < NWFS; i++)
        aao_freeWFS(&wfs[i]);
    aao_freeAtmosphere(&atm);
    // endregion
    */
}
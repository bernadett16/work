/*
 * aao_rec4octopus.c
 *
 *  Created on: Oct 13, 2014
 *      Author: misha
 */

#include "aao_rec4octopus_DEBUG.h"
#include "aao_atmosphere.h"
#include "aao_dm.h"
#include "aao_octopus_aron.h"
#include "aao_star.h"
#include "aao_system.h"
#include "aao_wfs.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef M_PI
#define M_PI 3.14159265358979323846264338327950288 /* pi */
#endif

void aao_freeRecParam(struct_aao_rec_param *aao_rec_param) {
  if (aao_rec_param->rec_layer_height != NULL) {
    free(aao_rec_param->rec_layer_height);
    aao_rec_param->rec_layer_height = NULL;
  }
  if (aao_rec_param->rec_layer_cn2 != NULL) {
    free(aao_rec_param->rec_layer_cn2);
    aao_rec_param->rec_layer_cn2 = NULL;
  }
  if (aao_rec_param->d_lay != NULL) {
    free(aao_rec_param->d_lay);
    aao_rec_param->d_lay = NULL;
  }
  if (aao_rec_param->J_lay != NULL) {
    free(aao_rec_param->J_lay);
    aao_rec_param->J_lay = NULL;
  }
}

/*void aao_initFEWHA4octopus(
                aao_FEWHA4octopus * fewha4oct,
                float time_unit,
                struct_telescope const * telescope,
                int n_wfs,
                struct_sh_phys_param const * sh_phys_param, // size = n_wfs
                struct_sh_simul_param const * sh_simul_param, // size = n_wfs
                struct_guidestar const * guidestar, // size = n_wfs
                int n_psf,
                struct_guidestar const * probestar,
                int n_layers,
                struct_turbulentlayer const * turbulentlayer,
                int n_dms,
                struct_deformable_mirror const * deformable_mirror,
                char const * communication_dir,
                struct_aao_rec_param const * params
        )
{
        // local var
        aao_System AOsys = {0};
        aao_DM * dm = NULL;
        aao_Star * star = NULL;
        aao_WFS * wfs = NULL;
        aao_Atmosphere atm = {0};
        aao_Star * target;
        aao_FEWHA_Params   fewha_params={0};
        aao_FEWHA_Parallel fewha_parall={0};
        int iDM, iWFS, iStar, iLay, iTarget;
        int nDM, nWFS, nStar, nLay, nTarget;
        int i;
        float teldiam;

        // 1 arcsec -> rad
        // 1sec = 1/60 minute, 1min = 1/60 deg
        // 1 deg = pi/180 rad
        // double const ARCMIN_TO_RAD = M_PI / 180. / 60.;
        double const RAD_TO_ARCMIN = 60*180/M_PI;

        // octopus aron file import variables
        char subapertures_filename[128] = "subapertures_aron.txt";
        char actuators_filename[128]    = "actuators_aron.txt";
        aao_OctopusAronFile subapertures_aron = {0};	// contents of a
subapertures aron file
        aao_OctopusAronFile actuators_aron = {0};		// contents of
an actuators aron file aao_OctopusAronBlock subapertures_block = {0};	// one
block re-used for all WFS aao_OctopusAronBlock actuators_block = {0};
// one block re-used for all DMs

        // read aron files
        subapertures_aron =
aao_readOctopusAronFile(communication_dir,subapertures_filename); actuators_aron
= aao_readOctopusAronFile(communication_dir,actuators_filename);

        if (subapertures_aron.read_status != 0)
        {
                printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
                           "!!! FEWHA initialization ERROR !!!\n"
                           "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
                           "File %s could not be found in directory %s\n"
                           "Aborting
initialization\n",subapertures_filename,communication_dir);
                aao_freeOctopusAronFile(&subapertures_aron);
                aao_freeOctopusAronFile(&actuators_aron);
                return;
        }
        if (actuators_aron.read_status != 0)
        {
                printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
                           "!!! FEWHA initialization ERROR !!!\n"
                           "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
                           "File %s could not be found in directory %s\n"
                           "Aborting
initialization\n",actuators_filename,communication_dir);
                aao_freeOctopusAronFile(&subapertures_aron);
                aao_freeOctopusAronFile(&actuators_aron);
                return;
        }

        // local var init
        nDM 	= n_dms;
        nWFS 	= n_wfs;
        nStar 	= n_wfs;
        nTarget = n_psf;

        dm 		= (aao_DM*)  calloc(nDM,sizeof(aao_DM));
        star 	= (aao_Star*)calloc(nWFS,sizeof(aao_Star));
        wfs 	= (aao_WFS*) calloc(nWFS,sizeof(aao_WFS));
        target 	= (aao_Star*)calloc(nTarget,sizeof(aao_Star));

        // telescope diameter
        teldiam = telescope->Diam;

        // code
        // === Set DMs ===
        for(iDM=0; iDM<nDM; iDM++)
        {
                int n_act;
                float dm_height;
                unsigned char const * i_act;
                float const * actpos_x;
                float const * actpos_y;

                // read aron file
                actuators_block = aao_deriveOctopusAronBlock(&actuators_aron,
iDM, teldiam);

                n_act = actuators_block.n_act;
        //	n_act = 8;
                dm_height = deformable_mirror[iDM].dm_height;
//		unsigned char i_act[8*8]={0,0,1,1,1,1,0,0,
//							      0,1,1,1,1,1,1,0,
//							      1,1,1,1,1,1,1,1,
//							      1,1,1,1,1,1,1,1,
//							      1,1,1,1,1,1,1,1,
//							      1,1,1,1,1,1,1,1,
//							      0,1,1,1,1,1,1,0,
//							      0,0,1,1,1,1,0,0};
//		unsigned char i_act[8*8]={0,1,1,1,1,1,1,0,
// 1,1,1,1,1,1,1,1,
// 1,1,1,1,1,1,1,1,
// 1,1,1,1,1,1,1,1,
// 1,1,1,1,1,1,1,1,
// 1,1,1,1,1,1,1,1,
// 1,1,1,1,1,1,1,1,
// 0,1,1,1,1,1,1,0}; i_act = actuators_block.i_act;

                actpos_x = actuators_block.actpos_x;
                actpos_y = actuators_block.actpos_y;
        //	float actpos_x[8] = {-2.1000, -1.5000, -0.9000, -0.3000, 0.3000,
0.9000, 1.5000, 2.1000};
        //	float actpos_y[8] = {-2.1000, -1.5000, -0.9000, -0.3000, 0.3000,
0.9000, 1.5000, 2.1000};

                // !!! INFO: at the moment only square DMs are possible!
                aao_setSquareDM(&dm[iDM], n_act, dm_height, i_act, actpos_x,
actpos_y);

                aao_freeOctopusAronBlock(&actuators_block);
        }

        // === Set guide stars ===
        for(iStar=0; iStar<nStar; iStar++)
        {
                int n_subap=sh_phys_param[iStar].n_subap;
                if(guidestar[iStar].height==0 && n_subap >2) // NGS
                {
                        float pos_x = guidestar[iStar].position_x*RAD_TO_ARCMIN;
                        float pos_y = guidestar[iStar].position_y*RAD_TO_ARCMIN;
                        int n_photons =
40000;//sh_phys_param[iStar].n_photons_persubap_integ;
                        // TODO MY 13.10.2014 is this the right parameter?
spotsizefudge? float spot_fwhm = sh_simul_param[iStar].spotsizefudge;

                        aao_setNGS(&star[iStar], pos_x, pos_y, n_photons,
spot_fwhm);
                }
                else if(guidestar[iStar].height==0 && n_subap <=2) // TTS
                {
                        float pos_x = guidestar[iStar].position_x*RAD_TO_ARCMIN;
                        float pos_y = guidestar[iStar].position_y*RAD_TO_ARCMIN;
//			int n_photons =
sh_phys_param[iStar].n_photons_persubap_integ;
                        // TODO MY 13.10.2014 is this the right parameter?
spotsizefudge? float spot_fwhm = sh_simul_param[iStar].spotsizefudge;

                        aao_setTTS(&star[iStar], pos_x, pos_y, n_photons,
spot_fwhm);
                }
                else // LGS
                {
                        float pos_x = guidestar[iStar].position_x*RAD_TO_ARCMIN;
                        float pos_y = guidestar[iStar].position_y*RAD_TO_ARCMIN;
                        int n_photons =
sh_phys_param[iStar].n_photons_persubap_integ;
                        // TODO MY 13.10.2014 is this the right parameter?
spotsizefudge? float spot_fwhm = sh_simul_param[iStar].spotsizefudge; float
gs_llt_x = sh_phys_param[iStar].llt_x; float gs_llt_y =
sh_phys_param[iStar].llt_y;

                        aao_setLGS(&star[iStar], pos_x, pos_y, n_photons,
spot_fwhm, gs_llt_x, gs_llt_y);
                }
                // !!! INFO: at the moment no Tip/Tilt sensors are treated!!!
                // TODO MY 13.10.2014 TTS!!!!!!!!
        }

        // === Set WFS ===
        for(iWFS=0; iWFS<nWFS; iWFS++)
        {
                aao_WFSType wfs_type = ShackHartmann;
                int n_sub, n_sub_virtual;
                unsigned char const * i_sub;
                float subap_size;
                float wavelength;

                // read aron file
                subapertures_block =
aao_deriveOctopusAronBlock(&subapertures_aron, iWFS, teldiam);

                n_sub = sh_phys_param[iWFS].n_subap;
//		unsigned char i_sub[7*7]={0,0,1,1,1,0,0,
//							      0,1,1,1,1,1,0,
//							      1,1,1,1,1,1,1,
//							      1,1,1,0,1,1,1,
//							      1,1,1,1,1,1,1,
//							      0,1,1,1,1,1,0,
//							      0,0,1,1,1,0,0};
                i_sub = subapertures_block.i_sub;

//		subap_size = 0.6;
                subap_size = subapertures_block.spacing;

                wavelength = sh_phys_param[iWFS].wfs_wavelength; // in meters,
e.g., 0.589e-6 wavelength *= 1e9; // in nanometers, e.g., 589

/*******************************************************************************************************************/
/*        if (star[iWFS].type_==TTS)
        {
                        n_sub=sh_phys_param[0].n_subap;
                aao_freeOctopusAronBlock(&subapertures_block);
                subapertures_block =
aao_deriveOctopusAronBlock(&subapertures_aron, 0, teldiam); i_sub =
subapertures_block.i_sub;

                subap_size = subapertures_block.spacing;
                wavelength = sh_phys_param[iWFS].wfs_wavelength; // in meters,
e.g., 0.589e-6 wavelength *= 1e9; // in nanometers, e.g., 589
        }
/*******************************************************************************************************************/
/*		aao_setWFS(&wfs[iWFS], wfs_type, n_sub, i_sub, subap_size,
wavelength, &star[iWFS]);

                aao_freeOctopusAronBlock(&subapertures_block);
        }

        // === Set Atmosphere ===
        {
                float * cn2 = NULL;
                float * height_layer;

                switch (params->rec_layer_type)
                {
                        case GroundLayer:
                                nLay = 1;
                                break;
                        case SimulatedLayers:
                                nLay = n_layers;
                                break;
                        case DMsAsLayers:
                                nLay = n_dms;
                                break;
                        case CustomLayers:
                                nLay = params->n_layers;
                                break;
                }

                cn2          = (float*)calloc(nLay,sizeof(float));
                height_layer = (float*)calloc(nLay,sizeof(float));

                for(iLay=0; iLay<nLay; iLay++)
                {
                        switch (params->rec_layer_type)
                        {
                                case GroundLayer:
                                        cn2[iLay] 		   = 1;
                                        height_layer[iLay] = 0;
                                        break;
                                case SimulatedLayers:
                                        cn2[iLay]          =
turbulentlayer[iLay].weight_thislayer; height_layer[iLay] =
turbulentlayer[iLay].height_thislayer; break; case DMsAsLayers: cn2[iLay] =
params->rec_layer_cn2[iLay]; height_layer[iLay] =
deformable_mirror[iLay].dm_height; break; case CustomLayers: cn2[iLay] =
params->rec_layer_cn2[iLay]; height_layer[iLay] =
params->rec_layer_height[iLay]; break;
                        }
                }

                float r0 = turbulentlayer[0].total_r0_05um;
                float L0 = turbulentlayer[0].L0;
                float na_height = guidestar[0].height;
                float na_fwhm   = guidestar[0].Na_thickness;

                aao_setAtmosphere(&atm, nLay, cn2, height_layer, r0, L0,
na_height, na_fwhm);

                if(cn2 != NULL)
                {
                        free(cn2);
                        cn2 = NULL;
                }
                if(height_layer != NULL)
                {
                        free(height_layer);
                        height_layer= NULL;
                }
        }

        // === Set Probe Stars ===
        for(iTarget=0; iTarget<nTarget; iTarget++)
        {
                float pos_x = probestar[iTarget].position_x*RAD_TO_ARCMIN;
                float pos_y = probestar[iTarget].position_y*RAD_TO_ARCMIN;

                // TODO MY 20.11.2014 set FOV (MCAO/GLAO type) targets
                aao_setTarget(&target[iTarget], pos_x, pos_y);
        }

        // === Set AO system ===
        {
                aao_SystemType sys_type = params->sys_type;
                aao_LoopType loop_type = params->loop_type;

                aao_initSystem(&AOsys, sys_type, teldiam, loop_type, time_unit,
nWFS, wfs, nDM, dm, &atm, nTarget, target);
        }

        // GOOD! AO system is now fully copied from the parameter files!
        // NOW: initialize FEWHA struct

        // Set FEWHA parameters
        aao_setFEWHAparams(&fewha_params, nLay, params->d_lay, params->J_lay,
params->J_glcs, params->alpha, params->alpha_eta, params->alpha_J, params->gain,
params->leaky_int, params->max_iter, params->useGLMS, params->usePrecond);

        // Set FEWHA parallelization
        aao_setFEWHAparall(&fewha_parall, params->global,
params->max_thr_system);

        // Init FEWHA: this is the heavy step!
        aao_initFEWHA(&fewha4oct->fewha, &AOsys, &fewha_params, &fewha_parall);

        // [optional] print fewha struct to screen
        //aao_printFEWHAversion(NULL);
        aao_printFEWHAconfig(&fewha4oct->fewha);

        // allocate additional variables
        fewha4oct->dmshapes =
(float*)calloc(fewha4oct->fewha.n_tot_act,sizeof(float)); fewha4oct->slopes   =
(float*)calloc(fewha4oct->fewha.n_tot_meas,sizeof(float)); for(i=0;
i<fewha4oct->fewha.n_tot_act; i++) fewha4oct->dmshapes[i] = 0.; for(i=0;
i<fewha4oct->fewha.n_tot_meas; i++) fewha4oct->slopes[i] = 0.;


        // free local var
    aao_freeFEWHAparams(&fewha_params);
    aao_freeFEWHAparall(&fewha_parall);
        aao_freeOctopusAronFile(&subapertures_aron);
        aao_freeOctopusAronFile(&actuators_aron);
        if(dm != NULL)
        {
                for(iDM=0; iDM<nDM; iDM++)
                        aao_freeDM(&dm[iDM]);
                free(dm);
                dm = NULL;
        }
        if(star != NULL)
        {
                // no freeStar, b/c no dynamically allocated memory in star
                free(star);
                star = NULL;
        }
        if(wfs != NULL)
        {
                for(iWFS=0; iWFS<nWFS; iWFS++)
                        aao_freeWFS(&wfs[iWFS]);
                free(wfs);
                wfs = NULL;
        }
        aao_freeAtmosphere(&atm);
        if(target != NULL)
        {
                // no freeStar, b/c no dynamically allocated memory in star
                free(target);
                target = NULL;
        }
        aao_freeSystem(&AOsys);
}*/
void aao_runFEWHA4octopus(double *command_vector, double const *dall_slopes,
                          aao_FEWHA4octopus *fewha4oct) {
  // local var
  int iWFS, iDM, ix, iy, i, j;
  int nWFS, nDM;
  float *Sx, *Sy, *Act;
  unsigned char const *I_sub;
  unsigned char const *I_act;
  int const *os_meas;
  int const *os_act;
  int n_sub, n_sub2, n_act;

  // local var init
  nDM = fewha4oct->fewha.nDM;
  nWFS = fewha4oct->fewha.nWFS;
  os_meas = fewha4oct->fewha.os_meas_at_wfs;
  os_act = fewha4oct->fewha.os_act_at_dm;

  // code

  // use i=n_sub*ix+iy for writing, j for reading
  j = 0;
  for (iWFS = 0; iWFS < nWFS; iWFS++) {
    n_sub = fewha4oct->fewha.n_sub_at_wfs[iWFS];
    n_sub2 = n_sub * n_sub;
    I_sub = fewha4oct->fewha.AOsys.wfs_[iWFS].i_sub_;

    Sx = fewha4oct->slopes + os_meas[iWFS];
    Sy = fewha4oct->slopes + os_meas[iWFS] + n_sub2;

    // read all x slopes
    // traverse upwards (as in FEWHA & OCTOPUS)
    for (ix = 0; ix < n_sub; ix++)
      for (iy = 0; iy < n_sub; iy++) {
        i = ix * n_sub + iy;
        if (I_sub[i]) // is active subaperture
        {
          Sx[i] = (float)dall_slopes[j]; // read slope
          j++;                           // increment j
        } else                           // inactive subaperture
        {
          Sx[i] = 0.;
        }
      }

    // read all y slopes
    for (ix = 0; ix < n_sub; ix++)
      for (iy = 0; iy < n_sub; iy++) {
        i = ix * n_sub + iy;
        if (I_sub[i]) // is active subaperture
        {
          Sy[i] = (float)dall_slopes[j]; // read slope
          j++;                           // increment j
        } else                           // inactive subaperture
        {
          Sy[i] = 0.;
        }
      }
  }
  //	save_vec("dall_slopes", dall_slopes, j);

  //	aao_print_vec_float("slopes_before",fewha4oct->slopes,
  //fewha4oct->fewha.n_tot_meas);
  //	aao_print_vec_float("dmshapes_before",fewha4oct->dmshapes,
  //fewha4oct->fewha.n_tot_act);

  // perform the reconstruction
  aao_runFEWHA(fewha4oct->dmshapes, fewha4oct->slopes, &fewha4oct->fewha);

  //	aao_print_vec_float("slopes_after",fewha4oct->slopes,
  //fewha4oct->fewha.n_tot_meas);

  // use i=n_sub*ix+iy for reading, j for writing
  j = 0;
  for (iDM = 0; iDM < nDM; iDM++) {
    n_act = fewha4oct->fewha.n_act_at_dm[iDM];
    I_act = fewha4oct->fewha.AOsys.dm_[iDM].i_act_;
    Act = fewha4oct->dmshapes + os_act[iDM];

    // read DM commands
    // traverse upwards (as in FEWHA & OCTOPUS)
    for (ix = 0; ix < n_act; ix++)
      for (iy = 0; iy < n_act; iy++) {
        i = ix * n_act + iy;
        if (I_act[i]) // is active actuator
        {
          command_vector[j] =
              -(double)Act[i]; // write actuator // MINUS for the output???!
          j++;                 // increment j
        } else                 // inactive actuator
        {
          // force actuator value to zero...
          Act[i] = 0.;
        }
      }
  }
  //	aao_print_vec_float("dmshapes_after",fewha4oct->dmshapes,
  //fewha4oct->fewha.n_tot_act);
}

void aao_freeFEWHA4octopus(aao_FEWHA4octopus *fewha4oct) {
  aao_freeFEWHA(&fewha4oct->fewha);
  if (fewha4oct->dmshapes != NULL) {
    free(fewha4oct->dmshapes);
    fewha4oct->dmshapes = NULL;
  }
  if (fewha4oct->slopes != NULL) {
    free(fewha4oct->slopes);
    fewha4oct->slopes = NULL;
  }
}

void save_vec(const char *name, float *vec, int length) {
  FILE *file;
  file = fopen(name, "a");
  fwrite(&length, sizeof(int), 1, file);
  fwrite(vec, sizeof(float), length, file);
  fclose(file);
}

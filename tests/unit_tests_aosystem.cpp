/*
 * unit_tests_aosystem.cpp
 *
 *  Created on: Jan 24, 2019
 *      Author: bernadett
 */
extern "C" {
#include "aao_atmosphere.h"
#include "aao_dm.h"
#include "aao_star.h"
#include "aao_wfs.h"
}
#include "catch.hpp"

#define NLGS 4
#define NNGS 3 // ignoring the on-axis truth sensor.
#define NWFS 7
#define NSUBX 7
#define NACTX 8
#define TELDIAM 4.2
#define NLAY 3

TEST_CASE("test set square DM", "[DM]") {
  unsigned char i_act[NACTX * NACTX];
  float actpos_x[NACTX];
  aao_DM dm;

  for (int i = 0; i < NACTX * NACTX; i++)
    i_act[i] = 1;
  for (int i = 0; i < NACTX; i++)
    actpos_x[i] = (float)(i * (TELDIAM / NSUBX) - TELDIAM / 2);

  REQUIRE(aao_setSquareDM(&dm, NACTX, 0, i_act, actpos_x, actpos_x) == 0);
  CHECK(dm.isSquare == 1);
  CHECK(dm.n_act_ == 8);
  CHECK(dm.actpos_x_[1] == Approx(-1.5).epsilon(0.001));
  CHECK(dm.actpos_y_[0] == Approx(-2.1).epsilon(0.0000001));
}

TEST_CASE("test set LGS", "[LGS]") {
  aao_Star star[NWFS];

  REQUIRE(aao_setLGS(&star[0], 0.333333333333333, 0, 10000, 0.9, 0., 0.) == 0);
  REQUIRE(aao_setLGS(&star[1], 0., 0.333333333333333, 10000, 0.9, 0., 0.) == 0);
  CHECK(star[0].pos_x_ == Approx(0.33333).epsilon(0.0001));
}

TEST_CASE("test set NGS", "[NGS]") {
  aao_Star star[NWFS];

  REQUIRE(aao_setNGS(&star[0], 0.707106781186548, 0.707106781186548, 10000,
                     0.9) == 0);
  REQUIRE(aao_setNGS(&star[1], -0.707106781186548, 0.707106781186548, 10000,
                     0.9) == 0);
  CHECK(star[0].pos_x_ == Approx(0.707106781186548).epsilon(0.0001));
}

TEST_CASE("test set WFS", "[WFS]") {
  aao_WFS wfs[NWFS];
  unsigned char i_sub[NSUBX * NSUBX];
  aao_Star star[NWFS];

  for (int i = 0; i < NSUBX * NSUBX; i++)
    i_sub[i] = 1;
  aao_setNGS(&star[0], 0.707106781186548, 0.707106781186548, 10000, 0.9);

  REQUIRE(aao_setWFS(&wfs[0], ShackHartmann, NSUBX, i_sub, 0.5, 700.,
                     &star[0]) == 0);
  CHECK(wfs->gs_.pos_x_ == Approx(0.707106781186548).epsilon(0.0001));
}

TEST_CASE("test set atmosphere") {
  float r0 = 0.106;
  float l0 = 20.;
  float lgsheight = 90000.;
  float lgsfwhm = 10000.;
  aao_Atmosphere atm;
  float cn2[NLAY];
  float height_layer[NLAY];

  cn2[0] = 0.4;
  cn2[1] = 0.1;
  cn2[2] = 0.5;
  height_layer[0] = (float)0.;
  height_layer[1] = (float)1000.;
  height_layer[2] = (float)2000.;

  REQUIRE(aao_setAtmosphere(&atm, NLAY, cn2, height_layer, r0, l0, lgsheight,
                            lgsfwhm) == 0);
  CHECK(atm.height_layer_[1] == 1000);
}

TEST_CASE("test set probe star") {
  aao_Star target;

  REQUIRE(aao_setTarget(&target, 0., 0.) == 0);
}

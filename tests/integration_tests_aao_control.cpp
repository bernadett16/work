/*
 * integration_tests_aao_control.c
 *
 *  Created on: Jan 24, 2019
 *      Author: bernadett
 *
 *      full test of FEWHA for CANARY
 *      without TT integration
 *      aao_control functions are tested separately, thus, functions aao_runFEWHA(..) and aao_control(...) are not used
 */

extern "C" {
#include "aao_FEWHA.h"
#include "aao_FEWHA_parallel.h"
#include "aao_FEWHA_params.h"
#include "aao_algorithms.h"
#include "aao_operators.h"
#include "aao_system.h"
};
#include "catch.hpp"

#define NLGS 4
#define NNGS 3
#define NWFS 7
#define NSUBX 7
#define NACTX 8
#define TELDIAM 4.2
#define NLAY 3

TEST_CASE("test aao control") {
    // region variable declaration
    aao_DM dm;
    aao_Star star[NWFS];
    aao_WFS wfs[NWFS];
    float r0 = 0.129;
    float l0 = 25.;
    float lgsheight = 22000.;
    float lgsfwhm = 1200.;
    aao_Atmosphere atm;
    float cn2[NLAY];
    float height_layer[NLAY];
    aao_Star target;
    float gain = 0.4;       // loop gain
    float leaky_int = 0.99; // leaky integrator value
    int max_iter = 10;      // number of CG iterations
    int useGLMS = 1;        // GLMS method, turned off
    int usePrecond = 0;     // preconditioner, turned off
    char *createPrecondFromFile = NULL; // use precond from a txt file, if not null (heavy computation)
    float alpha_J = 0;      // preconditioner parameter, turned off for now
    int J_glcs = 4;         // GLMS (ground layer multi-scale) parameter
    float alpha = 512;      // regularization tuning parameter
    float alpha_eta = 0.;
    float d_lay[NLAY] = {0.5, 0.5, 0.5};
    int J_lay[NLAY] = {6, 5, 5};
    int global_par = 0;      // global parallelization on
    int max_thr_system = 12; // number of threads

    aao_System LTAO_on_CANARY;
    aao_FEWHA_Params fewha_param = {0};
    aao_FEWHA_Parallel fewha_parall = {0};
    aao_FEWHA fewha = {0};
    float *slopes = NULL;
    float *b_new = NULL, *b = NULL;
    float *c = NULL;
    float *r = NULL;
    float *a_fit = NULL;
    // endregion

    // region init FEWHA params
    float actpos_x[NACTX];
    unsigned char i_act[NACTX * NACTX] = {0, 0, 1, 1, 1, 1, 0, 0,
                                          0, 1, 1, 1, 1, 1, 1, 0,
                                          1, 1, 1, 1, 1, 1, 1, 1,
                                          1, 1, 1, 1, 1, 1, 1, 1,
                                          1, 1, 1, 1, 1, 1, 1, 1,
                                          1, 1, 1, 1, 1, 1, 1, 1,
                                          0, 1, 1, 1, 1, 1, 1, 0,
                                          0, 0, 1, 1, 1, 1, 0, 0};

    unsigned char i_sub[NSUBX * NSUBX] = {0, 0, 1, 1, 1, 0, 0,
                                          0, 1, 1, 1, 1, 1, 0,
                                          1, 1, 1, 1, 1, 1, 1,
                                          1, 1, 1, 0, 1, 1, 1,
                                          1, 1, 1, 1, 1, 1, 1,
                                          0, 1, 1, 1, 1, 1, 0,
                                          0, 0, 1, 1, 1, 0, 0};
    for (int i = 0; i < NACTX; i++)
        actpos_x[i] = i * (TELDIAM / NSUBX) - TELDIAM / 2;
    cn2[0] = 0.7;
    cn2[1] = 0.2;
    cn2[2] = 0.1;
    height_layer[0] = 0;
    height_layer[1] = 4000;
    height_layer[2] = 12700;

    aao_setSquareDM(&dm, NACTX, 0, i_act, actpos_x, actpos_x);

    float lgsdiam = 0.3;
    aao_setLGS(&star[0], lgsdiam, 0, 300, 1.5, 0., 0.);
    aao_setLGS(&star[1], 0, lgsdiam, 300, 1.5, 0., 0.);
    aao_setLGS(&star[2], -lgsdiam, 0, 300, 1.5, 0., 0.);
    aao_setLGS(&star[3], 0, -lgsdiam, 300, 1.5, 0., 0.);
    aao_setNGS(&star[4], 0.5, 0.1, 300, 0.7);
    aao_setNGS(&star[5], -0.3, 0.2, 300, 0.7);
    aao_setNGS(&star[6], -0.1, -0.4, 300, 0.7);

    for (int i = 0; i < NLGS; i++)
        aao_setWFS(&wfs[i], ShackHartmann, NSUBX, i_sub, 0.6, 532., &star[i]);
    for (int i = NLGS; i < NWFS; i++)
        aao_setWFS(&wfs[i], ShackHartmann, NSUBX, i_sub, 0.6, 650, &star[i]);

    aao_setAtmosphere(&atm, 3, cn2, height_layer, r0, l0, lgsheight, lgsfwhm);
    aao_setTarget(&target, 0., 0.);
    aao_initSystem(&LTAO_on_CANARY, LTAO, TELDIAM, ClosedLoop, 1 / 150., NWFS, wfs, 1, &dm, &atm, 1, &target);

    aao_setFEWHAparams(&fewha_param, NLAY, d_lay, J_lay, J_glcs, alpha, alpha_eta,
                       alpha_J, gain, leaky_int, max_iter, useGLMS, usePrecond, createPrecondFromFile);
    aao_setFEWHAparall(&fewha_parall, global_par, max_thr_system);
    aao_initFEWHA(&fewha, &LTAO_on_CANARY, &fewha_param, &fewha_parall);
    // endregion

    // region create dummy slopes and actuator commands
    slopes = (float *) calloc(fewha.n_tot_meas, sizeof(float));
    for (int i = 0; i < fewha.n_tot_meas; i++)
        slopes[i] = 1.0;
    float a_old[NACTX * NACTX];
    for (int i = 0; i < NACTX * NACTX; i++) {
        a_old[i] = i % 11 - 5;
    }
    // endregion

    // region aao control test
    // 1) pseudo-open loop measurements are generated
    aao_applyH(slopes, &(fewha.H), a_old, &fewha, &fewha_parall);
    // output: s = s + H*a_old
    CHECK(Approx(9.0).epsilon(0.1) == slopes[0]);
    CHECK(Approx(3.5).epsilon(0.1) == slopes[2]);
    CHECK(Approx(-2.0).epsilon(0.1) == slopes[4]);
    CHECK(Approx(-3.5).epsilon(0.1) == slopes[fewha.n_tot_meas-1]);


    // 2) RHS vector computed
    b_new = fewha.loop.tmp_b;
    aao_applyB(b_new, &(fewha.A), slopes, &fewha, &fewha_parall);
    // output: b_new = B*s
    CHECK(Approx(0.00023).epsilon(0.01) == b_new[0]);
    CHECK(Approx(89.60309617).epsilon(0.01) == b_new[1]);
    CHECK(Approx(-50.027710).epsilon(0.01) == b_new[4]);
    CHECK(Approx(0.000).epsilon(0.01) == b_new[fewha.n_tot_lay - 1]);


    // 3) residual updated
    b = fewha.loop.b;
    r = fewha.loop.r; // r from old iteration, becomes r from this iteration
    for (int iLay = 0; iLay < fewha.nLay; iLay++) {
        aao_update_r_b(r + fewha.os_lay_at_lay[iLay], b + fewha.os_lay_at_lay[iLay], b_new + fewha.os_lay_at_lay[iLay],
                       fewha.n_lay_at_lay[iLay] * fewha.n_lay_at_lay[iLay]);
    }
    // output: r = b_new-A*c
    CHECK(Approx(0.00023).epsilon(0.01) == r[0]);
    CHECK(Approx(89.60309617).epsilon(0.01) == r[1]);
    CHECK(Approx(-50.027710).epsilon(0.01) == r[4]);
    CHECK(Approx(0.000).epsilon(0.01) == r[fewha.n_tot_lay - 1]);
    // output: b = b_new
    CHECK(Approx(0.00023).epsilon(0.01) == b[0]);
    CHECK(Approx(89.60309617).epsilon(0.01) == b[1]);
    CHECK(Approx(-50.027710).epsilon(0.01) == b[4]);
    CHECK(Approx(0.000).epsilon(0.01) == b[fewha.n_tot_lay - 1]);


    // 4) GLMS [optional]
    c = fewha.loop.c;
    aao_glms(c, r, &fewha.G, &fewha.A, &fewha, &fewha_parall);
    // output: c
    CHECK(Approx(50.7556).epsilon(0.1) == c[1]);
    CHECK(Approx(16.695040).epsilon(0.1) == c[3]);
    CHECK(Approx(2.654370).epsilon(0.1) == c[8]);
    CHECK(Approx(0.000).epsilon(0.01) == c[fewha.n_tot_lay - 1]);
    // output: r = b-Mc
    CHECK(Approx(0.000107).epsilon(0.1) == r[0]);
    CHECK(Approx(0.000229).epsilon(0.1) == r[3]);
    CHECK(Approx(-0.004227).epsilon(0.1) == r[8]);
    CHECK(Approx(0.000).epsilon(0.01) == r[fewha.n_tot_lay - 1]);


    // 5) PCG
    aao_pcg_jacobi(c, r, &fewha.pcg, &fewha.A, &fewha.iJ, max_iter, &fewha, &fewha_parall);
    // output: c = M^{-1}r
    CHECK(Approx(50.765461).epsilon(0.1) == c[1]);
    CHECK(Approx(16.691063).epsilon(0.1) == c[3]);
    CHECK(Approx(2.487512).epsilon(0.1) == c[8]);
    CHECK(Approx(0.000).epsilon(0.01) == c[fewha.n_tot_lay - 1]);


    // 6) Fitting
    // b_new serves as the tmp_lay variable for applyF
    a_fit = fewha.loop.tmp_a;
    aao_applyF(a_fit, &fewha.F, c, b_new, &fewha, &fewha_parall);
    // output: a_fit = F*W^{-1}*c
    CHECK(Approx(-27.409630).epsilon(0.01) == a_fit[0]);
    CHECK(Approx(-26.81156).epsilon(0.01) == a_fit[1]);
    CHECK(Approx(-27.764532).epsilon(0.01) == a_fit[8]);
    CHECK(Approx(-12.665101).epsilon(0.01) == a_fit[fewha.n_tot_act-1]);


    // 7) control applied
    float *dmshape = (float *)calloc(fewha.n_tot_act, sizeof(float));
    for (int iDM = 0; iDM < fewha.nDM; iDM++) {
        // TODO MY 06.07.2014 n_act[iDM]*n_act[iDM] assumes we use a square DM!!!
        aao_integrator_POLC(dmshape + fewha.os_act_at_dm[iDM], a_old + fewha.os_act_at_dm[iDM],
                            a_fit + fewha.os_act_at_dm[iDM], gain, leaky_int,
                            fewha.n_act_at_dm[iDM] * fewha.n_act_at_dm[iDM]);
    }
    // output: dmshape = a_fit + gain*(a-a^(-1))
    CHECK(Approx(-8.96385).epsilon(0.1) == dmshape[0]);
    CHECK(Approx(-9.124623).epsilon(0.1) == dmshape[1]);
    CHECK(Approx(-12.305813).epsilon(0.1) == dmshape[8]);
    CHECK(Approx(-6.266040).epsilon(0.1) == dmshape[fewha.n_tot_act-1]);
    //endregion
}

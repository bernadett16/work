/*
 * test_Projection.cpp
 *
 *  Created on: Jun 21, 2014
 *      Author: misha
 */


#include <iostream>
#include <vector>
#include <limits>
#include "matrix_class.h"
extern "C" {
#include "aao_proj_op.h"
}

using std::cout;
using std::endl;
using std::vector;

int test_Projection()
{
	// local var
	int i,j;
	int ret;
	int n_lay, n_phi; // dimensions
	int n_lay2, n_phi2;
	float d_lay, d_phi;
	float h, H;
	double angle_x, angle_y;

	// Allocated P and its transposed
	Matrix_T P;
	Matrix_T Ptr;
	Matrix_T Ptr_test;

	// local var init
	ret = 0;

	// code
	cout << "=== Testing Projection functions ===" << endl;
	{
		n_lay = 128;
		n_phi = 85;
		d_lay = 0.5;
		d_phi = 0.5;
		h = 4000;
		H = 90000; // NGS

		angle_x = 1.; // 1 arcmin
		angle_y = 0.; // 0 arcmin


		aao_Interp2 I;
		aao_initInterp2(&I, angle_x, angle_y, h, H, n_phi, d_phi, n_lay, d_lay, -n_lay*d_lay/2.);

		aao_freeInterp2(&I);
	}

	{
		// Transposed comparison
		n_lay = 4;
		n_phi = 2;
		n_lay2 = n_lay*n_lay;
		n_phi2 = n_phi*n_phi;
		d_lay = 1.;
		d_phi = 1.;
		h = 400;
		H = 0; // NGS

		angle_x = 0.; // 0 arcmin
		angle_y = 0.; // 0 arcmin


		aao_Interp2 I;
		aao_initInterp2(&I, angle_x, angle_y, h, H, n_phi, d_phi, n_lay, d_lay, -n_lay*d_lay/2.);

		{
			// Generate a simple Projection matrix

			P      = Matrix_T(n_phi2, n_lay2);
			Ptr    = Matrix_T(n_lay2, n_phi2);

			float * phi_ptr = NULL;	// lay pointer

			phi_ptr = new float[n_phi2];

			for (j=0; j<n_lay2; j++)
			{
				// set phi_ptr to 0
				for(i=0; i<n_phi2; i++)
					phi_ptr[i] = 0;

				// phi = e_j
				vector<float> lay(n_lay2, 0.);
				lay[j] = 1.0;

				// Perform P-computation phi = P(lay)
				aao_interp2_ac(phi_ptr, I, lay.data(), n_phi, n_lay);

				// copy column of P
				for(i=0; i<n_phi2; i++)
					P(i,j) = phi_ptr[i];
			}

			P.print();
			delete[] phi_ptr;
			phi_ptr = NULL;
		}

		{
			// Compare P and its transposed
			Ptr_test = P.transposed();
			Ptr_test.print();
			cout << endl;

			float * lay_ptr = NULL;

			lay_ptr = new float[n_lay2];

			for (j=0; j<n_phi2; j++)
			{
				// s = e_j
				vector<float> phi(n_phi2, 0.);
				phi[j] = 1.0;

				// Perform P^T-computation lay = P^T(phi)
				aao_interp2_tr(lay_ptr, I, phi.data(), n_phi, n_lay);

				// copy column of Ptr
				for(i=0; i<n_lay2; i++)
					Ptr(i,j) = lay_ptr[i];
			}

			Ptr.print();
			cout << endl;

			// free local variables
			delete[] lay_ptr;
			lay_ptr = NULL;

		}

		Ptr_test -= Ptr;
		float tmp = Ptr_test.frobenius_norm();
		cout << "Frobenius norm = " << tmp << ", correct = " << 0 << endl;
		ret += (tmp != 0);

		aao_freeInterp2(&I);
	}



	cout << "Number of errors = " << ret << endl;

	cout << endl;


	return ret;
}


/*
 * test_Cholesky.c
 *
 *  Created on: Oct 10, 2014
 *      Author: misha
 */


#include <iostream>
#include <vector>
#include <cmath>
#include "matrix_class.h"
//#include "test.h"
extern "C" {
#include "aao_matr_op.h"
}

using std::cout;
using std::endl;
using std::vector;



int test_Cholesky()
{
	// local var
	int ret;

	// local var init
	ret = 0;

	// code
	cout << "=== Testing Cholesky decomposition and solver ===" << endl;

	// Test non-empty matrix
	{
		// L =
		//     1 0 0
		//    11 1 0
		//     8 1 1
		// D = diag(5 7 1)
		// LDL' =
		//	    210    85     8
		//	     85    50     1
		//	      8     1     1

		Matrix_T A(3,3);
		A(0,0) = 210;
		A(0,1) = A(1,0) = 85;
		A(0,2) = A(2,0) = 8;
		A(1,1) = 50;
		A(1,2) = A(2,1) = 1;
		A(2,2) = 1;

		A.print();

		float mL[9];
		float mD[9];
		float x[3] = {0,0,0};
		float b[3] = {1,2,3};

		chol_decomp(mL, mD, A.data(), 3);

		chol_solve(x,mL, mD, b, 3);

		cout << "x = ";
		for(int i=0; i<3; i++)
			cout << x[i] << " ";
		cout << endl;

		vector<float> err(3);
		err[0] = x[0] + 0.857142857142858;
		err[1] = x[1] - 1.326530612244899;
		err[2] = x[2] - 8.530612244897963;

		ret += !(vector_norm(err) < 1e-6);
	}

	{
		// LDL' =
		//    0     0     0
		//    0     7     7
		//    0     7     8
		// L =
		//     1 0 0
		//    11 1 0
		//     8 1 1
		// D = diag(0 7 1)

		Matrix_T A(3,3);
		A(0,0) = 0;
		A(0,1) = A(1,0) = 0;
		A(0,2) = A(2,0) = 0;
		A(1,1) = 7;
		A(1,2) = A(2,1) = 7;
		A(2,2) = 8;

		A.print();

		float mL[9];
		float mD[9];
		float x[3] = {0,0,0};
		float b[3] = {1,2,3};

		chol_decomp(mL, mD, A.data(), 3);

		chol_solve(x,mL, mD, b, 3);

		cout << "x = ";
		for(int i=0; i<3; i++)
			cout << x[i] << " ";
		cout << endl;

		vector<float> err(3);
		err[0] = x[0];
		err[1] = x[1] + 0.714285714285714;
		err[2] = x[2] - 1.000000000000000;

		ret += !(vector_norm(err) < 1e-6);
	}

	cout << "Number of errors = " << ret << endl;

	cout << endl;

	return ret;
}

/*
 * test_octopus_aron.cpp
 *
 *  Created on: Nov 15, 2014
 *      Author: misha
 */

#include <iostream>
extern "C" {
#include "aao_octopus_aron.h"
#include "aao_sh_op.h"
}

using std::cout;
using std::endl;

int test_OctopusAronIO()
{
	// local var
	int ret;


	// local var init
	ret = 0;

	// code
	cout << "=== Testing Octopus Aron File I/O ===" << endl;

	aao_OctopusAronFile actuators_aron = {0};
	aao_OctopusAronFile subapertures_aron = {0};

	aao_OctopusAronBlock wfs5 = {0};
	aao_OctopusAronBlock wfs6 = {0};
	aao_OctopusAronBlock wfs8 = {0};
	aao_OctopusAronBlock dm0 = {0};
	aao_OctopusAronBlock dm2 = {0};

	{
		// Read file test
		cout << "reading aron files" << endl;

		actuators_aron	  = aao_readOctopusAronFile("./tests","actuators_aron.txt");
		subapertures_aron = aao_readOctopusAronFile("./tests","subapertures_aron.txt");

		cout << "read status actuators_aron.txt = " << actuators_aron.read_status << endl;
		cout << "read status subapertures_aron.txt = " << subapertures_aron.read_status << endl;

		ret += (actuators_aron.read_status!=0);
		ret += (subapertures_aron.read_status!=0);
	}

	{
		// read WFS 5 block
		wfs5 = aao_deriveOctopusAronBlock(&subapertures_aron,5,42.);

		cout << "wfs5: n_sub = " << wfs5.n_act-1 << endl;
		cout << "wfs5: number of active subaps = " << aao_numActSubap(wfs5.i_sub, wfs5.n_act-1) << endl;

		ret += (wfs5.n_act != 85);
		ret += (aao_numActSubap(wfs5.i_sub, wfs5.n_act-1) != 5040);
	}

	{
		// read WFS 6 block
		wfs6 = aao_deriveOctopusAronBlock(&subapertures_aron,6,42.);

		cout << "wfs6: n_sub = " << wfs6.n_act-1 << endl;
		cout << "wfs6: number of active subaps = " << aao_numActSubap(wfs6.i_sub, wfs6.n_act-1) << endl;

		ret += (wfs6.n_act != 3);
		ret += (aao_numActSubap(wfs6.i_sub, wfs6.n_act-1) != 4);
	}

	{
		// read WFS 8 block
		wfs8 = aao_deriveOctopusAronBlock(&subapertures_aron,8,42.);

		cout << "wfs8: n_sub = " << wfs8.n_act-1 << endl;
		cout << "wfs8: number of active subaps = " << aao_numActSubap(wfs8.i_sub, wfs8.n_act-1) << endl;

		ret += (wfs8.n_act != 2);
		ret += (aao_numActSubap(wfs8.i_sub, wfs8.n_act-1) != 1);
	}

	{
		// read DM 0 block
		dm0 = aao_deriveOctopusAronBlock(&actuators_aron,0,42.);

		cout << "dm0: n_act = " << dm0.n_act << endl;
		cout << "dm0: number of active actuators = " << aao_numActSubap(dm0.i_act, dm0.n_act) << endl;

		ret += (dm0.n_act != 85);
		ret += (aao_numActSubap(dm0.i_act, dm0.n_act) != 5402);
	}

	{
		// read DM 2 block
		dm2 = aao_deriveOctopusAronBlock(&actuators_aron,2,42.);

		cout << "dm2: n_act = " << dm2.n_act << endl;
		cout << "dm2: number of active actuators = " << aao_numActSubap(dm2.i_act, dm2.n_act) << endl;

		ret += (dm2.n_act != 53);
		ret += (aao_numActSubap(dm2.i_act, dm2.n_act) != 2225);
	}



	// free aron files
	aao_freeOctopusAronFile(&actuators_aron);
	aao_freeOctopusAronFile(&subapertures_aron);

	// free aron blocks
	aao_freeOctopusAronBlock(&wfs5);
	aao_freeOctopusAronBlock(&wfs6);
	aao_freeOctopusAronBlock(&wfs8);
	aao_freeOctopusAronBlock(&dm0);
	aao_freeOctopusAronBlock(&dm2);


	cout << "Number of errors = " << ret << endl;

	cout << endl;


	return ret;
}




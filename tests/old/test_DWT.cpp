/*
 * test_SH.cpp
 *
 *  Created on: Jun 20, 2014
 *      Author: misha
 */


#include <iostream>
#include <vector>
#include <cmath>
#include <limits>
#include "matrix_class.h"
extern "C" {
#include "aao_wav_op.h"
}

using std::cout;
using std::endl;
using std::vector;

int test_DWT()
{
	// local var
	int ret;
	int i,j;
	int J, n_wav, n_wav2; // dimensions

	// local var init
	ret = 0;

	// code
	cout << "=== Testing DWT functions ===" << endl;

	// Dimensions
	J = 7;
	n_wav = 128;
	n_wav2 = n_wav*n_wav;

	// sin periodic function
	{
		float * C0 = new float[n_wav2];
		float * C1 = new float[n_wav2];
		float * T  = new float[n_wav2];
		for(i=0; i<n_wav; i++)
		{
			for(j=0; j<n_wav; j++)
				C0[i*n_wav+j] = 0;//sinf(2*M_PI*(float)j/(n_wav-1.))*sinf(10*2*M_PI*(float)i/(n_wav-1.));
		}
		for(i=0; i<n_wav2; i++)
			C1[i] = C0[i];

		aao_wavedec2_db3(C0, n_wav, T);
		aao_waverec2_db3(C0, n_wav, T);

		float tmp = 0.;
		for(i=0; i<n_wav2; i++)
			tmp += (C1[i] - C0[i])*(C1[i] - C0[i]);
		tmp = sqrtf(tmp);

		cout << "||C - W*WC|| = " << tmp << endl;
		ret += (fabsf(tmp) > 1e-4);

		delete[] C0;
		delete[] C1;
		delete[] T;
		C0 = NULL;
		C1 = NULL;
		T = NULL;
	}

	// sin periodic with a decay to 0 at the boundaries
	{
		float * C0 = new float[n_wav2];
		float * C1 = new float[n_wav2];
		float * T  = new float[n_wav2];
		for(i=0; i<n_wav; i++)
		{
			for(j=0; j<n_wav; j++)
			{
				float x = (float)i/(n_wav-1.);
				float y = (float)j/(n_wav-1.);
				C0[i*n_wav+j] = 0;//sinf(2*M_PI*x)*sinf(10*2*M_PI*y);
				x = 2*x-1;
				y = 2*y-1;
				C0[i*n_wav+j] *= expf( -(x*x+y*y)/0.25 );
			}

		}
		for(i=0; i<n_wav2; i++)
			C1[i] = C0[i];

		aao_wavedec2_db3(C0, n_wav, T);
		aao_waverec2_db3(C0, n_wav, T);

		float tmp = 0.;
		for(i=0; i<n_wav2; i++)
			tmp += (C1[i] - C0[i])*(C1[i] - C0[i]);
		tmp = sqrtf(tmp);

		cout << "||C - W*WC|| = " << tmp << endl;
		ret += (fabsf(tmp) > 1e-4);

		delete[] C0;
		delete[] C1;
		delete[] T;
		C0 = NULL;
		C1 = NULL;
		T = NULL;
	}



	cout << "Number of errors = " << ret << endl;

	cout << endl;


	return ret;
}


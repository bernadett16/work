/*
 * test_MatrixCRS.cpp
 *
 *  Created on: Jul 4, 2014
 *      Author: misha
 */


#include <iostream>
#include <vector>
#include <limits>
#include "matrix_class.h"
extern "C" {
#include "aao_matr_crs_op.h"
}

using std::cout;
using std::endl;
using std::vector;

int test_MatrixCRS()
{
	// local var
	int i,j;
	int ret;


	// local var init
	ret = 0;

	// code
	cout << "=== Testing Matrix CRS functions ===" << endl;

	{ // fullmatrix test
		Matrix_T B(3,3);
		Matrix_T C(3,3);

		for(i=0; i<3; i++)
			for(j=0; j<3; j++)
				B(i,j) = i*3+j+1;

		cout << "Testing a full matrix" << endl;
		B.print();

		aao_MatrixCRS_T A;

		aao_initMatrixCRS(&A, B.data(), B.rows(), B.cols());

		// get elements of A
		float * b = new float[3];
		for(j=0; j<3; j++)
		{
			vector<float> x(3,0.);
			x[j] = 1;
			aao_matrixCRS_vec_prod(b, &A, x.data(), 3, 3);
			for(i=0; i<3; i++)
				C(i,j) = b[i];
		}

		cout << "full matrix in CRS:" << endl;
		C.print();

		C -= B;
		float tmp = C.frobenius_norm();
		cout << "Frobenius norm = " << tmp << ", correct = " << 0 << endl;
		ret += (tmp != 0);

		// get elements of A^T
		for(j=0; j<3; j++)
		{
			vector<float> x(3,0.);
			x[j] = 1;
			aao_matrixCRS_vec_prod_tr(b, &A, x.data(), 3, 3);
			for(i=0; i<3; i++)
				C(i,j) = b[i];
		}

		cout << "transposed full matrix in CRS:" << endl;
		C.print();

		Matrix_T D = B.transposed();

		C -= D;
		tmp = C.frobenius_norm();
		cout << "Frobenius norm = " << tmp << ", correct = " << 0 << endl;
		ret += (tmp != 0);


		delete [] b;
		b = NULL;
		aao_freeMatrixCRS(&A);
	}

	{ // diagonal matrix test
		Matrix_T B(3,3);
		Matrix_T C(3,3);

		for(i=0; i<3; i++)
			B(i,i) = i+1;

		cout << "diagonal matrix full" << endl;
		B.print();

		aao_MatrixCRS_T A;

		aao_initMatrixCRS(&A, B.data(), B.rows(), B.cols());

		// "get elements" of A
		float * b = new float[3];
		for(j=0; j<3; j++)
		{
			vector<float> x(3,0.);
			x[j] = 1;
			aao_matrixCRS_vec_prod(b, &A, x.data(), 3, 3);
			for(i=0; i<3; i++)
				C(i,j) = b[i];
		}

		cout << "diagonal matrix CRS" << endl;
		C.print();

		C -= B;
		float tmp = C.frobenius_norm();
		cout << "Frobenius norm = " << tmp << ", correct = " << 0 << endl;
		ret += (tmp != 0);


		// "Get element of A^T"
		for(j=0; j<3; j++)
		{
			vector<float> x(3,0.);
			x[j] = 1;
			aao_matrixCRS_vec_prod_tr(b, &A, x.data(), 3, 3);
			for(i=0; i<3; i++)
				C(i,j) = b[i];
		}

		cout << "diagonal matrix transposed CRS" << endl;
		C.print();

		C -= B;
		tmp = C.frobenius_norm();
		cout << "Frobenius norm = " << tmp << ", correct = " << 0 << endl;
		ret += (tmp != 0);


		delete [] b;
		b = NULL;
		aao_freeMatrixCRS(&A);
	}

	{ // very sparse matrix test
		Matrix_T B(3,3);
		Matrix_T C(3,3);

		B(1,1) = 1;
		B(2,0) = 2;

		cout << "very sparse matrix full" << endl;
		B.print();

		aao_MatrixCRS_T A;

		aao_initMatrixCRS(&A, B.data(), B.rows(), B.cols());

		// "get elements" of A
		float * b = new float[3];
		for(j=0; j<3; j++)
		{
			vector<float> x(3,0.);
			x[j] = 1;
			aao_matrixCRS_vec_prod(b, &A, x.data(), 3, 3);
			for(i=0; i<3; i++)
				C(i,j) = b[i];
		}

		cout << "diagonal matrix CRS" << endl;
		C.print();

		C -= B;
		float tmp = C.frobenius_norm();
		cout << "Frobenius norm = " << tmp << ", correct = " << 0 << endl;
		ret += (tmp != 0);

		delete [] b;
		b = NULL;
		aao_freeMatrixCRS(&A);
	}

	cout << "Number of errors = " << ret << endl;

	cout << endl;


	return ret;
}

/*
 * test_SH.cpp
 *
 *  Created on: Jun 20, 2014
 *      Author: misha
 */


#include <iostream>
#include <vector>
#include <cmath>
#include "matrix_class.h"
extern "C" {
#include "aao_sh_op.h"
}

using std::cout;
using std::endl;
using std::vector;

int test_ShackHartmann()
{
	// local var
	int ret;
	int i,j;
	Matrix_T SH;			// Shack-Hartmann matrix
	Matrix_T SHtr;			// Shack-Hartmann transposed
	int n_sub, n_phi, n_sub2, n_phi2; // dimensions

	// local var init
	ret = 0;

	// code
	cout << "=== Testing Shack-Hartmann operator functions ===" << endl;

	// Dimensions
	n_sub  = 2;
	n_phi  = n_sub+1;
	n_sub2 = n_sub*n_sub;
	n_phi2 = n_phi*n_phi;

	// Allocated SH and its transposed, s and tmp memory
	SH      = Matrix_T(2*n_sub2, n_phi2);
	SHtr    = Matrix_T(n_phi2, 2*n_sub2);

	// Set up a Shack-Hartmann matrix
	{
		float * s_ptr = NULL;	// SH-measurements
		float * tmp_ptr = NULL; // temporary memory

		s_ptr   = new float[2*n_sub2];
		tmp_ptr = new float[n_sub*n_phi];

		for (j=0; j<n_phi2; j++)
		{
			// phi = e_j
			vector<float> e(n_phi2, 0.);
			e[j] = 1.0;

			// Perform SH-computation s = Gamma(e)
			aao_sh(s_ptr, e.data(), n_sub, tmp_ptr);

			// copy column of SH
			for(i=0; i<2*n_sub2; i++)
				SH(i,j) = s_ptr[i];
		}

		SH.print();

		// free local variables
		delete[] s_ptr;
		delete[] tmp_ptr;
		s_ptr = NULL;
		tmp_ptr = NULL;
	}

	// Test Min/Max/nnz
	{
		float min_el = SH.min_element();
		float max_el = SH.max_element();
		unsigned int nnz = SH.nnz();

		cout << "min(SH) = " << min_el << endl;
		cout << "max(SH) = " << max_el << endl;
		cout << "nnz(SH) = " << nnz  << endl;
		ret += (min_el != -0.5);
		ret += (max_el != 0.5);
		ret += (nnz != (unsigned int)(4*2*n_sub2));
	}

	// Test Frob. norm of SH
	{
		float tmp;
		float tmp1 = SH.frobenius_norm();

		//for(i=0; i<2*n_sub2; i++)
		//	tmp += 4 * 0.5*0.5; // 4 (1/2) entries per row
		tmp = 2*n_sub2 * 4 * 0.5*0.5; // 4 (1/2) entries per row
		tmp = sqrtf(tmp);

		cout << "Frobenius norm of the " << 2*n_sub2 << "x" << n_phi2 << " matrix = "
				<< tmp1 << ", correct = " << tmp << endl;
		ret += (tmp1 != tmp);
	}

	// Test SH transposed
	{
		Matrix_T SHtr_test = SH.transposed();
		SHtr_test.print();
		cout << endl;

		float * phi_ptr = NULL;	// phi

		phi_ptr = new float[n_phi2];

		for (j=0; j<2*n_sub2; j++)
		{
			// s = e_j
			vector<float> e(2*n_sub2, 0.);
			e[j] = 1.0;

			// Perform SH-computation phi = Gamma^T(e)
			aao_sh_tr(phi_ptr, e.data(), n_sub);

			// copy column of SH
			for(i=0; i<n_phi2; i++)
				SHtr(i,j) = phi_ptr[i];
		}

		SHtr.print();
		cout << endl;

		// free local variables
		delete[] phi_ptr;
		phi_ptr = NULL;

		SHtr_test -= SHtr;
		float tmp = SHtr_test.frobenius_norm();
		cout << "Frobenius norm = " << tmp << ", correct = " << 0 << endl;
		ret += (tmp != 0);

	}



	cout << "Number of errors = " << ret << endl;

	cout << endl;


	return ret;
}


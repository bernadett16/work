/*
 * integration_tests_aao_control.c
 *
 *  Created on: Jan 31, 2019
 *      Author: bernadett
 */

#include "catch.hpp"
extern "C"{
#include "aao_atmosphere.h"
#include "aao_dm.h"
#include "aao_star.h"
#include "aao_system.h"
#include "aao_wfs.h"
#include "aao_FEWHA.h"
#include "aao_FEWHA_parallel.h"
#include "aao_FEWHA_params.h"
#include "aao_print.h"
}

#define USE_LGS 1
#define USE_NGS 1
#define USE_TTS 1
#define NLGS 4
#define NNGS 4 // ignoring the on-axis truth sensor.
#define NTTS 3 //#define NWFS USE_LGS*NLGS+USE_NGS*NNGS+USE_TTS*NTTS
#define NWFS NLGS + NNGS + NTTS
#define NSUBX 10 // 20
#define NSUBX_TT_1 1
#define NSUBX_TT_2 2
#define NACTX 11 // 21
#define NACTX_TT_1 2
#define NACTX_TT_2 3
#define TELDIAM 10.
#define NLAY 9

TEST_CASE("Test 10m"){
    // region Variable declaration
    aao_System LTAO_on_10m;
    aao_DM dm;
    aao_Star star[NWFS];
    aao_WFS wfs[NWFS];
    aao_Atmosphere atm;
    aao_Star target;
    aao_FEWHA fewha = {0};
    aao_FEWHA_Params fewha_param = {0};
    aao_FEWHA_Parallel fewha_parall = {0};
    float *slopes = NULL, *dmshape = NULL;
    int i, test;
    int status[NWFS];
    float actpos_x[NACTX];
    unsigned char i_act[NACTX * NACTX];
    unsigned char i_sub[NSUBX * NSUBX]; // subaperture mask for NGS, LGS
    unsigned char i_sub_tt_1[NSUBX_TT_1 * NSUBX_TT_1]; // subaperture mask for TTS
    unsigned char i_sub_tt_2[NSUBX_TT_2 * NSUBX_TT_2]; // subaperture mask for TTS
    float cn2[NLAY];
    float height_layer[NLAY];
    float d_lay[NLAY] = {
            0.5, 0.5, 0.5, 0.5, 0.5,
            0.5, 0.5, 0.5, 0.5}; // discretization spacing on each layer, in meters
    // discretization of the *first* layer must be the
    // same as of the ground DM!!
    int J_lay[NLAY] = {6, 5, 5, 5, 5,
                       5, 5, 5, 5}; // number of wavelet scales on each layer
    // corresponds to 2^5x2^5 = 32x32 point grids
    float alpha = 512;              // regularization tuning parameter
    float alpha_eta =
            0.; // spot elongation tuning parameter (=0 NGS model, =1 full LGS model)
    float gain = 0.4;        // loop gain
    float leaky_int = 0.99;  // leaky integrator value
    int max_iter = 10;       // number of CG iterations
    int useGLMS = 1;         // GLMS method, turned off
    int usePrecond = 0;      // preconditioner, turned off
    char *createPrecondFromFile = "jacobi.txt"; // use precond from a txt file, if not null (heavy computation)
    float alpha_J = 0;       // preconditioner parameter, turned off for now
    int J_glcs = 4;          // GLMS (ground layer multi-scale) parameter
    int global_par = 0;      // global parallelization on
    int max_thr_system = 12; // number of threads
    // endregion

    // region Init FEWHA
    for (i = 0; i < NSUBX_TT_2 * NSUBX_TT_2; i++)
        i_sub_tt_2[i] = 1;
    for (i = 0; i < NSUBX * NSUBX; i++)
        i_sub[i] = 1;
    for (i = 0; i < NACTX * NACTX; i++)
        i_act[i] = 1;
    for (i = 0; i < NSUBX_TT_1 * NSUBX_TT_1; i++)
        i_sub_tt_1[i] = 1;
    for (i = 0; i < NACTX; i++)
        actpos_x[i] = (float)(i * (TELDIAM / NSUBX) - TELDIAM / 2);

    cn2[0] = 0.4;
    cn2[1] = 0.1;
    cn2[2] = 0.05;
    cn2[3] = 0.05;
    cn2[4] = 0.2;
    cn2[5] = 0.05;
    cn2[6] = 0.05;
    cn2[7] = 0.05;
    cn2[8] = 0.05;

    height_layer[0] = (float)0.;
    height_layer[1] = (float)1000.;
    height_layer[2] = (float)2000.;
    height_layer[3] = (float)3000.;
    height_layer[4] = (float)4000.;
    height_layer[5] = (float)5000.;
    height_layer[6] = (float)6000.;
    height_layer[7] = 8000.;
    height_layer[8] = 10000.;

    // Set DM
    REQUIRE(aao_setSquareDM(&dm, NACTX, 0, i_act, actpos_x, actpos_x) == 0);

    // Set GS
    REQUIRE(aao_setLGS(&star[0], 0.333333333333333, 0, 10000, 0.9, 0., 0.) == 0);
    REQUIRE(aao_setLGS(&star[1], 0., 0.333333333333333, 10000, 0.9, 0., 0.) == 0);
    REQUIRE(aao_setLGS(&star[2], -0.333333333333333, 0., 10000, 0.9, 0., 0.) == 0);
    REQUIRE(aao_setLGS(&star[3], 0., -0.333333333333333, 10000, 0.9, 0., 0.) == 0);
    REQUIRE(aao_setNGS(&star[4], 0.707106781186548, 0.707106781186548, 10000, 0.9) == 0);
    REQUIRE(aao_setNGS(&star[5], -0.707106781186548, 0.707106781186548, 10000, 0.9) == 0);
    REQUIRE(aao_setNGS(&star[6], -0.707106781186548, -0.707106781186548, 10000, 0.9) == 0);
    REQUIRE(aao_setNGS(&star[7], 0.707106781186548, -0.707106781186548, 10000, 0.9) == 0);
    REQUIRE(aao_setNGS(&star[8], 0.8, 0, 1000, 0.9) == 0);
    REQUIRE(aao_setNGS(&star[9], -0.666666666666666, 0.8, 1000, 0.9) == 0);
    REQUIRE(aao_setNGS(&star[10], -0.666666666666667, -0.8, 1000, 0.9) == 0);

    // Set WFS
    for (i = 0; i < NLGS; i++)
        REQUIRE(aao_setWFS(&wfs[i], ShackHartmann, NSUBX, i_sub, 0.5, 700., &star[i]) == 0);
    for (i = NLGS; i < NLGS + NNGS; i++)
        REQUIRE(aao_setWFS(&wfs[i], ShackHartmann, NSUBX, i_sub, 0.5, 700, &star[i]) == 0);
    i = NLGS + NNGS;
    REQUIRE(aao_setWFS(&wfs[i], ShackHartmann, NSUBX_TT_1, i_sub_tt_1,
                           TELDIAM / NSUBX_TT_1, 700, &star[i]) == 0);
    i++;
    REQUIRE(aao_setWFS(&wfs[i], ShackHartmann, NSUBX_TT_1, i_sub_tt_1,
                           TELDIAM / NSUBX_TT_1, 700, &star[i]) == 0);
    i++;
    REQUIRE(aao_setWFS(&wfs[i], ShackHartmann, NSUBX_TT_2, i_sub_tt_2,
                           TELDIAM / NSUBX_TT_2, 700, &star[i]) == 0);

    // Set Atmosphere
    float r0 = 0.106;
    float l0 = 20.;
    float lgsheight = 90000.;
    float lgsfwhm = 10000.;
    REQUIRE(aao_setAtmosphere(&atm, NLAY, cn2, height_layer, r0, l0,
                                  lgsheight, lgsfwhm) == 0);

    // Set Probestar
    REQUIRE(aao_setTarget(&target, 0., 0.) == 0);

    // Init system
    if (USE_LGS && USE_NGS && USE_TTS) // LGS and NGS and TTS
        REQUIRE(aao_initSystem(&LTAO_on_10m, LTAO, TELDIAM, ClosedLoop,
                                   1 / 400., NWFS, wfs, 1, &dm, &atm, 1, &target) == 0);
    else if (USE_LGS && USE_NGS && !USE_TTS) // LGS and NGS
        REQUIRE(aao_initSystem(&LTAO_on_10m, LTAO, TELDIAM, ClosedLoop, 1 / 400.,
                               NWFS - NTTS, wfs, 1, &dm, &atm, 1, &target) == 0);
    else if (USE_LGS && !USE_NGS) // LGS only
        REQUIRE(aao_initSystem(&LTAO_on_10m, LTAO, TELDIAM, ClosedLoop, 1 / 400.,
                               (NWFS - NTTS) / 2, wfs, 1, &dm, &atm, 1, &target) == 0);
    else if (!USE_LGS && USE_NGS) // NGS only
        //		status[0] = aao_initSystem(&LTAO_on_10m, LTAO, TELDIAM,
        // ClosedLoop, 1/400., NNGS-NTTS, wfs+NLGS, 1, &dm, &atm, 1, &target);
        REQUIRE(aao_initSystem(&LTAO_on_10m, SCAO, TELDIAM, ClosedLoop, 1 / 400., 1,
                               wfs + NLGS, 1, &dm, &atm, 1, &target) == 0);
    else {
        printf("error: use LGS and/or NGS");
        return;
    }

    // Init FEWHA
    REQUIRE(aao_setFEWHAparams(&fewha_param, NLAY, d_lay, J_lay, J_glcs,
                                   alpha, alpha_eta, alpha_J, gain, leaky_int,
                                   max_iter, useGLMS, usePrecond, createPrecondFromFile) == 0);
    REQUIRE(aao_setFEWHAparall(&fewha_parall, global_par, max_thr_system) == 0);
    aao_initFEWHA(&fewha, &LTAO_on_10m, &fewha_param, &fewha_parall);

    // Init sample measurements
    slopes = (float *)calloc(fewha.n_tot_meas, sizeof(float));
    for (i = 0; i < fewha.n_tot_meas; i++)
        slopes[i] = 1.0;
    dmshape = (float *)calloc(fewha.n_tot_act, sizeof(float));
    // endregion

    // region Run FEWHA
    aao_runFEWHA(dmshape, slopes, &fewha);
    CHECK(dmshape[0] == Approx(-9.08006763458251953125).epsilon(0.001));
    CHECK(dmshape[1] == Approx(-8.6808395385742187500).epsilon(0.001));
    CHECK(dmshape[10] == Approx(-5.08582925796508789062).epsilon(0.001));
    CHECK(dmshape[fewha.n_tot_act - 1] == Approx( -1.09453344345092773438).epsilon(0.001));

    aao_invFEWHA(slopes, dmshape, &fewha);
    {
        int i1, i2;
        for (i1 = 0; i1 < NACTX; i1++)
            for (i2 = 0; i2 < NACTX; i2++)
                dmshape[NACTX * i1 + i2] = actpos_x[i1];

        aao_invFEWHA(slopes, dmshape, &fewha);
        CHECK(slopes[0] == Approx(1.00000000000).epsilon(0.001));
    }
    // endregion

    // region Destroy FEWHA
    aao_freeFEWHA(&fewha);
    aao_freeSystem(&LTAO_on_10m);
    free(slopes);
    slopes = NULL;
    free(dmshape);
    dmshape = NULL;
    aao_freeDM(&dm);
    for (i = 0; i < NWFS; i++)
        aao_freeWFS(&wfs[i]);
    aao_freeAtmosphere(&atm);
    // endregion
}
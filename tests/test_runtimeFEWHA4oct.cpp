/*
 * test_runtimeFEWHA4oct.cpp
 *
 *  Created on: Jun 17, 2015
 *      Author: sraffetseder
 */

#include <stdio.h>
#include <stdlib.h>

#include "aao_FEWHA.h"

typedef struct aao_FEWHA4octopus {
  float *dmshapes;   // size = fewha->n_tot_act; current dm shapes; fewha format
  float *slopes;     // size = fewha->n_tot_meas; current slopes; fewha format
  aao_FEWHA fewha;   // FEWHA struct
  int write_logfile; // true or false (0 or 1)
  int save_fewha_data;
} aao_FEWHA4octopus;

void compare(aao_FEWHA4octopus *f, aao_FEWHA4octopus *g);
float diff_norm(float *v1, float *v2, int size);

int main_test() {
  aao_FEWHA4octopus fewha4oct, fewha4oct_next;
  int time_step, time_start, time_end;
  int i_time_step;
  time_start = 0;
  time_end = time_start + 10;
  time_step = 1;

  aao_readFEWHAdata(&fewha4oct.fewha, &fewha4oct.dmshapes, &fewha4oct.slopes);
  //	aao_printFEWHAconfig(&fewha4oct.fewha);
  aao_readFEWHAdata(&fewha4oct_next.fewha, &fewha4oct_next.dmshapes,
                    &fewha4oct_next.slopes);

  for (i_time_step = time_start; i_time_step <= time_end;
       i_time_step += time_step) {
    aao_read_runtimeFEWHAdata(&fewha4oct.fewha, fewha4oct.dmshapes,
                              fewha4oct.slopes, i_time_step,
                              0); // read data before calc
    aao_read_runtimeFEWHAdata(&fewha4oct_next.fewha, fewha4oct_next.dmshapes,
                              fewha4oct_next.slopes, i_time_step,
                              1); // read data after calc

    aao_runFEWHA(fewha4oct.dmshapes, fewha4oct.slopes,
                 &fewha4oct.fewha); // do calc with "old" data ...

    compare(&fewha4oct, &fewha4oct_next); // ... and compare
  }
  printf("works fine");
  return 0;
}

void compare(aao_FEWHA4octopus *f, aao_FEWHA4octopus *g) {
  float dm, a_old, b, r, c;
  dm = diff_norm(f->dmshapes, g->dmshapes, f->fewha.n_tot_act);
  a_old =
      diff_norm(f->fewha.loop.a_old, g->fewha.loop.a_old, f->fewha.n_tot_act);
  b = diff_norm(f->fewha.loop.b, g->fewha.loop.b, f->fewha.n_tot_lay);
  r = diff_norm(f->fewha.loop.r, g->fewha.loop.r, f->fewha.n_tot_lay);
  c = diff_norm(f->fewha.loop.c, g->fewha.loop.c, f->fewha.n_tot_lay);

  printf("squared error in\n");
  printf("dm: %f, a_old: %f, b: %f, r: %f, c: %f\n", dm, a_old, b, r, c);
}

float diff_norm(float *v1, float *v2, int size) {
  int i;
  float diff = 0;
  for (i = 0; i < size; i++)
    diff += (v1[i] - v2[i]) * (v1[i] - v2[i]);
  return diff;
}

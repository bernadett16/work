/*
 * test_runFEWHA4octopus.cpp
 *
 *  Created on: Jun 9, 2015
 *      Author: sraffetseder
 */

#include<stdlib.h>
#include<stdio.h>
#include <float.h>
#include <string.h>
#include <math.h>

#include"aao_FEWHA.h"
#include"aao_octopus_aron.h"
#include"aao_rec4octopus_DEBUG.h"
#include"test_init37m_FEWHA_DEBUG.h"

//aao_FEWHA *test_init37m_FEWHA();
void read_stream(const char *name, double **vec, int *length, int block);
void save_txt_data(const char *name, double *vec, int length, int newline);

/*typedef struct aao_FEWHA4octopus
{
	float * dmshapes;	// size = fewha->n_tot_act; current dm shapes; fewha format
	float * slopes;		// size = fewha->n_tot_meas; current slopes; fewha format
	aao_FEWHA fewha;	// FEWHA struct
} aao_FEWHA4octopus;*/

int main3()
{
	double *dall_slopes=NULL;
	double *command_vector=NULL;
	int size_of_command_vector=10000;
	int length;
	int block=1;
	int n_act;

	aao_FEWHA4octopus fewha4oct;

	// read SH data
	read_stream("dall_slopes.dat", &dall_slopes, &length, block);
//	save_txt_data("dall_slopes.txt", dall_slopes, length, length);

	// allocate memory for mirror commands
	command_vector=(double *)malloc(sizeof(double)*size_of_command_vector);
	// initialize FEWHA, parameters are set inside function, aron is used
	n_act=test_init37m_FEWHA_DEBUG(&(fewha4oct.fewha));
	// prepare fewha4oct
	fewha4oct.slopes=(float *)malloc(sizeof(float)*2*(n_act-1)*(n_act-1));
	fewha4oct.dmshapes=(float *)calloc(sizeof(float),n_act*n_act); // use 0 mirror

	aao_runFEWHA4octopus(command_vector, dall_slopes, &fewha4oct);

	save_txt_data("command.txt", command_vector, n_act*n_act, n_act);

	free(command_vector);
	free(dall_slopes); // memory is allocated in read_stream
	free(fewha4oct.slopes);
	free(fewha4oct.dmshapes);
	// Destroy FEWHA
	aao_freeFEWHA(&(fewha4oct.fewha));


	printf("works fine");
	return 0;
}

void read_stream(const char *name, double **vec, int *length, int block)
{
        FILE *file;
        int i;
        int ret;
        double *stream=NULL;

        file = fopen(name,"r");
        for(i=0;i<block;i++){
        	ret=fread(length, sizeof(int), 1, file);
        	stream=(double *)malloc(sizeof(double)*(*length));
        	ret=fread((void *)stream, sizeof(double), *length, file);
        	if(i+1<block) free(stream); // continue to read
        }
        fclose(file);
        *vec=stream;
}

void save_txt_data(const char *name, double *vec, int length, int newline)
{
    FILE *file;
    int count=0;
    int ret;
    double *stream=NULL;

    file = fopen(name,"w");
    while(count<length)
    {
		fprintf(file, "%e", vec[count]);
    	count++;
    	if(count%newline==0) // newline
    		fprintf(file, "\n");
    	else
    		fprintf(file, "\t");
    }
    fclose(file);
}

int test_init37m_FEWHA_DEBUG(aao_FEWHA *fewha)
{
	// System structs
	aao_System SCAO_on_37m;
	aao_DM dm;
	aao_Star star[NWFS];
	aao_WFS wfs[NWFS];
	aao_Atmosphere atm;
	aao_Star target;

	// FEWHA structs
//	aao_FEWHA fewha = {0};
	aao_FEWHA_Params fewha_param = {0};
	aao_FEWHA_Parallel fewha_parall = {0};

	// sample measurements, sample DM
	float * slopes = NULL, * dmshape = NULL;

	// local variables
	int i, test;
	int status[NWFS];

	// System settings
	float actpos_x[NACTX];
	unsigned char i_act[NACTX*NACTX];
	for(i=0; i<NACTX*NACTX; i++)
		i_act[i] = 1;

	unsigned char i_sub[NSUBX*NSUBX]; // subaperture mask for NGS, LGS
	for(i=0; i<NSUBX*NSUBX; i++)
		i_sub[i] = 1;

	unsigned char i_sub_tt_1[NSUBX_TT_1*NSUBX_TT_1];  // subaperture mask for TTS with NSUB_X_TT_1 subapertures
	for(i=0; i<NSUBX_TT_1*NSUBX_TT_1; i++)
		i_sub_tt_1[i] = 1;

	unsigned char i_sub_tt_2[NSUBX_TT_2*NSUBX_TT_2];  // subaperture mask for TTS with NSUB_X_TT_2 subapertures
	for(i=0; i<NSUBX_TT_2*NSUBX_TT_2; i++)
		i_sub_tt_2[i] = 1;

	float cn2[NLAY];
	float height_layer[NLAY];

	// init settings
	for(i=0; i<NACTX; i++)
	  actpos_x[i] =i*(TELDIAM/NSUBX)-TELDIAM/2;

	cn2[0] = 1;//0.4;
/*	cn2[1] = 0.1;
	cn2[2] = 0.05;
	cn2[3] = 0.05;
	cn2[4] = 0.2;
	cn2[5] = 0.05;
	cn2[6] = 0.05;
	cn2[7] = 0.05;
	cn2[8] = 0.05;*/

	height_layer[0] =0.;
/*	height_layer[1] = 1000.;
	height_layer[2] = 2000.;
	height_layer[3] = 3000.;
	height_layer[4] = 4000.;
	height_layer[5] = 5000.;
	height_layer[6] = 6000.;
	height_layer[7] = 8000.;
	height_layer[8] = 10000.;*/


	// FEWHA settings
	float d_lay[NLAY] = {0.5};//, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5};
											// discretization spacing on each layer, in meters
											// discretization of the *first* layer must be the same as of the ground DM!!
	int   J_lay[NLAY] = {8};//, 5, 5, 5, 5, 5, 5, 5, 5};
											// number of wavelet scales on each layer
											// corresponds to 2^5x2^5 = 32x32 point grids
	float alpha     = 512;					// regularization tuning parameter
	float alpha_eta = 0.;					// spot elongation tuning parameter (=0 NGS model, =1 full LGS model)
	float gain      = 0.4;					// loop gain
	float leaky_int = 0.99;					// leaky integrator value
	int max_iter    = 10;					// number of CG iterations

	int useGLMS = 0;//1;						// GLMS method, turned off
	int usePrecond = 0;						// preconditioner, turned off
	char *createPrecondFromFile = NULL; // use precond from a txt file, if not null (heavy computation)
	float alpha_J = 0;						// preconditioner parameter, turned off for now
	int J_glcs = 4;							// GLMS (ground layer multi-scale) parameter, turned off for now

	int global_par = 0;						// global parallelization on
	int max_thr_system = 12;				// number of threads

	printf("==== Testing init 37m ====\n");

	/* ***************************************************************************** */
	/*                            init fewha 4 octopus                               */
	/* ***************************************************************************** */

	char subapertures_filename[128] = "subapertures_aron.txt";
	char actuators_filename[128]    = "actuators_aron.txt";
	char communication_dir[]=".";
	aao_OctopusAronFile subapertures_aron = {0};	// contents of a subapertures aron file
	aao_OctopusAronFile actuators_aron = {0};		// contents of an actuators aron file
	aao_OctopusAronBlock subapertures_block = {0};	// one block re-used for all WFS
	aao_OctopusAronBlock actuators_block = {0};		// one block re-used for all DMs

	// read aron files
	subapertures_aron = aao_readOctopusAronFile(communication_dir,subapertures_filename);
	actuators_aron    = aao_readOctopusAronFile(communication_dir,actuators_filename);

	if (subapertures_aron.read_status != 0)
	{
		printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
			   "!!! FEWHA initialization ERROR !!!\n"
			   "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
			   "File %s could not be found in directory %s\n"
			   "Aborting initialization\n",subapertures_filename,communication_dir);
		aao_freeOctopusAronFile(&subapertures_aron);
		aao_freeOctopusAronFile(&actuators_aron);
		return -1;
	}
	if (actuators_aron.read_status != 0)
	{
		printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
			   "!!! FEWHA initialization ERROR !!!\n"
			   "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
			   "File %s could not be found in directory %s\n"
			   "Aborting initialization\n",actuators_filename,communication_dir);
		aao_freeOctopusAronFile(&subapertures_aron);
		aao_freeOctopusAronFile(&actuators_aron);
		return -1;
	}


	int nDM=1;
	// === Set DMs ===
	status[0]=0;
	int n_act;
	for(int iDM=0; iDM<nDM; iDM++)
	{
		float dm_height;
		unsigned char const * i_act;
		float const * actpos_x;
		float const * actpos_y;

		// read aron file
		actuators_block = aao_deriveOctopusAronBlock(&actuators_aron, iDM, TELDIAM);

		n_act = actuators_block.n_act;
	//	n_act = 8;
		dm_height = 0;//deformable_mirror[iDM].dm_height;
//		unsigned char i_act[8*8]={0,0,1,1,1,1,0,0,
//							      0,1,1,1,1,1,1,0,
//							      1,1,1,1,1,1,1,1,
//							      1,1,1,1,1,1,1,1,
//							      1,1,1,1,1,1,1,1,
//							      1,1,1,1,1,1,1,1,
//							      0,1,1,1,1,1,1,0,
//							      0,0,1,1,1,1,0,0};
//		unsigned char i_act[8*8]={0,1,1,1,1,1,1,0,
//								  1,1,1,1,1,1,1,1,
//								  1,1,1,1,1,1,1,1,
//								  1,1,1,1,1,1,1,1,
//								  1,1,1,1,1,1,1,1,
//								  1,1,1,1,1,1,1,1,
//								  1,1,1,1,1,1,1,1,
//								  0,1,1,1,1,1,1,0};
		i_act = actuators_block.i_act;

		actpos_x = actuators_block.actpos_x;
		actpos_y = actuators_block.actpos_y;
	//	float actpos_x[8] = {-2.1000, -1.5000, -0.9000, -0.3000, 0.3000, 0.9000, 1.5000, 2.1000};
	//	float actpos_y[8] = {-2.1000, -1.5000, -0.9000, -0.3000, 0.3000, 0.9000, 1.5000, 2.1000};

		// !!! INFO: at the moment only square DMs are possible!
		status[0] += aao_setSquareDM(&dm, n_act, dm_height, i_act, actpos_x, actpos_y);

		aao_freeOctopusAronBlock(&actuators_block);
	}
	if (status[0]==0)
		printf("DM set\n");
	else
		printf("DM set failed \n");


	// === Set WFS ===
	status[0] = aao_setNGS(&star[0],  0,  0, 10000, 0.9);
	if (status[0] == 0)
		printf("Stars set\n");
	else
		printf("Stars set failed\n");

	status[0]=0;
	for(int iWFS=0; iWFS<NWFS; iWFS++)
	{
		aao_WFSType wfs_type = ShackHartmann;
		int n_sub, n_sub_virtual;
		unsigned char const * i_sub;
		float subap_size;
		float wavelength;

		// read aron file
		subapertures_block = aao_deriveOctopusAronBlock(&subapertures_aron, iWFS, TELDIAM);

		n_sub = n_act-1; //NSUBX;//sh_phys_param[iWFS].n_subap;
//		unsigned char i_sub[7*7]={0,0,1,1,1,0,0,
//							      0,1,1,1,1,1,0,
//							      1,1,1,1,1,1,1,
//							      1,1,1,0,1,1,1,
//							      1,1,1,1,1,1,1,
//							      0,1,1,1,1,1,0,
//							      0,0,1,1,1,0,0};
		i_sub = subapertures_block.i_sub;

//		subap_size = 0.6;
		subap_size = subapertures_block.spacing;

		wavelength = 0.700e-6;//sh_phys_param[iWFS].wfs_wavelength; // in meters, e.g., 0.589e-6
		wavelength *= 1e9; // in nanometers, e.g., 589

		status[0]+=aao_setWFS(&wfs[iWFS], wfs_type, n_sub, i_sub, subap_size, wavelength, &star[iWFS]);

		aao_freeOctopusAronBlock(&subapertures_block);
	}

	/* ***************************************************************************** */
	/*                        end of init fewha 4 octopus                            */
	/* ***************************************************************************** */

	// Set DM
//	status[0] = aao_setSquareDM(&dm, NACTX, 0, i_act, actpos_x, actpos_x);


	// Set guide stars
/*	status[0] = aao_setLGS(&star[0],  0.333333333333333,  0, 10000, 0.9, 0.,0.);
	status[1] = aao_setLGS(&star[1],  0., 			      0.333333333333333, 10000, 0.9, 0.,0.);
	status[2] = aao_setLGS(&star[2], -0.333333333333333,  0., 10000, 0.9, 0.,0.);
	status[3] = aao_setLGS(&star[3],  0.,				 -0.333333333333333, 10000, 0.9, 0.,0.);
	status[4] = aao_setNGS(&star[4],  0.707106781186548,  0.707106781186548, 10000, 0.9);
	status[5] = aao_setNGS(&star[5], -0.707106781186548,  0.707106781186548, 10000, 0.9);
	status[6] = aao_setNGS(&star[6], -0.707106781186548, -0.707106781186548, 10000, 0.9);
	status[7] = aao_setNGS(&star[7],  0.707106781186548, -0.707106781186548, 10000, 0.9);
	// coordinates for tip tilt from MatLab to big, atmospheric layer too small!!! Number of photons equal to 1000??
/*	status[8] = aao_setTTS(&star[8],  1.333333333333333,  0                , 1000, 0.9);
	status[9] = aao_setTTS(&star[9], -0.666666666666666,  1.154700538379251, 1000, 0.9);
	status[10]= aao_setTTS(&star[10],-0.666666666666667, -1.154700538379251, 1000, 0.9);*/
	// old ones
/*	status[8] = aao_setTTS(&star[8],  0.8,                  0, 1000, 0.9);
	status[9] = aao_setTTS(&star[9], -0.666666666666666,  0.8, 1000, 0.9);
	status[10]= aao_setTTS(&star[10],-0.666666666666667, -0.8, 1000, 0.9);*/
// Mishas suggestion
/*	status[8] = aao_setNGS(&star[8],  0.8,                  0, 1000, 0.9);
	status[9] = aao_setNGS(&star[9], -0.666666666666666,  0.8, 1000, 0.9);
	status[10]= aao_setNGS(&star[10],-0.666666666666667, -0.8, 1000, 0.9);*/


/*	test=0;
	for(i=0;i<NWFS;i++)
		test+=status[i];

	// Set WFS
	for(i=0; i<NLGS; i++)
		status[i] = aao_setWFS(&wfs[i], ShackHartmann, NSUBX, i_sub, 0.5, 700., &star[i]);
	for(i=NLGS; i<NLGS+NNGS; i++)
		status[i] = aao_setWFS(&wfs[i], ShackHartmann, NSUBX, i_sub, 0.5, 700, &star[i]);
	/*
	 * for TTS, number of NSUBX is virtual and it's used as grid to project to incoming wave!! geometry of virtual grid determined by aperture mask?
	 * From its SH data simply the mean is taken.
	 * Additional parameter for real amount of subapertures (1, 4, ...)??
	 */
/*	for(i=NLGS+NNGS; i<NWFS; i++)
		status[i] = aao_setWFS(&wfs[i], ShackHartmann, NSUBX, i_sub, 0.5, 700, &star[i]);*/

/*	i=NLGS+NNGS;
	status[i] = aao_setWFS(&wfs[i], ShackHartmann, NSUBX_TT_1, i_sub_tt_1, TELDIAM/NSUBX_TT_1, 700, &star[i]);i++;
	status[i] = aao_setWFS(&wfs[i], ShackHartmann, NSUBX_TT_1, i_sub_tt_1, TELDIAM/NSUBX_TT_1, 700, &star[i]);i++;
	status[i] = aao_setWFS(&wfs[i], ShackHartmann, NSUBX_TT_2, i_sub_tt_2, TELDIAM/NSUBX_TT_2, 700, &star[i]);*/

	if (status[0] == 0)
		printf("WFS set\n");
	else
		printf("WFS set failed\n");


	// Set Atmosphere
	float r0=0.106;
	float l0=20.;
	float lgsheight=90000.;
	float lgsfwhm=10000.;
/*******************************************************/ // cn2 is set !!!
//	cn2[0]=1;
//	status[0] = aao_setAtmosphere(&atm,    1, cn2, height_layer, r0, l0, lgsheight, lgsfwhm);

	status[0] = aao_setAtmosphere(&atm, NLAY, cn2, height_layer, r0, l0, lgsheight, lgsfwhm);

	if (status[0]==0)
		printf("Atmosphere set\n");
	else
		printf("Atmosphere set failed\n");

	// Set Probestar
	status[0] = aao_setTarget(&target, 0., 0.);

	if (status[0]==0)
		printf("Probestar set\n");
	else
		printf("Probestar set failed\n");


	// Init system
/*	if (USE_LGS && USE_NGS && USE_TTS) // LGS and NGS and TTS
		status[0] = aao_initSystem(&LTAO_on_10m, LTAO, TELDIAM, ClosedLoop, 1/400., NWFS, wfs, 1, &dm, &atm, 1, &target);
	else if (USE_LGS && USE_NGS && !USE_TTS) // LGS and NGS
		status[0] = aao_initSystem(&LTAO_on_10m, LTAO, TELDIAM, ClosedLoop, 1/400., NWFS-NTTS, wfs, 1, &dm, &atm, 1, &target);
	else if(USE_LGS && !USE_NGS) // LGS only
		status[0] = aao_initSystem(&LTAO_on_10m, LTAO, TELDIAM, ClosedLoop, 1/400., (NWFS-NTTS)/2, wfs, 1, &dm, &atm, 1, &target);
	else if(!USE_LGS && USE_NGS) // NGS only
//		status[0] = aao_initSystem(&LTAO_on_10m, LTAO, TELDIAM, ClosedLoop, 1/400., NNGS-NTTS, wfs+NLGS, 1, &dm, &atm, 1, &target);
		status[0] = aao_initSystem(&LTAO_on_10m, SCAO, TELDIAM, ClosedLoop, 1/400., 1, wfs+NLGS, 1, &dm, &atm, 1, &target);
	else
	{
		printf("error: use LGS and/or NGS");
		return;
	}*/
	status[0] = aao_initSystem(&SCAO_on_37m, SCAO, TELDIAM, ClosedLoop, 1/400., NWFS, wfs, 1, &dm, &atm, 1, &target);
	if (status[0]==0)
		printf("System init successful\n");
	else
		printf("System init failed\n");
	printf("\n");

	// Init FEWHA params
/******************************************************************************/
//	status[0] = aao_setFEWHAparams(&fewha_param,    1, d_lay, J_lay, J_glcs, alpha, alpha_eta, alpha_J, gain, leaky_int, max_iter, useGLMS, usePrecond);
	status[0] = aao_setFEWHAparams(&fewha_param, NLAY, d_lay, J_lay, J_glcs, alpha, alpha_eta, alpha_J, gain, leaky_int, max_iter, useGLMS, usePrecond, createPrecondFromFile);
	if (status[0]==0)
		printf("FEWHA params init successful\n");
	else
		printf("FEWHA params init failed\n");
	printf("\n");

	// Init FEWHA Parallelization configuration
	status[0] = aao_setFEWHAparall(&fewha_parall, global_par, max_thr_system);
	if (status[0]==0)
		printf("FEWHA parallelization set successful\n");
	else
		printf("FEWHA parallelization set failed\n");
	printf("\n");

	// Init FEWHA
	aao_initFEWHA(fewha, &SCAO_on_37m, &fewha_param, &fewha_parall);

	// Print FEWHA config
	aao_printFEWHAconfig(fewha);

	// Init sample measurements
/*	slopes = (float*)calloc(fewha.n_tot_meas,sizeof(float));
	for(i=0; i<fewha.n_tot_meas; i++)
		slopes[i] = 1.0;
	dmshape = (float*)calloc(fewha.n_tot_act, sizeof(float));

	// Call the FEWHA reconstructor
	aao_runFEWHA(dmshape, slopes, &fewha);*/

	// free local variables
	free(slopes);
	slopes = NULL;
	free(dmshape);
	dmshape = NULL;

	aao_freeDM(&dm);
	for(i=0; i<NWFS; i++)
		aao_freeWFS(&wfs[i]);
	aao_freeAtmosphere(&atm);
	// no freeStar(&target) or freeStar(&star[i]) needed -> no dynamic memory in Star
	printf("init works fine\n");
	return n_act;
}





/*
 * aaao_octopus_aron.c
 *
 *  Created on: Oct 17, 2014
 *      Author: misha
 */
// ========= Local functions ==========
// use for mask computation
// returns 1 if |x - x_i| <=smallval && |y - y_i| <= smallval for at least 1 point (x_i,y_i) in file
// else returns 0
unsigned char aao_octopusAronContains(aao_OctopusAronFile const * octfile, int indx, float x, float y, float smallval);
// ====================================

aao_OctopusAronFile aao_readOctopusAronFile(char const * communication_dir, char const * filename)
{
	// local var
	aao_OctopusAronFile octfile = {0};
	FILE * file = NULL;
	char fullpath[2048] = "";
	int c, len, i;
	float col0, col1;
	int col2, n_read;

	// local var init
	sprintf(fullpath,"%s/%s",communication_dir,filename);
	strcpy(octfile.filename, filename);
	octfile.read_status = -1; // NOK

	// code
	file = fopen(fullpath,"r");

	// if opening file failed
	if(file == NULL)
	{
		octfile.col0 = NULL;
		octfile.col1 = NULL;
		octfile.col2 = NULL;
		octfile.len = 0;
		octfile.read_status = -1;	// set status to NOK
		return octfile;
	}

	// count lines
	len = 0;
	while (EOF != (c=fgetc(file)))
	    if (c=='\n')
	        len++;
	len++; // add the last line

	// allocate arrays
	octfile.col0 = (float*)calloc(len,sizeof(float));
	octfile.col1 = (float*)calloc(len,sizeof(float));
	octfile.col2 = (int*)  calloc(len,sizeof(float));

	// rewind file to beginning
	rewind(file);

	// read values; format: "%f %f %d \n"
	i=0; // write counter
	while ( (n_read=fscanf(file, "%f %f %d", &col0, &col1, &col2)) != EOF)
	{
		if(n_read==3)
		{
			octfile.col0[i] = col0;
			octfile.col1[i] = col1;
			octfile.col2[i] = col2;
			i++;
		}
	}
	octfile.len = i; // number of entries written
	fclose(file);

	// set status to OK
	octfile.read_status = 0;

	return octfile;
}

unsigned char aao_octopusAronContains(aao_OctopusAronFile const * octfile, int indx, float x, float y, float smallval)
{
	// local var
	int i;
	unsigned int contains;

	// code
	contains = 0;

	for(i=0; i<octfile->len; i++)
	{
		// index matched
		if(octfile->col2[i] == indx)
		{
			if(fabsf(x-octfile->col0[i])<=smallval && fabsf(y-octfile->col1[i])<=smallval)
			{
				contains = 1;
				break;
			}
		}
	}

	return contains;
}

aao_OctopusAronBlock aao_deriveOctopusAronBlock(aao_OctopusAronFile const * octfile, int indx, float teldiam)
{
	// local var
	aao_OctopusAronBlock octblk = {0};
	int n_act, n_sub, n_act2, n_sub2;
	unsigned char * i_act = NULL;
	unsigned char * i_sub = NULL;
	float * actpos_x = NULL;
	float * actpos_y = NULL;
	float spacing; // need teldiam ONLY if 1x1 sensor!!

	float x, y; // help quantities
	int i, j;
	float min, max; // for searching min and max
	float smallval = 1e-6;

	// code
	// task 1: compute the spacing
	spacing = teldiam; // ONLY if 1x1 sensor
	for(i=0; i<octfile->len-1; i++)
	{
		if(octfile->col2[i] == indx && octfile->col2[i+1] == indx && octfile->col0[i] == octfile->col0[i+1])
		{
			// e.g., -2.100000e+01 -4.000000e+00 0  and  -2.100000e+01 -3.500000e+00 0
			spacing = octfile->col1[i+1] - octfile->col1[i]; // substract the y-values
			break;
		}
	}

	// task 2: compute min and max values
	min = FLT_MAX;
	max = -FLT_MAX;

	for(i=0; i<octfile->len; i++)
	{
		// index matched
		if(octfile->col2[i] == indx)
		{
			// find block's minimum value, doesn't matter if x or y
			min = (octfile->col0[i] < min) ? octfile->col0[i] : min;
			min = (octfile->col1[i] < min) ? octfile->col1[i] : min;

			// find block's maximum value, doesn't matter if x or y
			max = (octfile->col0[i] > max) ? octfile->col0[i] : max;
			max = (octfile->col1[i] > max) ? octfile->col1[i] : max;
		}
	}

	// !!! ASSUMPTION 1 !!! domain is square
	// !!! ASSUMPTION 2 !!! domain is not "shifted", i.e., the middle point *is* (0,0)
	// note: subapertures_aron.txt entries: -21, -20.5, ..., 20.5     => min = -21, max = 20.5
	//          actuators_aron.txt entries: -21, -20.5, ..., 20.5, 21 => min = -21, max = 21

	// do allow domains where -min != max
	max = (max > -min) ? max : -min;	// maximum (max, -min)
	min = -max;
	// note: in both subap and act examples above, now min = -21, max = 21

	// task 3: compute n_sub and n_act
	n_sub = (int)roundf(2.0*max/spacing);
	n_act = n_sub+1;

	n_sub2 = n_sub*n_sub;
	n_act2 = n_act*n_act;

	// task 4: derive points
	actpos_x = (float*)calloc(n_act,sizeof(float));
	actpos_y = (float*)calloc(n_act,sizeof(float));

	for(i=0; i<n_act; i++)
	{
		actpos_x[i] = min+i*spacing;
		actpos_y[i] = min+i*spacing;
	}

	// task 5: derive masks
	i_sub = (unsigned char*)calloc(n_sub2,sizeof(unsigned char));
	i_act = (unsigned char*)calloc(n_act2,sizeof(unsigned char));

	// subaperture mask (subapertures_aron.txt contains only lower left corner points, only these points are traversed)
	for(i=0; i<n_sub; i++)
	for(j=0; j<n_sub; j++)
	{
		x = actpos_x[i];
		y = actpos_y[j];
		i_sub[i*n_sub+j] = aao_octopusAronContains(octfile,indx,x,y,smallval);
	}

	// actuator mask (actuators_aron.txt contains all active actuator points)
	for(i=0; i<n_act; i++)
	for(j=0; j<n_act; j++)
	{
		x = actpos_x[i];
		y = actpos_y[j];
		i_act[i*n_act+j] = aao_octopusAronContains(octfile,indx,x,y,smallval);
	}

	// copy to struct
	octblk.indx     = indx;
	octblk.i_act    = i_act;
	octblk.n_act    = n_act;
	octblk.i_sub    = i_sub;
	octblk.actpos_x = actpos_x;
	octblk.actpos_y = actpos_y;
	octblk.spacing  = spacing;

	return octblk;
}

// Free aao_OctopusAronFile struct
//  Input: octopus_aron_file struct
void aao_freeOctopusAronFile(aao_OctopusAronFile * octfile)
{
	if(octfile->col0 != NULL)
	{
		free(octfile->col0);
		octfile->col0 = NULL;
	}
	if(octfile->col1 != NULL)
	{
		free(octfile->col1);
		octfile->col1 = NULL;
	}
	if(octfile->col2 != NULL)
	{
		free(octfile->col2);
		octfile->col2 = NULL;
	}
	strcpy(octfile->filename,"");
	octfile->len = 0;
}
void aao_freeOctopusAronBlock(aao_OctopusAronBlock * octblk)
{
	if(octblk->i_act != NULL)
	{
		free(octblk->i_act);
		octblk->i_act = NULL;
	}
	if(octblk->i_sub != NULL)
	{
		free(octblk->i_sub);
		octblk->i_sub = NULL;
	}
	if(octblk->actpos_x != NULL)
	{
		free(octblk->actpos_x);
		octblk->actpos_x = NULL;
	}
	if(octblk->actpos_y != NULL)
	{
		free(octblk->actpos_y);
		octblk->actpos_y = NULL;
	}
}


/*
 * debug_functions.h
 *
 *  Created on: Aug 8, 2014
 *      Author: misha
 */

#ifndef DEBUG_FUNCTIONS_H_
#define DEBUG_FUNCTIONS_H_

// writes a vector x of length n to filename
// e.g., int x[3] = {1,2,3}; n=3; filename="data.txt"
void print_vec_to_file(char const * filename, float const * x, int n);

void print_vec_double_to_file(char const * filename, double const * x, int n);


#endif /* DEBUG_FUNCTIONS_H_ */

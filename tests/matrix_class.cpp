/*
 * matrix.cpp
 *
 *  Created on: Jun 20, 2014
 *      Author: misha
 */

#include <stdexcept>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <iostream>
#include "matrix_class.h"

using std::cout;
using std::endl;


Matrix_T::Matrix_T() : rows_(0), cols_(0), data_(0) { }
Matrix_T::Matrix_T(unsigned int rows, unsigned int cols) : rows_(rows), cols_(cols), data_(rows*cols, 0.) { }
Matrix_T::Matrix_T(Matrix_T const & M) : rows_(M.rows_), cols_(M.cols_), data_(M.data_) { }
Matrix_T::~Matrix_T() { }
Matrix_T & Matrix_T::operator=(Matrix_T const & M)
{
	if (this != &M)
	{
		rows_  = M.rows_;
		cols_  = M.cols_;
		data_  = M.data_;
	}
	return *this;
}
Matrix_T Matrix_T::transposed() const
{
	// local var
	Matrix_T Ret(cols_, rows_);
	unsigned int i,j;

	// code
	for(i=0; i<rows_; i++)
		for(j=0; j<cols_; j++)
			Ret(j,i) = operator ()(i,j);

	return Ret;
}
float & Matrix_T::operator[](unsigned int i)
{
	if(i > rows_*cols_)
	{
		char msg[256];
		sprintf(msg," index [%d] is out of bounds in %d x %d", i, rows_, cols_);
		throw std::out_of_range(msg);
	}
	return data_[i];
}
float Matrix_T::operator[](unsigned int i) const
{
	if(i > rows_*cols_)
	{
		char msg[256];
		sprintf(msg," index [%d] is out of bounds in %d x %d", i, rows_, cols_);
		throw std::out_of_range(msg);
	}
	return data_[i];
}
float & Matrix_T::operator()(unsigned int i, unsigned int j)
{
	if(i > rows_ || j > cols_)
	{
		char msg[256];
		sprintf(msg," index (%d,%d) is out of bounds in %d x %d", i,j, rows_, cols_);
		throw std::out_of_range(msg);
	}
	return operator[](i*cols_+j);
}
float Matrix_T::operator()(unsigned int i, unsigned int j) const
{
	if(i > rows_ || j > cols_)
	{
		char msg[256];
		sprintf(msg," index (%d,%d) is out of bounds in %d x %d", i,j, rows_, cols_);
		throw std::out_of_range(msg);
	}
	return operator[](i*cols_+j);
}
float const * Matrix_T::data() const
{
	return data_.data();
}
Matrix_T & Matrix_T::operator+=(Matrix_T const & M)
{
	// local var
	unsigned int i;

	// code
	if(rows_ != M.rows_ || cols_ != M.cols_)
	{
		char msg[256];
		sprintf(msg,"dim mismatch: %d x %d != %d x %d", rows_, cols_, M.rows_, M.cols_);
		throw std::out_of_range(msg);
	}

	for(i=0; i<rows_*cols_; i++)
		data_[i] += M.data_[i];

	return *this;
}
Matrix_T & Matrix_T::operator-=(Matrix_T const & M)
{
	// local var
	unsigned int i;

	// code
	if(rows_ != M.rows_ || cols_ != M.cols_)
	{
		char msg[256];
		sprintf(msg,"dim mismatch: %d x %d != %d x %d", rows_, cols_, M.rows_, M.cols_);
		throw std::out_of_range(msg);
	}

	for(i=0; i<rows_*cols_; i++)
		data_[i] -= M.data_[i];

	return *this;
}
vector<float> Matrix_T::operator*(vector<float> const & x) const
{
	// local var
	unsigned int i,j;
	float tmp;
	vector<float> ret(rows_,0.);

	if(cols_ != x.size())
	{
		char msg[256];
		sprintf(msg,"dim mismatch: size(M) = %d x %d, size(x) = %d", rows_, cols_, (unsigned int)x.size());
		throw std::out_of_range(msg);
	}

	for (i=0; i<rows_; i++)
	{
		tmp = 0.0;
		for(j=0; j<cols_; j++)
			tmp += operator()(i,j)*x.at(j);
		ret[i] = tmp;
	}

	return ret;
}
unsigned int Matrix_T::rows() const
{
	return rows_;
}
unsigned int Matrix_T::cols() const
{
	return cols_;
}
float Matrix_T::frobenius_norm() const
{
	// local var
	unsigned int i;
	float ret;

	// code
	ret = 0.;
	for(i=0; i<rows_*cols_; i++)
		ret += data_[i]*data_[i];

	ret = sqrtf(ret);

	return ret;
}
float Matrix_T::max_element() const
{
	// local var
	float ret;
	unsigned int i;

	// code
	if (rows_*cols_ == 0)
		return 0.;	// if no elements, return 0

	ret = std::numeric_limits<float>::min();
	for(i=0; i<rows_*cols_; i++)
	{
		if(data_[i] > ret)
			ret = data_[i];
	}

	return ret;

}
float Matrix_T::min_element() const
{
	// local var
	float ret;
	unsigned int i;

	// code
	if (rows_*cols_ == 0)
		return 0.;	// if no elements, return 0

	ret = std::numeric_limits<float>::max();
	for(i=0; i<rows_*cols_; i++)
	{
		if(data_[i] < ret)
			ret = data_[i];
	}

	return ret;
}
unsigned int Matrix_T::nnz() const
{
	// local var
	unsigned int ret;
	unsigned int i;

	// code
	ret = 0;
	for(i=0; i<rows_*cols_; i++)
	{
		if(data_[i] != 0)
			ret++;
	}

	return ret;
}
void Matrix_T::print() const
{
	// local var
	unsigned int i,j;

	for(i=0; i<rows_; i++)
	{
		for(j=0; j<cols_; j++)
		{
			cout << operator ()(i,j) << " ";
		}
		cout << endl;
	}
}
float vector_norm(vector<float> const & x)
{
	// local var
	unsigned int i;
	float ret;

	// code
	ret = 0.;
	for(i=0; i<x.size(); i++)
		ret += x[i]*x[i];

	ret = sqrtf(ret);

	return ret;
}




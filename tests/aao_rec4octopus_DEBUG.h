/*
 * aao_rec4octopus.h
 *
 *  Created on: Oct 13, 2014
 *      Author: misha
 */

#ifndef AAO_REC4OCTOPUS_H_
#define AAO_REC4OCTOPUS_H_

#include "aao_FEWHA.h"
#include "structures_DEBUG.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * AAO reconstructors available:
 *    CuReD: SCAO w/ SH or Pyramid (set via sh_simul_param[0].wfs_type)
 *    FEWHA: SCAO/LTAO/MOAO/MCAO w/ SH
 *
 *    Note: to use P-CuReD set sh_simul_param[0].wfs_type=1 in parameter file and
 *          set rec_type=CuReD in aao_rec_param.
 */
typedef enum { CuReD, FEWHA } aao_ReconstructorType;

/*
 * Reconstruction layer settings:
 *      GroundLayer: 1 layer at height 0 with cn2=1; use this setting for SCAO reconstructors
 *                   e.g.: rec_layer_height = NULL; rec_layer_cn2 = NULL;
 *  SimulatedLayers: layer height and strength is read from turbulentlayer struct
 *                   e.g.: rec_layer_height = NULL; rec_layer_cn2 = NULL;
 *      DMsAsLayers: layer heights are at DMs, cn2 is read from rec_layer_cn2 in aao_rec_param struct
 *                   e.g.: rec_layer_height = NULL; rec_layer_cn2 = (float*)malloc(n_dms,sizeof(float));
 *                         rec_layer_cn2[0] = ...;
 *     CustomLayers: layer heights and weights are read from rec_layer_height, rec_layer_cn2 in aao_rec_param struct
 *                   e.g.: rec_layer_height = (float*)malloc(n_dms,sizeof(float));
 *                         rec_layer_cn2    = (float*)malloc(n_dms,sizeof(float));
 *                         rec_layer_height[0] = ...; rec_layer_cn2[0] = ...;
 */
typedef enum { GroundLayer, SimulatedLayers, DMsAsLayers, CustomLayers } aao_RecLayerType;

/*
 * AAO Reconstructor parameters
 *
 * !!! IMPORTANT !!!
 * allocate aao_rec_param struct variables to zero, e.g.,
 * aao_rec_param params = {0};
 * otherwise, aao_freeRecParam will crash
 * !!! IMPORTANT !!!
 */
typedef struct struct_aao_rec_param {

    // Reconstructor
    aao_ReconstructorType rec_type; // AAO reconstructor type, range: see above; fixed

    // System parameters
    aao_SystemType sys_type;  // system type, range: {SCAO, LTAO, MOAO, MCAO}; fixed
    aao_LoopType loop_type;   // loop type, range: {OpenLoop, ClosedLoop}; fixed

    // Gain parameters
    float gain;               // controller gain, range: (0,1], typical value: 0.4; variable
    float leaky_int;          // leaky integrator value, range: (0,1], typical value: 0.99; variable

    // Iterative reconstructor parameters
    int max_iter;             // number of iterations of PCG; range: |N; variable

    // Layer parameters
    aao_RecLayerType rec_layer_type; // reconstruction layers type, range: see above; fixed
    int n_layers;             // number of layers, range: |N; fixed
    float * rec_layer_height; // size = n_layers, height of rec. layers, range: [0,infty), units: meters
    float * rec_layer_cn2;    // size = n_layers, cn2 fraction of rec. layers, range: (0,1]; units: fraction of 1


    // FEWHA layer parameters
    float * d_lay;            // size = n_layers, equidistant spacing; units: meters; fixed
    int   * J_lay;            // size = n_layers, number of wavelet scales on layer; fixed
    int     J_glcs;           // ground layer coarse scales, J_glcs_ = 0 means GLMS turned off; fixed

    // FEWHA Regularization tuning parameters
    float alpha;              // regularization tuning, range: [0,infty), typical value: 1; variable
    float alpha_eta;          // elongation tuning, range: [0,1], 0...NGS model, 1...LGS model; variable
    float alpha_J;            // preconditioner thresholding; range: [0,infty), typical value: 10e7; variable

    // FEWHA Options switches
    int useGLMS;              // set to 1 to use GLMS, set to skip GLMS
    int usePrecond;           // set to 1 to use PCG, set to 0 to use CG

    // FEWHA Parallelization parameter
    int global;               // true or false (0 or 1)
    int max_thr_system;       // maximum number of threads on this system, let it be 1 for now

    // CuReD parameter
    int CuReD_nSubDivs;       // number of domains for decomposition, range: |N;
                              // typical value: s.t. 10-20 subaps are in 1 domain; fixed

} struct_aao_rec_param;

/*
 * Free aao_rec_param struct
 * In/Out: params
 */
void aao_freeRecParam(struct_aao_rec_param * aao_rec_param);



/*
 * FEWHA struct for octopus. contents:
 * - FEWHA struct (see aao_FEWHA.h), containing the whole reconstructor
 * - local copy of dm shapes vector and slopes vector
 */
typedef struct aao_FEWHA4octopus
{
	float * dmshapes;	// size = fewha->n_tot_act; current dm shapes; fewha format
	float * slopes;		// size = fewha->n_tot_meas; current slopes; fewha format
	aao_FEWHA fewha;	// FEWHA struct
} aao_FEWHA4octopus;

/*
 * Initialize the fewha4oct struct
 * Output: fewha4octopus struct
 * Input: octopus settings, reconstructor settings (see above)
 */
void aao_initFEWHA4octopus(
		// Output: FEWHA struct
		aao_FEWHA4octopus * fewha4oct,

		// Input: OCTOPUS system definitions
		float time_unit,
		struct_telescope const * telescope,
		int n_wfs,
		struct_sh_phys_param const * sh_phys_param, // size = n_wfs
		struct_sh_simul_param const * sh_simul_param, // size = n_wfs
		struct_guidestar const * guidestar, // size = n_wfs
		int n_psf,
		struct_guidestar const * probestar,
		int n_layers,
		struct_turbulentlayer const * turbulentlayer,
		int n_dms,
		struct_deformable_mirror const * deformable_mirror,
		char const * communication_dir,

		// Input: reconstruction parameters
		struct_aao_rec_param const * aao_rec_param
		);

/*
 * Run the FEWHA recosntructor in octopus
 *  Output: command_vector (this vector will be modified !!!!)
 *          ... the new DM shapes, octopus style
 *   Input: dall_slopes (this vector will *not* modified)
 *          ...  measurements, octopus style
 *          fewha4oct struct (this struct will be modified: internal temp storage)
 */
void aao_runFEWHA4octopus(double * command_vector, double const * dall_slopes, aao_FEWHA4octopus * fewha4oct);

/*
 * Frees the fewha4oct struct
 * all memory is dealloacted
 * In/Out: fewha4oct
 */
void aao_freeFEWHA4octopus(aao_FEWHA4octopus * fewha4oct);

#ifdef __cplusplus
}
#endif

#endif /* AAO_REC4OCTOPUS_H_ */

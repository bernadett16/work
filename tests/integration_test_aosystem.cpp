/*
 * integration_tests_aosystem.cpp
 *
 *  Created on: Jan 24, 2019
 *      Author: bernadett
 */
extern "C" {
#include "aao_system.h"
}
#include "catch.hpp"

#define TELDIAM 10.
#define NWFS 3
#define NTTS 0
#define NACTX 11
#define NSUBX 10
#define NLAY 3

TEST_CASE("test init system") {
  aao_System LTAO_on_10m;
  unsigned char i_act[NACTX * NACTX];
  float actpos_x[NACTX];
  aao_DM dm;
  aao_Star star[NWFS];
  unsigned char i_sub[NSUBX * NSUBX];
  aao_WFS wfs[NWFS];
  float r0 = 0.106;
  float l0 = 20.;
  float lgsheight = 90000.;
  float lgsfwhm = 10000.;
  aao_Atmosphere atm;
  float cn2[NLAY];
  float height_layer[NLAY];
  aao_Star target;

  for (int i = 0; i < NACTX * NACTX; i++)
    i_act[i] = 1;
  for (int i = 0; i < NACTX; i++)
    actpos_x[i] = (float)(i * (TELDIAM / NSUBX) - TELDIAM / 2);
  for (int i = 0; i < NSUBX * NSUBX; i++)
    i_sub[i] = 1;
  cn2[0] = 0.4;
  cn2[1] = 0.1;
  cn2[2] = 0.5;
  height_layer[0] = (float)0.;
  height_layer[1] = (float)1000.;
  height_layer[2] = (float)2000.;

  aao_setSquareDM(&dm, NACTX, 0, i_act, actpos_x, actpos_x);

  aao_setLGS(&star[0], 0.333333333333333, 0, 10000, 0.9, 0., 0.);
  aao_setLGS(&star[1], 0., 0.333333333333333, 10000, 0.9, 0., 0.);
  aao_setNGS(&star[2], 0.707106781186548, 0.707106781186548, 10000, 0.9);

  aao_setWFS(&wfs[0], ShackHartmann, NSUBX, i_sub, 0.5, 700., &star[0]);
  aao_setWFS(&wfs[1], ShackHartmann, NSUBX, i_sub, 0.5, 700., &star[1]);
  aao_setWFS(&wfs[2], ShackHartmann, NSUBX, i_sub, 0.5, 700., &star[2]);

  aao_setAtmosphere(&atm, NLAY, cn2, height_layer, r0, l0, lgsheight, lgsfwhm);

  aao_setTarget(&target, 0., 0.);

  REQUIRE(aao_initSystem(&LTAO_on_10m, LTAO, TELDIAM, ClosedLoop, 1 / 400.,
                         NWFS - NTTS, wfs, 1, &dm, &atm, 1, &target) == 0);
  CHECK(LTAO_on_10m.atmosphere_.cn2_[1] == (float)0.1);
}
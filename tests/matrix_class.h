/*
 * matrix.h
 *
 * Some very standard functions of a full matrix. In particular: storage, +/-, Frobenius norm
 *
 *  Created on: Jun 20, 2014
 *      Author: misha
 */

#ifndef MATRIX_CLASS_H_
#define MATRIX_CLASS_H_

#include <vector>

using std::vector;

class Matrix_T
{
private:
	unsigned int rows_;
	unsigned int cols_;
	vector<float> data_;

public:
	// Constructors / copy / destructor
	Matrix_T();
	Matrix_T(unsigned int rows, unsigned int cols);
	Matrix_T(Matrix_T const & M);
	~Matrix_T();
	Matrix_T & operator=(Matrix_T const &);
	Matrix_T   transposed() const;

	// Access / Mofifiers
	float & operator[](unsigned int i); 		  		// global index = i*cols_+j
	float   operator[](unsigned int i) const; 			// global index = i*cols_+j
	float & operator()(unsigned int i, unsigned int j); // matrix index = i*cols_+j
	float   operator()(unsigned int i, unsigned int j) const; // matrix index = i*cols_+j
	float const * data() const;

	// Addition, subtraction
	Matrix_T & operator+=(Matrix_T const &);
	Matrix_T & operator-=(Matrix_T const &);

	// Matrix-vector multiplication
	vector<float> operator*(vector<float> const &) const;

	// Properties
	unsigned int rows() const;
	unsigned int cols() const;
	float frobenius_norm() const;
	float max_element() const;
	float min_element() const;
	unsigned int nnz() const;

	// Display
	void print() const;
};

float vector_norm(vector<float> const & x);



#endif /* MATRIX_CLASS_H_ */

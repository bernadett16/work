/*
 * test.cpp
 *
 *  Created on: Jan 23, 2019
 *      Author: bernadett
 */

#include "catch.hpp"
extern "C" {
#include "aao_vec_op.h"
}

// TODO tests to be continued
TEST_CASE("test vector times const") {
  float test[5];

  test[0] = 0;
  test[1] = 2;
  test[2] = 3;
  test[3] = -1;
  test[4] = 5;

  aao_vec_ti_const_ac(test, 5, 5);

  CHECK(test[0] == 0);
  CHECK(test[1] == 10);
  CHECK(test[2] == 15);
  CHECK(test[3] == -5);
  CHECK(test[4] == 25);
}

TEST_CASE("test vector copy") {
  float toCopy[5];
  float result[5];

  toCopy[0] = 1;
  toCopy[1] = 2;
  toCopy[2] = 3;
  toCopy[3] = 4;
  toCopy[4] = 5;

  aao_vec_copy(toCopy, result, 5);
  for (int i = 0; i < 5; i++) {
    CHECK(toCopy[i] == result[i]);
  }
}

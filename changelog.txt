FEWHA v1.1.7beta
Date: 21.05.2015
Author: MY
Notes: 
  - SR is working on the code
  - corrected a comment in aao_normAwoD.c
  - added doc/implementation/tip_tilt_sensors: checklist of where/how TTS should be added
  - added write to logfile options, print configuration options to aao_rec4octopus.h, .c

FEWHA v1.1.6
Date: 21.05.2015
Author: MY
Notes: 
  - updated makefile's debuggin settings
  - added some todos
  - attempted a MOST interface via MATLAB's loadlibrary(...):
    FAILED b/c MATLAB doesn't seem to support a pointer to array of structs,
    see src/interfaces/most/readme_IMPORTANT.txt for more details
  - updated install_fewha.sh for octopus, manual.txt
  - fixed "GroundLayer" bug in aao_rec4octopus.c - there was a missing break;

FEWHA v1.1.5
Date: 04.12.2014
Author: MY
Notes: 
  - print fewha version inside of print fewha config
  - updated mex compilation for linux script
  - write videos in matlab log script 
  - added a test_init10m_FEWHA.c file for testing
  - aao_glms: n_lay_glcs2 is an unused variabled -> commented it out
  - bug fix: ali found a bug in found in aao_FEWHA.c: aao_initFEWHAstruct_:
    fewha->n_tot_sub_lgs was set wrong if nLGS == nWFS (index out of bound access)

FEWHA v1.1.4
Date: 28.11.2014
Author: MY
Notes: 
  - print a config along with the logs for the matlab script
  - added a docs/logs/aao_plotFEWHAlog.m to plot logs

FEWHA v1.1.3
Date: 28.11.2014
Author: MY
Notes: 
  - found a small bug in aao_printFEWHAconfig

FEWHA v1.1.2
Date: 28.11.2014
Author: MY
Notes: 
  - found small bugs in aao_writeFEWHAlogfile_

FEWHA v1.1.1
Date: 28.11.2014
Author: MY
Notes: 
  - added doc/tests/verified_systems.txt - a log of all verified systems (so far only
    NTAO and LTAO on OCTOPUS)
  - added a todo-list doc/fewha_todo_list.txt
  - updated interface to octopus: pcured setting is now removed (run via cured)
  - deleted include/aao_export_api.h
  - write debug info (layer shapes, wavelet coefficients, etc) to a files via
    aao_openFEWHAlog, aao_writeFEWHAlog, aao_closeFEWHAlog
  - new FEWHA_log header and c file to store FILE* to log files


FEWHA v1.1.0
Date: 21.11.2014
Author: MY
Notes:
  - finalizing octopus interace!!
  - updated examples: install script, copy of octopus Makefile, close_loop.c, etc.
  - added an installation/run manual for octopus
  - added struct_aao_rec_param
  - renamed: aao_FEWHA4octopus -> aao_rec4octopus
  - added /etc folder with project instructions + configs for Eclipse 
  - code is now on the eso/Software svn

FEWHA v1.0.9
Date: 16.11.2014
Author: MY
Notes:
  - interface with octopus (written and tested; some parameters are still hard-coded, 
    e.g., system type, see aao_FEWHA4octopus.h)
  - added src/interfaces/octopus: aao_FEWHA4octopus.h, aao_FEWHA4octopus.c
  - added aao_printFEWHAversion to display current version of FEWHA
  - added folder: src/io, put aao_print.* and aao_octopus_aron.* (these are new files) in there
  - added tests for aao_octopus_aron with real aron txt files (I/O for octopus)

FEWHA v1.0.8
Date: 12.10.2014
Author: MY
Notes:
  - fixed aao_invFEWHA bug: slopes=0

FEWHA v1.0.7
Date: 12.10.2014
Author: MY
Notes:
  - GLMS bug fixed; GLMS can now be used

FEWHA v1.0.6
Date: 11.10.2014
Author: MY
Notes:
  - fixed small errors in printing functions (for debugging); these errors were
    found with valgrind: gcc -g -O0 ...; valgrind --leak-check=yes ./bin/fewha
  - added the NTAO configuration
  - updated mex compilation files
  - added cholesky solver (decomposition, back-substitution) (aao_matr_op.c, .h)
  - added a very simple cholesky test
  - added GLMS (inversion via Cholesky)... not verified!!! see: aao_glms.c
  - added OperatorG (for GLMS) that stores GLMS matrices, help-vectors, LDL^T 
    decomposition; wrote init/free/print routines for the operator
  - fixed bugs (GLMS related) in aao_wavemult_ac_coarse_scales and related 
    functions; added a aao_wavemult_ac_coarse_scales function
  - fixed GLMS related bugs in applyA
  - printing (debugging) now also writes the min and max element of an array
  - added aao_invFEWHA in aao_FEWHA.h: DM shapes to slopes map (applyH)

FEWHA v1.0.5
Date: 05.10.2014
Author: MY
Notes:
  - fixed a bug in aao_initP_template
  - added aao_print.c, aao_print.h with funcitons to print debug information
    to console
  - added funcions aao_print<StructType>(...) for each struct type
  - added aao_printFEWHAconfig(...); to print the whole config to console

FEWHA v1.0.4
Date: 06.09.2014
Author: MY
Notes:
  - make file can make library as well as binary now
  - loop setting is saved once in FEWHA now, in fewha.AOsys->loop_
  - aao_resetFEWHAloop -> opens the loop
  - leaky integrator parameter added
  - aao_setFEWHAparall, aao_freeFEWHAparall functions to set/free 
    parallelization parameters, aao_initFEWHAparall that initializes 
    parallelization struct
  - aao_FEWHA_private.h, aao_FEWHA_parallel.c files
  - in FEWHA struct: aao_FEWHA_Parallel, aao_FEWHA_Param, AOsys get copied! 
    not set via pointer! (can delete originals)

FEWHA v1.0.3
Date: 31.08.2014
Author: MY
Notes:
  - included test_initEELT_FEWHA.c with MOAO config for E-ELT;
  - testing if the preconditioner works
  - cleaned up some unused variables (after linux compilation warnings)
  - init iJ now prints iteration number
  - some init iJ bugs fixed (not tested again MATLAB code yet)
  - test_initCANARY that defines the system only is back
  - renaming all files to and all functions to aao_*
  - organized the include directory
  - extern "C" for all headers in include dir
  - updated readme.txt file

FEWHA v1.0.2
Date: 22.08.2014
Author: MY
Notes:
  - renamed folder 'system' to 'fewha'
  - added runFEWHA function that encapsulates 'control'
  - renamed parallelizaiton to FEWHA_parallel
  - removed unnecessary files from interface
  - renamed this file to changelog.txt
  * this version was sent to Alastair! *
  
FEWHA v1.0.1
Date: 19.08.2014
Author: MY
Notes:
  - added makefile
  - added bin directory

FEWHA v1.0.0
Date: 14.08.2014
Author: MY
Notes:
  - cleanly written update from few_rtc_v_0_09 
  - main update: standalone code / does not rely on MATLAB code that genreates
    binray files; parameters are defined via atmo tomo inteface or a matlab
    struct, the rest is generated within the code
  - also, uses cartesian (x,y) coordinate system (no weird transposes here)
  - code capabilities:
    - fully functional code from measurements to mirror commands with many
      major features turned off
    - CG (no prec tested, although prec computational implemented)
    - no GLMS
    - no parallelization tested (although its implmemented)
    - no TTS, just NGS and LGS
    - mex interface
    - adapted to atmo tomo intefrace draft 1.1
    - no updating parameters on the fly implemented
  - code tested and compared in MOAO CG (no prec, no GLMS, no parallelization)
    to MATLAB code -> success!

/*
 * sh_op.c
 *
 *  Created on: June 16, 2014
 *      Author: Misha Yudytskiy
 *       Notes:
 *
 *  (C) 2014 Austrian Adaptive Optics Team
 *      http://eso-ao.indmath.uni-linz.ac.at/
 */

#include "aao_sh_op.h"
#include <math.h>
#include <stdlib.h>

#ifndef M_PI
#define M_PI 3.14159265358979323846264338327950288 /* pi */
#endif
#define FACTOR 0.2
int ceil_(int x, int y) { return x % y ? x / y + 1 : x / y; }

void aao_sh_tt(float *s, float const *phi, int n_sub_tt, int n_sub_virtual,
               unsigned char const *i_sub_virtual, float *tmp) {
  // local variables
  int ix, iy, iSub_x, iSub_y;
  int x_start, x_end, y_start, y_end;
  int index_tt;
  int n_sub_active_virtual;
  int n_phi_virtual; // x2 means x^2
  float *Sx = NULL;
  float *Sy = NULL;

  // local variables init
  n_phi_virtual = n_sub_virtual + 1; // is set in n_phi_at_wfs[tts] !!!!!!!!!!!
  Sx = s;
  Sy = s + n_sub_tt * n_sub_tt;
  for (ix = 0; ix < n_sub_tt * n_sub_tt; ix++) {
    *(Sx + ix) = 0;
    *(Sy + ix) = 0;
  }

  // Convention:
  // function       matrix     matrix     in memory
  // phi(x_i,y_j) = phi_{ij} = phi(i,j) = phi[i*n_phi+j]

  // code

  for (ix = 0; ix < n_sub_virtual; ix++) {
    for (iy = 0; iy < n_phi_virtual; iy++) {
      // size tmp = n_sub x n_phi
      // tmp(i,j) = phi(i+1,j) - phi(i,j), 0<=i<n_sub, 0<=j<n_phi
      tmp[ix * n_phi_virtual + iy] =
          phi[(ix + 1) * n_phi_virtual + iy] - phi[ix * n_phi_virtual + iy];
    }
  }

  // run through TT subapertures and compute on each the virtual SH data and
  // take its mean
  for (iSub_x = 0; iSub_x < n_sub_tt; iSub_x++) {
    for (iSub_y = 0; iSub_y < n_sub_tt; iSub_y++) {
      // compute virtual indices of incoming wave w.r.t. TT sub aperture ->
      // precomputation?
      /*   x_start and x_end:
       *   h_tt:=1/n_sub_tt ... "relative" size of TT sub aperture, use size 1;
       * h:=1/n_sub_virtual ... "relative" size of virtual sub aperture iSub *
       * h_tt + rest = x_start * h, where 0 <= rest < h;  x_start = iSub / h *
       * h_tt + rest / h = ceil(iSub / h * h_tt) (iSub + 1)* h_tt - rest = x_end
       * * h;                 x_end = (iSub + 1)/ h * h_tt - rest / h =
       * floor((iSub + 1)/ h * h_tt)
       */
      x_start = ceil_(iSub_x * n_sub_virtual, n_sub_tt);
      x_end = (int)((iSub_x + 1.) * n_sub_virtual / n_sub_tt); // floor
      y_start = ceil_(iSub_y * n_sub_virtual, n_sub_tt);
      y_end = (int)((iSub_y + 1.) * n_sub_virtual / n_sub_tt);
      index_tt = iSub_x * n_sub_tt + iSub_y;
      n_sub_active_virtual = 0;
      for (ix = x_start; ix < x_end; ix++) {
        for (iy = y_start; iy < y_end; iy++) {
          if (i_sub_virtual[ix * n_sub_virtual + iy]) // active subaperture?
          {
            //						*(Sx+index_tt)+=(tmp[ix*n_phi_virtual+(iy+1)]+tmp[ix*n_phi_virtual+iy])
            /// 2.; // sum up for mean
            n_sub_active_virtual++;
          }
          /* without mask */ *(Sx + index_tt) +=
              (tmp[ix * n_phi_virtual + (iy + 1)] +
               tmp[ix * n_phi_virtual + iy]) /
              2.; // sum up for mean
        }
      }
      *(Sx + index_tt) /= n_sub_active_virtual; // mean
      // from matlab code, use upper result and multiply by factor
      // n_sub_virtual/n_sub_tt:
      *(Sx + index_tt) *= (n_sub_virtual / n_sub_tt) * FACTOR;
      // try instead only 1/n_sub_tt -> no change
      //*(Sx+index_tt)/=n_sub_tt;
    }
  }

  for (ix = 0; ix < n_phi_virtual; ix++) {
    for (iy = 0; iy < n_sub_virtual; iy++) {
      // size tmp = n_phi x n_sub
      // tmp(i,j) = phi(i,j+1) - phi(i,j), 0<=i<n_phi, 0<=j<n_sub
      tmp[ix * n_sub_virtual + iy] =
          phi[ix * n_phi_virtual + (iy + 1)] - phi[ix * n_phi_virtual + iy];
    }
  }

  for (iSub_x = 0; iSub_x < n_sub_tt; iSub_x++) {
    for (iSub_y = 0; iSub_y < n_sub_tt; iSub_y++) {
      // compute virtual indices of incoming wave wrt. TT subaperture
      x_start = ceil_(iSub_x * n_sub_virtual, n_sub_tt);
      x_end = (int)((iSub_x + 1.) * n_sub_virtual / n_sub_tt); // floor
      y_start = ceil_(iSub_y * n_sub_virtual, n_sub_tt);
      y_end = (int)((iSub_y + 1.) * n_sub_virtual / n_sub_tt);
      index_tt = iSub_x * n_sub_tt + iSub_y;
      n_sub_active_virtual = 0;
      for (ix = x_start; ix < x_end; ix++) {
        for (iy = y_start; iy < y_end; iy++) {
          if (i_sub_virtual[ix * n_sub_virtual + iy]) // active subaperture?
          {
            //						*(Sy+index_tt)+=i_sub_virtual[ix*n_sub_virtual+iy]*(tmp[(ix+1)*n_sub_virtual+iy]+tmp[ix*n_sub_virtual+iy])
            /// 2.;
            n_sub_active_virtual++;
          }
          /* without mask */ *(Sy + index_tt) +=
              (tmp[(ix + 1) * n_sub_virtual + iy] +
               tmp[ix * n_sub_virtual + iy]) /
              2.;
        }
      }
      *(Sy + index_tt) /= n_sub_active_virtual; // mean
      /* from matlab code, use upper result and multiply by factor
       * n_sub_virtual/n_sub_tt: */
      *(Sy + index_tt) *= (n_sub_virtual / n_sub_tt) * FACTOR;
    }
  }

  /*// only for one subaperture !!
          (*Sx)=0;(*Sy)=0;
          for(ix=0; ix<n_sub; ix++)
          {
                  for(iy=0; iy<n_phi; iy++)
                  {
                          // size tmp = n_sub x n_phi
                          // tmp(i,j) = phi(i+1,j) - phi(i,j), 0<=i<n_sub,
     0<=j<n_phi tmp[ix*n_phi+iy] = phi[(ix+1)*n_phi+iy]-phi[ix*n_phi+iy];
                  }
          }
          for(ix=0; ix<n_sub; ix++)
          {
                  for(iy=0; iy<n_sub; iy++)
                  {
                          // Sx(i,j) = (tmp(i,j+1) + tmp(i,j))/2, 0<=i<n_sub,
     0<=j<n_sub
                          // Sx[ix*n_sub+iy] =
     (tmp[ix*n_phi+(iy+1)]+tmp[ix*n_phi+iy]) / 2.;
                          (*Sx)+=(tmp[ix*n_phi+(iy+1)]+tmp[ix*n_phi+iy]) / 2.;
                  }
          }


          for(ix=0; ix<n_phi; ix++)
          {
                  for(iy=0; iy<n_sub; iy++)
                  {
                          // size tmp = n_phi x n_sub
                          // tmp(i,j) = phi(i,j+1) - phi(i,j), 0<=i<n_phi,
     0<=j<n_sub tmp[ix*n_sub+iy] = phi[ix*n_phi+(iy+1)]-phi[ix*n_phi+iy];
                  }
          }
          for(ix=0; ix<n_sub; ix++)
          {
                  for(iy=0; iy<n_sub; iy++)
                  {
                          // Sy(i,j) = (tmp(i+1,j) + tmp(i,j))/2, 0<=i<n_sub,
     0<=j<n_sub
                          // Sy[ix*n_sub+iy] =
     (tmp[(ix+1)*n_sub+iy]+tmp[ix*n_sub+iy]) / 2.;
                          (*Sy)+=(tmp[(ix+1)*n_sub+iy]+tmp[ix*n_sub+iy]) / 2.;
                  }
          }
          // free local variables
          (*Sx)/=n_sub_active_virtual; // number of ones in i_sub_virtual
          (*Sy)/=n_sub_active_virtual;*/
}

void aao_sh(float *s, float const *phi, int n_sub, float *tmp) {
  // s = [Sx; Sy]
  // Sx, Sy size = n_sub^2
  // n_phi = n_sub + 1
  // phi size = n_phi^2
  // tmp size = n_sub*n_phi

  // local variables
  int ix, iy;
  int n_phi, n_sub2; // x2 means x^2
  float *Sx = NULL;
  float *Sy = NULL;

  // local variables init
  n_phi = n_sub + 1;
  n_sub2 = n_sub * n_sub;
  Sx = s;
  Sy = s + n_sub2;

  // Convention:
  // function       matrix     matrix     in memory
  // phi(x_i,y_j) = phi_{ij} = phi(i,j) = phi[i*n_phi+j]

  // code
  for (ix = 0; ix < n_sub; ix++) {
    for (iy = 0; iy < n_phi; iy++) {
      // size tmp = n_sub x n_phi
      // tmp(i,j) = phi(i+1,j) - phi(i,j), 0<=i<n_sub, 0<=j<n_phi
      tmp[ix * n_phi + iy] = phi[(ix + 1) * n_phi + iy] - phi[ix * n_phi + iy];
    }
  }
  for (ix = 0; ix < n_sub; ix++) {
    for (iy = 0; iy < n_sub; iy++) {
      // Sx(i,j) = (tmp(i,j+1) + tmp(i,j))/2, 0<=i<n_sub, 0<=j<n_sub
      Sx[ix * n_sub + iy] =
          (tmp[ix * n_phi + (iy + 1)] + tmp[ix * n_phi + iy]) / 2.;
    }
  }

  for (ix = 0; ix < n_phi; ix++) {
    for (iy = 0; iy < n_sub; iy++) {
      // size tmp = n_phi x n_sub
      // tmp(i,j) = phi(i,j+1) - phi(i,j), 0<=i<n_phi, 0<=j<n_sub
      tmp[ix * n_sub + iy] = phi[ix * n_phi + (iy + 1)] - phi[ix * n_phi + iy];
    }
  }
  for (ix = 0; ix < n_sub; ix++) {
    for (iy = 0; iy < n_sub; iy++) {
      // Sy(i,j) = (tmp(i+1,j) + tmp(i,j))/2, 0<=i<n_sub, 0<=j<n_sub
      Sy[ix * n_sub + iy] =
          (tmp[(ix + 1) * n_sub + iy] + tmp[ix * n_sub + iy]) / 2.;
    }
  }

  // free local variables
}

/* similar to aao_sh_tr, but uses "prolongation" operator for tip tilt data s,
 * i.e. Sx is equal on each virtual subaperture */
void aao_sh_tt_tr(float *phi, float const *s, int n_sub_tt, int n_sub_virtual,
                  unsigned char const *i_sub_virtual) {
  // s = [Sx; Sy]
  // Sx, Sy size = n_sub^2
  // n_phi = n_sub + 1
  // phi size = n_phi^2

  // local variables
  int ix, iy, tt_i, tt_i0, tt_x, tt_y, tt_y0, index0, index;
  int n_phi, n_sub_tt2; // x2 means x^2
  float const *Sx = NULL;
  float const *Sy = NULL;
  float tmp_x, tmp_y; // t^x_{ij}, t^y_{ij}

  int iSub_x, iSub_y, x_start, y_start, x_end, y_end, *n_sub_active_virtual = 0;
  float sx, sy, sx0, sy0;

  // local variables init
  n_phi = n_sub_virtual + 1;
  n_sub_tt2 = n_sub_tt * n_sub_tt;
  Sx = s;
  Sy = s + n_sub_tt2;

  // count virtual active subapertures for tt subaperture
  n_sub_active_virtual = (int *)malloc(n_sub_tt2 * sizeof(int));
  for (iSub_x = 0; iSub_x < n_sub_tt; iSub_x++) {
    for (iSub_y = 0; iSub_y < n_sub_tt; iSub_y++) {
      x_start = ceil_(iSub_x * n_sub_virtual, n_sub_tt);
      x_end = (int)((iSub_x + 1.) * n_sub_virtual / n_sub_tt); // floor
      y_start = ceil_(iSub_y * n_sub_virtual, n_sub_tt);
      y_end = (int)((iSub_y + 1.) * n_sub_virtual / n_sub_tt);
      n_sub_active_virtual[iSub_x * n_sub_tt + iSub_y] = 0;
      for (ix = x_start; ix < x_end; ix++)
        for (iy = y_start; iy < y_end; iy++)
          n_sub_active_virtual[iSub_x * n_sub_tt + iSub_y] +=
              i_sub_virtual[ix * n_sub_virtual +
                            iy]; // count virtual active subapertures for tt
                                 // subaperture
    }
  }

  // Convention:
  // function       matrix     matrix     in memory
  // phi(x_i,y_j) = phi_{ij} = phi(i,j) = phi[i*n_phi+j]
  // index		  index
  // (i,j)		  (ix,iy)

  // code
  ix = 0;
  {
    // pre-set the first "column" of phi to 0
    for (iy = 0; iy < n_phi; iy++)
      phi[ix * n_phi + iy] = 0.;
  }
  for (ix = 0; ix < n_sub_virtual; ix++) // n_sub = n_phi-1
  {
    iy = 0;

    // phi_{ij}    = -s^x_{ij} + s^x_{i-1,j} - s^y_{ij} - s^y_{i-1,j} for j=0,
    // do it in two steps, first
    // phi_{ij}   += -s^x_{ij}               - s^y_{ij}
    // second,
    // phi_{i+1,j} =           + s^x_{ij}               - s^y_{ij}

    // new //
    tt_x = (int)(((float)ix) * n_sub_tt / n_sub_virtual);
    tt_y = 0;

    tt_i = tt_x * n_sub_tt + tt_y;
    index = ix * n_sub_virtual + iy;

    sx = Sx[tt_i] / n_sub_active_virtual[tt_i];
    sx *= (n_sub_virtual / n_sub_tt) * FACTOR;
    sy = Sy[tt_i] / n_sub_active_virtual[tt_i];
    sy *= (n_sub_virtual / n_sub_tt) * FACTOR;

    //		phi[ix*n_phi+iy]     += i_sub_virtual[index]*( -sx-sy )/2.;
    //		phi[(ix+1)*n_phi+iy]  = i_sub_virtual[index]*(  sx-sy )/2.;
    phi[ix * n_phi + iy] += (-sx - sy) / 2.; // unmasked
    phi[(ix + 1) * n_phi + iy] = (sx - sy) / 2.;
    // *** //

    //  old  //
    //		phi[ix*n_phi+iy]     += ( -Sx[ix*n_sub+iy]
    //		                          -Sy[ix*n_sub+iy] )/2.;
    //		phi[(ix+1)*n_phi+iy]  = (  Sx[ix*n_sub+iy]
    //							      -Sy[ix*n_sub+iy]
    //)/2.;
    //		// ***** //

    for (iy = 1; iy < n_sub_virtual; iy++) // n_sub = n_phi-1
    {
      // t^x_{ij} = s^x_{i,j-1} + s^x_{ij}
      // t^y_{ij} = s^y_{i,j-1} - s^y_{ij}
      // new //
      tt_y = (int)(((float)iy) * n_sub_tt / n_sub_virtual); // iy=0
      tt_y0 = (int)((iy - 1.) * n_sub_tt / n_sub_virtual);  // iy=0
      tt_i = tt_x * n_sub_tt + tt_y;
      tt_i0 = tt_x * n_sub_tt + tt_y0;

      index = ix * n_sub_virtual + iy;
      index0 = ix * n_sub_virtual + (iy - 1);

      sx0 = Sx[tt_i0] / n_sub_active_virtual[tt_i0];
      sx0 *= (n_sub_virtual / n_sub_tt) * FACTOR;
      sy0 = Sy[tt_i0] / n_sub_active_virtual[tt_i0];
      sy0 *= (n_sub_virtual / n_sub_tt) * FACTOR;
      sx = Sx[tt_i] / n_sub_active_virtual[tt_i];
      sx *= (n_sub_virtual / n_sub_tt) * FACTOR;
      sy = Sy[tt_i] / n_sub_active_virtual[tt_i];
      sy *= (n_sub_virtual / n_sub_tt) * FACTOR;

      //			tmp_x = sx0*i_sub_virtual[index0] +
      //sx*i_sub_virtual[index]; 			tmp_y = sy0*i_sub_virtual[index0] -
      //sy*i_sub_virtual[index];
      tmp_x = sx0 + sx; // unmasked
      tmp_y = sy0 - sy;
      // *** //

      //   old   //
      //			tmp_x = Sx[ix*n_sub_virtual+(iy-1)] +
      //Sx[ix*n_sub_virtual+iy]; 			tmp_y = Sy[ix*n_sub_virtual+(iy-1)] -
      //Sy[ix*n_sub_virtual+iy];
      //			// ******* //

      // General formula for phi:
      // phi_{ij}     = (t^x_{i-1,j} - t^x_{ij}    + t^y_{i-1,j} + t^y_{ij})/2
      // in this step we add
      // phi_{ij}    += (             -t^x_{ij}                  + t^y_{ij})/2
      phi[ix * n_phi + iy] += (-tmp_x + tmp_y) / 2.;

      // in the next step we have
      // phi_{ij}     = (t^x_{i-1,j} - t^x_{ij}    + t^y_{i-1,j} + t^y_{ij})/2,
      // therefore phi_{i+1,j}  = (t^x_{ij}    - t^x_{i+1,j} + t^y_{ij}    +
      // t^y_{i+1,j})/2, therefore we set phi_{i+1,j}  = ( t^x_{i+1,j} +
      // t^y_{i+1,j})/2
      phi[(ix + 1) * n_phi + iy] = (tmp_x + tmp_y) / 2.;
    }

    iy = n_sub_virtual; // n_sub = n_phi-1

    // phi_{ij}    = -s^x_{i,j-1} + s^x_{i-1,j-1} + s^y_{i,j-1}  + s^y_{i-1,j-1}
    // for j=n_phi-1, like before do it in two steps, first phi_{ij}   +=
    // -s^x_{i,j-1}                 + s^y_{i,j-1} second, phi_{i+1,j} = +
    // s^x_{i,j-1}   				 + s^y_{i,j-1}

    // new //
    tt_x = (int)(((float)ix) * n_sub_tt / n_sub_virtual);
    tt_y = (int)(((float)iy - 1.) * n_sub_tt / n_sub_virtual);

    tt_i = tt_x * n_sub_tt + tt_y;
    index = ix * n_sub_virtual + (iy - 1);

    sx = Sx[tt_i] / n_sub_active_virtual[tt_i];
    sx *= (n_sub_virtual / n_sub_tt) * FACTOR;
    sy = Sy[tt_i] / n_sub_active_virtual[tt_i];
    sy *= (n_sub_virtual / n_sub_tt) * FACTOR;

    //		phi[ix*n_phi+iy]     += i_sub_virtual[index]*( -sx+sy )/2.;
    //		phi[(ix+1)*n_phi+iy]  = i_sub_virtual[index]*( +sx+sy )/2.;
    phi[ix * n_phi + iy] += (-sx + sy) / 2.; // unmasked
    phi[(ix + 1) * n_phi + iy] = (+sx + sy) / 2.;
    // *** //

    //  old  //
    //		phi[ix*n_phi+iy]     += ( -Sx[ix*n_sub_virtual+(iy-1)]
    //							      +Sy[ix*n_sub_virtual+(iy-1)]
    //)/2.; 		phi[(ix+1)*n_phi+iy]  = ( +Sx[ix*n_sub_virtual+(iy-1)]
    //								  +Sy[ix*n_sub_virtual+(iy-1)]
    //)/2.;
    //      // ***** //
  }

  free(n_sub_active_virtual);
  // phi_{ij}, where ix=n_phi-1=n_sub is set automatically

  /**********************************************************************************************/
  /*  old version    */
  /*	// only works with one subaperture !!
          n_sub_tt=1;
          // i_sub_virtual unused !!!

          // local variables
          int ix, iy;
          int n_phi, n_sub2; // x2 means x^2
          float Sx;
          float Sy;
          float tmp_x, tmp_y; // t^x_{ij}, t^y_{ij}

          // local variables init
          n_phi  = n_sub_tt + 1;
          n_sub2 = n_sub_tt*n_sub_tt;
          Sx = (*s)/n_sub2;
          Sy = *(s + 1)/n_sub2;

          // Convention:
          // function       matrix     matrix     in memory
          // phi(x_i,y_j) = phi_{ij} = phi(i,j) = phi[i*n_phi+j]
          // index		  index
          // (i,j)		  (ix,iy)

          // code
          ix=0;
          {
                  // pre-set the first "column" of phi to 0
                  for(iy=0; iy<n_phi; iy++)
                          phi[ix*n_phi+iy] = 0.;
          }
          for (ix=0; ix<n_sub_tt; ix++) // n_sub = n_phi-1
          {
                  iy=0;

                  // phi_{ij}    = -s^x_{ij} + s^x_{i-1,j} - s^y_{ij} -
     s^y_{i-1,j} for j=0,
                  // do it in two steps, first
                  // phi_{ij}   += -s^x_{ij}               - s^y_{ij}
                  // second,
                  // phi_{i+1,j} =           + s^x_{ij}               - s^y_{ij}

                  phi[ix*n_phi+iy]     += (-Sx-Sy)/2.;
                  phi[(ix+1)*n_phi+iy]  = ( Sx-Sy)/2.;

                  for(iy=1; iy<n_sub_tt; iy++) // n_sub = n_phi-1
                  {
                          // t^x_{ij} = s^x_{i,j-1} + s^x_{ij}
                          // t^y_{ij} = s^y_{i,j-1} - s^y_{ij}
                          tmp_x = Sx+Sx;
                          tmp_y = Sy-Sy;

                          // General formula for phi:
                          // phi_{ij}     = (t^x_{i-1,j} - t^x_{ij}    +
     t^y_{i-1,j} + t^y_{ij})/2
                          // in this step we add
                          // phi_{ij}    += (             -t^x_{ij} +
     t^y_{ij})/2 phi[ix*n_phi+iy] += ( -tmp_x + tmp_y )/2.;

                          // in the next step we have
                          // phi_{ij}     = (t^x_{i-1,j} - t^x_{ij}    +
     t^y_{i-1,j} + t^y_{ij})/2, therefore
                          // phi_{i+1,j}  = (t^x_{ij}    - t^x_{i+1,j} +
     t^y_{ij}    + t^y_{i+1,j})/2, therefore we set
                          // phi_{i+1,j}  = (              t^x_{i+1,j} +
     t^y_{i+1,j})/2 phi[(ix+1)*n_phi+iy] = (tmp_x + tmp_y)/2.;
                  }

                  iy=n_sub_tt; //n_sub = n_phi-1

                  // phi_{ij}    = -s^x_{i,j-1} + s^x_{i-1,j-1} + s^y_{i,j-1}  +
     s^y_{i-1,j-1} for j=n_phi-1,
                  // like before do it in two steps, first
                  // phi_{ij}   += -s^x_{i,j-1}                 + s^y_{i,j-1}
                  // second,
                  // phi_{i+1,j} =              + s^x_{i,j-1}
     + s^y_{i,j-1}

                  phi[ix*n_phi+iy]     += ( -Sx+Sy )/2.;
                  phi[(ix+1)*n_phi+iy]  = ( +Sx+Sy )/2.;
          }
          // phi_{ij}, where ix=n_phi-1=n_sub is set automatically*/
}

void aao_sh_tr(float *phi, float const *s, int n_sub) {
  // s = [Sx; Sy]
  // Sx, Sy size = n_sub^2
  // n_phi = n_sub + 1
  // phi size = n_phi^2

  // local variables
  int ix, iy;
  int n_phi, n_sub2; // x2 means x^2
  float const *Sx = NULL;
  float const *Sy = NULL;
  float tmp_x, tmp_y; // t^x_{ij}, t^y_{ij}

  // local variables init
  n_phi = n_sub + 1;
  n_sub2 = n_sub * n_sub;
  Sx = s;
  Sy = s + n_sub2;

  // Convention:
  // function       matrix     matrix     in memory
  // phi(x_i,y_j) = phi_{ij} = phi(i,j) = phi[i*n_phi+j]
  // index		  index
  // (i,j)		  (ix,iy)

  // code
  ix = 0;
  {
    // pre-set the first "column" of phi to 0
    for (iy = 0; iy < n_phi; iy++)
      phi[ix * n_phi + iy] = 0.;
  }
  for (ix = 0; ix < n_sub; ix++) // n_sub = n_phi-1
  {
    iy = 0;

    // phi_{ij}    = -s^x_{ij} + s^x_{i-1,j} - s^y_{ij} - s^y_{i-1,j} for j=0,
    // do it in two steps, first
    // phi_{ij}   += -s^x_{ij}               - s^y_{ij}
    // second,
    // phi_{i+1,j} =           + s^x_{ij}               - s^y_{ij}

    phi[ix * n_phi + iy] += (-Sx[ix * n_sub + iy] - Sy[ix * n_sub + iy]) / 2.;
    phi[(ix + 1) * n_phi + iy] =
        (Sx[ix * n_sub + iy] - Sy[ix * n_sub + iy]) / 2.;

    for (iy = 1; iy < n_sub; iy++) // n_sub = n_phi-1
    {
      // t^x_{ij} = s^x_{i,j-1} + s^x_{ij}
      // t^y_{ij} = s^y_{i,j-1} - s^y_{ij}
      tmp_x = Sx[ix * n_sub + (iy - 1)] + Sx[ix * n_sub + iy];
      tmp_y = Sy[ix * n_sub + (iy - 1)] - Sy[ix * n_sub + iy];

      // General formula for phi:
      // phi_{ij}     = (t^x_{i-1,j} - t^x_{ij}    + t^y_{i-1,j} + t^y_{ij})/2
      // in this step we add
      // phi_{ij}    += (             -t^x_{ij}                  + t^y_{ij})/2
      phi[ix * n_phi + iy] += (-tmp_x + tmp_y) / 2.;

      // in the next step we have
      // phi_{ij}     = (t^x_{i-1,j} - t^x_{ij}    + t^y_{i-1,j} + t^y_{ij})/2,
      // therefore phi_{i+1,j}  = (t^x_{ij}    - t^x_{i+1,j} + t^y_{ij}    +
      // t^y_{i+1,j})/2, therefore we set phi_{i+1,j}  = ( t^x_{i+1,j} +
      // t^y_{i+1,j})/2
      phi[(ix + 1) * n_phi + iy] = (tmp_x + tmp_y) / 2.;
    }

    iy = n_sub; // n_sub = n_phi-1

    // phi_{ij}    = -s^x_{i,j-1} + s^x_{i-1,j-1} + s^y_{i,j-1}  + s^y_{i-1,j-1}
    // for j=n_phi-1, like before do it in two steps, first phi_{ij}   +=
    // -s^x_{i,j-1}                 + s^y_{i,j-1} second, phi_{i+1,j} = +
    // s^x_{i,j-1}   				 + s^y_{i,j-1}

    phi[ix * n_phi + iy] +=
        (-Sx[ix * n_sub + (iy - 1)] + Sy[ix * n_sub + (iy - 1)]) / 2.;
    phi[(ix + 1) * n_phi + iy] =
        (+Sx[ix * n_sub + (iy - 1)] + Sy[ix * n_sub + (iy - 1)]) / 2.;
  }
  // phi_{ij}, where ix=n_phi-1=n_sub is set automatically
}

void aao_mask(float *s, unsigned char const *I_sub, int n_sub) {
  // s = [Sx; Sy]
  // Sx, Sy size = n_sub^2
  // I_act size = n_sub^2

  // local variables
  int i;
  int n_sub2; // x2 means x^2
  float *Sx = NULL;
  float *Sy = NULL;

  // local variables init
  n_sub2 = n_sub * n_sub;
  Sx = s;
  Sy = s + n_sub2;

  // code
  for (i = 0; i < n_sub2; i++) {
    if (I_sub[i] == 0) {
      // Sx[ix*n_sub+iy] *= mask[ix*n_sub+iy]
      // Sy[ix*n_sub+iy] *= mask[ix*n_sub+iy]
      // where i=ix*n_sub+iy
      Sx[i] = 0.;
      Sy[i] = 0.;
    }
  }
}

void aao_removeTT(float *s, int n_sub, int n_active_sub2) {
  // s = [Sx; Sy]
  // Sx, Sy size = n_sub^2

  // local variables
  int i;
  int n_sub2; // x2 means x^2
  float *Sx = NULL;
  float *Sy = NULL;
  float tmp;

  // local variables init
  n_sub2 = n_sub * n_sub;
  Sx = s;
  Sy = s + n_sub2;

  // code
  // 1. compute the tip
  tmp = 0.;
  for (i = 0; i < n_sub2; i++)
    tmp += Sx[i];
  tmp /= n_active_sub2;

  // 2. remove the tip
  for (i = 0; i < n_sub2; i++)
    Sx[i] -= tmp;

  // 3. compute the tilt
  tmp = 0.;
  for (i = 0; i < n_sub2; i++)
    tmp += Sy[i];
  tmp /= n_active_sub2;

  // 4. remove the tilt
  for (i = 0; i < n_sub2; i++)
    Sy[i] -= tmp;
}

int aao_numActSubap(unsigned char const *I_sub, int n_sub) {
  // s = [Sx; Sy]
  // Sx, Sy size = n_sub^2
  // I_act size = n_sub^2

  // local variables
  int i;
  int n_sub2; // x2 means x^2
  int n_active_sub2;

  // local variables init
  n_sub2 = n_sub * n_sub;

  // code
  n_active_sub2 = 0;
  for (i = 0; i < n_sub2; i++)
    n_active_sub2 += I_sub[i];

  return n_active_sub2;
}

void aao_invC_NGS(float *s, float sigma2, int n_sub) {
  // s = [Sx; Sy]
  // Sx, Sy size = n_sub^2

  // local variables
  int i;
  int n_sub2; // x2 means x^2
  float *Sx = NULL;
  float *Sy = NULL;

  // local variables init
  n_sub2 = n_sub * n_sub;
  Sx = s;
  Sy = s + n_sub2;

  // code
  for (i = 0; i < n_sub2; i++) {
    // Sx[ix*n_sub+iy] = Sx[ix*n_sub+iy] / (sigma^2)
    // Sy[ix*n_sub+iy] = Sy[ix*n_sub+iy] / (sigma^2)
    Sx[i] /= sigma2;
    Sy[i] /= sigma2;
  }
}

void aao_invC_LGS(float *s, float const *invCxx, float const *invCxy,
                  float const *invCyy, int n_sub) {
  // s = [Sx; Sy]
  // Sx, Sy size = n_sub^2
  // invCxx, invCxy, invCyy size = n_sub^2

  // local variables
  int i;
  int n_sub2; // x2 means x^2
  float *Sx = NULL;
  float *Sy = NULL;
  float tmp_x, tmp_y;

  // local variables init
  n_sub2 = n_sub * n_sub;
  Sx = s;
  Sy = s + n_sub2;

  // code
  for (i = 0; i < n_sub2; i++) {
    tmp_x = invCxx[i] * Sx[i] + invCxy[i] * Sy[i];
    tmp_y = invCxy[i] * Sx[i] + invCyy[i] * Sy[i];
    Sx[i] = tmp_x;
    Sy[i] = tmp_y;
  }
}

void aao_init_invC_LGS(float *invCxx, float *invCxy, float *invCyy, int n_sub,
                       unsigned char const *I_sub, float inv_sigma2,
                       float subap_size, float spot_fwhm, float na_height,
                       float na_fwhm, float gs_llt_x, float gs_llt_y,
                       float alpha_eta) {
  // local variables
  int ix, iy;
  double iC2x2xx, iC2x2xy, iC2x2yy;
  double betax, betay;
  double betax2, betaxy, betay2, beta2;
  double na_height2, spot_fwhm2, alpha_eta2;

  double *sub_cent_x; // subaperture centers
  double x0;

  double const RAD_TO_ARCSEC = 60. * 60. * 180. / M_PI;

  // local var init
  sub_cent_x = (double *)calloc(n_sub, sizeof(double));
  na_height2 = na_height * na_height;
  spot_fwhm2 = spot_fwhm * spot_fwhm;
  alpha_eta2 = alpha_eta * alpha_eta;

  // code

  // compute centers of subapertures
  x0 = -n_sub * subap_size / 2. +
       subap_size / 2.; // left-most center e.g., -84*0.5/2 + 0.5/2 = -21 + 0.5
                        // = -20.5, right-most center = 20.5
  for (ix = 0; ix < n_sub; ix++)
    sub_cent_x[ix] = x0 + subap_size * ix;

  // compute matrix entries
  for (ix = 0; ix < n_sub; ix++) {
    for (iy = 0; iy < n_sub; iy++) {
      if (I_sub[n_sub * ix + iy] == 0) {
        // subaperture is inactive; set values to zero
        // (used for automatic masking at runtime)
        invCxx[n_sub * ix + iy] = 0.;
        invCxy[n_sub * ix + iy] = 0.;
        invCyy[n_sub * ix + iy] = 0.;
      } else {
        // subaperture is active so compute the matrix entries:

        // compute the elongation vector beta = (beta_x, beta_y)
        betax =
            na_fwhm / na_height2 * (sub_cent_x[ix] - gs_llt_x) * RAD_TO_ARCSEC;
        betay =
            na_fwhm / na_height2 * (sub_cent_x[iy] - gs_llt_y) * RAD_TO_ARCSEC;

        // components of C^{-1}_{2x2} blocks
        betax2 = betax * betax;
        betaxy = betax * betay;
        betay2 = betay * betay;
        beta2 = betax2 + betay2; // scalar product squared

        // compute C^{-1}_{2x2} blocks
        // step 1
        iC2x2xx = 1. + alpha_eta2 * betay2 / spot_fwhm2;
        iC2x2xy = -alpha_eta2 * betaxy / spot_fwhm2;
        iC2x2yy = 1. + alpha_eta2 * betax2 / spot_fwhm2;

        // step 2
        iC2x2xx *= spot_fwhm2 / (spot_fwhm2 + alpha_eta2 * beta2);
        iC2x2xy *= spot_fwhm2 / (spot_fwhm2 + alpha_eta2 * beta2);
        iC2x2yy *= spot_fwhm2 / (spot_fwhm2 + alpha_eta2 * beta2);

        // step 3
        iC2x2xx *= inv_sigma2; // inv_sigma2 = 1/(sigma^2)
        iC2x2xy *= inv_sigma2;
        iC2x2yy *= inv_sigma2;

        // put entries to matrix
        invCxx[n_sub * ix + iy] = (float)iC2x2xx;
        invCxy[n_sub * ix + iy] = (float)iC2x2xy;
        invCyy[n_sub * ix + iy] = (float)iC2x2yy;
      }
    }
  }

  // free local var
  if (sub_cent_x != NULL) {
    free(sub_cent_x);
    sub_cent_x = NULL;
  }
}

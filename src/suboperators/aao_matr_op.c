/*
 * aao_matr_op.c
 *
 *  Created on: Oct 10, 2014
 *      Author: misha
 */

#include "aao_matr_op.h"

void chol_decomp(float * L, float * D, float const * A, int n)
{
	// A ... size = nxn
	// L ... size = nxn
	// D ... size = n (storing only the diagonal)

	// local var
	int i,j,k;

	// code
	for(j=0; j<n; j++)
	{
		// D_j = A_jj - \sum_{k=1}^{j-1} L_{jk}^2 D_k
		D[j] = A[n*j+j];
		for(k=0; k<j; k++)
			D[j] -= L[n*j+k]*L[n*j+k]*D[k];

		// L_{ii} = 1
		L[n*j+j] = 1;

		// L_{ij} = (1/D_j) (A_{ij} - \sum_{k=1}^{j-1} L_{ik} L_{jk} D_k) for i>j
		for(i=j+1; i<n; i++)
		{
			L[n*i+j] = A[n*i+j];
			for(k=0; k<j; k++)
				L[n*i+j] -= L[n*i+k]*L[n*j+k]*D[k];

			if(D[j] == 0 && L[n*i+j] == 0)
				L[n*i+j] = 0;
			else
				L[n*i+j] /= D[j];
		}
	}
}

void chol_solve(float * x, float const * L, float const * D, float const * b, int n)
{
	// L ... size nxn
	// D ... size n (only the diagonal)
	// b ... nx1
	// x ... nx1

	// local var
	int i,j;

	// code
	// 1. solve Lx1 = b
	for(i=0; i<n; i++)
	{
		x[i] = b[i];
		for(j=0; j<i; j++)
			x[i] -= L[n*i+j]*x[j];
	}
	// 2. solve Dx2 = x1;
	for(i=0; i<n; i++)
	{
		if (D[i] == 0)
			x[i] = 0.;
		else
			x[i] /= D[i];
	}


	// 3. solve L'x = x2;
	for(i=n-1; i>=0; i--)
	{
		//x[i] = x[i];
		for(j=n-1; j>i; j--)
			x[i] -= L[n*j+i]*x[j]; // L^T(i,j) = L(j,i)
	}
}

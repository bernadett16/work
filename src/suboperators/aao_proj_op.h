/*
 * proj_op.h
 *
 *	Operations related to the bilinear interpolation and geometric propagation
 *
 *  Created on: June 21, 2014
 *      Author: Misha Yudytskiy
 *       Notes: see below
 *
 *  (C) 2014 Austrian Adaptive Optics Team
 *      http://eso-ao.indmath.uni-linz.ac.at/
 */

#ifndef AAO_PROJ_OP_H_
#define AAO_PROJ_OP_H_

// =========== Interpolation structs ===========
#include "aao_proj_op_structs.h"

// =========== Interpolation init/free ===========
void aao_initInterp2(aao_Interp2 * I, float posx, float posy, float h, float H, int n_phi, float d_phi, int n_lay, float d_lay, float x0_lay);
void aao_initInterp2_NULL(aao_Interp2 * I);
void aao_freeInterp2(aao_Interp2 * I);

// =========== Interpolation actions ===========

// Bilinear interpolation operator: layer (bilinear coefficients) -> projected wavefront
// Accumulation operation, i.e., phi += Interp2(lay)
//  Notes: uses temporary variable I.tmp.tmp
// Output: phi   ... wavefront, size = n_phi x n_phi (e.g., 85 x 85)
//  Input: I     ... interpolation struct, see above
//         lay   ... bilinear layer coefficinets, size = n_lay x n_lay (e.g., 128x128)
//		   n_phi ... number of wavefront grid points in one line (e.g., 85)
//         n_lay ... number of layer grid points in one line (e.g., 128)
void aao_interp2_ac(float * phi, aao_Interp2 I, float const * lay, int n_phi, int n_lay);

// Transposed bilinear interpolation operator:
// projected wavefront -> layer (bilinear coefficients)
//  Notes: uses temporary variable I.tmp.tmp
// Output: lay   ... bilinear layer coefficinets, size = n_lay x n_lay (e.g., 128x128)
//  Input: I     ... interpolation struct, see above
//         phi   ... wavefront, size = n_phi x n_phi (e.g., 85 x 85)
//		   n_phi ... number of wavefront grid points in one line (e.g., 85)
//         n_lay ... number of layer grid points in one line (e.g., 128)
void aao_interp2_tr(float * lay, struct aao_Interp2 I, float const * phi, int n_phi, int n_lay);

// Transposed bilinear interpolation operator:
// projected wavefront -> layer (bilinear coefficients)
// Accumulation operation, i.e., lay += Interp2^T(phi)
//  Notes: uses temporary variable I.tmp.tmp
// Output: lay   ... bilinear layer coefficinets, size = n_lay x n_lay (e.g., 128x128)
//  Input: I     ... interpolation struct, see above
//         phi   ... wavefront, size = n_phi x n_phi (e.g., 85 x 85)
//		   n_phi ... number of wavefront grid points in one line (e.g., 85)
//         n_lay ... number of layer grid points in one line (e.g., 128)
void aao_interp2_tr_ac(float * lay, struct aao_Interp2 I, float const * phi, int n_phi, int n_lay);

#endif /* AAO_PROJ_OP_H_ */

/*
 * matr_crs_op.h
 *
 *  Created on: Jul 4, 2014
 *      Author: misha
 */

#ifndef AAO_MATR_CRS_OP_H_
#define AAO_MATR_CRS_OP_H_

// ============= operations used by P_SH_tt, P_SH_tt' ======
typedef struct aao_MatrixCRS_T{
	float * val;				// size = n_val
	int   * col_ind;			// size = n_val
	int   * row_ptr;			// size = n_row_ptr

	// do not really need these
	int n_rows;
	int n_cols;
	int nnz; // n_val
} aao_MatrixCRS_T;

// A = sparse(B);
void aao_initMatrixCRS(aao_MatrixCRS_T * A, float const * B, int n_rows, int n_cols);

// A = sparse(B);
// b_{ij} is considered "0" if abs(b_{ij}) <= tol
void aao_initMatrixCRS_tol(aao_MatrixCRS_T * A, float const * B, float tol, int n_rows, int n_cols);

// allocate space for matrix A
void aao_allocMatrixCRS(aao_MatrixCRS_T * A, int nnz, int n_rows, int n_cols);

// free dynamic memory of A
void aao_freeMatrixCRS(aao_MatrixCRS_T * A);


// return 0 on success and -1 on dim-mismatch
int aao_matrixCRS_vec_prod   (float * b, aao_MatrixCRS_T const * A, float const * x, int n_b, int n_x); // b = Ax
int aao_matrixCRS_vec_prod_ac(float * b, aao_MatrixCRS_T const * A, float const * x, int n_b, int n_x); // b+= Ax
int aao_matrixCRS_vec_prod_tr(float * b, aao_MatrixCRS_T const * A, float const * x, int n_b, int n_x); // b = A^Tx



#endif /* AAO_MATR_CRS_OP_H_ */

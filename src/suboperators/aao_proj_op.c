/*
 * proj_op.c
 *
 *  Created on: Feb 13, 2013
 *      Author: misha
 */

#include <stdlib.h>
#include <math.h>
#include "aao_proj_op.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846264338327950288   /* pi */
#endif



void aao_initInterp2_NULL(aao_Interp2 * I)
{
	I->ref_x_indx = NULL;
	I->ref_x_wght = NULL;
	I->ref_y_indx = NULL;
	I->ref_y_wght = NULL;
	I->tmp = NULL;
}
void aao_initInterp2(aao_Interp2 * I, float pos_x, float pos_y, float h, float H, int n_phi, float d_phi, int n_lay, float d_lay, float x0_lay)
{
	// local var
	int i;
	unsigned short j;
	int n_tmp;  // size of the temporary variable = n_phi*(I.ref_y_indx[n_phi0-1]+1 - I.ref_y_indx[0] + 1);
	double * phi_x = NULL;  // coordinates of the phi corner points (symmetric)
	double * lay_x = NULL;  // coordinates of the lay corner points (symmetric)
	double * prj_x = NULL;  // projection of phi_x onto layer, x coordinates
	double * prj_y = NULL;  // projection of phi_x onto layer, y coordinates
	double x0;
	double cone_fct;
	// 1 arcsec -> rad
	// 1sec = 1/60 minute, 1min = 1/60 deg
	// 1 deg = pi/180 rad
	double const ARCMIN_TO_RAD = M_PI / 180. / 60.;
	double theta_x, theta_y;

	// local var init
	phi_x  = (double*) calloc(n_phi,sizeof(double));
	lay_x  = (double*) calloc(n_lay,sizeof(double));
	prj_x  = (double*) calloc(n_phi,sizeof(double));
	prj_y  = (double*) calloc(n_phi,sizeof(double));


	// code

	// allocate output arrays
	I->ref_x_indx = (unsigned short*) calloc(n_phi,sizeof(unsigned short));
	I->ref_y_indx = (unsigned short*) calloc(n_phi,sizeof(unsigned short));
	I->ref_x_wght = (float*) calloc(n_phi,sizeof(float));
	I->ref_y_wght = (float*) calloc(n_phi,sizeof(float));

	// wavefront grid
	x0 = -(n_phi-1)*d_phi/2.; // left-most point e.g., -(85-1)*0.5/2 = -21, right-most point = 21
	for(i=0; i<n_phi; i++)
		phi_x[i] = x0 + d_phi*i;

	// layer grid
	// MY 10.08.2014 corner point coordinate is determined outside (at initFEWHA)
	x0 = x0_lay;
//	x0 = -n_lay*d_lay/2.;  // left-most point, e.g., -128*0.5/2 = -32, right-most point = 127*0.5/2 = 31.5
	for(i=0; i<n_lay; i++)
		lay_x[i] = x0 + d_lay*i;

	// projected wavefront grids
	if(H>0) // cone effect
		cone_fct = (1.-h/H);
	else
		cone_fct = 1.;

	// tan(angle) = O/A
	// O ... theta[i], A = 1 (unit altitude)
	// O = arctan(angle);
	theta_x = tan(pos_x*ARCMIN_TO_RAD);
	theta_y = tan(pos_y*ARCMIN_TO_RAD);

	for(i=0; i<n_phi; i++)
	{
		prj_x[i] = cone_fct*phi_x[i] + theta_x*h;
		prj_y[i] = cone_fct*phi_x[i] + theta_y*h;
	}

	// traverse the grids in search of x indices and weights
	for(i=0; i<n_phi; i++)
	{
		x0 = prj_x[i];
		// find the index j s.t. lay_x[j] <= x0 < lay_x[j+1]
		for(j=0; j<n_lay-1; j++) // SR: it should be enough, that, when this loop starts again, that j starts from the index, where the previous loop did break
		{
			if(lay_x[j] <= x0 && x0 < lay_x[j+1])
			{
				// interpolation in 1D: (x-a)/(b-a)*(f(b)-f(a))+f(a)
				I->ref_x_indx[i] = j;					// found the index!
				I->ref_x_wght[i] = (float) (x0-lay_x[j])/d_lay; // weight: (x-a)/(b-a)
				break; // go to outer loop
			}
		}
	}

	// traverse the grids in search of y indices and weights
	for(i=0; i<n_phi; i++)
	{
		x0 = prj_y[i];
		// find the index j s.t. lay_x[j] <= x0 < lay_x[j+1]
		for(j=0; j<n_lay-1; j++)
		{
			if(lay_x[j] <= x0 && x0 < lay_x[j+1])
			{
				// interpolation in 1D: (x-a)/(b-a)*(f(b)-f(a))+f(a)
				I->ref_y_indx[i] = j;					// found the index!
				I->ref_y_wght[i] = (float) (x0-lay_x[j])/d_lay; // weight: (x-a)/(b-a)
				break;	// go to outer loop
			}
		}
	}

	// allocate the temporary variable
	// size of the temporary variable
	n_tmp = n_phi*( (I->ref_y_indx[n_phi-1]+1) - I->ref_y_indx[0] + 1); // <= n_phi2
	I->tmp = (float*)calloc(n_tmp,sizeof(float)); // this is too much; can be less if LGS are used



	// clear local variables
	free(phi_x);
	free(lay_x);
	free(prj_x);
	free(prj_y);
	phi_x = NULL;
	lay_x = NULL;
	prj_x = NULL;
	prj_y = NULL;

}

void aao_freeInterp2(aao_Interp2 * I)
{
	if(I != NULL)
	{
		if(I->ref_x_indx)
		{
			free(I->ref_x_indx);
			I->ref_x_indx = NULL;
		}
		if(I->ref_x_wght)
		{
			free(I->ref_x_wght);
			I->ref_x_wght = NULL;
		}
		if(I->ref_y_indx)
		{
			free(I->ref_y_indx);
			I->ref_y_indx = NULL;
		}
		if(I->ref_y_wght)
		{
			free(I->ref_y_wght);
			I->ref_y_wght = NULL;
		}
		if(I->tmp)
		{
			free(I->tmp);
			I->tmp = NULL;
		}
	}
}

void aao_interp2_ac(float * phi, struct aao_Interp2 I, float const * lay, int n_phi, int n_lay)
{
	// Here: 1) interpolate w.r.t. x,
	// 		 2) interpolate w.r.t. y

	// 1D interpolation scheme:
	// g(x) = (x-a)/(b-a)*(f(b)-f(a))+f(a)
	// wght := (x-a)/(b-a)
	// indx := a


	// local variables
	int ix, iy;
	float fa, fb, ga, gb;
	float * tmp = NULL;
	int n_y_ect; // number of lay points w.r.t. y-coordinate where interpolation takes place
	int i_y_ect_first;
	int i_y_ect_last;

	int   indx0,indx1,indx2;
	float wght0;

	// init local variables
	i_y_ect_first = I.ref_y_indx[0];			// lower bound
	i_y_ect_last  = I.ref_y_indx[n_phi-1]+1;	// upper bound
	n_y_ect = i_y_ect_last - i_y_ect_first + 1;	// last lay point - first lay point where interpolation takes place

	tmp = I.tmp;

//	#pragma omp parallel private(ix,iy,fa,fb,ga,gb) num_threads(N_MAX_THR) if(0)
//	{
//		#pragma omp for
		for(ix=0; ix<n_phi; ix++)
		{
			indx1 = n_lay* I.ref_x_indx[ix];
			indx2 = n_lay*(I.ref_x_indx[ix]+1);
			indx0 = n_y_ect*ix-i_y_ect_first;
			wght0 = I.ref_x_wght[ix];
			for(iy=i_y_ect_first; iy<=i_y_ect_last; iy++)
			{
				//fa = lay[n_ect* I.ref_x_indx[ix]   +iy];
				//fb = lay[n_ect*(I.ref_x_indx[ix]+1)+iy];
				// index shift:
				fa = lay[indx1+iy]; // fa = ect0[n_ect*(I.ref_x_indx[ix]-1)+(iy-1)];
				fb = lay[indx2+iy]; // fb = ect0[n_ect*(I.ref_x_indx[ix]  )+(iy-1)];

				tmp[indx0+iy] = wght0*(fb-fa)+fa; // tmp[n_y_ect*ix+(iy-i_y_ect_first)] = I.ref_x_wght[ix]*(fb-fa)+fa;
			}
		}

//		#pragma omp for
		for(ix=0; ix<n_phi; ix++)
		{
		//	indx1 = 0+I.ref_y_indx[iy]-i_y_ect_first;
		//	indx2 = 0+I.ref_y_indx[iy]-i_y_ect_first+1;
		//	wght0 = I.ref_y_wght[iy];
			for(iy=0; iy<n_phi; iy++)
			{
				ga = tmp[n_y_ect*ix+I.ref_y_indx[iy]-i_y_ect_first]; // ga = tmp[n_y_ect*ix+indx1];
				gb = tmp[n_y_ect*ix+I.ref_y_indx[iy]-i_y_ect_first+1]; // gb = tmp[n_y_ect*ix+indx2];

				// WARNING: accumulation
				phi[n_phi*ix+iy] += I.ref_y_wght[iy]*(gb-ga)+ga; // phi0[n_phi0*ix+iy] += wght0*(gb-ga)+ga;
				// should be transposed:
				//phi0[n_phi0*iy+ix] += I.ref_y_wght[iy]*(gb-ga)+ga;
			}
		}
//	}

//	free(tmp);


	/*
	% Generate lits (offline)
	% For whatever reason x is y and y is x... :-)
	tmp   = ect_x;
	ect_x = ect_y;
	ect_y = tmp;

	ref_x_indx = zeros(n_ect,1); % reference table: index of left- act_x point where interpolation takes place (the 'a')
	ref_x_wght = zeros(n_ect,1); % reference table: weight need for interpolation (the '(x-a)/(b-a)')
	ref_y_indx = zeros(n_ect,1);
	ref_y_wght = zeros(n_ect,1);

	for ix=1:n_ect
		x = ect_x(ix);
		ref_x_indx(ix) = find(act_x <= x,1,'last');
		a = act_x(ref_x_indx(ix));
		b = act_x(ref_x_indx(ix)+1);
		ref_x_wght(ix) = (x-a)/(b-a);
	end

	for iy=1:n_ect
		y = ect_y(iy);
		ref_y_indx(iy) = find(act_x <= y,1,'last');
		a = act_x(ref_y_indx(iy));
		b = act_x(ref_y_indx(iy)+1);
		ref_y_wght(iy) = (y-a)/(b-a);
	end


	% Interpolate (online)
	n_act = (ref_y_indx(end)+1)-(ref_y_indx(1))+1;
	ect    = zeros(n_ect, n_ect); %(x,y)-interpolants
	intp_x = zeros(n_ect, n_act); % x-interpolants

	for ix=1:n_ect
		for iy=ref_y_indx(1):ref_y_indx(end)+1
			fa = act(ref_x_indx(ix),  iy);
			fb = act(ref_x_indx(ix)+1,iy);
			intp_x(ix,iy-ref_y_indx(1)+1) = ref_x_wght(ix)*(fb-fa)+fa;
		end
	end


	for ix=1:n_ect
		for iy=1:n_ect
			ga = intp_x(ix, ref_y_indx(iy)-ref_y_indx(1)+1);
			gb = intp_x(ix, ref_y_indx(iy)-ref_y_indx(1)+2);
			ect(ix,iy) = ref_y_wght(iy)*(gb-ga)+ga;

		end
	end

	*/

}
void aao_interp2_tr(float * lay, struct aao_Interp2 I, float const * phi, int n_phi, int n_lay)
{
	// local variables
	int ix;
	int n_lay2; // lay^2 and size of I.tmp

	// local var init
	n_lay2 = n_lay*n_lay;

	// code
	for(ix=0; ix<n_lay2; ix++)
		lay[ix] = 0.;

	// call lay += Interp2(phi)
	aao_interp2_tr_ac(lay, I, phi, n_phi, n_lay);
}

void aao_interp2_tr_ac(float * lay, struct aao_Interp2 I, float const * phi, int n_phi, int n_lay)
{
	// local variables
	int ix, iy;
	float fa;
	float * tmp = NULL;
	int n_y_ect; // number of ect points w.r.t. y-coordinate where interpolation takes place
	int i_y_ect_first;
	int i_y_ect_last;
	int n_tmp;

	int   indx0,indx1,indx2;
	float wght1,wght2;

	// init local variables
	i_y_ect_first = I.ref_y_indx[0];			// lower bound
	i_y_ect_last  = I.ref_y_indx[n_phi-1]+1;	// upper bound
	n_y_ect = i_y_ect_last - i_y_ect_first + 1;	// last ect point - first ect point where interpolation takes place
	n_tmp = n_phi*n_y_ect;

	tmp = I.tmp;

	// code
	// in the accumulation operation, lay is not initialized with zero
//	for(ix=0; ix<n_lay2; ix++)
//		lay[ix] = 0.;
	for(ix=0; ix<n_tmp; ix++)
		tmp[ix] = 0.;

	for(iy=0; iy<n_phi; iy++)
	{
		indx1 = (I.ref_y_indx[iy]-i_y_ect_first);
		indx2 = (I.ref_y_indx[iy]-i_y_ect_first+1);
		wght1 = (1.-I.ref_y_wght[iy]);
		wght2 =     I.ref_y_wght[iy];
		for(ix=0; ix<n_phi; ix++)
		{
			fa = phi[ix*n_phi+iy];
			// should be transposed:<--- NO!
			//fa = phi[iy*n_phi0+ix];
			indx0 = ix*n_y_ect;
			tmp[indx0+indx1] += wght1*fa; // tmp[ix*n_y_ect+(I.ref_y_indx[iy]-i_y_ect_first)  ] += (1-I.ref_y_wght[iy])*fa;
			tmp[indx0+indx2] += wght2*fa; // tmp[ix*n_y_ect+(I.ref_y_indx[iy]-i_y_ect_first+1)] +=    I.ref_y_wght[iy] *fa;
		}
	}

	for(ix=0; ix<n_phi; ix++)
	{
		indx0 = ix*n_y_ect+(0-i_y_ect_first);
		indx1 =  I.ref_x_indx[ix]   *n_lay+0;
		indx2 = (I.ref_x_indx[ix]+1)*n_lay+0;
		wght1 = (1-I.ref_x_wght[ix]);
		wght2 =    I.ref_x_wght[ix];
		for(iy=i_y_ect_first; iy<=i_y_ect_last; iy++)
		{
// MY 03.07.2014: updated indices
			fa = tmp[indx0+iy]; // fa = tmp[ix*n_y_ect+(iy-i_y_ect_first)];
			//ect[ I.ref_x_indx[ix]   *n_ect+iy] += (1-I.ref_x_wght[ix])*fa;
			//ect[(I.ref_x_indx[ix]+1)*n_ect+iy] +=    I.ref_x_wght[ix] *fa;
			// index shift:
// MY 03.07.2014: updated indices
			lay[indx1+iy] += wght1*fa; // lay[ I.ref_x_indx[ix]   *n_lay+iy] += wght1*fa;
			lay[indx2+iy] += wght2*fa; // lay[(I.ref_x_indx[ix]+1)*n_lay+iy] += wght2*fa
//			lay[indx1+iy] += wght1*fa; // ect[(I.ref_x_indx[ix]-1)*n_ect+(iy-1)] += (1-I.ref_x_wght[ix])*fa;
//			lay[indx2+iy] += wght2*fa; // ect[(I.ref_x_indx[ix]  )*n_ect+(iy-1)] +=    I.ref_x_wght[ix] *fa;
			// should be transposed:<--- NO!
			//ect[(iy-1)*n_ect+I.ref_x_indx[ix]-1] += (1-I.ref_x_wght[ix])*fa;
			//ect[(iy-1)*n_ect+I.ref_x_indx[ix]]   +=    I.ref_x_wght[ix] *fa;
		}
	}
	// TODO MY 27.06.2014 !!! Make a write-up for this!!!!



/*  % Generate lits (offline)
	-- like in interp2_ac(...)


	% Interpolate (online)
	ect = zeros(n_ect, n_ect);
	n_tmp = (ref_y_indx(end)+1)-(ref_y_indx(1))+1;
	tmp = zeros(n_phi, n_tmp);

	for ix=1:n_ect
		for iy=1:n_ect
			ect(ix,iy) = 0;
		end
	end
	for ix=1:n_phi
		for iy=1:n_tmp
			tmp(ix,iy) = 0;
		end
	end

	for ix=1:n_phi
		for iy=1:n_phi
			fa = phi(ix,iy);
			tmp(ix,ref_y_indx(iy)-ref_y_indx(1)+1) = tmp(ix,ref_y_indx(iy)-ref_y_indx(1)+1) + (1-ref_y_wght(iy))*fa;
			tmp(ix,ref_y_indx(iy)-ref_y_indx(1)+2) = tmp(ix,ref_y_indx(iy)-ref_y_indx(1)+2) +    ref_y_wght(iy) *fa;
		end
	end

	for ix=1:n_phi
		for iy=1:n_tmp
			fa = tmp(ix,iy);
			ect(ref_x_indx(ix),  iy+ref_y_indx(1)-1) = ect(ref_x_indx(ix),  iy+ref_y_indx(1)-1) + (1-ref_x_wght(ix))*fa;
			ect(ref_x_indx(ix)+1,iy+ref_y_indx(1)-1) = ect(ref_x_indx(ix)+1,iy+ref_y_indx(1)-1) +    ref_x_wght(ix) *fa;
		end
	end
 */
}


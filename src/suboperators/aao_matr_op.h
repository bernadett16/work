/*
 * aao_matr_op.h
 *
 *  Created on: Oct 10, 2014
 *      Author: misha
 */

#ifndef AAO_MATR_OP_H_
#define AAO_MATR_OP_H_

// cholesky decomposition A=LDL^T
// A must be symmetric!
// A ... size = nxn
// L ... size = nxn
// D ... size = n (storing only the diagonal)
void chol_decomp(float * L, float * D, float const * A, int n);

// cholesky solver
// solving Ax = LDL^Tx = b
// L ... size nxn
// D ... size n (only the diagonal)
// b ... nx1
// x ... nx1
void chol_solve(float * x, float const * L, float const * D, float const * b, int n);



#endif /* AAO_MATR_OP_H_ */

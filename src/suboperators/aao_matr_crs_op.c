/*
 * matr_crs_op.c
 *
 *  Created on: Jul 4, 2014
 *      Author: misha
 */

#include <stdlib.h>
#include <math.h>
#include "aao_matr_crs_op.h"


// A = sparse(B);
void aao_initMatrixCRS(aao_MatrixCRS_T * A, float const * B, int n_rows, int n_cols)
{
	float tol = 0.;
	aao_initMatrixCRS_tol(A, B, tol, n_rows, n_cols);
}

// A = sparse(B);
// b_{ij} is considered "0" if abs(b_{ij}) <= tol
void aao_initMatrixCRS_tol(aao_MatrixCRS_T * A, float const * B, float tol, int n_rows, int n_cols)
{
	int nnz;
	int i,j,k;
	float b_ij;

	// Count the number of non-zero elements
	nnz = 0;
	for(i=0; i<n_rows; i++)
		for(j=0; j<n_cols; j++)
			if(fabsf(B[i*n_cols+j]) > tol)
				nnz++;

	// Allocate space for matrix A
	aao_allocMatrixCRS(A, nnz, n_rows, n_cols);

	// Assign A: val, col_ind, row_ptr
	k=0;
	for(i=0; i<n_rows; i++)
	{
		A->row_ptr[i] = k;
		for(j=0; j<n_cols; j++)
		{
			b_ij = B[i*n_cols+j];
			if(fabsf(b_ij) > tol)
			{
				A->val[k] = b_ij;
				A->col_ind[k] = j;
				k++;
			}
		}
	}
	A->row_ptr[n_rows] = nnz;
}

// allocate space for matrix A
void aao_allocMatrixCRS(aao_MatrixCRS_T * A, int nnz, int n_rows, int n_cols)
{
	int n_row_ptr = n_rows+1;

	A->val     = (float*) calloc(nnz, sizeof(float));
	A->col_ind = (int*)   calloc(nnz, sizeof(int));
	A->row_ptr = (int*)   calloc(n_row_ptr, sizeof(int));

	A->n_rows  = n_rows;
	A->n_cols  = n_cols;
	A->nnz     = nnz;
}
void aao_freeMatrixCRS(aao_MatrixCRS_T * A)
{
	if(A != NULL)
	{
		if(A->val != NULL)
		{
			free(A->val);
			A->val = NULL;
		}
		if(A->col_ind != NULL)
		{
			free(A->col_ind);
			A->col_ind = NULL;
		}
		if(A->row_ptr != NULL)
		{
			free(A->row_ptr);
			A->row_ptr = NULL;
		}
	}
}

// b = Ax
int aao_matrixCRS_vec_prod(float * b, aao_MatrixCRS_T const * A, float const * x, int n_b, int n_x)
{
	// size A = n_rows x n_cols
	// size x = n_cols
	// size b = n_rows
	int i,j;
	if(n_b != A->n_rows)
		return -1;
	if(n_x != A->n_cols)
		return -1;

	for(i = 0; i < A->n_rows; i++)
	{
		b[i] = 0.;
		for(j = A->row_ptr[i]; j < A->row_ptr[i+1]; j++)
		{
			b[i] += A->val[j] * x[A->col_ind[j]];
		}
	}

	return 0;
}
// b+= Ax
int aao_matrixCRS_vec_prod_ac(float * b, aao_MatrixCRS_T const * A, float const * x, int n_b, int n_x)
{
	// size A = n_rows x n_cols
	// size x = n_cols
	// size b = n_rows
	int i,j;

	if(n_b != A->n_rows)
		return -1;
	if(n_x != A->n_cols)
		return -1;

	for(i = 0; i < A->n_rows; i++)
	{
		for(j = A->row_ptr[i]; j < A->row_ptr[i+1]; j++)
		{
			b[i] += A->val[j] * x[A->col_ind[j]];
		}
	}

	return 0;
}
// b = A^Tx
int aao_matrixCRS_vec_prod_tr(float * b, aao_MatrixCRS_T const * A, float const * x, int n_b, int n_x)
{
	// size A  = n_rows x n_cols
	// size A' = n_cols x n_rows
	// size x  = n_rows
	// size b  = n_cols
	int i,j;
	if(n_b != A->n_cols)
		return -1;
	if(n_x != A->n_rows)
		return -1;

	for(i=0; i < A->n_cols; i++)
		b[i] = 0.;

	for(i=0; i < A->n_rows; i++)
	{
		for(j=A->row_ptr[i]; j < A->row_ptr[i+1]; j++)
		{
			b[A->col_ind[j]] += A->val[j] * x[i];
		}
	}

	return 0;
}

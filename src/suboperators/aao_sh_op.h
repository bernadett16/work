/*
 * sh_op.h
 *
 *  Operations related to the Shack-Hartmann operator
 *
 *  Created on: June 16, 2014
 *      Author: Misha Yudytskiy
 *       Notes:
 *
 *  (C) 2014 Austrian Adaptive Optics Team
 *      http://eso-ao.indmath.uni-linz.ac.at/
 */

#ifndef AAO_SH_OP_H_
#define AAO_SH_OP_H_


/* similar to aao_sh_tr, but uses "prolongation" operator for tip tilt data s, i.e. Sx is equal on each virtual subaperture */
void aao_sh_tt_tr(float * phi, float const * s, int n_sub_tt, int n_sub_virtual, unsigned char const *i_sub_virtual);

// Shack-Hartmann tip tilt operator for one sensor:
// computes the mean of average derivatives
// mean s^x=1/(n_sub^2)*sum(s_{ij}^x,i,j)
// Notes: uses computation of aao_sh
// i_sub_virtual is a virtual mask for the virtual number of subapertures n_sub_virtual
void aao_sh_tt(float * s, float const * phi, int n_sub_tt, int n_sub_virtual, unsigned char const *i_sub_virtual, float * tmp);

// Shack-Hartmann operator for one sensor:
// computes the average derivatives
// s_{ij}^x = [ (phi_{i+1,j+1} - phi_{i,j+1}) + (phi_{i+1,j} - phi_{i,j}) ]/2
// s_{ij}^y = [ (phi_{i+1,j+1} - phi_{i+1,j}) + (phi_{i,j+1} - phi_{i,j}) ]/2
// Notes: uses temporary variable to store (phi_{i+1,j} - phi_{i,j}) for s^x computation,
//        the temporary variable is reused to store (phi_{i,j+1} - phi_{i,j}) for s^y
//     Input: phi   ... wavefront, size = n_phi x n_phi (e.g., 85 x 85)
//            n_sub ... number of subapertures in one line (e.g., 84)
//    Output: s     ... SH measurements, size = 2 x n_sub x n_sub (e.g., 2 x 84 x 84)
//                      s = [Sx; Sy] size of Sx, Sy = n_sub x n_sub
// Temporary: tmp   ... Temporary memory size = n_phi x n_sub (e.g., 85 x 84)
void aao_sh(float * s, float const * phi, int n_sub, float * tmp);

// Transposed Shack-Hartmann operator for one sensor:
// phi_{ij} = [ -s^x_{i,j} + s^x_{i-1,j} - s^x_{i,j-1} + s^x_{i-1,j-1}
//				-s^y_{i,j} - s^y_{i-1,j} + s^y_{i,j-1} + s^y_{i-1,j-1}} ] /2
// Notes: none
//     Input: s     ... SH measurements, size = 2 x n_sub x n_sub (e.g., 2 x 84 x 84)
//       			    s = [Sx; Sy] size of Sx, Sy = n_sub x n_sub
//            n_sub ... number of subapertures in one line (e.g., 84)
//    Output: phi   ... wavefront, size = n_phi x n_phi (e.g., 85 x 85)
void aao_sh_tr(float * phi, float const * s, int n_sub);

// Apply the mask:
// s = [Sx Sy] = [Sx Sy] .* [I_sub, I_sub]
// Notes: sets those s^x_{ij}, s^y_{ij} that are outside of the mask.
// 		  The mask is given by a boolean matrix of size n_sub x n_sub.  No symmetry is assumed
//    In/Out: s     ... SH measurements, size = 2 x n_sub x n_sub (e.g., 2 x 84 x 84)
//       			    s = [Sx; Sy] size of Sx, Sy = n_sub x n_sub
//     Input: I_sub ... mask matrix of true/false values, size = n_sub x n_sub (e.g., 84 x 84)
//			  n_sub ... number of subapertures in one line (e.g., 84)
void aao_mask(float * s, unsigned char const * I_sub, int n_sub);

// Remove the "Gradient" tilt
// s = (I-T)s
// Notes: computes, then subtracts mean(Sx) mean(Sy).
//        !!! Input s MUST be "masked" !!! Output s is unmasked !!!
//    In/Out: s     ... SH measurements, size = 2 x n_sub x n_sub (e.g., 2 x 84 x 84)
//       			    s = [Sx; Sy] size of Sx, Sy = n_sub x n_sub
//     Input: n_sub ... number of subapertures in one line (e.g., 84)
//    n_active_sub2 ... number of active subapertures (e.g., (1-0.28)*84^2 = 5080 <= 84^2)
void aao_removeTT(float * s, int n_sub, int n_active_sub2);

// Number of active subapertures
// n_active_sub2 = nnz(I_sub)
//     Input: mask  ... mask matrix of true/false values, size = n_sub x n_sub (e.g., 84 x 84)
//			  n_sub ... number of subapertures in one line (e.g., 84)
//    Output:
//    n_active_sub2 ... number of active subapertures (e.g., (1-0.28)*84^2 = 5080 <= 84^2)
int aao_numActSubap(unsigned char const * I_sub, int n_sub);

// Apply NGS inv noise covariance
// s = C^{-1}_{\eta}*s = 1/sigma^2 * s
//    In/Out: s     ... SH measurements, size = 2 x n_sub x n_sub (e.g., 2 x 84 x 84)
//       			    s = [Sx; Sy] size of Sx, Sy = n_sub x n_sub
//     Input: sigma2... sigma^2, noise variance, e.g., 1/nPhot, where nPhot = 100
//			  n_sub ... number of subapertures in one line (e.g., 84)
void aao_invC_NGS(float * s, float sigma2, int n_sub);

// Apply LGS inv noise covariance
// s = C^{-1}_{\eta}*s
//    In/Out: s     ... SH measurements, size = 2 x n_sub x n_sub (e.g., 2 x 84 x 84)
//       			    s = [Sx; Sy] size of Sx, Sy = n_sub x n_sub
//     Input: invCxx... x-to-x part of the C^{-1}_{\eta}
//			  invCxy... x-to-y (or y-to-x, symmetry!) part of the C^{-1}_{\eta}
//			  invCyy... y-to-y part of the C^{-1}_{\eta}
//						size of invCxx, invCxy, invCyy = n_sub x n_sub (e.g., 84 x 84)
//			  n_sub ... number of subapertures in one line (e.g., 84)
void aao_invC_LGS(float * s, float const * invCxx, float const * invCxy, float const * invCyy, int n_sub);

// Compute/Assemble LGS inv noise covariance
//	   Input: n_sub ... number of subapertures in one line (e.g., 84)
// 			  I_sub ... mask matrix of true/false values, size = n_sub x n_sub (e.g., 84 x 84)
//           sigma2 ... sigma^2, noise variance, e.g., 1/nPhot, where nPhot = 100
//       subap_size ... subaperture size, e.g., 0.5 meters
//        spot_fwhm ... spotsize fudge of the non-elongated spot in arsec, e.g., 1.1 arcsec
//        na_height ... sodium layer height, e.g., 90000 meters
//          na_fwhm ... sodium layer full width at half maximum, e.g., 11000 meters
//gs_llt_x,gs_llt_y ... laser launch position, x and y, e.g., -16.16, 16.16
//        alpha_eta ... spot elongation tuning parameter, between 0 (NGS model) and 1 (full LGS model), e.g., 0.4
//    Output: invCxx... x-to-x part of the C^{-1}_{\eta}
//			  invCxy... x-to-y (or y-to-x, symmetry!) part of the C^{-1}_{\eta}
//			  invCyy... y-to-y part of the C^{-1}_{\eta}
//						size of invCxx, invCxy, invCyy = n_sub x n_sub (e.g., 84 x 84)
void aao_init_invC_LGS(float * invCxx, float * invCxy, float * invCyy, int n_sub,
		unsigned char const * I_sub,
		float inv_sigma2, float subap_size, float spot_fwhm,
		float na_height, float na_fwhm, float gs_llt_x, float gs_llt_y,
		float alpha_eta);


#endif /* AAO_SH_OP_H_ */

/*
 * wav_op.h
 *
 *	Operations related to the Discrete Wavelet Transform (DWT)
 *	Wavelets: db3, periodic implementation on the boundary
 *
 *  Created on: June 20, 2014
 *      Author: Misha Yudytskiy
 *       Notes: see below
 *
 *  (C) 2014 Austrian Adaptive Optics Team
 *      http://eso-ao.indmath.uni-linz.ac.at/
 */

#ifndef AAO_WAV_OP_H_
#define AAO_WAV_OP_H_

// 2-dimensional direct wavelet transform
// In/Out: V     ... approx coeff -> coarser approx + detail coeffs
//                   size = n x n (e.g., 128 x 128)
//  Input: n     ... matrix size, one dimension (e.g., 128)
//					 !!! n = 2^p !!!
//   Temp: tmp_U ... temporary memory used for transform
//					 size = n x n (e.g., 128 x 128)
void aao_wavedec2_db3		(float * V, int n, float * tmp_U);

// parallel implementation of wavedec2_db3 (see above)
// Params: see above
//  Input: n_threads ... number of threads (e.g., 1 or 4)
//         n_dwt_pa ... grid size for parllel transform (smaller grids carried out on 1 thread)
void aao_wavedec2_db3_pa	(float * V, int n, float * tmp_U, int n_threads, int n_dwt_pa);

// 2-dimensional inverse wavelet transform
// In/Out: V     ... coarser approx + detail coeffs -> approx coeff
//                   size = n x n (e.g., 128 x 128)
//  Input: n     ... matrix size, one dimension (e.g., 128)
//					 !!! n = 2^p !!!
//   Temp: tmp_U ... temporary memory used for transform
//					 size = n x n (e.g., 128 x 128)
void aao_waverec2_db3		(float * V, int n, float * tmp_U);

// parallel implementation of waverec2_db3 (see above)
// Params: see above
//  Input: n_threads ... number of threads (e.g., 1 or 4)
//         n_dwt_pa ... grid size for parllel transform (smaller grids carried out on 1 thread)
void aao_waverec2_db3_pa	(float * V, int n, float * tmp_U, int n_threads, int n_dwt_pa);

// 2-dimensional inverse wavelet transform
// with the assumption that V is non-zero only on n_cs x n_cs grid
// useful for GLMS, with a special structure of the wavelet coefficient vector
// In/Out: V     ... coarser approx + detail coeffs -> approx coeff
//                   size = n x n (e.g., 128 x 128)
//  Input: n     ... matrix size, one dimension (e.g., 128)
//					 !!! n = 2^p !!!
//         n_cs  ... coarse scale grid size (non-zero elements), one dimension
//					 (e.g., 16 corresponds to 4 coarse scales, vector of 16^2 = 256 elements)
//   Temp: tmp_U ... temporary memory used for transform
//					 size = n x n (e.g., 128 x 128)
void aao_waverec2_db3_coarse_scales(float * V, int n, float * tmp_U, int n_cs);

// multiplication of the wavelet coefficients with weights (operation U *= d)
// function uses the special structure of wavelet coefficient vector
// In/Out: V     ... wavelet coefficients, size = n x n = 2^J x 2^J (e.g., 128x128)
//  Input: d     ... vector of weights, size = log2(n) (e.g., J=7), i.e.,
//                   d[0] ~ approx coeff, detail coeff, scale j=0 (coarsest)
// 					 d[1] ~ details coeff, scale j=1
//                   d[J-1] ~ detail coeff, scale j=J-1 (finest)
//         n     ... matrix size, one dimension (e.g., 128)
void aao_wavemult(float * U, float const * d, int n);

// multiplication of the wavelet coefficients with weights, with accumulation (operation U += d*V)
// function uses the special structure of wavelet coefficient vector
// In/Out: U     ... wavelet coefficients, size = n x n = 2^J x 2^J (e.g., 128x128)
//  Input: V     ... wavelet coefficients, size = n x n = 2^J x 2^J (e.g., 128x128)
// Params: d     ... vector of weights, size = log2(n) (e.g., J=7)
//     alpha     ... "regularization" parameter
//         n     ... matrix size, one dimension (e.g., 128)
void aao_wavemult_ac(float * U, float alpha, float const * d, float const * V, int n);

// multiplication of the wavelet coefficients with weights, with accumulation (operation U += d*V)
// with the assumption that V is non-zero only on n_cs x n_cs grid
// useful for GLMS, with a special structure of the wavelet coefficient vector
// function uses the special structure of wavelet coefficient vector
// In/Out: U     ... wavelet coefficients, size = n x n = 2^J x 2^J (e.g., 128x128)
//  Input: V     ... wavelet coefficients, size = n x n = 2^J x 2^J (e.g., 128x128)
// Params: d     ... vector of weights, size = log2(n) (e.g., J=7)
//     alpha     ... "regularization" parameter
//         n     ... matrix size, one dimension (e.g., 128)
//      n_cs     ... coarse scale grid size (non-zero elements), one dimension
//					 (e.g., 16 corresponds to 4 coarse scales, vector of 16^2 = 256 elements)
void aao_wavemult_ac_coarse_scales(float * U, float alpha, float const * d, float const * V, int n, int n_cs);

// operation: b = b + v = b + A*x
// accumulates to b; also returns the temporary result v=A*x
// function uses the special structure of wavelet coefficient vector
// In/Out: b     ... long vector, size = n_sqrt^2 x 1 (actually n0_sqrt x n0_sqrt restricted to n_sqrt x n_sqrt)
// Output: v     ... short vector, size = n_sqrt^2 x 1 (actually n_sqrt  x n_sqrt)
//  Input: A 	 ... full short matrix, size = n_sqrt^2 x n_sqrt^2
//		   x     ... long vector, size = n_sqrt^2 x 1 (actually n0_sqrt x n0_sqrt restricted to n_sqrt x n_sqrt)
void aao_matrix_wav_vec_prod_ac(float * b, float * v, float const * A, float const * x, int n_sqrt, int n0_sqrt);


#endif /* AAO_WAV_OP_H_ */

/*
 * Note: wavelets have the following structure in memory.
 * Let 0,1,2 be scales. Then, C is a matrix:
 * 	00112222
 * 	00112222
 * 	11112222
 * 	11112222
 * 	22222222
 * 	22222222
 * 	22222222
 * 	22222222
 *
 * 	I'm using this numbering scheme
 * 	[ 1  2  5  7]
 * 	[ 3  4  6  8]
 * 	[ 9 11 13 15]
 * 	[10 12 14 16]
 * 	  for a vector
 * 	[1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16]^T
 * 	  e.g.,: f_wav_vec_mcao.wav_vec_to_mat((1:16)')
 */

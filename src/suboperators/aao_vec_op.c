/*
 * vec_op.c
 *
 *  Created on: Feb 14, 2013
 *      Author: misha
 */

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include "aao_vec_op.h"



// b *= alpha, size = n
void aao_vec_ti_const_ac(float * b, float alpha, int n)
{
	int i;

	if (alpha != 1.)
	{
		for (i=0; i<n; i++)
			b[i] *= alpha;
	}
}
// c  = alpha*b, size = n
void aao_vec_ti_const(float * c, float alpha, float const * b, int n)
{
	int i;

	if (alpha == 1.)
	{
		for (i=0; i<n; i++)
			c[i] = b[i];
	}
	else // alpha != 1.
	{
		for (i=0; i<n; i++)
			c[i] = alpha*b[i];
	}
}


// a = R(a0), R ... restrict, size a0 = n0xn0, size a = nxn
void aao_vec_rest_sqr(float * a,  float const * a0, int n_sqrt, int n0_sqrt)
{
	// size a0 = n0_sqrt x n0_sqrt
	// size a  = n_sqrt x n_sqrt

	// local variables
	int ix, iy;

	// code
	for(ix=0; ix<n_sqrt; ix++)
	{
		for(iy=0; iy<n_sqrt; iy++)
			a[ix*n_sqrt+iy] = a0[ix*n0_sqrt+iy];
	}
}
// a0 = P(a), P ... pad with zeros
void aao_vec_prol_sqr(float * a0, float const * a, int n_sqrt, int n0_sqrt)
{
	// size a0 = n0_sqrt x n0_sqrt
	// size a  = n_sqrt x n_sqrt

	// local variables
	int ix, iy;

	// code
	for(ix=0; ix<n_sqrt; ix++)
	{
		for(iy=0; iy<n_sqrt; iy++)
			a0[ix*n0_sqrt+iy] = a[ix*n_sqrt+iy];
		for(iy=n_sqrt; iy<n0_sqrt; iy++)
			a0[ix*n0_sqrt+iy] = 0.;
	}
	for(ix=n_sqrt*n0_sqrt; ix<n0_sqrt*n0_sqrt; ix++)
		a0[ix]=0.;

}
// a0+= P(a)
void aao_vec_prol_sqr_ac(float * a0, float const * a,  int n_sqrt, int n0_sqrt)
{
	// size a0 = n0_sqrt x n0_sqrt
	// size a  = n_sqrt x n_sqrt

	// local variables
	int ix, iy;

	// code
	for(ix=0; ix<n_sqrt; ix++)
	{
		for(iy=0; iy<n_sqrt; iy++)
			a0[ix*n0_sqrt+iy] += a[ix*n_sqrt+iy];
	}
}
// a0 = alpha*P(a)  size a0 = n0xn0, size a = nxn
void aao_vec_ti_const_prol_sqr(float * a0, float alpha, float const * a, int n_sqrt, int n0_sqrt)
{
	// size a0 = n0_sqrt x n0_sqrt
	// size a  = n_sqrt x n_sqrt

	// local variables
	int ix, iy;

	// code
	for(ix=0; ix<n_sqrt; ix++)
	{
		for(iy=0; iy<n_sqrt; iy++)
			a0[ix*n0_sqrt+iy] = alpha*a[ix*n_sqrt+iy];
		for(iy=n_sqrt; iy<n0_sqrt; iy++)
			a0[ix*n0_sqrt+iy] = 0.;
	}
	for(ix=n_sqrt*n0_sqrt; ix<n0_sqrt*n0_sqrt; ix++)
		a0[ix]=0.;

}
// a0+= alpha*P(a)  size a0 = n0xn0, size a = nxn
void aao_vec_ti_const_prol_sqr_ac(float * a0, float alpha, float const * a, int n_sqrt, int n0_sqrt)
{
	// size a0 = n0_sqrt x n0_sqrt
	// size a  = n_sqrt x n_sqrt

	// local variables
	int ix, iy;

	// code
	for(ix=0; ix<n_sqrt; ix++)
	{
		for(iy=0; iy<n_sqrt; iy++)
			a0[ix*n0_sqrt+iy] += alpha * a[ix*n_sqrt+iy];
	}
}
// a += b, size = n
void  aao_vec_pl_vec_ac(float * a, float const * b, int n)
{
	int i;

	for (i=0; i<n; i++)
		a[i] += b[i];
}

/*
 * Diagonal matrix - vector - multiplication:
 * a = D * b
 * input:  D, size = n (just the vector corresponding to the diagonal)
 *		   b, size = n (vector)
 * output: a, size = n (vector)
 */
void aao_diag_vec_prod(float * a, float const * D, float const * b, int n)
{
	int i;

	for (i=0; i<n; i++)
		a[i] = D[i] * b[i];
}
// a'*b, size = n
float aao_vec_scal_prod(float const * a, float const * b, int n)
{
	int i;
	float ret = 0.;

	for (i=0; i<n; i++)
		ret += a[i]*b[i];

	return ret;
}
// sqrt(a'*a), size = n
float aao_vec_norm(float const * a, int n)
{
	float ret = 0.;

	ret = aao_vec_scal_prod(a, a, n);
	ret = sqrtf(ret);

	return ret;
}
// a = b, size = n
void aao_vec_copy(float * a, float const * b, int n)
{
	int i;

	for (i=0; i<n; i++)
		a[i] = b[i];
}
// a = b + alpha * a, size = n
void aao_vec_pl_sc_vec_type2(float * a, float alpha, float const * b, int n)
{
	int i;

	for (i=0; i<n; i++)
		a[i] = b[i] + alpha * a[i];
}
// a = a + alpha * b, size = n
void aao_vec_pl_sc_vec_ac(float * a, float alpha, float const * b, int n)
{
	int i;

	for (i=0; i<n; i++)
		a[i] += alpha * b[i];
}
// a = a - alpha * b, size = n
void aao_vec_mi_sc_vec_ac(float * a, float alpha, float const * b, int n)
{
	int i;

	for (i=0; i<n; i++)
		a[i] -= alpha * b[i];
}



// a -= b, size = n
void aao_vec_mi_vec_ac(float * a, float const * b, int n)
{
	int i;

	for (i=0; i<n; i++)
		a[i] -= b[i];
}

int read_vec_from_file_cast_uint8(char const *filename, unsigned char *x, int n) {
	// local var
	FILE *file = NULL;
	int i;
	float r;
	char line[256] = "";

	// code
	file = fopen(filename, "r");
	if (file == NULL) {
		printf("File %s could not be opened\n", filename);
		return 1;
	}

	for (i = 0; i < n; i++) {
		if (fgets(line, 256, file) == NULL) {
			printf("File %s encountered EOF on i = %d from %d lines\n", filename, i, n);
			return 2;
		}
		sscanf(line, "%f", &r);
		x[i] = (unsigned char) r;
	}


	fclose(file);

	printf("File %s read OK\n", filename);
	return 0;
}




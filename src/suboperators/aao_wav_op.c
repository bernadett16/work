/*
 * wav_op.c
 *
 *  Created on: June 20, 2014
 *      Author: Misha Yudytskiy
 *       Notes:
 *
 *  (C) 2014 Austrian Adaptive Optics Team
 *      http://eso-ao.indmath.uni-linz.ac.at/
 */

#include <stdlib.h>
#include <omp.h>
#include "aao_FEWHA_parallel.h"
#include "aao_wav_op.h"

/* =========== Local Functions =========== */

// 2-dimensional direct wavelet transform on *one* scale
// In/Out: V     ... approx coeff -> coarser approx + detail coeffs
//                   size = os x os, only sub-matrix n x n is transformed
//  Input: n     ... sub-matrix size, one dimension (e.g., 32)
//         os    ... full-matrix size, one dimension (e.g., 128)
//   Temp: tmp_U ... temporary memory used for transform
//					 size = n x n (e.g., 32x32), (may be larger!)
void aao_wavedec2_db3_1sc (float * V, int n, int os, float * tmp_U);

// 1-dimensional direct wavelet transform on *one* scale
//  Input: v     ... approx coeff (1 dim) size = n
//         n     ... sub-matrix size, one dimension (e.g., 32)
// Output: u     ... coarser approx + detail coeffs (1 dim)
//					 size = n
void aao_wavedec_db3_1sc (float const * v, float * u, int n, int os);

// 2-dimensional inverse wavelet transform on *one* scale
// In/Out: V     ... approx coeff -> coarser approx + detail coeffs
//                   size = os x os, only sub-matrix n x n is transformed
//  Input: n     ... sub-matrix size, one dimension (e.g., 32)
//         os    ... full-matrix size, one dimension (e.g., 128)
//   Temp: tmp_U ... temporary memory used for transform
//					 size = n x n (e.g., 32x32), (may be larger!)
void aao_waverec2_db3_1sc	(float * V, int n, int os, float * tmp_U);

// 1-dimensional inverse wavelet transform on *one* scale
//  Input: v     ... approx coeff (1 dim) size = n
//         n     ... sub-matrix size, one dimension (e.g., 32)
// Output: u     ... coarser approx + detail coeffs (1 dim)
//					 size = n
void aao_waverec_db3_1sc 	(float const * v, float * u, int n, int os);


// 2-dimensional inverse wavelet transform on *one* scale
// performs only the lowpass filter (useful if known that highpass coefficients are zero, like in GLMS)
// Params: see waverec2_db3_1sc
void aao_waverec2_db3_1sc_lowpass  (float * V, int n, int os, float * tmp_U);

// 1-dimensional inverse wavelet transform on *one* scale
// performs only the lowpass filter
// Params: see waverec_db3_1sc
void aao_waverec_db3_1sc_lowpass   (float const * v, float * u, int n, int os);


// multiplication of the wavelet coefficients with weights on *one* scale (U*=d)
void aao_wavemult_1sc		(float d, float * U, int n_beg, int n_end, int os);
// multiplication of the wavelet coefficients with weights on *one* scale with accumulation (U +=d*V)
void aao_wavemult_1sc_ac	(float d, float const * V, float * U, int n_beg, int n_end, int osU, int osV);


/* ======================================= */



// size V = n x n
// n = 2^p !!!
void aao_wavedec2_db3(float * V, int n, float * tmp_U)
{
	// no cropping, since n = 2^p !!!

	// local var
	int os;

	// init local var
	os = n;

	while (n > 1)
	{
		aao_wavedec2_db3_1sc(V, n, os, tmp_U);
		n = n/2;
	}

	/*
	function U = wavedec2_db3(V,Lo,Hi)
		p = 2.^(1:15);
		n = size(V,1);
		n = p(n<=p);
		n = n(1);

		U = zeros(n,n);
		U(1:size(V,1),1:size(V,2)) = V;

		while n>1
			U = f_wav_vec_mcao.wavedec2_db3_1sc(U,n,Lo,Hi);
			n = n/2;
		end
	end
	*/
}

// size V = os x os, os = offset
// size of submatrix U = n x n
void aao_wavedec2_db3_1sc(float * V, int n, int os, float * tmp_U)
{
	// local vars
	int i;
	float * U = tmp_U;
	float * v = NULL;
	float * u = NULL;

	// DWT each row, U = DWT(V)
	for (i=0; i<n; i++)
	{
		v = V+i*os;
		u = U+i*n;
		aao_wavedec_db3_1sc(v, u, n, 0);
	}

	// DWT each column, V = DWT(U)
	for (i=0; i<n; i++)
	{
		u = U+i;
		v = V+i;
		aao_wavedec_db3_1sc(u, v, n, os);
	}

	/*
    function U = wavedec2_db3_1sc(V,n,Lo,Hi)
        U = V;

        for i=1:n
            v = V(:,i);
            u = f_wav_vec_mcao.wavedec_db3_1sc(v,n,Lo,Hi);
            U(:,i) = u;
        end
        for i=1:n
            v = U(i,:);
            u = f_wav_vec_mcao.wavedec_db3_1sc(v,n,Lo,Hi);
            U(i,:) = u;
        end

    end
    */
}

// combination of wavedec2_db3 & wavedec2_db3_1sc with parallel support
void aao_wavedec2_db3_pa(float * V, int n, float * tmp_U, int n_proc_dwt, int n_dwt_pa)
{
	// remark: n = 2^p !!!

	// Local variables
	int os;
	int i;
	float * U = tmp_U;
	float * v = NULL;
	float * u = NULL;

	// Local variables init
	os = n;

	// code
	while (n > 1)
	{
		#pragma omp parallel private(i,u,v) num_threads(n_proc_dwt) if(n >= n_dwt_pa)
		{
			// DWT each row, U = DWT(V)
			#pragma omp for
			for (i=0; i<n; i++)
			{
				v = V+i*os;
				u = U+i*n;
				aao_wavedec_db3_1sc(v, u, n, 0);
			}

			// DWT each column, V = DWT(U)
			#pragma omp for
			for (i=0; i<n; i++)
			{
				u = U+i;
				v = V+i;
				aao_wavedec_db3_1sc(u, v, n, os);
			}
		}
		n = n/2;
	}
}

// size v = n
// size u = n
// if not-transposed, os = 0
// if     transposed, os > 0 and offset of u = os, offset of v = n
void aao_wavedec_db3_1sc(float const * v, float * u, int n, int os)
{
	// db3 coefficients
	const float Lo_D [6] =
	{
		 0.035226291882101,
		-0.085441273882241,
		-0.135011020010391,
		 0.459877502119331,
		 0.806891509313339,
		 0.332670552950957
	};
	const float Hi_D [6] =
	{
		-0.332670552950957,
		 0.806891509313339,
		-0.459877502119331,
		-0.135011020010391,
		 0.085441273882241,
		 0.035226291882101
	};

	// local variables
	int m, i, ibeg;
	int u_os, v_os;

	m = n/2;
	if (os==0) // not transposed
	{
		if (m > 1)
		{
			// i=1;
			// u(i)   = sum(v(1:4).*Lo_D(3:6)) + sum(v(n-1:n).*Lo_D(1:2));
			// u(i+m) = sum(v(1:4).*Hi_D(3:6)) + sum(v(n-1:n).*Hi_D(1:2));
			u[0] = v[0]*Lo_D[2] + v[1]*Lo_D[3] + v[2]*Lo_D[4] + v[3]*Lo_D[5] + v[n-2]*Lo_D[0] + v[n-1]*Lo_D[1];
			u[m] = v[0]*Hi_D[2] + v[1]*Hi_D[3] + v[2]*Hi_D[4] + v[3]*Hi_D[5] + v[n-2]*Hi_D[0] + v[n-1]*Hi_D[1];

			// for i=2:m-1
			// ibeg = 2*i-4+1;
			// iend = 2*i+2+0;
			// 	u(i)   = sum(v(ibeg:iend).*Lo_D);
			//	u(i+m) = sum(v(ibeg:iend).*Hi_D);
			// end
			for (i=1; i<m-1; i++)
			{
				ibeg = 2*i-2;
				u[i]   = v[ibeg]*Lo_D[0] + v[ibeg+1]*Lo_D[1] + v[ibeg+2]*Lo_D[2] + v[ibeg+3]*Lo_D[3] + v[ibeg+4]*Lo_D[4] + v[ibeg+5]*Lo_D[5];
				u[i+m] = v[ibeg]*Hi_D[0] + v[ibeg+1]*Hi_D[1] + v[ibeg+2]*Hi_D[2] + v[ibeg+3]*Hi_D[3] + v[ibeg+4]*Hi_D[4] + v[ibeg+5]*Hi_D[5];
			}

			// i=m;
			// u(i)   = sum(v(1:2).*Lo_D(5:6)) + sum(v(n-3:n).*Lo_D(1:4));
			// u(i+m) = sum(v(1:2).*Hi_D(5:6)) + sum(v(n-3:n).*Hi_D(1:4));
			u[m-1] = v[0]*Lo_D[4] + v[1]*Lo_D[5] + v[n-4]*Lo_D[0] + v[n-3]*Lo_D[1] + v[n-2]*Lo_D[2] + v[n-1]*Lo_D[3];
			u[n-1] = v[0]*Hi_D[4] + v[1]*Hi_D[5] + v[n-4]*Hi_D[0] + v[n-3]*Hi_D[1] + v[n-2]*Hi_D[2] + v[n-1]*Hi_D[3];
		}
		else // m = 1
		{
			u[0] = (Lo_D[0]+Lo_D[2]+Lo_D[4])*v[0] + (Lo_D[1]+Lo_D[3]+Lo_D[5])*v[1];
			u[1] = (Hi_D[0]+Hi_D[2]+Hi_D[4])*v[0] + (Hi_D[1]+Hi_D[3]+Hi_D[5])*v[1];
		}
	}
	else // transposed
	{
		// elements of u: u[0], u[u_os], u[2*u_os], ..., u[(n-1)*u_os]
		// elements of v: v[0], v[v_os], v[2*v_os], ..., v[(n-1)*v_os]
		u_os = os;
		v_os = n;
		if (m > 1)
		{
			u[0]      = v[0]*Lo_D[2] + v[v_os]*Lo_D[3] + v[2*v_os]*Lo_D[4] + v[3*v_os]*Lo_D[5] + v[(n-2)*v_os]*Lo_D[0] + v[(n-1)*v_os]*Lo_D[1];
			u[m*u_os] = v[0]*Hi_D[2] + v[v_os]*Hi_D[3] + v[2*v_os]*Hi_D[4] + v[3*v_os]*Hi_D[5] + v[(n-2)*v_os]*Hi_D[0] + v[(n-1)*v_os]*Hi_D[1];

			for (i=1; i<m-1; i++)
			{
				ibeg = 2*i-2;
				u[i    *u_os] = v[ibeg*v_os]*Lo_D[0] + v[(ibeg+1)*v_os]*Lo_D[1] + v[(ibeg+2)*v_os]*Lo_D[2] + v[(ibeg+3)*v_os]*Lo_D[3] + v[(ibeg+4)*v_os]*Lo_D[4] + v[(ibeg+5)*v_os]*Lo_D[5];
				u[(i+m)*u_os] = v[ibeg*v_os]*Hi_D[0] + v[(ibeg+1)*v_os]*Hi_D[1] + v[(ibeg+2)*v_os]*Hi_D[2] + v[(ibeg+3)*v_os]*Hi_D[3] + v[(ibeg+4)*v_os]*Hi_D[4] + v[(ibeg+5)*v_os]*Hi_D[5];
			}

			u[(m-1)*u_os] = v[0]*Lo_D[4] + v[v_os]*Lo_D[5] + v[(n-4)*v_os]*Lo_D[0] + v[(n-3)*v_os]*Lo_D[1] + v[(n-2)*v_os]*Lo_D[2] + v[(n-1)*v_os]*Lo_D[3];
			u[(n-1)*u_os] = v[0]*Hi_D[4] + v[v_os]*Hi_D[5] + v[(n-4)*v_os]*Hi_D[0] + v[(n-3)*v_os]*Hi_D[1] + v[(n-2)*v_os]*Hi_D[2] + v[(n-1)*v_os]*Hi_D[3];
		}
		else // m = 1
		{
			u[0]    = (Lo_D[0]+Lo_D[2]+Lo_D[4])*v[0] + (Lo_D[1]+Lo_D[3]+Lo_D[5])*v[v_os];
			u[u_os] = (Hi_D[0]+Hi_D[2]+Hi_D[4])*v[0] + (Hi_D[1]+Hi_D[3]+Hi_D[5])*v[v_os];
		}
	} // if-else transposed

	/*
	function u = wavedec_db3_1sc(v,n,Lo_D,Hi_D)
	% size(Lo_D) = 1x6
	% size(Hi_D) = 1x6

		v = v(:)';

		m = n/2;

		u = v;

		if m > 1
			i=1;
			u(i)   = sum(v(1:4).*Lo_D(3:6)) + sum(v(n-1:n).*Lo_D(1:2));
			u(i+m) = sum(v(1:4).*Hi_D(3:6)) + sum(v(n-1:n).*Hi_D(1:2));
			for i=2:m-1
				ibeg = 2*i-4+1;
				iend = 2*i+2+0;
				u(i)   = sum(v(ibeg:iend).*Lo_D);
				u(i+m) = sum(v(ibeg:iend).*Hi_D);
			end
			i=m;
			u(i)   = sum(v(1:2).*Lo_D(5:6)) + sum(v(n-3:n).*Lo_D(1:4));
			u(i+m) = sum(v(1:2).*Hi_D(5:6)) + sum(v(n-3:n).*Hi_D(1:4));
		elseif m==1
			u(1) = sum([v(1:2) v(1:2) v(1:2)] .* Lo_D);
			u(2) = sum([v(1:2) v(1:2) v(1:2)] .* Hi_D);
		end
	*/
}



void aao_waverec2_db3(float * V, int n, float * tmp_U)
{

	int i;

	i = 2;
	while (i <= n)
	{
		aao_waverec2_db3_1sc(V, i, n, tmp_U);
		i = 2*i;
	}

	/*
	function U = waverec2_db3(V,Lo,Hi)
		p = 2.^(1:15);
		n = size(V,1);
		n = p(n<=p);
		n = n(1);

		U=V;

		i=2;
		while i<=n
			U = f_wav_vec_mcao.waverec2_db3_1sc(U,i,Lo,Hi);
			i = 2*i;
		end
	end
	*/
}


// n_cs = coarse scales
// structure of V:
// [v0]
// [00]
// size of V: n x n
// size of v: n_cs x n_cs
void aao_waverec2_db3_coarse_scales(float * V, int n, float * tmp_U, int n_cs)
{

	int i;

	i = 2;
	while (i <= n_cs)	// process the v (coarse scales)
	{
		aao_waverec2_db3_1sc(V, i, n, tmp_U);
		i = 2*i;
	}
	while (i <= n)		// process the zeros (high scales)
	{
		aao_waverec2_db3_1sc_lowpass(V, i, n, tmp_U);
		i = 2*i;
	}

	/*
	function U = waverec2_db3(V,Lo,Hi)
		p = 2.^(1:15);
		n = size(V,1);
		n = p(n<=p);
		n = n(1);

		U=V;

		i=2;
		while i<=n
			U = f_wav_vec_mcao.waverec2_db3_1sc(U,i,Lo,Hi);
			i = 2*i;
		end
	end
	*/
}


// size V = os x os, os = offset
// size of submatrix U = n x n
void aao_waverec2_db3_1sc(float * V, int n, int os, float * tmp_U)
{
	int i;
//	int n_sq;
	float * U = tmp_U;
	float * v = NULL;
	float * u = NULL;

//	n_sq = n*n;
//	U = (float*) malloc(sizeof(float)*n_sq);



	// DWT each row, U = DWT(V)
	for (i=0; i<n; i++)
	{
		v = V+i*os;
		u = U+i*n;
		aao_waverec_db3_1sc(v, u, n, 0);
	}

	// DWT each column, V = DWT(U)
	for (i=0; i<n; i++)
	{
		u = U+i;
		v = V+i;
		aao_waverec_db3_1sc(u, v, n, os);
	}


//	free(U);


	/*
	function U = waverec2_db3_1sc(V,n,Lo_R,Hi_R)
		U = V;

		for i=1:n
			v = V(:,i);
			u = f_wav_vec_mcao.waverec_db3_1sc(v,n,Lo_R,Hi_R);
			U(:,i) = u;
		end
		for i=1:n
			v = U(i,:);
			u = f_wav_vec_mcao.waverec_db3_1sc(v,n,Lo_R,Hi_R);
			U(i,:) = u;
		end
	end
	*/
}

void aao_waverec2_db3_pa(float * V, int n, float * tmp_U, int n_proc_dwt, int n_dwt_pa)
{
	// Local variables
	int os;
	int i;
	float * U = tmp_U;
	float * v = NULL;
	float * u = NULL;

	// Local variables init
	os = n;
	n  = 2;

	//code


	while (n <= os)
	{
		#pragma omp parallel private(i,u,v) num_threads(n_proc_dwt) if(n >= n_dwt_pa)
		{
			// DWT each row, U = DWT(V)
			#pragma omp for
			for (i=0; i<n; i++)
			{
				v = V+i*os;
				u = U+i*n;
				aao_waverec_db3_1sc(v, u, n, 0);
			}

			// DWT each column, V = DWT(U)
			#pragma omp for
			for (i=0; i<n; i++)
			{
				u = U+i;
				v = V+i;
				aao_waverec_db3_1sc(u, v, n, os);
			}
		}
		n = 2*n;
	}
}

void aao_waverec2_db3_1sc_lowpass(float * V, int n, int os, float * tmp_U)
{
	int i;
//	int n_sq;
	float * U = tmp_U;
	float * v = NULL;
	float * u = NULL;

//	n_sq = n*n;
//	U = (float*) malloc(sizeof(float)*n_sq);



	// DWT each row, U = DWT(V)
	for (i=0; i<n; i++)
	{
		v = V+i*os;
		u = U+i*n;
		aao_waverec_db3_1sc_lowpass(v, u, n, 0);
	}

	// DWT each column, V = DWT(U)
	for (i=0; i<n; i++)
	{
		u = U+i;
		v = V+i;
		aao_waverec_db3_1sc_lowpass(u, v, n, os);
	}


//	free(U);


	/*
	function U = waverec2_db3_1sc(V,n,Lo_R,Hi_R)
		U = V;

		for i=1:n
			v = V(:,i);
			u = f_wav_vec_mcao.waverec_db3_1sc(v,n,Lo_R,Hi_R);
			U(:,i) = u;
		end
		for i=1:n
			v = U(i,:);
			u = f_wav_vec_mcao.waverec_db3_1sc(v,n,Lo_R,Hi_R);
			U(i,:) = u;
		end
	end
	*/
}

// size v = n
// size u = n
// if not-transposed, os = 0
// if     transposed, os > 0 and offset of u = os, offset of v = n
void aao_waverec_db3_1sc(float const * v, float * u, int n, int os)
{
	// db3 coefficients
	const float Lo_R [6] =
	{
		 0.806891509313339, // Lo_D[4] = Lo_R(1,1)
		-0.135011020010391,	// Lo_D[2] = Lo_R(1,2)
		 0.035226291882101,	// Lo_D[0] = Lo_R(1,3)
		 0.332670552950957,	// Lo_D[5] = Lo_R(2,1)
		 0.459877502119331,	// Lo_D[3] = Lo_R(2,2)
		-0.085441273882241	// Lo_D[1] = Lo_R(2,3)
	};
	const float Hi_R [6] =
	{
		 0.085441273882241,	// Hi_D[4]
		-0.459877502119331,	// Hi_D[2]
		-0.332670552950957,	// Hi_D[0]
		 0.035226291882101,	// Hi_D[5]
		-0.135011020010391,	// Hi_D[3]
		 0.806891509313339	// Hi_D[1]
	};

	// local variables
	int m, i, j, ione;
	int u_os, v_os;

	m = n/2;
	if (os==0) // not transposed
	{
		if (m>1)
		{
			// i=1;
			// ione = 2*i-1;
			// itwo = 2*i;
			// u(ione) = sum(v(1:2).*Lo_R(1,2:3)) + sum(v(m+1:m+2).*Hi_R(1,2:3)) + ...
			//   v(m)*Lo_R(1,1) + v(n)*Hi_R(1,1);
			// u(itwo) = sum(v(1:2).*Lo_R(2,2:3)) + sum(v(m+1:m+2).*Hi_R(2,2:3)) + ...
			//   v(m)*Lo_R(2,1) + v(n)*Hi_R(2,1);
			u[0] = v[0]*Lo_R[1] + v[1]*Lo_R[2] + v[m-1]*Lo_R[0]   +   v[m]*Hi_R[1] + v[m+1]*Hi_R[2] + v[n-1]*Hi_R[0];
			u[1] = v[0]*Lo_R[4] + v[1]*Lo_R[5] + v[m-1]*Lo_R[3]   +   v[m]*Hi_R[4] + v[m+1]*Hi_R[5] + v[n-1]*Hi_R[3];


			// for i=2:m-1
			//	ione = 2*i-1;
			//	itwo = 2*i;
			//	ibeg_L = i-1;
			//	iend_L = i+1;
			//	ibeg_H = ibeg_L+m;
			//	iend_H = iend_L+m;
			//	u(ione) = sum(v(ibeg_L:iend_L).*Lo_R(1,:)) + sum(v(ibeg_H:iend_H).*Hi_R(1,:));
			//	u(itwo) = sum(v(ibeg_L:iend_L).*Lo_R(2,:)) + sum(v(ibeg_H:iend_H).*Hi_R(2,:));
			// end
			for (i=1; i<m-1; i++)
			{
				ione = 2*i;
				j    = i+m;
				u[ione]   = v[i-1]*Lo_R[0] + v[i]*Lo_R[1] + v[i+1]*Lo_R[2]   +   v[j-1]*Hi_R[0] + v[j]*Hi_R[1] + v[j+1]*Hi_R[2];
				u[ione+1] = v[i-1]*Lo_R[3] + v[i]*Lo_R[4] + v[i+1]*Lo_R[5]   +   v[j-1]*Hi_R[3] + v[j]*Hi_R[4] + v[j+1]*Hi_R[5];
			}


			// i=m;
			// ione = 2*i-1;
			// itwo = 2*i;
			// u(ione) = v(1)*Lo_R(1,3) + v(m+1)*Hi_R(1,3) + ...
			//	sum(v(m-1:m).*Lo_R(1,1:2)) + sum(v(n-1:n).*Hi_R(1,1:2));
			// u(itwo) = v(1)*Lo_R(2,3) + v(m+1)*Hi_R(2,3) + ...
			//	sum(v(m-1:m).*Lo_R(2,1:2)) + sum(v(n-1:n).*Hi_R(2,1:2));
			u[n-2] = v[0]*Lo_R[2] + v[m-2]*Lo_R[0] + v[m-1]*Lo_R[1]   +   v[m]*Hi_R[2] + v[n-2]*Hi_R[0] + v[n-1]*Hi_R[1];
			u[n-1] = v[0]*Lo_R[5] + v[m-2]*Lo_R[3] + v[m-1]*Lo_R[4]   +   v[m]*Hi_R[5] + v[n-2]*Hi_R[3] + v[n-1]*Hi_R[4];


		}
		else // m = 1
		{
			// u(1) = sum([sum(Lo_R(1,:)) sum(Hi_R(1,:))].*v(1:2));
			// u(2) = sum([sum(Lo_R(2,:)) sum(Hi_R(2,:))].*v(1:2));
			u[0] = (Lo_R[0]+Lo_R[1]+Lo_R[2])*v[0] + (Hi_R[0]+Hi_R[1]+Hi_R[2])*v[1];
			u[1] = (Lo_R[3]+Lo_R[4]+Lo_R[5])*v[0] + (Hi_R[3]+Hi_R[4]+Hi_R[5])*v[1];
		}

	}
	else // transposed
	{
		// elements of u: u[0], u[u_os], u[2*u_os], ..., u[(n-1)*u_os]
		// elements of v: v[0], v[v_os], v[2*v_os], ..., v[(n-1)*v_os]
		u_os = os;
		v_os = n;

		if (m>1)
		{
			u[0]    = v[0]*Lo_R[1] + v[v_os]*Lo_R[2] + v[(m-1)*v_os]*Lo_R[0]   +   v[m*v_os]*Hi_R[1] + v[(m+1)*v_os]*Hi_R[2] + v[(n-1)*v_os]*Hi_R[0];
			u[u_os] = v[0]*Lo_R[4] + v[v_os]*Lo_R[5] + v[(m-1)*v_os]*Lo_R[3]   +   v[m*v_os]*Hi_R[4] + v[(m+1)*v_os]*Hi_R[5] + v[(n-1)*v_os]*Hi_R[3];

			for (i=1; i<m-1; i++)
			{
				ione = 2*i;
				j    = i+m;
				u[ ione   *u_os] = v[(i-1)*v_os]*Lo_R[0] + v[i*v_os]*Lo_R[1] + v[(i+1)*v_os]*Lo_R[2]   +   v[(j-1)*v_os]*Hi_R[0] + v[j*v_os]*Hi_R[1] + v[(j+1)*v_os]*Hi_R[2];
				u[(ione+1)*u_os] = v[(i-1)*v_os]*Lo_R[3] + v[i*v_os]*Lo_R[4] + v[(i+1)*v_os]*Lo_R[5]   +   v[(j-1)*v_os]*Hi_R[3] + v[j*v_os]*Hi_R[4] + v[(j+1)*v_os]*Hi_R[5];
			}

			u[(n-2)*u_os] = v[0]*Lo_R[2] + v[(m-2)*v_os]*Lo_R[0] + v[(m-1)*v_os]*Lo_R[1]   +   v[m*v_os]*Hi_R[2] + v[(n-2)*v_os]*Hi_R[0] + v[(n-1)*v_os]*Hi_R[1];
			u[(n-1)*u_os] = v[0]*Lo_R[5] + v[(m-2)*v_os]*Lo_R[3] + v[(m-1)*v_os]*Lo_R[4]   +   v[m*v_os]*Hi_R[5] + v[(n-2)*v_os]*Hi_R[3] + v[(n-1)*v_os]*Hi_R[4];
		}
		else // m = 1
		{
			u[0]    = (Lo_R[0]+Lo_R[1]+Lo_R[2])*v[0] + (Hi_R[0]+Hi_R[1]+Hi_R[2])*v[v_os];
			u[u_os] = (Lo_R[3]+Lo_R[4]+Lo_R[5])*v[0] + (Hi_R[3]+Hi_R[4]+Hi_R[5])*v[v_os];
		}
	}




	/*
	function u = waverec_db3_1sc(v,n,Lo_R,Hi_R)
	% size(Lo_R) = 2x3
	% size(Hi_R) = 2x3

		v = v(:)';

		m = n/2;

		u = v;

		if m > 1
			i=1;
			ione = 2*i-1;
			itwo = 2*i;
			u(ione) = sum(v(1:2).*Lo_R(1,2:3)) + sum(v(m+1:m+2).*Hi_R(1,2:3)) + ...
				v(m)*Lo_R(1,1) + v(n)*Hi_R(1,1);
			u(itwo) = sum(v(1:2).*Lo_R(2,2:3)) + sum(v(m+1:m+2).*Hi_R(2,2:3)) + ...
				v(m)*Lo_R(2,1) + v(n)*Hi_R(2,1);
			for i=2:m-1
				ione = 2*i-1;
				itwo = 2*i;
				ibeg_L = i-1;
				iend_L = i+1;
				ibeg_H = ibeg_L+m;
				iend_H = iend_L+m;
				u(ione) = sum(v(ibeg_L:iend_L).*Lo_R(1,:)) + sum(v(ibeg_H:iend_H).*Hi_R(1,:));
				u(itwo) = sum(v(ibeg_L:iend_L).*Lo_R(2,:)) + sum(v(ibeg_H:iend_H).*Hi_R(2,:));
			end
			i=m;
			ione = 2*i-1;
			itwo = 2*i;
			u(ione) = v(1)*Lo_R(1,3) + v(m+1)*Hi_R(1,3) + ...
				sum(v(m-1:m).*Lo_R(1,1:2)) + sum(v(n-1:n).*Hi_R(1,1:2));
			u(itwo) = v(1)*Lo_R(2,3) + v(m+1)*Hi_R(2,3) + ...
				sum(v(m-1:m).*Lo_R(2,1:2)) + sum(v(n-1:n).*Hi_R(2,1:2));
		elseif m==1
			u(1) = sum([sum(Lo_R(1,:)) sum(Hi_R(1,:))].*v(1:2));
			u(2) = sum([sum(Lo_R(2,:)) sum(Hi_R(2,:))].*v(1:2));
		end
	end
	*/
}


// size v = n
// size u = n
// if not-transposed, os = 0
// if     transposed, os > 0 and offset of u = os, offset of v = n
void aao_waverec_db3_1sc_lowpass(float const * v, float * u, int n, int os)
{
	// db3 coefficients
	const float Lo_R [6] =
	{
		 0.806891509313339, // Lo_D[4] = Lo_R(1,1)
		-0.135011020010391,	// Lo_D[2] = Lo_R(1,2)
		 0.035226291882101,	// Lo_D[0] = Lo_R(1,3)
		 0.332670552950957,	// Lo_D[5] = Lo_R(2,1)
		 0.459877502119331,	// Lo_D[3] = Lo_R(2,2)
		-0.085441273882241	// Lo_D[1] = Lo_R(2,3)
	};

	// local variables
	int m, i, ione;
	int u_os, v_os;

	m = n/2;
	if (os==0) // not transposed
	{
		if (m>1)
		{
			// i=1;
			// ione = 2*i-1;
			// itwo = 2*i;
			// u(ione) = sum(v(1:2).*Lo_R(1,2:3)) + sum(v(m+1:m+2).*Hi_R(1,2:3)) + ...
			//   v(m)*Lo_R(1,1) + v(n)*Hi_R(1,1);
			// u(itwo) = sum(v(1:2).*Lo_R(2,2:3)) + sum(v(m+1:m+2).*Hi_R(2,2:3)) + ...
			//   v(m)*Lo_R(2,1) + v(n)*Hi_R(2,1);
			u[0] = v[0]*Lo_R[1] + v[1]*Lo_R[2] + v[m-1]*Lo_R[0];
			u[1] = v[0]*Lo_R[4] + v[1]*Lo_R[5] + v[m-1]*Lo_R[3];


			// for i=2:m-1
			//	ione = 2*i-1;
			//	itwo = 2*i;
			//	ibeg_L = i-1;
			//	iend_L = i+1;
			//	ibeg_H = ibeg_L+m;
			//	iend_H = iend_L+m;
			//	u(ione) = sum(v(ibeg_L:iend_L).*Lo_R(1,:)) + sum(v(ibeg_H:iend_H).*Hi_R(1,:));
			//	u(itwo) = sum(v(ibeg_L:iend_L).*Lo_R(2,:)) + sum(v(ibeg_H:iend_H).*Hi_R(2,:));
			// end
			for (i=1; i<m-1; i++)
			{
				ione = 2*i;
				u[ione]   = v[i-1]*Lo_R[0] + v[i]*Lo_R[1] + v[i+1]*Lo_R[2];
				u[ione+1] = v[i-1]*Lo_R[3] + v[i]*Lo_R[4] + v[i+1]*Lo_R[5];
			}


			// i=m;
			// ione = 2*i-1;
			// itwo = 2*i;
			// u(ione) = v(1)*Lo_R(1,3) + v(m+1)*Hi_R(1,3) + ...
			//	sum(v(m-1:m).*Lo_R(1,1:2)) + sum(v(n-1:n).*Hi_R(1,1:2));
			// u(itwo) = v(1)*Lo_R(2,3) + v(m+1)*Hi_R(2,3) + ...
			//	sum(v(m-1:m).*Lo_R(2,1:2)) + sum(v(n-1:n).*Hi_R(2,1:2));
			u[n-2] = v[0]*Lo_R[2] + v[m-2]*Lo_R[0] + v[m-1]*Lo_R[1];
			u[n-1] = v[0]*Lo_R[5] + v[m-2]*Lo_R[3] + v[m-1]*Lo_R[4];


		}
		else // m = 1
		{
			// u(1) = sum([sum(Lo_R(1,:)) sum(Hi_R(1,:))].*v(1:2));
			// u(2) = sum([sum(Lo_R(2,:)) sum(Hi_R(2,:))].*v(1:2));
			u[0] = (Lo_R[0]+Lo_R[1]+Lo_R[2])*v[0];
			u[1] = (Lo_R[3]+Lo_R[4]+Lo_R[5])*v[0];
		}

	}
	else // transposed
	{
		// elements of u: u[0], u[u_os], u[2*u_os], ..., u[(n-1)*u_os]
		// elements of v: v[0], v[v_os], v[2*v_os], ..., v[(n-1)*v_os]
		u_os = os;
		v_os = n;

		if (m>1)
		{
			u[0]    = v[0]*Lo_R[1] + v[v_os]*Lo_R[2] + v[(m-1)*v_os]*Lo_R[0];
			u[u_os] = v[0]*Lo_R[4] + v[v_os]*Lo_R[5] + v[(m-1)*v_os]*Lo_R[3];

			for (i=1; i<m-1; i++)
			{
				ione = 2*i;
				u[ ione   *u_os] = v[(i-1)*v_os]*Lo_R[0] + v[i*v_os]*Lo_R[1] + v[(i+1)*v_os]*Lo_R[2];
				u[(ione+1)*u_os] = v[(i-1)*v_os]*Lo_R[3] + v[i*v_os]*Lo_R[4] + v[(i+1)*v_os]*Lo_R[5];
			}

			u[(n-2)*u_os] = v[0]*Lo_R[2] + v[(m-2)*v_os]*Lo_R[0] + v[(m-1)*v_os]*Lo_R[1];
			u[(n-1)*u_os] = v[0]*Lo_R[5] + v[(m-2)*v_os]*Lo_R[3] + v[(m-1)*v_os]*Lo_R[4];
		}
		else // m = 1
		{
			u[0]    = (Lo_R[0]+Lo_R[1]+Lo_R[2])*v[0];
			u[u_os] = (Lo_R[3]+Lo_R[4]+Lo_R[5])*v[0];
		}
	}
}


// U *= d
void aao_wavemult(float * U, float const * d, int n)
{
	// size U,V = n x n, n = 2^p !!!
	// size d = p

	// local variables
	int i,os,n_end;

	// local variables init
	os=n;

	// code
	U[0] *= d[0];

	i=0;
	for (n=1; n<os; n*=2)
	{
		n_end = 2*n;
		aao_wavemult_1sc(d[i], U, n, n_end, os);
		i++;
	}
}
void aao_wavemult_1sc(float d, float * U, int n_beg, int n_end, int os)
{
	int i,j;

	// .x
	// ..
	for (i=0; i<n_beg; i++)
	{
		for(j=n_beg; j<n_end; j++)
		{
			U[i*os+j] *= d;
		}
	}
	// ..
	// xx
	for (i=n_beg; i<n_end; i++)
	{
		for(j=0; j<n_end; j++)
		{
			U[i*os+j] *= d;
		}
	}
}
// n_cs = coarse scales
// structure of V, U:
// [v0]
// [00]
// size of U: n x n
// size of V: n_cs x n_cs
// U += alpha*d*V
void aao_wavemult_ac_coarse_scales(float * U, float alpha, float const * d, float const * V, int n, int n_cs)
{
	// size U = n x n, n = 2^p !!!
	// size V = n_cs x n_cs, n_cs = 2^p !!!
	// size d = p

	// local variables
	int i,os,n_end;

	// local variables init
	os=n;

	// code
	U[0] += alpha*d[0]*V[0];

	i=0;
	for (n=1; n<n_cs; n*=2)
	{
		n_end = 2*n;
		aao_wavemult_1sc_ac(alpha*d[i], V, U, n, n_end, os, n_cs);
		i++;
	}
}
// U += alpha*d*V
void aao_wavemult_ac(float * U, float alpha, float const * d, float const * V, int n)
{
	// size U,V = n x n, n = 2^p !!!
	// size d = p

	// local variables
	int i,os,n_end;

	// local variables init
	os=n;

	// code
	U[0] += alpha*d[0]*V[0];

	i=0;
	for (n=1; n<os; n*=2)
	{
		n_end = 2*n;
		aao_wavemult_1sc_ac(alpha*d[i], V, U, n, n_end, os, os);
		i++;
	}
}
void aao_wavemult_1sc_ac(float d, float const * V, float * U, int n_beg, int n_end, int osU, int osV)
{
	int i,j;

	// .x
	// ..
	for (i=0; i<n_beg; i++)
	{
		for(j=n_beg; j<n_end; j++)
		{
			U[i*osU+j] += d*V[i*osV+j];
		}
	}
	// ..
	// xx
	for (i=n_beg; i<n_end; i++)
	{
		for(j=0; j<n_end; j++)
		{
			U[i*osU+j] += d*V[i*osV+j];
		}
	}
}
// b = b + A*x = b + v
void aao_matrix_wav_vec_prod_ac(float * b, float * v, float const * A, float const * x, int n_sqrt, int n0_sqrt)
{
	// size A = n_sqrt^2 x n_sqrt^2
	// size x = n_sqrt^2 x 1 (actually n0_sqrt x n0_sqrt restricted to n_sqrt x n_sqrt)
	// size b = n_sqrt^2 x 1 (actually n0_sqrt x n0_sqrt restricted to n_sqrt x n_sqrt)
	// size v = n_sqrt^2 x 1 (actually n_sqrt  x n_sqrt)

	// storage of x,b:
	// [0 1 2 3 4  ...         n_sqrt ]
	// [n0_sqrt+1  ... n0_sqrt+n_sqrt ]
	// [           ...                ]
	// [n0_sqrt*(n_sqrt-1) ... n_sqrt*n0_sqrt]

	// storage of v:
	// [0 1 2 3 4  ...  n_sqrt ]
	// [n_sqrt+1  ... 2*n_sqrt ]
	// [           ...                ]
	// [n_sqrt*(n_sqrt-1) ... n_sqrt^2]

	// local variables
	int ix,jx,indx;
	int ib,jb,indb,indv;
	int indA,n;
	float tmp;

	// local variables init
	n = n_sqrt*n_sqrt;


	// code

 	for(ib=0; ib<n_sqrt; ib++) // the same as for(i...)
	{
		for(jb=0; jb<n_sqrt; jb++)
		{
			indb = ib*n0_sqrt+jb; // the global index of wavelet "vector" b
			indv = ib*n_sqrt+jb;  // the global index of wavelet "vector" v

			v[indv] = 0.;

			for(ix=0; ix<n_sqrt; ix++) // the same as for(j...)
			{
				for(jx=0; jx<n_sqrt; jx++)
				{
					indx = ix*n0_sqrt+jx; 				// the global index of wavelet "vector" x
					indA = indv*n + (ix*n_sqrt+jx); 	// the  local index of matrix A
					tmp = A[indA] * x[indx];
					v[indv] += tmp;
					b[indb] += tmp;
				}
			}
		}
	}

	/*
	for(ib=0; ib<n_sqrt*n_sqrt; ib++) // the same as for(i...)
	{
		b[ib] = 0.;
		for (ix=0; ix<n_sqrt*n_sqrt; ix++)
			b[ib] += A[ib*(n_sqrt*n_sqrt)+ix]*x[ix];
	}
	*/
}


/*
 * vec_op.h
 *
 *  Created on: Feb 14, 2013
 *      Author: misha
 */


#ifndef AAO_VEC_OP_H_
#define AAO_VEC_OP_H_

// ============= operations used by iW, iW' ================
void aao_vec_ti_const_ac(float * b, float alpha,                  int n); // b *= alpha, size = n
void aao_vec_ti_const   (float * c, float alpha, float const * b, int n); // c  = alpha*b, size = n

// ============= operations used by P, P' ==================
//size a0 = n0_sqrt x n0_sqrt, size a = n_sqrt x n_sqrt
void aao_vec_rest_sqr	(float * a,  float const * a0, int n_sqrt, int n0_sqrt); // a = R(a0), R ... restrict
void aao_vec_prol_sqr	(float * a0, float const * a,  int n_sqrt, int n0_sqrt); // a0 = P(a), P ... pad with zeros
void aao_vec_prol_sqr_ac(float * a0, float const * a,  int n_sqrt, int n0_sqrt); // a0+= P(a)

// ============= operations used in pcg_jacobi =============
void  aao_vec_pl_vec_ac		 (float * a, float const * b, int n); // a += b, size = n
void  aao_diag_vec_prod		 (float * a, float const * D, float const * b, int n); // a=D*b, D ... diagonal matrix, size = n
float aao_vec_scal_prod		 (float const * a, float const * b, int n); // a'*b, size = n
float aao_vec_norm           (float const * a, int n); // sqrt(a'*a), size = n
void  aao_vec_copy			 (float * a, float const * b, int n); // a = b, size = n
void  aao_vec_pl_sc_vec_type2(float * a, float alpha, float const * b, int n); // a = b + alpha * a, size = n
void  aao_vec_pl_sc_vec_ac	 (float * a, float alpha, float const * b, int n); // a = a + alpha * b, size = n
void  aao_vec_mi_sc_vec_ac	 (float * a, float alpha, float const * b, int n); // a = a - alpha * b, size = n

// ============= operations used by GLMS ===================
//size a0 = n0_sqrt x n0_sqrt, size a = n_sqrt x n_sqrt
void aao_vec_mi_vec_ac		     (float * a,  float const * b, int n); // a -= b, size = n
void aao_vec_ti_const_prol_sqr   (float * a0, float alpha, float const * a, int n_sqrt, int n0_sqrt); 	// a0 = alpha*P(a)
void aao_vec_ti_const_prol_sqr_ac(float * a0, float alpha, float const * a, int n_sqrt, int n0_sqrt);	// a0+= alpha*P(a)

int read_vec_from_file_cast_uint8(char const *filename, unsigned char *x, int n);

#endif /* AAO_VEC_OP_H_ */

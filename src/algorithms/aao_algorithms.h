/*
 * algorithm.h
 *
 *  Created on: Jul 6, 2014
 *      Author: misha
 */

#ifndef AAO_ALGORITHM_H_
#define AAO_ALGORITHM_H_

#include "aao_FEWHA.h"
#include "aao_operators.h"
#include "aao_FEWHA_parallel.h"

// ======== Structs ========
#include "aao_algorithms_structs.h"


// ======== Algorithms ========
// (r,b) get updated from old iteration to this iteration
// b_delta = b_new - b
// b = b_new;
// r = b_new - A*c = r + b_new - b = r + delta_b
// In/Out: r, b_old
//  Input: b, n
void aao_update_r_b(float *r, float *b, float const *b_new, int n);

// a_delta = a_ol - a_old_old;
// a = leaky_int*a_old + gain * a_delta;
// a_old_old = a_old;
// In/Out: a_old, a_old_old
//  Input: a, gain, leaky_int, n
void aao_integrator_POLC(float *a_old, float *a_old_old, float const *a,
                         float gain, float leaky_int, int n);

// a  = (leaky_int-gain)*a_old + gain*a_ol;
// In/Out: a_old
//  Input: a, gain, leaky_int, n
void aao_integrator_OL(float *a_old, float const *a, float gain,
                       float leaky_int, int n);

// PCG algorithm with Jacobi (diagonal) preconditioner
// Performs a few (max_iter) PCG iterations on residual equation
//    Ac=r,
// where
//    r = b-Ac0 is the residual,
//    c0 is initial guess,
//    c is update and
//    c1 = c0+c is the new solution
// Frequency-dependent preconditioner J^{-1} is used
// In/Out: c, r
//  Input: pcg, A, iJ, max_iter, fewha, parall
void aao_pcg_jacobi(float * c, float * r, aao_PCG const * pcg, aao_OperatorA const * A, aao_OperatoriJ const * iJ, int max_iter, aao_FEWHA const * fewha, aao_FEWHA_Parallel const * parall);

// GLMS algorithm
// Solves a problems on the coarse scales of the ground layer exactly
//    A_{cs}v=r_{cs},
// then v is used for updating the initial guess and the residual
//    c = c+v;
//    r = r-A_{cs}v;
// In/Out: c, r
//  Input: G, A, fewha, parall
void aao_glms(float * c, float * r, aao_OperatorG const * G, aao_OperatorA const * A, aao_FEWHA const * fewha, aao_FEWHA_Parallel const * parall);

// Control: open loop or pseudo-open loop
// 1) pseudo-open loop measurements are generated if necessary
// 2) RHS vector computed
// 3) residual updated
// 4) GLMS [optional]
// 5) PCG
// 6) mirror shapes computed
// 7) control applied
// In/Out: a
//     In: s (writable), loop, pcg, A, iJ, F, H, fewha, par
void aao_control(float * a, aao_Loop const * loop, aao_PCG const * pcg, aao_OperatorA const * A, aao_OperatoriJ const * iJ, aao_OperatorF const * F, aao_OperatorH const * H, aao_OperatorG const * G, float * s, aao_FEWHA const * fewha, aao_FEWHA_Parallel const * par);


// =========== Algorithm init/free =========
void aao_initPCG(aao_PCG * pcg, aao_FEWHA const * fewha);
void aao_freePCG(aao_PCG * pcg);

void aao_initLoop(aao_Loop * loop, aao_FEWHA const * fewha);
void aao_freeLoop(aao_Loop * loop);

// =========== Algorithm print =============
void aao_printPCG(aao_PCG const * pcg, aao_FEWHA const * fewha);
void aao_printLoop(aao_Loop const * loop, aao_FEWHA const * fewha);


#endif /* AAO_ALGORITHM_H_ */

/*
 * pcg_jacobi.c
 *
 *  Created on: Jul 6, 2014
 *      Author: misha
 */

#include "aao_algorithms.h"
#include "aao_vec_op.h"
#include <stdlib.h>

void aao_pcg_jacobi(float *c, float *r, aao_PCG const *pcg,
                    aao_OperatorA const *A, aao_OperatoriJ const *iJ,
                    int max_iter, aao_FEWHA const *fewha,
                    aao_FEWHA_Parallel const *par) {
  // local variables
  int iter;
  float *z = NULL;
  float *p = NULL;
  float *q = NULL;
  float alpha, beta, rho, rho_old = 0, tmp;

  int iLay, nLay;
  int const *os_lay, *n_lay;

  // local var init
  z = pcg->tmp_z;
  p = pcg->tmp_p;
  q = pcg->tmp_q;

  nLay = fewha->nLay;
  n_lay = fewha->n_lay_at_lay;
  os_lay = fewha->os_lay_at_lay;

  for (iter = 0; iter < max_iter; iter++) {

    //#pragma omp parallel private(iLay) num_threads(par->max_thr_lay)
    //if(par->global) //n.lay
    //{
    // apply preconditioner
    // z = J^{-1} * r;
    //	#pragma omp for
    for (iLay = 0; iLay < nLay; iLay++) {
      aao_diag_vec_prod(z + os_lay[iLay], iJ->d + os_lay[iLay],
                        r + os_lay[iLay], n_lay[iLay] * n_lay[iLay]);
    }

    // rho = r'*z;
    //	#pragma omp single
    //	{
    rho = 0.;
    //	}
    //	#pragma omp for reduction(+:rho)
    for (iLay = 0; iLay < nLay; iLay++) {
      rho += aao_vec_scal_prod(r + os_lay[iLay], z + os_lay[iLay],
                               n_lay[iLay] * n_lay[iLay]);
    }

    if (iter == 0) {
      // p = z;
      //	#pragma omp for
      for (iLay = 0; iLay < nLay; iLay++) {
        aao_vec_copy(p + os_lay[iLay], z + os_lay[iLay],
                     n_lay[iLay] * n_lay[iLay]);
      }
    } else {
      // p = z + (rho / rho_old) * p;
      //	#pragma omp single
      { beta = rho / rho_old; }
      //	#pragma omp for
      for (iLay = 0; iLay < nLay; iLay++) {
        aao_vec_pl_sc_vec_type2(p + os_lay[iLay], beta, z + os_lay[iLay],
                                n_lay[iLay] * n_lay[iLay]);
      }
    }
    //} // end of prgma omp parallel

    // q = A*p;
    aao_applyA(q, A, p, fewha, par);

    //	#pragma omp parallel private(iLay) num_threads(par->max_thr_lay)
    //if(par->global) //n.lay
    //	{
    // alpha = rho / (p'*q);
    // alpha = rho / vec_scal_prod(p, q, n.ect);
    //		#pragma omp single
    { tmp = 0.; }
    //		#pragma omp for reduction(+:tmp)
    for (iLay = 0; iLay < nLay; iLay++) {
      tmp += aao_vec_scal_prod(p + os_lay[iLay], q + os_lay[iLay],
                               n_lay[iLay] * n_lay[iLay]);
    }
    //		#pragma omp single
    { alpha = rho / tmp; }

    // c = c + alpha * p;
    // r = r - alpha * q;
    //		#pragma omp for
    for (iLay = 0; iLay < nLay; iLay++) {
      aao_vec_pl_sc_vec_ac(c + os_lay[iLay], alpha, p + os_lay[iLay],
                           n_lay[iLay] * n_lay[iLay]);
      aao_vec_mi_sc_vec_ac(r + os_lay[iLay], alpha, q + os_lay[iLay],
                           n_lay[iLay] * n_lay[iLay]);
    }

    // rho_old = rho;
    //		#pragma omp single
    { rho_old = rho; }
    //	} // end of prgma omp parallel

  } // end of for iter
}

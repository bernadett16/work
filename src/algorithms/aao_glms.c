/*
 * aao_glms.c
 *
 *  Created on: Oct 10, 2014
 *      Author: misha
 */

#include "aao_algorithms.h"
#include "aao_matr_op.h"
#include "aao_operators.h"
#include "aao_vec_op.h"
#include "aao_wav_op.h"
#include <stdlib.h>

// here c,r are input and output variables
void aao_glms(float *c, float *r, aao_OperatorG const *G,
              aao_OperatorA const *A, aao_FEWHA const *fewha,
              aao_FEWHA_Parallel const *par) {
  // local variables
  int n_lay_glcs, nnz_diag_AwoD; // n_lay_glcs2
  int const *os_lay;
  int const *n_lay;
  int iLay, nLay;
  float *tmp_r_nz = NULL;
  float *tmp_v_nz = NULL;
  float *tmp_v_cs = NULL;
  float *tmp_q = NULL;
  float const *L = NULL;
  float const *D = NULL;
  int const *nz2lay = NULL;
  int const *glcs2nz = NULL;
  // TODO MY 11.10.2014 this should be put away in a different function
  int i, ics, inz, i1, i2;

  // local variables init
  tmp_r_nz = G->tmp_r_cs;
  tmp_v_nz = G->tmp_v_cs;
  tmp_v_cs = G->tmp_r_cs; // !!!! reuse the memory of r !!!!
  tmp_q = G->tmp_q;
  L = G->L_nz;
  D = G->D_nz;
  nz2lay = G->nz2lay;
  glcs2nz = G->glcs2nz;
  n_lay_glcs =
      fewha->n_lay_at_glcs; // in 1 dim; e.g.,16 (16^2 = 256 elements total)
  //	n_lay_glcs2   = n_lay_glcs*n_lay_glcs;		// uncompressed size of
  //LDL'
  nnz_diag_AwoD = G->nnz_diag_AwoD; // compressed size of LDL' (<=n_lay_glcs2)
  os_lay = fewha->os_lay_at_lay;
  n_lay = fewha->n_lay_at_lay;
  nLay = fewha->nLay;

  // code

  // 1. restriction, A_GLMS^{-1} * r, prolongation, accumulation to c all in one
  // step: this should be used if an explicit inverse of A_GLMS is computed:
  // aao_matrix_wav_vec_prod_ac(c, tmp_v, iA_GLMS, r, n_lay_glcs, n_lay[0]);
  // here i use cholesky
  // r_cs = Rr
  // TODO DELETE MY 12.10.2014
  //	for(i1=0; i1<n_lay_glcs; i1++) // i1,i2 read indices
  //	for(i2=0; i2<n_lay_glcs; i2++)
  //	{
  //		i   = n_lay[0]*i1+i2;
  //		ics = n_lay_glcs*i1+i2;
  //		// tmp_r_cs[ics] = r[i];
  //		innz = glcs2nnz[ics];		// write index
  //		if(innz != -1) 				// ics is an "acceptable index" (in
  //cropped diagAwoD) 			tmp_r_nnz[innz] = r[i];
  //	}
  for (inz = 0; inz < nnz_diag_AwoD; inz++)
    tmp_r_nz[inz] = r[nz2lay[inz]];

  // compute v=A_GLMS^{-1}*r
  chol_solve(tmp_v_nz, L, D, tmp_r_nz, nnz_diag_AwoD);

  // c = c + R'v
  for (i1 = 0; i1 < n_lay_glcs; i1++)
    for (i2 = 0; i2 < n_lay_glcs; i2++) {
      i = n_lay[0] * i1 + i2;
      ics = n_lay_glcs * i1 + i2;
      inz = glcs2nz[ics];
      if (inz == -1) // ics is a bad index
      {
        tmp_v_cs[ics] = 0.;
      } else // ics is an "acceptable index" (in cropped diagAwoD)
      {
        tmp_v_cs[ics] = tmp_v_nz[inz];
        c[i] += tmp_v_nz[inz];
      }
    }

  // 2. update the residual
  // q = A_{GLMS}v
  aao_applyA_GLMS(tmp_q, A, tmp_v_cs, fewha, par);

  // r -= q
  //#pragma omp parallel private(iLay) num_threads(par->max_thr_lay)
  //if(par->global) //n.lay
  //{
  //	#pragma omp for
  for (iLay = 0; iLay < nLay; iLay++) {
    aao_vec_mi_vec_ac(r + os_lay[iLay], tmp_q + os_lay[iLay],
                      n_lay[iLay] * n_lay[iLay]);
  }
  //}
}

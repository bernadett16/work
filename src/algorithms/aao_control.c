/*
 * polc.c
 *
 *  Created on: Jul 6, 2014
 *      Author: misha
 */

#include "aao_algorithms.h"
#include "aao_system.h"
#include <stdlib.h>

void aao_control(float *a, aao_Loop const *loop, aao_PCG const *pcg,
                 aao_OperatorA const *A, aao_OperatoriJ const *iJ,
                 aao_OperatorF const *F, aao_OperatorH const *H,
                 aao_OperatorG const *G, float *s, aao_FEWHA const *fewha,
                 aao_FEWHA_Parallel const *par) {
  // local var
  int nLay, nDM;
  int iLay, iDM;

  float *b_new = NULL, *b = NULL;
  float *r = NULL;
  float *c = NULL;
  float *a_old = NULL, *a_fit = NULL;

  int const *n_lay, *os_lay;
  int const *n_act, *os_act;

  aao_LoopType loop_type;
  int GLMS_step; // true or false
  float gain, leaky_int;
  int max_iter;

  // local var init
  nLay = fewha->nLay;
  nDM = fewha->nDM;

  b = loop->b; // b from old iteration, becomes b from this iteration
  r = loop->r; // r from old iteration, becomes r from this iteration
  c = loop->c; // c from old iteration, becomes c from this iteration

  b_new = loop->tmp_b; // b from this iteration, i.e., b_new = Bs

  a_old = loop->a_old; // a_old is a_{n-2}, a is a_{n-1}
  a_fit = loop->tmp_a; // mirror shapes coming from the fitting step

  n_lay = fewha->n_lay_at_lay;
  os_lay = fewha->os_lay_at_lay;
  n_act = fewha->n_act_at_dm;
  os_act = fewha->os_act_at_dm;

  loop_type = fewha->AOsys.loop_;
  GLMS_step =
      (fewha->param.useGLMS && fewha->param.J_glcs_ > 0); // == 0 means no GLMS
  gain = fewha->param.gain_;           // variable params of the method
  leaky_int = fewha->param.leaky_int_; // leaky integrator value
  max_iter = fewha->param.max_iter_;   // variable params of the method

  // code
  // 1) pseudo-open loop measurements are generated if necessary
  if (loop_type == ClosedLoop) {
    // Compute Pseudo Open Loop measurements
    // here we use a_old b/c of the two-step delay (i.e., a_{n-2})
    aao_applyH(s, H, a_old, fewha, par);
  }
  // else loop_type == OpenLoop => do nothing

  // 2) RHS vector computed
  // b_new = Bs
  aao_applyB(b_new, A, s, fewha, par);

  // 3) residual updated
  // r = b_new-Ac
  // b = b_new
  //#pragma omp parallel for private(iLay) num_threads(par->max_thr_lay)
  // if(par->global)
  for (iLay = 0; iLay < nLay; iLay++) {
    aao_update_r_b(r + os_lay[iLay], b + os_lay[iLay], b_new + os_lay[iLay],
                   n_lay[iLay] * n_lay[iLay]);
  }

  // 4) GLMS [optional]
  if (GLMS_step) {
    aao_glms(c, r, G, A, fewha, par);
  }

  // 5) PCG
  aao_pcg_jacobi(c, r, pcg, A, iJ, max_iter, fewha, par);

  // 6) mirror shapes computed
  // b_new serves as the tmp_lay variable for applyF
  aao_applyF(a_fit, F, c, b_new, fewha, par);

  // 7) control applied
  if (loop_type == ClosedLoop) {
    //#pragma omp parallel for private(iDM)                                          \
    num_threads(par->max_thr_DM) if (par->global && nDM > 1)
    for (iDM = 0; iDM < nDM; iDM++) {
      // TODO MY 06.07.2014 n_act[iDM]*n_act[iDM] assumes we use a square DM!!!
      aao_integrator_POLC(a + os_act[iDM], a_old + os_act[iDM],
                          a_fit + os_act[iDM], gain, leaky_int,
                          n_act[iDM] * n_act[iDM]);
    }
  } else // loop_type == OpenLoop
  {
    //#pragma omp parallel for private(iDM)                                          \
    num_threads(par->max_thr_DM) if (par->global && nDM > 1)
    for (iDM = 0; iDM < nDM; iDM++) {
      // TODO MY 06.07.2014 n_act[iDM]*n_act[iDM] assumes we use a square DM!!!
      aao_integrator_OL(a + os_act[iDM], a_fit + os_act[iDM], gain, leaky_int,
                        n_act[iDM] * n_act[iDM]);
    }
  }
}

void aao_update_r_b(float *r, float *b, float const *b_new, int n) {
  // b_delta = b_new - b
  // b = b_new;
  // r = b_new - A*c = r + b_new - b = r + delta_b
  int i;

  for (i = 0; i < n; i++) {
    r[i] += (b_new[i] - b[i]); // += delta_b[i]
    b[i] = b_new[i];
  }
}

void aao_integrator_POLC(float *a_old, float *a_old_old, float const *a,
                         float gain, float leaky_int, int n) {
  // a_delta = a_ol - loop.data.a_old_old;
  // a = leaky_int*loop.data.a_old + fm.loop_gain * a_delta;
  // loop.data.a_old_old = loop.data.a_old;
  float tmp;
  int i;

  for (i = 0; i < n; i++) {
    // MY 05.09.2014 added a leaky integrator
    tmp = leaky_int * a_old[i] + gain * (a[i] - a_old_old[i]);
    a_old_old[i] = a_old[i];
    a_old[i] = tmp;
  }
}
void aao_integrator_OL(float *a_old, float const *a, float gain,
                       float leaky_int, int n) {
  //[a_ol, c] = f_mcao_comp(s_ol, fm, loop.data.c_old, loop.data.a_old);
  // a         = (leaky_int-fm.loop_gain)*loop.data.a_old + fm.loop_gain*a_ol;
  // act       = fm.tool_a.a2act(a);
  int i;

  for (i = 0; i < n; i++) {
    // MY 05.09.2014 added a leaky integrator
    // a_old[i] = (leaky_int-gain)*a_old[i] + gain*a[i];
    a_old[i] = leaky_int * a_old[i] + gain * (a[i] - a_old[i]);
  }
}

/*
 * alg_init_free.c
 *
 *  Created on: Jul 27, 2014
 *      Author: misha
 */

#include <stdlib.h>
#include <stdio.h>
#include "aao_algorithms.h"
#include "aao_FEWHA.h"
#include "aao_print.h"

void aao_initPCG(aao_PCG * pcg, aao_FEWHA const * fewha)
{
	int n_tot_lay = fewha->n_tot_lay;

	pcg->tmp_z = (float*)calloc(n_tot_lay,sizeof(float));
	pcg->tmp_p = (float*)calloc(n_tot_lay,sizeof(float));
	pcg->tmp_q = (float*)calloc(n_tot_lay,sizeof(float));
}
void aao_freePCG(aao_PCG * pcg)
{
	if(pcg != NULL)
	{
		if(pcg->tmp_z != NULL)
		{
			free(pcg->tmp_z);
			pcg->tmp_z = NULL;
		}
		if(pcg->tmp_p != NULL)
		{
			free(pcg->tmp_p);
			pcg->tmp_p = NULL;
		}
		if(pcg->tmp_q != NULL)
		{
			free(pcg->tmp_q);
			pcg->tmp_q = NULL;
		}
	}
}

void aao_initLoop(aao_Loop * loop, aao_FEWHA const * fewha)
{
	int n_tot_act = fewha->n_tot_act;
	int n_tot_lay = fewha->n_tot_lay;

	// MY 05.09.2014 loop type should stay in fewha->AOsys->loop_!
	// loop->loop_type = fewha->AOsys->loop_;

	loop->a_old = (float*)calloc(n_tot_act, sizeof(float));

	loop->b = (float*)calloc(n_tot_lay, sizeof(float));
	loop->r = (float*)calloc(n_tot_lay, sizeof(float));
	loop->c = (float*)calloc(n_tot_lay, sizeof(float));

	loop->tmp_b = (float*)calloc(n_tot_lay, sizeof(float));
	loop->tmp_a = (float*)calloc(n_tot_act, sizeof(float));

}
void aao_freeLoop(aao_Loop * loop)
{
	if(loop != NULL)
	{
		if(loop->a_old != NULL)
		{
			free(loop->a_old);
			loop->a_old = NULL;
		}
		if(loop->b != NULL)
		{
			free(loop->b);
			loop->b = NULL;
		}
		if(loop->r != NULL)
		{
			free(loop->r);
			loop->r = NULL;
		}
		if(loop->c != NULL)
		{
			free(loop->c);
			loop->c = NULL;
		}
		if(loop->tmp_b != NULL)
		{
			free(loop->tmp_b);
			loop->tmp_b = NULL;
		}
		if(loop->tmp_a != NULL)
		{
			free(loop->tmp_a);
			loop->tmp_a = NULL;
		}
	}
}
void aao_printPCG(aao_PCG const * pcg, aao_FEWHA const * fewha)
{
	printf("===== FEWHA PCG struct ======\n");
	aao_print_vec_float_compact("tmp_z",pcg->tmp_z,fewha->n_tot_lay);
	aao_print_vec_float_compact("tmp_p",pcg->tmp_p,fewha->n_tot_lay);
	aao_print_vec_float_compact("tmp_q",pcg->tmp_q,fewha->n_tot_lay);
	printf("\n");
}
void aao_printLoop(aao_Loop const * loop, aao_FEWHA const * fewha)
{
	printf("===== FEWHA Loop struct ======\n");
	aao_print_vec_float_compact("a_old",loop->a_old,fewha->n_tot_act);
	aao_print_vec_float_compact("b",loop->b,fewha->n_tot_lay);
	aao_print_vec_float_compact("r",loop->r,fewha->n_tot_lay);
	aao_print_vec_float_compact("c",loop->c,fewha->n_tot_lay);
	aao_print_vec_float_compact("tmp_b",loop->tmp_b,fewha->n_tot_lay);
	aao_print_vec_float_compact("tmp_a",loop->tmp_a,fewha->n_tot_act);
	printf("\n");
}

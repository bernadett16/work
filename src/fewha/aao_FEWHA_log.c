/*
 * aao_FEWHA_log.c
 *
 *  Created on: Nov 28, 2014
 *      Author: misha
 */

#include <stdlib.h>
#include "aao_FEWHA_log.h"

void aao_zeroFEWHAlog(aao_FEWHA_Log * fewha_log)
{
	fewha_log->counter = 0;
	fewha_log->wav_coef = NULL;
	fewha_log->res_coef = NULL;
	fewha_log->lay_coef = NULL;
	fewha_log->dmshapes = NULL;
}



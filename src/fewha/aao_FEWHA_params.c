/*
 * FEWHA_layers.c
 *
 *  Created on: Jul 6, 2014
 *      Author: misha
 */


#include <stdlib.h>
#include <stdio.h>
#include "aao_FEWHA_params.h"
#include "aao_print.h"

int aao_setFEWHAparams(aao_FEWHA_Params * par, int n_layer, float const * d_lay, int const * J_lay, int J_glcs,
		float alpha, float alpha_eta, float alpha_J, float gain, float leaky_int, int max_iter, int useGLMS, int usePrecond, char *createPrecondFromFile)
{
	// local var
	int i;

	// code
	par->d_lay_ = NULL;
	par->J_lay_ = NULL;

	for(i=0; i<n_layer; i++)
		if(d_lay[i] < 0)
			return -1;		// distance between points must be positive

	par->d_lay_ = (float*) calloc(n_layer, sizeof(float));
	for(i=0; i<n_layer; i++)
		par->d_lay_[i] = d_lay[i];

	for(i=0; i<n_layer; i++)
		if(J_lay[i] < 1)
			return -2;		// at least one scale per layer

	par->J_lay_ = (int*) calloc(n_layer, sizeof(int));
		for(i=0; i<n_layer; i++)
			par->J_lay_[i] = J_lay[i];

	if (0 <= J_glcs && J_glcs <= J_lay[0])
		par->J_glcs_ = J_glcs;
	else
		return -3;			// ground layer coarse scales must be positive (0 means GLMS off)
							// but at most all scales of ground layer

	if (alpha >= 0)
		par->alpha_ = alpha;
	else
		return -4;	// alpha must be non-negative

	if (alpha_eta >= 0)
		par->alpha_eta_ = alpha_eta;
	else
		return -5;	// alpha_eta must be non-negative

	if(alpha_J >= 0)
		par->alpha_J_ = alpha_J;
	else
		return -6;	// alpha_J must be non-negative

	if(gain > 0)
		par->gain_ = gain;
	else
		return -7;	// gain must be positive

	if(leaky_int>0 && leaky_int <=1)
		par->leaky_int_ = leaky_int;
	else
		return -9;	// leaky integrator must be a value in (0,1]

	if(max_iter >= 0)
		par->max_iter_ = max_iter;
	else
		return -8;	// max iter must be non-negative

	par->useGLMS    = (useGLMS != 0);	 // value: either 0 or 1
	par->usePrecond = (usePrecond != 0); // value: either 0 or 1
    par->createPrecondFromFile = createPrecondFromFile;
	return 0;

}
int aao_copyFEWHAparams_(aao_FEWHA_Params * to, aao_FEWHA_Params const * from, int nLay)
{
	return aao_setFEWHAparams(to, nLay, from->d_lay_, from->J_lay_, from->J_glcs_, from->alpha_, from->alpha_eta_, from->alpha_J_,
			from->gain_, from->leaky_int_, from->max_iter_, from->useGLMS, from->usePrecond, from->createPrecondFromFile);
}
int aao_updateFEWHAparams_gain_(aao_FEWHA_Params * par, float gain)
{
	if(gain > 0)
		par->gain_ = gain;
	else
		return -7;	// gain must be positive

	return 0;
}

void aao_freeFEWHAparams(aao_FEWHA_Params * par)
{
	if(par != NULL)
	{
		if(par->d_lay_ != NULL)
		{
			free(par->d_lay_);
			par->d_lay_ = NULL;
		}
		if(par->J_lay_ != NULL)
		{
			free(par->J_lay_);
			par->J_lay_ = NULL;
		}
	}
}
void aao_printFEWHAparams(aao_FEWHA_Params const * fewha_par, int n_layer)
{
	printf("===== FEWHA params struct ======\n");
	aao_print_vec_float("d_lay_",fewha_par->d_lay_,n_layer);
	aao_print_vec_int("J_lay_",fewha_par->J_lay_,n_layer);
	printf("J_glcs_: %d\n",fewha_par->J_glcs_);
	printf("\n");

	printf("alpha_: %f\n",fewha_par->alpha_);
	printf("alpha_eta_: %f\n",fewha_par->alpha_eta_);
	printf("alpha_J_: %f\n",fewha_par->alpha_J_);
	printf("gain_: %f\n",fewha_par->gain_);
	printf("leaky_int_: %f\n",fewha_par->leaky_int_);
	printf("\n");

	printf("max_iter_: %d\n",fewha_par->max_iter_);
	printf("useGLMS: %d\n",fewha_par->useGLMS);
	printf("usePrecond: %d\n",fewha_par->usePrecond);
	printf("\n");
}


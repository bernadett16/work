/*
 * aao_FEWHA_parallel.c
 *
 *  Created on: Sep 5, 2014
 *      Author: misha
 */

#include <stdio.h>
#include "aao_FEWHA_parallel.h"
#include "aao_FEWHA_private.h"

int aao_setFEWHAparall(aao_FEWHA_Parallel * fewha_parall, int global, int max_thr_system)
{
	// code
	if (global == 0 || global == 1)
		fewha_parall->global = global;
	else
		return -1;	// global must be 0 or 1

	if(max_thr_system > 0)
		fewha_parall->max_thr_system = max_thr_system;
	else
		return -2; // max_thr_system must be at least 1

	// set all max threads to 1; will be initialized in initFEWHA
	fewha_parall->max_thr_applyA = 1;
	fewha_parall->max_thr_applyF = 1;
	fewha_parall->max_thr_applyH = 1;
	fewha_parall->max_thr_lay = 1;
	fewha_parall->max_thr_DM = 1;

	// TODO MY 05.09.2014 work on this:
	// set to 64, as this was found an optimal number of threads
	fewha_parall->n_dwt_pa = 64;

	return 0;
}
void aao_freeFEWHAparall(aao_FEWHA_Parallel * fewha_parall)
{
	// do nothing, as there is no dynamic memory
}

int aao_copyFEWHAparall_(aao_FEWHA_Parallel * to, aao_FEWHA_Parallel const * from)
{
	// no dynamic memory: copy by value
	*to = *from;
	return 0;
}

void aao_initFEWHAparall_(aao_FEWHA_Parallel * parall, aao_FEWHA const * fewha)
{
	// local var
	int nLay, nWFS, nDM, nDIR;
	int max_thr_system;		// maximum number of threads on the system
	int max_thr_applyA;		// e.g., min(max(nLay, nWFS),max_thr_system)
	int max_thr_applyF;		// e.g., min(max(nLay, nDIR),max_thr_system)
	int max_thr_applyH;		// e.g., min(nWFS,max_thr_system)
	int max_thr_lay; 		// layer operations, e.g., min(nLay,max_thr_system); used in e.g., pcg or control algorithms
	int max_thr_DM;			// DM operations, e.g., min(nDM,max_thr_system), used in control algorithm


	// local var init
	nLay = fewha->nLay;
	nWFS = fewha->nWFS;
	nDM  = fewha->nDM;
	nDIR = fewha->nDIR;
	max_thr_system = parall->max_thr_system;

	// code
	// note 1) the parameters will be initialized either way;
	// parallelization however can be fully controlled by either enabling or disabling global
	// note 2) each parameter (even if nDIR=0) below is >=1
	max_thr_applyA = (nLay > nWFS) ? nLay : nWFS; // max(nLay,nWFS)
	max_thr_applyF = (nLay > nDIR) ? nLay : nDIR; // max(nLay,nDIR)
	max_thr_applyH = nWFS;
	max_thr_lay    = nLay;
	max_thr_DM     = nDM;

	// min(...,max_thr_system)
	parall->max_thr_applyA = (max_thr_applyA < max_thr_system) ? max_thr_applyA : max_thr_system;
	parall->max_thr_applyF = (max_thr_applyF < max_thr_system) ? max_thr_applyF : max_thr_system;
	parall->max_thr_applyH = (max_thr_applyH < max_thr_system) ? max_thr_applyH : max_thr_system;
	parall->max_thr_lay    = (max_thr_lay    < max_thr_system) ? max_thr_lay    : max_thr_system;
	parall->max_thr_DM     = (max_thr_DM     < max_thr_system) ? max_thr_DM     : max_thr_system;

}
void aao_printFEWHAparall(aao_FEWHA_Parallel const * fewha_parall)
{
	printf("===== FEWHA parallel struct ======\n");
	printf("global: %d\n",fewha_parall->global);
	printf("max_thr_system: %d\n",fewha_parall->max_thr_system);
	printf("\n");

	printf("max_thr_applyA: %d\n",fewha_parall->max_thr_applyA);
	printf("max_thr_applyF: %d\n",fewha_parall->max_thr_applyF);
	printf("max_thr_applyH: %d\n",fewha_parall->max_thr_applyH);
	printf("max_thr_lay: %d\n",fewha_parall->max_thr_lay);
	printf("max_thr_DM: %d\n",fewha_parall->max_thr_DM);
	printf("n_dwt_pa: %d\n",fewha_parall->n_dwt_pa);
	printf("\n");
}



/*
 * FEWHA.c
 *
 *  Created on: Jul 6, 2014
 *      Author: misha
 */

#include <math.h>
#include <stdio.h> // for aao_printFEWHAconfig, aao_*FEWHAlog
#include <stdlib.h>
#include <string.h> // strcpy, strcat
#include <time.h>   // aao_*FEWHAlog
//#include "aao_FEWHA.h"
#include "aao_FEWHA_private.h"
#include "aao_algorithms.h"
#include "aao_operators.h"
#include "aao_print.h"
#include "aao_proj_op.h"
#include "aao_system_private.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846264338327950288 /* pi */
#endif

// ===== Local Functions =====
void aao_initFEWHAstruct_(aao_FEWHA *fewha, aao_System const *AOsys,
                          aao_FEWHA_Params const *param,
                          aao_FEWHA_Parallel const *parall);
void aao_initFEWHA_x0_lay_(aao_FEWHA *fewha);
void aao_initFEWHAcomponents_(aao_FEWHA *fewha);
void aao_freeFEWHAstruct_(aao_FEWHA *fewha);
void aao_freeFEWHAcomponents_(aao_FEWHA *fewha);
void aao_setFEWHAlogpath_(char *path, char const *dir, char const *prefix,
                          char const *datestr, char const *name);
void aao_writeFEWHAlogfile_(FILE *file, int n, float *vec);

// ===========================

void aao_initFEWHA(aao_FEWHA *fewha, aao_System const *AOsys,
                   aao_FEWHA_Params const *param,
                   aao_FEWHA_Parallel const *parall) {
  // 1. init FEWHA struct (dimensions, parameters)
  // not much computation involved
  aao_initFEWHAstruct_(fewha, AOsys, param, parall);

  // 2. init FEWHA components
  // may be computationally heavy
  aao_initFEWHAcomponents_(fewha);
}

void aao_freeFEWHA(aao_FEWHA *fewha) {
  // 1. free FEWHA components
  aao_freeFEWHAcomponents_(fewha);

  // 2. free FEWHA struct (dimensions, parameters)
  aao_freeFEWHAstruct_(fewha);
}

void aao_runFEWHA(float *dmshapes, float *slopes, aao_FEWHA *fewha) {
  // TODO MY 22.08.2014 at the moment the slopes are ordered according to
  // internal WFS ordering; in the future they should be ordered according to
  // external WFS ordering, otherwise BUG!!!!
  clock_t start, end;
  double cpu_time_used;
  start = clock();
  aao_control(dmshapes, &fewha->loop, &fewha->pcg, &fewha->A, &fewha->iJ,
              &fewha->F, &fewha->H, &fewha->G, slopes, fewha, &fewha->parall);
  end = clock();
  printf("Measured Time: %f\n\n", ((double)(end - start)) / CLOCKS_PER_SEC);
}
void aao_invFEWHA(float *slopes, float const *dmshapes, aao_FEWHA *fewha) {
  // local var
  int i;

  // slopes = zeros(n_tot_meas,1);
  for (i = 0; i < fewha->n_tot_meas; i++)
    slopes[i] = 0.;

  // slopes += H*dmshapes
  aao_applyH(slopes, &fewha->H, dmshapes, fewha, &fewha->parall);
}
void aao_resetFEWHAloop(float *dmshapes, aao_FEWHA *fewha) {
  // local var
  int i;

  // code
  for (i = 0; i < fewha->n_tot_act; i++) {
    dmshapes[i] = 0.;
    fewha->loop.a_old[i] = 0.;
  }
  // reset the warm restart
  for (i = 0; i < fewha->n_tot_lay; i++) {
    fewha->loop.b[i] = 0.;
    fewha->loop.c[i] = 0.;
    fewha->loop.r[i] = 0.;
  }
}
int aao_updateFEWHAgain(aao_FEWHA *fewha, float gain) {
  // parameter can be updated on the fly without any re-computations
  int ret = 0;

  ret = aao_updateFEWHAparams_gain_(&fewha->param, gain);

  return ret;
}

void aao_initFEWHAstruct_(aao_FEWHA *fewha, aao_System const *AOsys,
                          aao_FEWHA_Params const *param,
                          aao_FEWHA_Parallel const *parall) {
  // local var
  int nWFS, nLGS, nNGS, nTTS, nLay, nDM, nDIR;
  int iWFS, iLGS, iNGS, iTTS, iLay, iDM, i;

  int const *i2e = NULL; // wfs_indx_int2ext

  int os, os2, n, n_sub_at_wfs_max;
  double d;

  // code

  // Set AO System config and the fewha param config
  aao_copySystem_(&fewha->AOsys, AOsys);
  aao_copyFEWHAparams_(&fewha->param, param, AOsys->atmosphere_.n_layer_);
  aao_copyFEWHAparall_(&fewha->parall, parall);

  // set log to 0
  aao_zeroFEWHAlog(&fewha->log);

  // Determine system dimensions
  nWFS = AOsys->n_wfs_;
  nLGS = 0;
  nNGS = 0;
  nTTS = 0;

  n_sub_at_wfs_max = 0;
  for (iWFS = 0; iWFS < nWFS;
       iWFS++) // find max number of subapertures of the WFS - for TT
    if (AOsys->wfs_[iWFS].n_sub_ > n_sub_at_wfs_max)
      n_sub_at_wfs_max = AOsys->wfs_[iWFS].n_sub_;

  for (iWFS = 0; iWFS < nWFS; iWFS++) {
    nLGS += (AOsys->wfs_[iWFS].gs_.type_ == LGS) ? 1 : 0;
    nNGS += (AOsys->wfs_[iWFS].gs_.type_ == NGS) ? 1 : 0;
    nTTS += (AOsys->wfs_[iWFS].gs_.type_ == NGS &&
             AOsys->wfs_[iWFS].n_sub_ < n_sub_at_wfs_max)
                ? 1
                : 0; // TTS
  }
  nNGS = nNGS - nTTS; // nNGS contains also TTS

  nLay = AOsys->atmosphere_.n_layer_;
  nDM = AOsys->n_dm_;
  if (AOsys->sys_type_ == SCAO || (AOsys->sys_type_ == MCAO && nLay == nDM))
    nDIR = 0;
  else if (AOsys->sys_type_ == LTAO)
    nDIR = 1;
  else if (AOsys->sys_type_ == MOAO)
    nDIR = AOsys->n_target_;
  else if (AOsys->sys_type_ == MCAO && nLay > nDM)
    // TODO MY 24.07.2014 handle the optimization case!!!
    nDIR = 0; // at the moment unhandled!
  else
    nDIR = 0; // for completeness sake... all case *are* covered above

  // Set system dimensions
  fewha->nWFS = nWFS;
  fewha->nLGS = nLGS;
  fewha->nNGS = nNGS;
  fewha->nTTS = nTTS;
  fewha->nLay = nLay;
  fewha->nDM = nDM;
  fewha->nDIR = nDIR;

  // Determine the order of WFS
  fewha->wfs_indx_int2ext = (int *)calloc(nWFS, sizeof(int));
  fewha->wfs_indx_ext2int = (int *)calloc(nWFS, sizeof(int));
  iLGS = 0;
  iNGS = 0;
  iTTS = 0;
  for (iWFS = 0; iWFS < nWFS; iWFS++) {
    if (AOsys->wfs_[iWFS].gs_.type_ == LGS) {
      fewha->wfs_indx_int2ext[iLGS] = iWFS;
      fewha->wfs_indx_ext2int[iWFS] = iLGS;
      iLGS++;
    }
    if (AOsys->wfs_[iWFS].gs_.type_ == NGS &&
        AOsys->wfs_[iWFS].n_sub_ < n_sub_at_wfs_max) // TTS
    {
      fewha->wfs_indx_int2ext[nLGS + nNGS + iTTS] = iWFS;
      fewha->wfs_indx_ext2int[iWFS] = nLGS + nNGS + iTTS;
      iTTS++;
    } else if (AOsys->wfs_[iWFS].gs_.type_ == NGS) // NGS
    {
      fewha->wfs_indx_int2ext[nLGS + iNGS] = iWFS;
      fewha->wfs_indx_ext2int[iWFS] = nLGS + iNGS;
      iNGS++;
    }
  }
  i2e = fewha->wfs_indx_int2ext;

  // Process subaperture vector sizes
  fewha->n_sub_at_wfs = (int *)calloc(nWFS, sizeof(int));
  fewha->os_sub_at_wfs = (int *)calloc(nWFS, sizeof(int));
  fewha->os_meas_at_wfs = (int *)calloc(nWFS, sizeof(int));

  os = 0, os2 = 0;
  for (iWFS = 0; iWFS < nWFS; iWFS++) {
    n = AOsys->wfs_[i2e[iWFS]].n_sub_;

    fewha->n_sub_at_wfs[iWFS] = n;
    fewha->os_sub_at_wfs[iWFS] = os;
    fewha->os_meas_at_wfs[iWFS] = os2;

    os += n * n;      // e.g., 84*84
    os2 += 2 * n * n; // e.g., 2*84*84
  }

  fewha->n_tot_meas = os2;

  // 04.12.2014 bug found by ali: if nLGS == nWFS then
  // fewha->os_sub_at_wfs[nLGS] is accessed out of bounds fewha->n_tot_sub_lgs =
  // fewha->os_sub_at_wfs[nLGS]; fix: (to be on the safe-side) count just the
  // LGS subapertures again
  os = 0;
  for (iLGS = 0; iLGS < nLGS; iLGS++) {
    n = AOsys->wfs_[i2e[iLGS]].n_sub_;
    os += n * n; // e.g., 84*84
  }
  fewha->n_tot_sub_lgs = os;

  // Process wavefront vector sizes
  fewha->n_phi_at_wfs = (int *)calloc(nWFS, sizeof(int));
  fewha->os_phi_at_wfs = (int *)calloc(nWFS, sizeof(int));

  os = 0;
  for (iWFS = 0; iWFS < nLGS + nNGS;
       iWFS++) // restrict to LGS and NGS, without TTS
  {
    n = AOsys->wfs_[i2e[iWFS]].n_sub_ + 1; // e.g., 84+1

    fewha->n_phi_at_wfs[iWFS] = n;
    fewha->os_phi_at_wfs[iWFS] = os;

    os += n * n; // e.g., 85*85
  }

  for (iWFS = nLGS + nNGS; iWFS < nWFS;
       iWFS++) // TTS, use the geometry of the first WFS for TTS (a virtual grid
               // for the incoming wave)
  {
    n = AOsys->wfs_[i2e[0]].n_sub_ + 1; // e.g., 84+1

    fewha->n_phi_at_wfs[iWFS] = n;
    fewha->os_phi_at_wfs[iWFS] = os;

    os += n * n; // e.g., 85*85
  }
  fewha->n_tot_phi = os;

  // Process layer vector sizes
  fewha->n_lay_at_lay = (int *)calloc(nLay, sizeof(int));
  fewha->os_lay_at_lay = (int *)calloc(nLay, sizeof(int));

  os = 0;
  for (iLay = 0; iLay < nLay; iLay++) {
    n = (int)pow(2, param->J_lay_[iLay]); // 2^J

    fewha->n_lay_at_lay[iLay] = n;
    fewha->os_lay_at_lay[iLay] = os;

    os += n * n; // e.g., 128*128
  }
  if (param->J_glcs_ > 0)
    fewha->n_lay_at_glcs = (int)pow(2, param->J_glcs_); // e.g., 2^4 = 16
  else
    fewha->n_lay_at_glcs = 0;
  fewha->n_tot_lay = os;

  // Process DM vector sizes
  fewha->n_act_at_dm = (int *)calloc(nDM, sizeof(int));
  fewha->os_act_at_dm = (int *)calloc(nDM, sizeof(int));
  fewha->d_act_at_dm = (float *)calloc(nDM, sizeof(float));

  os = 0;
  for (iDM = 0; iDM < nDM; iDM++) {
    if (AOsys->dm_[iDM].isSquare) {
      n = AOsys->dm_[iDM].n_act_;

      fewha->n_act_at_dm[iDM] = n;
      fewha->os_act_at_dm[iDM] = os;

      // Compute avg actuator spacing
      d = 0.;
      for (i = 0; i < n - 1; i++) {
        d += (AOsys->dm_[iDM].actpos_x_[i + 1] - AOsys->dm_[iDM].actpos_x_[i]);
        d += (AOsys->dm_[iDM].actpos_y_[i + 1] - AOsys->dm_[iDM].actpos_y_[i]);
      }
      d /= (2. * (n - 1));
      fewha->d_act_at_dm[iDM] = (float)d; // e.g., 0.5 meters

      os += n * n; // e.g., 85*85
    } else {
      // TODO MY 06.07.2014 Arbitrary DM: at the moment UNHANDLED!
    }
  }
  fewha->n_tot_act = os;

  // Determine the corner point (x0,x0) to position the layer
  aao_initFEWHA_x0_lay_(fewha);
}
void aao_freeFEWHAstruct_(aao_FEWHA *fewha) {
  if (fewha != NULL) {
    if (fewha->n_sub_at_wfs != NULL) {
      free(fewha->n_sub_at_wfs);
      fewha->n_sub_at_wfs = NULL;
    }
    if (fewha->os_sub_at_wfs != NULL) {
      free(fewha->os_sub_at_wfs);
      fewha->os_sub_at_wfs = NULL;
    }
    if (fewha->os_meas_at_wfs != NULL) {
      free(fewha->os_meas_at_wfs);
      fewha->os_meas_at_wfs = NULL;
    }

    if (fewha->n_phi_at_wfs != NULL) {
      free(fewha->n_phi_at_wfs);
      fewha->n_phi_at_wfs = NULL;
    }
    if (fewha->os_phi_at_wfs != NULL) {
      free(fewha->os_phi_at_wfs);
      fewha->os_phi_at_wfs = NULL;
    }

    if (fewha->n_lay_at_lay != NULL) {
      free(fewha->n_lay_at_lay);
      fewha->n_lay_at_lay = NULL;
    }
    if (fewha->os_lay_at_lay != NULL) {
      free(fewha->os_lay_at_lay);
      fewha->os_lay_at_lay = NULL;
    }
    if (fewha->x0_lay_at_lay != NULL) {
      free(fewha->x0_lay_at_lay);
      fewha->x0_lay_at_lay = NULL;
    }

    if (fewha->n_act_at_dm != NULL) {
      free(fewha->n_act_at_dm);
      fewha->n_act_at_dm = NULL;
    }
    if (fewha->os_act_at_dm != NULL) {
      free(fewha->os_act_at_dm);
      fewha->os_act_at_dm = NULL;
    }
    if (fewha->d_act_at_dm != NULL) {
      free(fewha->d_act_at_dm);
      fewha->d_act_at_dm = NULL;
    }

    aao_freeSystem(&fewha->AOsys);
    aao_freeFEWHAparams(&fewha->param);
    aao_freeFEWHAparall(&fewha->parall);
    aao_closeFEWHAlog(fewha);

    if (fewha->wfs_indx_int2ext != NULL) {
      free(fewha->wfs_indx_int2ext);
      fewha->wfs_indx_int2ext = NULL;
    }
    if (fewha->wfs_indx_ext2int != NULL) {
      free(fewha->wfs_indx_ext2int);
      fewha->wfs_indx_ext2int = NULL;
    }
  }
}
void aao_initFEWHA_x0_lay_(aao_FEWHA *fewha) {
  // local var
  int nLay, iDIR, nWFS, nDM;
  int nLGS, nNGS;
  int iLay, nDIR, iWFS, iWFS_ext;
  double d, x0;
  int n;
  double h, H;
  double cone_fct;
  int const *int2ext = fewha->wfs_indx_int2ext; // External WFS ordering
  double const ARCMIN_TO_RAD = M_PI / 180. / 60.;
  double theta_x, theta_y;
  double pos_x, pos_y, prj_x, prj_y;
  double tmp_x0;

  // for MR_magic_selection_method
  double max_abs_pos, max_theta, fovadd;

  int MR_magic_selection_method = 0;

  // local var init
  nLay = fewha->nLay;
  nWFS = fewha->nWFS;
  nDM = fewha->nDM;
  nDIR = fewha->nDIR;
  H = fewha->AOsys.atmosphere_.na_height_;

  nLGS = fewha->nLGS;
  nNGS = fewha->nNGS;

  // Determine the corner point (x0,x0) to position the layer
  fewha->x0_lay_at_lay = (float *)calloc(nLay, sizeof(float));

  if (fewha->AOsys.sys_type_ == SCAO ||
      (fewha->AOsys.sys_type_ == MCAO &&
       nLay == nDM)) // SR: layer heights need to be taken from DM heights
  {
    // Layers grids are determined via DM grids
    for (iLay = 0; iLay < nLay; iLay++) {
      d = fewha->d_act_at_dm[iLay]; // SR: same number of layers and mirrors!!
      n = fewha->n_act_at_dm[iLay];
      x0 = -(n - 1) * d /
           2.; // SR: left boundary of layer grid respective coordinate of first
               // node. In case of MCAO is the size of the layer equal to the
               // size of the dm?? To small!!??
      fewha->x0_lay_at_lay[iLay] = (float)x0;
    }
  } else if ((fewha->AOsys.sys_type_ == LTAO ||
              fewha->AOsys.sys_type_ == MOAO) &&
             !MR_magic_selection_method) // the "right way", but not perfect
                                         // results
  {
    // This is the *right* way to do it:
    // on each layer we look for the minimum of projected WFS domain coordinates
    // and projected DM domains (if applicable)
    // after we find them, we set the layer corner point to that

    // Ground layer is always determined via ground WF grid (special case of the
    // alt. layer computation) Altitutde layers are determined by the condition:
    // Union(\Omega_{g\ell}) union Union(\Omega_{i\ell}) \subseteq \Omega_\ell
    for (iLay = 0; iLay < nLay; iLay++) {
      h = fewha->AOsys.atmosphere_.height_layer_[iLay];

      x0 = 0.;
      for (iWFS = 0; iWFS < nWFS; iWFS++) {
        // External WFS numbering
        iWFS_ext = int2ext[iWFS];

        // cone effect
        if (fewha->AOsys.wfs_[iWFS_ext].gs_.type_ == LGS)
          cone_fct = (1. - h / H);
        else
          cone_fct = 1.;

        // Guide star positions
        pos_x = fewha->AOsys.wfs_[iWFS_ext].gs_.pos_x_;
        pos_y = fewha->AOsys.wfs_[iWFS_ext].gs_.pos_y_;

        // Compute left-post point of WF grid
        if (iWFS < nLGS + nNGS)
          d = fewha->AOsys.wfs_[iWFS_ext].subap_size_;
        else // use geometry of first WFS for TT
          d = fewha->AOsys.wfs_[int2ext[0]].subap_size_;

        n = fewha->n_phi_at_wfs[iWFS];
        tmp_x0 = -(n - 1) * d / 2.; // coordinates: (tmp_x0, tmp_x0)

        // Project the left-most point to layer in GS direction:
        // (tmp_x0, tmp_x0) -> (prj_x, prj_y)
        theta_x = tan(pos_x * ARCMIN_TO_RAD);
        theta_y = tan(pos_y * ARCMIN_TO_RAD);
        prj_x = cone_fct * tmp_x0 + theta_x * h;
        prj_y = cone_fct * tmp_x0 + theta_y * h;

        // Compute minimum of (x0, prj_x, prj_y)
        x0 = (prj_x < x0) ? prj_x : x0;
        x0 = (prj_y < x0) ? prj_y : x0;
      }
      for (iDIR = 0; iDIR < nDIR; iDIR++) {
        // only NGS-type projections
        cone_fct = 1.;

        // Target star star positions
        pos_x = fewha->AOsys.target_[iDIR].pos_x_;
        pos_y = fewha->AOsys.target_[iDIR].pos_y_;

        // Compute left-post point of DM grid
        // LTAO and MOAO its the configs of the ground DM
        d = fewha->d_act_at_dm[0];
        n = fewha->n_act_at_dm[0];
        tmp_x0 = -(n - 1) * d / 2.;

        // Project the left-most point to layer in target direction:
        // (tmp_x0, tmp_x0) -> (prj_x, prj_y)
        theta_x = tan(pos_x * ARCMIN_TO_RAD);
        theta_y = tan(pos_y * ARCMIN_TO_RAD);
        prj_x = cone_fct * tmp_x0 + theta_x * h;
        prj_y = cone_fct * tmp_x0 + theta_y * h;

        // Compute minimum of (x0, prj_x, prj_y)
        x0 = (prj_x < x0) ? prj_x : x0;
        x0 = (prj_y < x0) ? prj_y : x0;
      }
      //		// MY 10.08.2014 a small trick: (shift by two units to
      //the left)
      //		// TODO MY 10.08.2014 this is a "cheap" trick...
      //		// the cheap trick gives a small improvement: 13.87 LE
      //-> 13.96 LE strehl 		if (iLay>0) 			x0 = x0 - 2*fewha->param->d_lay_[iLay];
      fewha->x0_lay_at_lay[iLay] = (float)x0;
    }
  } else if ((fewha->AOsys.sys_type_ == LTAO ||
              fewha->AOsys.sys_type_ == MOAO) &&
             MR_magic_selection_method) // the "matthias magic way",
  {
    // This is the strange MR way to do it, which gives decent results

    // On layer one, the grid is determined via the aperture of the telescope
    iLay = 0;
    {
      // left-corner of the telescope aperture
      x0 = -fewha->AOsys.teldiam_ / 2.;
      fewha->x0_lay_at_lay[iLay] = (float)x0;
    }

    // On altitutde layers we compute the largest separation, then using this we
    // compute the layer grid positions
    for (iLay = 1; iLay < nLay; iLay++) {
      // height and spacing of layers
      h = fewha->AOsys.atmosphere_.height_layer_[iLay];
      d = fewha->param.d_lay_[iLay];

      // 1. compute the widest position away from zenith
      max_abs_pos = 0.;
      for (iWFS = 0; iWFS < nWFS; iWFS++) {
        // External WFS numbering
        iWFS_ext = int2ext[iWFS];

        // Guide star positions
        pos_x = fewha->AOsys.wfs_[iWFS_ext].gs_.pos_x_;
        pos_y = fewha->AOsys.wfs_[iWFS_ext].gs_.pos_y_;

        // compute max_i |pos_i|
        max_abs_pos = fmax(max_abs_pos, fabs(pos_x));
        max_abs_pos = fmax(max_abs_pos, fabs(pos_y));
      }
      for (iDIR = 0; iDIR < nDIR; iDIR++) {
        // Target star positions
        pos_x = fewha->AOsys.target_[iDIR].pos_x_;
        pos_y = fewha->AOsys.target_[iDIR].pos_y_;

        // compute max_i |pos_i|
        max_abs_pos = fmax(max_abs_pos, fabs(pos_x));
        max_abs_pos = fmax(max_abs_pos, fabs(pos_y));
      }

      // FoV = 2*max_abs_pos (widest field of observation)
      // vector (max_theta,0,1) is a vector pointing farthest away from the
      // zenith
      max_theta = tan(2 * max_abs_pos * ARCMIN_TO_RAD);
      // prj = cone_fct*0 + max_theta*h is the projection of (0,0) in direction
      // (max_theta,0,1) n=prj/d is the number of points away from 0 (???)
      fovadd = (max_theta * h) / d;
      // and here the real magic happens: 5? 4/3?
      // n is now the total number of points needed to represent a layer
      // (we of course extend this number of points to 2^(J-1) < n <= 2^J)
      n = round(fewha->AOsys.teldiam_ / d) + 5 + round(4 / 3 * fovadd);

      // n-1 is the number of subintervals between n points
      // we center it around 0 and use the left-most coordinate point
      x0 = -(n - 1) * d / 2.;
      fewha->x0_lay_at_lay[iLay] = (float)x0;
    }

  } else if (fewha->AOsys.sys_type_ == MCAO && nLay > nDM) {
    // TODO MY 10.08.2014 CASE UNHANDLED!!!
    free(fewha->x0_lay_at_lay);
    fewha->x0_lay_at_lay = NULL;
  }
}
void aao_initFEWHAcomponents_(aao_FEWHA *fewha) {
  aao_initFEWHAparall_(&fewha->parall, fewha);

  aao_initPCG(&fewha->pcg, fewha);
  aao_initLoop(&fewha->loop, fewha);

  aao_initA(&fewha->A, fewha);
  aao_initiJ(&fewha->iJ, fewha, &fewha->A, &fewha->parall, fewha->param.createPrecondFromFile);
  aao_initG(&fewha->G, fewha, &fewha->A, &fewha->parall);
  aao_initH(&fewha->H, fewha, &fewha->A);
  aao_initF(&fewha->F, fewha, &fewha->A);
}

void aao_freeFEWHAcomponents_(aao_FEWHA *fewha) {
  aao_freePCG(&fewha->pcg);
  aao_freeLoop(&fewha->loop);

  aao_freeA(&fewha->A, fewha);
  aao_freeiJ(&fewha->iJ);
  aao_freeG(&fewha->G);
  aao_freeH(&fewha->H, fewha);
  aao_freeF(&fewha->F, fewha);
}
void aao_printFEWHAversion(char *ver) {
  if (ver == NULL) {
    printf("FEWHA version: %s, release date: %s\n", aao_FEWHA_VERSION,
           aao_FEWHA_DATE);
  } else {
    strcpy(ver, aao_FEWHA_VERSION);
  }
}

void aao_printFEWHAconfig(aao_FEWHA const *fewha) {
  printf("=========================\n");
  printf("== FEWHA configuration ==\n");
  printf("=========================\n\n");

  aao_printFEWHAversion(NULL);
  printf("\n");

  aao_printSystem(&fewha->AOsys);
  aao_printFEWHAparams(&fewha->param, fewha->nLay);
  aao_printFEWHAparall(&fewha->parall);

  printf("===== FEWHA struct ======\n");
  printf("nWFS: %d\n", fewha->nWFS);
  printf("nLGS: %d\n", fewha->nLGS);
  printf("nNGS: %d\n", fewha->nNGS);
  printf("nTTS: %d\n", fewha->nTTS);
  printf("nLay: %d\n", fewha->nLay);
  printf(" nDM: %d\n", fewha->nDM);
  printf("nDIR: %d\n", fewha->nDIR);
  printf("\n");

  printf("n_tot_meas: %d (total number of measurements (twice the number of "
         "all subapertures))\n",
         fewha->n_tot_meas);
  printf("n_tot_phi: %d (total number of wavefront coefficients of the system "
         "(in bilinear domain))\n",
         fewha->n_tot_phi);
  printf("n_tot_sub_lgs: %d (total number of sub-apertures of all LGS)\n",
         fewha->n_tot_sub_lgs);
  printf("n_tot_lay: %d (total number of coefficients to represent all layers "
         "(in bilinear & wavelet domains))\n",
         fewha->n_tot_lay);
  printf("n_tot_act: %d (total number of coefficients to represent all "
         "actuators (in bilinear domain))\n",
         fewha->n_tot_act);
  printf("\n");

  aao_print_vec_int("n_sub_at_wfs", fewha->n_sub_at_wfs, fewha->nWFS);
  aao_print_vec_int("os_sub_at_wfs", fewha->os_sub_at_wfs, fewha->nWFS);
  aao_print_vec_int("os_meas_at_wfs", fewha->os_meas_at_wfs, fewha->nWFS);
  printf("\n");

  aao_print_vec_int("n_phi_at_wfs", fewha->n_phi_at_wfs, fewha->nWFS);
  aao_print_vec_int("os_phi_at_wfs", fewha->os_phi_at_wfs, fewha->nWFS);
  printf("\n");

  aao_print_vec_int("n_lay_at_lay", fewha->n_lay_at_lay, fewha->nLay);
  aao_print_vec_int("os_lay_at_lay", fewha->os_lay_at_lay, fewha->nLay);
  aao_print_vec_float("x0_lay_at_lay", fewha->x0_lay_at_lay, fewha->nLay);
  printf("n_lay_at_glcs: %d\n", fewha->n_lay_at_glcs);
  printf("\n");

  aao_print_vec_int("n_act_at_dm", fewha->n_act_at_dm, fewha->nDM);
  aao_print_vec_int("os_act_at_dm", fewha->os_act_at_dm, fewha->nDM);
  aao_print_vec_float("d_act_at_dm", fewha->d_act_at_dm, fewha->nDM);
  printf("\n");

  aao_print_vec_int("wfs_indx_int2ext", fewha->wfs_indx_int2ext, fewha->nWFS);
  aao_print_vec_int("wfs_indx_ext2int", fewha->wfs_indx_ext2int, fewha->nWFS);
  printf("\n");

  aao_printPCG(&fewha->pcg, fewha);
  aao_printLoop(&fewha->loop, fewha);

  aao_printA(&fewha->A, fewha);
  aao_printiJ(&fewha->iJ, fewha);
  aao_printG(&fewha->G, fewha);
  aao_printH(&fewha->H, fewha);
  aao_printF(&fewha->F, fewha);

  printf("==========================\n");
  printf("== End of configuration ==\n");
  printf("==========================\n\n");
}

void aao_setFEWHAlogpath_(char *path, char const *dir, char const *prefix,
                          char const *datestr, char const *name) {
  // filesystem separator
#ifdef _WIN32
  char const sep[] = "\\";
#else
  char const sep[] = "/";
#endif

  // generate string of the type:
  // /home/user/data/LTAO_2014_11_28-08_30_wav_coef.txt
  // dir = "/home/user/data"
  // sep = "/"
  // prefix = "LTAO"
  // datestr = "2014_11_28-08_30"
  // name = "wav_coef"
  strcpy(path, dir);
  strcat(path, sep);
  strcat(path, prefix);
  strcat(path, "_");
  strcat(path, datestr);
  strcat(path, "_");
  strcat(path, name);
  strcat(path, ".txt");
}

int aao_openFEWHAlog(aao_FEWHA *fewha, const char *dir, const char *prefix) {
  // filename declarations
  char const name_config[] = "config";
  char const name_wav_coef[] = "wav_coef";
  char const name_res_coef[] = "res_coef";
  char const name_lay_coef[] = "lay_coef";
  char const name_dmshapes[] = "dmshapes";

  // local var
  int status = 0;
  FILE *log_config = NULL;
  char path_config[1024] = "";
  char path_wav_coef[1024] = "";
  char path_res_coef[1024] = "";
  char path_lay_coef[1024] = "";
  char path_dmshapes[1024] = "";

  char datestr[64] = "";
  time_t now;
  struct tm *now_st = NULL;

  int nLay, nDM;
  int iLay, iDM;

  // local var init
  nLay = fewha->nLay;
  nDM = fewha->nDM;

  // reset counter to 0
  fewha->log.counter = 0;

  // now
  time(&now);
  now_st = localtime(&now);
  sprintf(datestr, "%d_%02d_%02d-%02d_%02d_%02d",
          now_st->tm_year + 1900, // 2014
          now_st->tm_mon + 1,     // 11 (november)
          now_st->tm_mday,        // 28
          now_st->tm_hour,        // 09
          now_st->tm_min,         // 05
          now_st->tm_sec);        // 00

  // set paths
  aao_setFEWHAlogpath_(path_config, dir, prefix, datestr, name_config);
  aao_setFEWHAlogpath_(path_wav_coef, dir, prefix, datestr, name_wav_coef);
  aao_setFEWHAlogpath_(path_res_coef, dir, prefix, datestr, name_res_coef);
  aao_setFEWHAlogpath_(path_lay_coef, dir, prefix, datestr, name_lay_coef);
  aao_setFEWHAlogpath_(path_dmshapes, dir, prefix, datestr, name_dmshapes);

  // file pointers
  log_config = fopen(path_config, "w");
  fewha->log.wav_coef = fopen(path_wav_coef, "w");
  fewha->log.res_coef = fopen(path_res_coef, "w");
  fewha->log.lay_coef = fopen(path_lay_coef, "w");
  fewha->log.dmshapes = fopen(path_dmshapes, "w");

  // check that all files were open
  if (log_config == NULL) {
    printf("Error in aao_openFEWHAlog: unable to open file %s", path_config);
    status = -1;
  }
  if (fewha->log.wav_coef == NULL) {
    printf("Error in aao_openFEWHAlog: unable to open file %s", path_wav_coef);
    status = -1;
  }
  if (fewha->log.res_coef == NULL) {
    printf("Error in aao_openFEWHAlog: unable to open file %s", path_res_coef);
    status = -1;
  }
  if (fewha->log.lay_coef == NULL) {
    printf("Error in aao_openFEWHAlog: unable to open file %s", path_lay_coef);
    status = -1;
  }
  if (fewha->log.dmshapes == NULL) {
    printf("Error in aao_openFEWHAlog: unable to open file %s", path_dmshapes);
    status = -1;
  }

  // write config
  // Layer config
  fprintf(log_config, "%% Layer config\n");
  fprintf(log_config, "nLay = %d;\n", nLay);
  fprintf(log_config, "n_tot_lay = %d;\n", fewha->n_tot_lay);

  fprintf(log_config, "n_lay = [");
  for (iLay = 0; iLay < nLay - 1; iLay++)
    fprintf(log_config, "%d, ", fewha->n_lay_at_lay[iLay]);
  fprintf(log_config, "%d];\n", fewha->n_lay_at_lay[nLay - 1]);

  fprintf(log_config, "os_lay = [");
  for (iLay = 0; iLay < nLay - 1; iLay++)
    fprintf(log_config, "%d, ", fewha->os_lay_at_lay[iLay]);
  fprintf(log_config, "%d];\n", fewha->os_lay_at_lay[nLay - 1]);
  fprintf(log_config, "\n");

  // DM config
  fprintf(log_config, "%% DM config\n");
  fprintf(log_config, "nDM = %d;\n", nDM);
  fprintf(log_config, "n_tot_act = %d;\n", fewha->n_tot_act);

  fprintf(log_config, "n_act = [");
  for (iDM = 0; iDM < nDM - 1; iDM++)
    fprintf(log_config, "%d, ", fewha->n_act_at_dm[iDM]);
  fprintf(log_config, "%d];\n", fewha->n_act_at_dm[nDM - 1]);

  fprintf(log_config, "os_act = [");
  for (iDM = 0; iDM < nDM - 1; iDM++)
    fprintf(log_config, "%d, ", fewha->os_act_at_dm[iDM]);
  fprintf(log_config, "%d];\n", fewha->os_act_at_dm[nDM - 1]);
  fprintf(log_config, "\n");

  // close config
  if (log_config != NULL) {
    fclose(log_config);
    log_config = NULL;
  }
  return status;
}
void aao_writeFEWHAlogfile_(FILE *file, int n, float *vec) {
  // local var
  int i;

  // code
  if (file != NULL) {
    //	fprintf(file, "%u", counter);
    i = 0;
    fprintf(file, "%e", vec[i]);
    for (i = 1; i < n; i++)
      fprintf(file, "\t%e", vec[i]);
    fprintf(file, "\n");
  } else {
    printf("Error in aao_writeFEWHAlog: unable to write to file");
  }
}

void aao_writeFEWHAlog(aao_FEWHA *fewha) {
  // local var
  float *tmp_lay = NULL; // size = n_tot_lay, reusing tmp memory of aao_Loop
  float *tmp_a = NULL;   // size = n_tot_act, reusing tmp memory of aao_Loop

  // code
  tmp_lay = fewha->loop.tmp_b;
  tmp_a = fewha->loop.tmp_a;

  // compute DM shapes
  // NOTE: here tmp_lay *are* the bilinear layers
  // tmp_a are the dm shapes
  aao_applyF(tmp_a, &fewha->F, fewha->loop.c, tmp_lay, fewha, &fewha->parall);

  // write to log files
  aao_writeFEWHAlogfile_(fewha->log.wav_coef, fewha->n_tot_lay, fewha->loop.c);
  aao_writeFEWHAlogfile_(fewha->log.res_coef, fewha->n_tot_lay, fewha->loop.r);
  aao_writeFEWHAlogfile_(fewha->log.lay_coef, fewha->n_tot_lay, tmp_lay);
  aao_writeFEWHAlogfile_(fewha->log.dmshapes, fewha->n_tot_act, tmp_a);

  // increment counter
  fewha->log.counter++;

  return;
}
void aao_closeFEWHAlog(aao_FEWHA *fewha) {
  if (fewha->log.wav_coef != NULL) {
    fclose(fewha->log.wav_coef);
    fewha->log.wav_coef = NULL;
  }
  if (fewha->log.res_coef != NULL) {
    fclose(fewha->log.res_coef);
    fewha->log.res_coef = NULL;
  }
  if (fewha->log.lay_coef != NULL) {
    fclose(fewha->log.lay_coef);
    fewha->log.lay_coef = NULL;
  }
  if (fewha->log.dmshapes != NULL) {
    fclose(fewha->log.dmshapes);
    fewha->log.dmshapes = NULL;
  }
}

void aao_save_runtimeFEWHAdata(aao_FEWHA *fewha, float *dmshapes, float *slopes,
                               int after_calculation) {
  FILE *file;
  char test_string[] = "Fewha4Oct runtime data";
  int ret;
  if (after_calculation)
    file = fopen("fewha4oct_runtime.dat", "ab");
  else
    file = fopen("fewha4oct_runtime_previous_step.dat", "ab");

  // do this extra, because it changes each iteration
  /* this is not set at the begin */
  printf(" write FEWHA runtime data...\n");
  ret = fwrite(dmshapes, sizeof(float), fewha->n_tot_act, file);
  if (ret == fewha->n_tot_act)
    printf("dm shapes successfull\n");
  ret = fwrite(slopes, sizeof(float), fewha->n_tot_meas, file);
  if (ret == fewha->n_tot_meas)
    printf("slopes successfull\n");

  // loop
  // do this extra, because this changes in each iteration
  ret = fwrite(fewha->loop.a_old, sizeof(float), fewha->n_tot_act, file);
  if (ret == fewha->n_tot_act)
    printf("a_old successfull\n");
  ret = fwrite(fewha->loop.b, sizeof(float), fewha->n_tot_lay, file);
  if (ret == fewha->n_tot_lay)
    printf("b successfull\n");
  ret = fwrite(fewha->loop.r, sizeof(float), fewha->n_tot_lay, file);
  if (ret == fewha->n_tot_lay)
    printf("r successfull\n");
  ret = fwrite(fewha->loop.c, sizeof(float), fewha->n_tot_lay, file);
  if (ret == fewha->n_tot_lay)
    printf("c successfull\n");

  ret = fwrite(test_string, sizeof(char), 22, file);
  if (ret == 22)
    printf("test string successfull\n");
  fclose(file);
}

void aao_saveFEWHAdata(aao_FEWHA *fewha) {
  FILE *file;
  int i, n, index;
  int ret;

  // if file exists, delete its content
  file = fopen("fewha4oct_runtime.dat", "wb");
  fclose(file);
  file = fopen("fewha4oct_runtime_previous_step.dat", "wb");
  fclose(file);

  file = fopen("fewha4oct.dat", "wb");

  // fewha
  ret = fwrite(&fewha->nWFS, sizeof(int), 1, file);
  ret = fwrite(&fewha->nLGS, sizeof(int), 1, file);
  ret = fwrite(&fewha->nNGS, sizeof(int), 1, file);
  ret = fwrite(&fewha->nTTS, sizeof(int), 1, file);
  ret = fwrite(&fewha->nLay, sizeof(int), 1, file);
  ret = fwrite(&fewha->nDM, sizeof(int), 1, file);
  ret = fwrite(&fewha->nDIR, sizeof(int), 1, file);
  ret = fwrite(&fewha->n_tot_meas, sizeof(int), 1, file);
  ret = fwrite(&fewha->n_tot_phi, sizeof(int), 1, file);
  ret = fwrite(&fewha->n_tot_sub_lgs, sizeof(int), 1, file);
  ret = fwrite(&fewha->n_tot_lay, sizeof(int), 1, file);
  ret = fwrite(&fewha->n_tot_act, sizeof(int), 1, file);

  // do this extra, because it changes each iteration
  /* this is not set at the begin */
  //	ret=fwrite(fewha4oct->dmshapes, sizeof(float), fewha->n_tot_act, file);
  //	ret=fwrite(fewha4oct->slopes, sizeof(float), fewha->n_tot_meas, file);

  ret = fwrite(fewha->n_sub_at_wfs, sizeof(int), fewha->nWFS, file);
  ret = fwrite(fewha->os_sub_at_wfs, sizeof(int), fewha->nWFS, file);
  ret = fwrite(fewha->os_meas_at_wfs, sizeof(int), fewha->nWFS, file);
  ret = fwrite(fewha->n_phi_at_wfs, sizeof(int), fewha->nWFS, file);
  ret = fwrite(fewha->os_phi_at_wfs, sizeof(int), fewha->nWFS, file);
  ret = fwrite(fewha->n_lay_at_lay, sizeof(int), fewha->nLay, file);
  ret = fwrite(fewha->os_lay_at_lay, sizeof(int), fewha->nLay, file);
  ret = fwrite(fewha->x0_lay_at_lay, sizeof(float), fewha->nLay, file);
  ret = fwrite(&fewha->n_lay_at_glcs, sizeof(int), 1, file);
  ret = fwrite(fewha->n_act_at_dm, sizeof(int), fewha->nDM, file);
  ret = fwrite(fewha->os_act_at_dm, sizeof(int), fewha->nDM, file);
  ret = fwrite(fewha->d_act_at_dm, sizeof(float), fewha->nDM, file);
  ret = fwrite(fewha->wfs_indx_int2ext, sizeof(int), fewha->nWFS, file);
  ret = fwrite(fewha->wfs_indx_ext2int, sizeof(int), fewha->nWFS, file);

  // AOsys
  ret = fwrite(&fewha->AOsys.sys_type_, sizeof(aao_SystemType), 1, file);
  ret = fwrite(&fewha->AOsys.teldiam_, sizeof(float), 1, file);
  ret = fwrite(&fewha->AOsys.loop_, sizeof(aao_LoopType), 1, file);
  ret = fwrite(&fewha->AOsys.time_unit_, sizeof(float), 1, file);
  ret = fwrite(&fewha->AOsys.zenith_, sizeof(float), 1, file);

  // AOsys, WFS
  ret = fwrite(&fewha->AOsys.n_wfs_, sizeof(int), 1, file);
  for (i = 0; i < fewha->AOsys.n_wfs_; i++) {
    ret = fwrite(&fewha->AOsys.wfs_[i].wfs_type_, sizeof(aao_WFSType), 1, file);
    ret = fwrite(&fewha->AOsys.wfs_[i].n_sub_, sizeof(int), 1, file);
    ret =
        fwrite(fewha->AOsys.wfs_[i].i_sub_, sizeof(unsigned char),
               fewha->AOsys.wfs_[i].n_sub_ * fewha->AOsys.wfs_[i].n_sub_, file);
    ret = fwrite(&fewha->AOsys.wfs_[i].subap_size_, sizeof(float), 1, file);
    ret = fwrite(&fewha->AOsys.wfs_[i].wavelength_, sizeof(float), 1, file);
    ret = fwrite(&fewha->AOsys.wfs_[i].shift_x_, sizeof(float), 1, file);
    ret = fwrite(&fewha->AOsys.wfs_[i].shift_y_, sizeof(float), 1, file);
    ret = fwrite(&fewha->AOsys.wfs_[i].scale_, sizeof(float), 1, file);
    ret = fwrite(&fewha->AOsys.wfs_[i].rotation_, sizeof(float), 1, file);
    // AOsys, WFS, GS
    ret = fwrite(&fewha->AOsys.wfs_[i].gs_, sizeof(aao_Star), 1,
                 file); // does this work?
  }

  // AOsys, DM
  ret = fwrite(&fewha->AOsys.n_dm_, sizeof(int), 1, file);
  for (i = 0; i < fewha->AOsys.n_dm_; i++) {
    ret = fwrite(&fewha->AOsys.dm_[i].isSquare, sizeof(int), 1, file);
    ret = fwrite(&fewha->AOsys.dm_[i].n_act_, sizeof(int), 1, file);
    ret = fwrite(&fewha->AOsys.dm_[i].dm_height_, sizeof(float), 1, file);
    ret = fwrite(fewha->AOsys.dm_[i].i_act_, sizeof(unsigned char),
                 fewha->AOsys.dm_[i].n_act_ * fewha->AOsys.dm_[i].n_act_, file);
    ret = fwrite(fewha->AOsys.dm_[i].actpos_x_, sizeof(float),
                 fewha->AOsys.dm_[i].n_act_, file);
    ret = fwrite(fewha->AOsys.dm_[i].actpos_y_, sizeof(float),
                 fewha->AOsys.dm_[i].n_act_, file);
  }

  // AOsys, atmosphere
  ret = fwrite(&fewha->AOsys.atmosphere_.n_layer_, sizeof(int), 1, file);
  ret = fwrite(fewha->AOsys.atmosphere_.cn2_, sizeof(float),
               fewha->AOsys.atmosphere_.n_layer_, file);
  ret = fwrite(fewha->AOsys.atmosphere_.height_layer_, sizeof(float),
               fewha->AOsys.atmosphere_.n_layer_, file);
  ret = fwrite(&fewha->AOsys.atmosphere_.r0_, sizeof(float), 1, file);
  ret = fwrite(&fewha->AOsys.atmosphere_.L0_, sizeof(float), 1, file);
  ret = fwrite(&fewha->AOsys.atmosphere_.na_height_, sizeof(float), 1, file);
  ret = fwrite(&fewha->AOsys.atmosphere_.na_fwhm_, sizeof(float), 1, file);

  // AOsys, target
  ret = fwrite(&fewha->AOsys.n_target_, sizeof(int), 1, file);
  for (i = 0; i < fewha->AOsys.n_target_; i++)
    ret = fwrite(&fewha->AOsys.target_[i], sizeof(aao_Star), 1,
                 file); // does this work?

  // param
  ret = fwrite(fewha->param.d_lay_, sizeof(float), fewha->nLay, file);
  ret = fwrite(fewha->param.J_lay_, sizeof(int), fewha->nLay, file);
  ret = fwrite(&fewha->param.J_glcs_, sizeof(int), 1, file);
  ret = fwrite(&fewha->param.alpha_, sizeof(float), 1, file);
  ret = fwrite(&fewha->param.alpha_eta_, sizeof(float), 1, file);
  ret = fwrite(&fewha->param.alpha_J_, sizeof(float), 1, file);
  ret = fwrite(&fewha->param.gain_, sizeof(float), 1, file);
  ret = fwrite(&fewha->param.leaky_int_, sizeof(float), 1, file);
  ret = fwrite(&fewha->param.max_iter_, sizeof(int), 1, file);
  ret = fwrite(&fewha->param.useGLMS, sizeof(int), 1, file);
  ret = fwrite(&fewha->param.usePrecond, sizeof(int), 1, file);

  // parall
  ret = fwrite(&fewha->parall, sizeof(aao_FEWHA_Parallel), 1,
               file); // does this work?

  // pcg
  // only temporary buffer

  // loop
  // do this extra, because this changes in each iteration
  /* this is not set at the beginning !!
  ret=fwrite(&fewha->loop.a_old, sizeof(float), fewha->n_tot_act, file);
  ret=fwrite(&fewha->loop.b, sizeof(float), fewha->n_tot_lay, file);
  ret=fwrite(&fewha->loop.r, sizeof(float), fewha->n_tot_lay, file);
  ret=fwrite(&fewha->loop.c, sizeof(float), fewha->n_tot_lay, file); */
  // and temporary buffer

  // A, W
  ret = fwrite(fewha->A.W.wscf_c2e, sizeof(float), fewha->n_tot_lay, file);
  // and tmp

  // A, P
  for (i = 0; i < fewha->nWFS; i++)
    for (n = 1; n < fewha->nLay; n++) // start with 2nd layer!
    {
      index = i * fewha->nLay + n;
      ret = fwrite(fewha->A.P.I_arr[index].ref_x_indx, sizeof(unsigned short),
                   fewha->n_phi_at_wfs[i], file);
      ret = fwrite(fewha->A.P.I_arr[index].ref_x_wght, sizeof(float),
                   fewha->n_phi_at_wfs[i], file);
      ret = fwrite(fewha->A.P.I_arr[index].ref_y_indx, sizeof(unsigned short),
                   fewha->n_phi_at_wfs[i], file);
      ret = fwrite(fewha->A.P.I_arr[index].ref_y_wght, sizeof(float),
                   fewha->n_phi_at_wfs[i], file);
    }

  // A, SH
  for (i = 0; i < fewha->nWFS; i++)
    ret =
        fwrite(fewha->A.SH.I_sub[i], sizeof(unsigned char),
               fewha->AOsys.wfs_[i].n_sub_ * fewha->AOsys.wfs_[i].n_sub_, file);
  ret = fwrite(fewha->A.SH.n_active_sub2, sizeof(int), fewha->nWFS, file);
  ret = fwrite(fewha->A.SH.os_tmp_SH, sizeof(int), fewha->nWFS, file);
  // and tmp. buffer

  // A, iC
  ret = fwrite(fewha->A.iC.invCxx, sizeof(float), fewha->n_tot_sub_lgs, file);
  ret = fwrite(fewha->A.iC.invCxy, sizeof(float), fewha->n_tot_sub_lgs, file);
  ret = fwrite(fewha->A.iC.invCyy, sizeof(float), fewha->n_tot_sub_lgs, file);
  ret = fwrite(fewha->A.iC.inv_sigma2, sizeof(float), fewha->nWFS, file);

  // A, D
  for (i = 0; i < fewha->nLay; i++)
    ret = fwrite(fewha->A.D.d_at_lay[i], sizeof(float), fewha->param.J_lay_[i],
                 file);

  // A, and tmp. buffer

  // iJ
  ret = fwrite(fewha->iJ.d, sizeof(float), fewha->n_tot_lay, file);
  if (fewha->param.usePrecond)
    ret = fwrite(fewha->iJ.diag_AwoD, sizeof(float), fewha->n_tot_lay, file);

  // H, SH, is the same as SH of A
  // H, Q
  for (i = 0; i < fewha->nWFS; i++)
    for (n = 1; n < fewha->nDM; n++) // start with 2nd mirror !!
    {
      index = i * fewha->nDM + n;
      ret = fwrite(fewha->H.Q.I_arr[index].ref_x_indx, sizeof(unsigned short),
                   fewha->n_phi_at_wfs[i], file);
      ret = fwrite(fewha->H.Q.I_arr[index].ref_x_wght, sizeof(float),
                   fewha->n_phi_at_wfs[i], file);
      ret = fwrite(fewha->H.Q.I_arr[index].ref_y_indx, sizeof(unsigned short),
                   fewha->n_phi_at_wfs[i], file);
      ret = fwrite(fewha->H.Q.I_arr[index].ref_y_wght, sizeof(float),
                   fewha->n_phi_at_wfs[i], file);
    }
  // and tmp buffer

  // F, W, is the same as W of A
  // F, P
  for (i = 0; i < fewha->nDIR; i++)
    for (n = 1; n < fewha->nLay; n++) // start with 2nd Layer !!
    {
      index = i * fewha->nLay + n;
      ret = fwrite(fewha->F.P.I_arr[index].ref_x_indx, sizeof(unsigned short),
                   fewha->n_act_at_dm[0], file);
      ret = fwrite(fewha->F.P.I_arr[index].ref_x_wght, sizeof(float),
                   fewha->n_act_at_dm[0], file);
      ret = fwrite(fewha->F.P.I_arr[index].ref_y_indx, sizeof(unsigned short),
                   fewha->n_act_at_dm[0], file);
      ret = fwrite(fewha->F.P.I_arr[index].ref_y_wght, sizeof(float),
                   fewha->n_act_at_dm[0], file);
    }

  // F, Q, not handled

  // G
  if (fewha->param.useGLMS) {
    ret = fwrite(&fewha->G.nnz_diag_AwoD, sizeof(int), 1, file);
    ret = fwrite(fewha->G.L_nz, sizeof(float),
                 fewha->G.nnz_diag_AwoD * fewha->G.nnz_diag_AwoD, file);
    ret = fwrite(fewha->G.D_nz, sizeof(float), fewha->G.nnz_diag_AwoD, file);
    ret = fwrite(fewha->G.AwoD_nz, sizeof(float),
                 fewha->G.nnz_diag_AwoD * fewha->G.nnz_diag_AwoD, file);
    ret = fwrite(fewha->G.nz2lay, sizeof(int), fewha->G.nnz_diag_AwoD, file);
    ret = fwrite(fewha->G.glcs2nz, sizeof(int),
                 fewha->n_lay_at_glcs * fewha->n_lay_at_glcs, file);
    // and tmp buffer
  }

  // log, not needed

  fclose(file);
}

void aao_read_runtimeFEWHAdata(aao_FEWHA *fewha, float *dmshapes, float *slopes,
                               int block, int after_calculation) {
  FILE *file;
  char test_string[23];
  long offset;
  int ret;
  if (after_calculation)
    file = fopen("fewha4oct_runtime.dat", "rb");
  else
    file = fopen("fewha4oct_runtime_previous_step.dat", "rb");

  offset = block * ((sizeof(float) * (2 * fewha->n_tot_act + fewha->n_tot_meas +
                                      3 * fewha->n_tot_lay)) +
                    sizeof(char) * 22);
  ret = fseek(file, offset, SEEK_SET);

  // do this extra, because it changes each iteration and therefore allocate
  // memory only once
  //	fewha4oct->dmshapes=(float *)malloc(sizeof(float)*fewha->n_tot_act); //
  //allocate memory
  ret = fread(dmshapes, sizeof(float), fewha->n_tot_act, file);
  //	fewha4oct->slopes=(float *)malloc(sizeof(float)*fewha->n_tot_meas); //
  //allocate memory
  ret = fread(slopes, sizeof(float), fewha->n_tot_meas, file);

  // loop
  // do this extra, because this changes in each iteration
  ret = fread(fewha->loop.a_old, sizeof(float), fewha->n_tot_act, file);
  ret = fread(fewha->loop.b, sizeof(float), fewha->n_tot_lay, file);
  ret = fread(fewha->loop.r, sizeof(float), fewha->n_tot_lay, file);
  ret = fread(fewha->loop.c, sizeof(float), fewha->n_tot_lay, file);

  ret = fread(test_string, sizeof(char), 22, file);
  test_string[22] = 0;
  printf("Test string: %s\n", test_string);
  fclose(file);
}

void aao_readFEWHAdata(aao_FEWHA *fewha, float **dmshapes, float **slopes) {
  FILE *file;
  int i, n, index, n_tmp;
  int ret;
  unsigned char *i_sub;
  file = fopen("fewha4oct.dat", "rb");

  // fewha
  ret = fread(&fewha->nWFS, sizeof(int), 1, file);
  ret = fread(&fewha->nLGS, sizeof(int), 1, file);
  ret = fread(&fewha->nNGS, sizeof(int), 1, file);
  ret = fread(&fewha->nTTS, sizeof(int), 1, file);
  ret = fread(&fewha->nLay, sizeof(int), 1, file);
  ret = fread(&fewha->nDM, sizeof(int), 1, file);
  ret = fread(&fewha->nDIR, sizeof(int), 1, file);
  ret = fread(&fewha->n_tot_meas, sizeof(int), 1, file);
  ret = fread(&fewha->n_tot_phi, sizeof(int), 1, file);
  ret = fread(&fewha->n_tot_sub_lgs, sizeof(int), 1, file);
  ret = fread(&fewha->n_tot_lay, sizeof(int), 1, file);
  ret = fread(&fewha->n_tot_act, sizeof(int), 1, file);

  // do this extra, because it changes each iteration and therefore allocate
  // memory only once
  *dmshapes =
      (float *)malloc(sizeof(float) * fewha->n_tot_act); // allocate memory
  //	ret=fread(fewha4oct->dmshapes, sizeof(float), fewha->n_tot_act, file);
  *slopes =
      (float *)malloc(sizeof(float) * fewha->n_tot_meas); // allocate memory
  //	ret=fread(fewha4oct->slopes, sizeof(float), fewha->n_tot_meas, file);

  fewha->n_sub_at_wfs =
      (int *)malloc(sizeof(int) * fewha->nWFS); // allocate memory
  ret = fread(fewha->n_sub_at_wfs, sizeof(int), fewha->nWFS, file);
  fewha->os_sub_at_wfs =
      (int *)malloc(sizeof(int) * fewha->nWFS); // allocate memory
  ret = fread(fewha->os_sub_at_wfs, sizeof(int), fewha->nWFS, file);
  fewha->os_meas_at_wfs =
      (int *)malloc(sizeof(int) * fewha->nWFS); // allocate memory
  ret = fread(fewha->os_meas_at_wfs, sizeof(int), fewha->nWFS, file);
  fewha->n_phi_at_wfs =
      (int *)malloc(sizeof(int) * fewha->nWFS); // allocate memory
  ret = fread(fewha->n_phi_at_wfs, sizeof(int), fewha->nWFS, file);
  fewha->os_phi_at_wfs =
      (int *)malloc(sizeof(int) * fewha->nWFS); // allocate memory
  ret = fread(fewha->os_phi_at_wfs, sizeof(int), fewha->nWFS, file);
  fewha->n_lay_at_lay =
      (int *)malloc(sizeof(int) * fewha->nLay); // allocate memory
  ret = fread(fewha->n_lay_at_lay, sizeof(int), fewha->nLay, file);
  fewha->os_lay_at_lay =
      (int *)malloc(sizeof(int) * fewha->nLay); // allocate memory
  ret = fread(fewha->os_lay_at_lay, sizeof(int), fewha->nLay, file);
  fewha->x0_lay_at_lay =
      (float *)malloc(sizeof(float) * fewha->nLay); // allocate memory
  ret = fread(fewha->x0_lay_at_lay, sizeof(float), fewha->nLay, file);

  ret = fread(&fewha->n_lay_at_glcs, sizeof(int), 1, file);
  fewha->n_act_at_dm =
      (int *)malloc(sizeof(int) * fewha->nDM); // allocate memory
  ret = fread(fewha->n_act_at_dm, sizeof(int), fewha->nDM, file);
  fewha->os_act_at_dm =
      (int *)malloc(sizeof(int) * fewha->nDM); // allocate memory
  ret = fread(fewha->os_act_at_dm, sizeof(int), fewha->nDM, file);
  fewha->d_act_at_dm =
      (float *)malloc(sizeof(float) * fewha->nDM); // allocate memory
  ret = fread(fewha->d_act_at_dm, sizeof(float), fewha->nDM, file);
  fewha->wfs_indx_int2ext =
      (int *)malloc(sizeof(int) * fewha->nWFS); // allocate memory
  ret = fread(fewha->wfs_indx_int2ext, sizeof(int), fewha->nWFS, file);
  fewha->wfs_indx_ext2int =
      (int *)malloc(sizeof(int) * fewha->nWFS); // allocate memory
  ret = fread(fewha->wfs_indx_ext2int, sizeof(int), fewha->nWFS, file);

  // AOsys
  ret = fread(&fewha->AOsys.sys_type_, sizeof(aao_SystemType), 1, file);
  ret = fread(&fewha->AOsys.teldiam_, sizeof(float), 1, file);
  ret = fread(&fewha->AOsys.loop_, sizeof(aao_LoopType), 1, file);
  ret = fread(&fewha->AOsys.time_unit_, sizeof(float), 1, file);
  ret = fread(&fewha->AOsys.zenith_, sizeof(float), 1, file);

  // AOsys, WFS
  ret = fread(&fewha->AOsys.n_wfs_, sizeof(int), 1, file);
  fewha->AOsys.wfs_ = (aao_WFS *)malloc(sizeof(aao_WFS) *
                                        fewha->AOsys.n_wfs_); // allocate memory
  for (i = 0; i < fewha->AOsys.n_wfs_; i++) {
    ret = fread(&fewha->AOsys.wfs_[i].wfs_type_, sizeof(aao_WFSType), 1, file);
    ret = fread(&fewha->AOsys.wfs_[i].n_sub_, sizeof(int), 1, file);

    fewha->AOsys.wfs_[i].i_sub_ = (unsigned char *)malloc(
        sizeof(unsigned char) * fewha->AOsys.wfs_[i].n_sub_ *
        fewha->AOsys.wfs_[i].n_sub_); // allocate memory
    ret =
        fread(fewha->AOsys.wfs_[i].i_sub_, sizeof(unsigned char),
              fewha->AOsys.wfs_[i].n_sub_ * fewha->AOsys.wfs_[i].n_sub_, file);

    ret = fread(&fewha->AOsys.wfs_[i].subap_size_, sizeof(float), 1, file);
    ret = fread(&fewha->AOsys.wfs_[i].wavelength_, sizeof(float), 1, file);
    ret = fread(&fewha->AOsys.wfs_[i].shift_x_, sizeof(float), 1, file);
    ret = fread(&fewha->AOsys.wfs_[i].shift_y_, sizeof(float), 1, file);
    ret = fread(&fewha->AOsys.wfs_[i].scale_, sizeof(float), 1, file);
    ret = fread(&fewha->AOsys.wfs_[i].rotation_, sizeof(float), 1, file);
    // AOsys, WFS, GS
    ret = fread(&fewha->AOsys.wfs_[i].gs_, sizeof(aao_Star), 1,
                file); // does this work?
  }

  // AOsys, DM
  ret = fread(&fewha->AOsys.n_dm_, sizeof(int), 1, file);
  fewha->AOsys.dm_ =
      (aao_DM *)malloc(sizeof(aao_DM) * fewha->AOsys.n_dm_); // allocate memory
  for (i = 0; i < fewha->AOsys.n_dm_; i++) {
    ret = fread(&fewha->AOsys.dm_[i].isSquare, sizeof(int), 1, file);
    ret = fread(&fewha->AOsys.dm_[i].n_act_, sizeof(int), 1, file);
    ret = fread(&fewha->AOsys.dm_[i].dm_height_, sizeof(float), 1, file);

    fewha->AOsys.dm_[i].i_act_ = (unsigned char *)malloc(
        sizeof(unsigned char) * fewha->AOsys.dm_[i].n_act_ *
        fewha->AOsys.dm_[i].n_act_); // allocate memory
    ret = fread(fewha->AOsys.dm_[i].i_act_, sizeof(unsigned char),
                fewha->AOsys.dm_[i].n_act_ * fewha->AOsys.dm_[i].n_act_, file);
    fewha->AOsys.dm_[i].actpos_x_ = (float *)malloc(
        sizeof(float) * fewha->AOsys.dm_[i].n_act_); // allocate memory
    ret = fread(fewha->AOsys.dm_[i].actpos_x_, sizeof(float),
                fewha->AOsys.dm_[i].n_act_, file);
    fewha->AOsys.dm_[i].actpos_y_ = (float *)malloc(
        sizeof(float) * fewha->AOsys.dm_[i].n_act_); // allocate memory
    ret = fread(fewha->AOsys.dm_[i].actpos_y_, sizeof(float),
                fewha->AOsys.dm_[i].n_act_, file);
  }

  // AOsys, atmosphere
  ret = fread(&fewha->AOsys.atmosphere_.n_layer_, sizeof(int), 1, file);
  fewha->AOsys.atmosphere_.cn2_ = (float *)malloc(
      sizeof(float) * fewha->AOsys.atmosphere_.n_layer_); // allocate memory
  ret = fread(fewha->AOsys.atmosphere_.cn2_, sizeof(float),
              fewha->AOsys.atmosphere_.n_layer_, file);
  fewha->AOsys.atmosphere_.height_layer_ = (float *)malloc(
      sizeof(float) * fewha->AOsys.atmosphere_.n_layer_); // allocate memory
  ret = fread(fewha->AOsys.atmosphere_.height_layer_, sizeof(float),
              fewha->AOsys.atmosphere_.n_layer_, file);
  ret = fread(&fewha->AOsys.atmosphere_.r0_, sizeof(float), 1, file);
  ret = fread(&fewha->AOsys.atmosphere_.L0_, sizeof(float), 1, file);
  ret = fread(&fewha->AOsys.atmosphere_.na_height_, sizeof(float), 1, file);
  ret = fread(&fewha->AOsys.atmosphere_.na_fwhm_, sizeof(float), 1, file);

  // AOsys, target
  ret = fread(&fewha->AOsys.n_target_, sizeof(int), 1, file);
  fewha->AOsys.target_ = (aao_Star *)malloc(
      sizeof(aao_Star) * fewha->AOsys.n_target_); // allocate memory
  for (i = 0; i < fewha->AOsys.n_target_; i++)
    ret = fread(&fewha->AOsys.target_[i], sizeof(aao_Star), 1,
                file); // does this work?

  // param
  fewha->param.d_lay_ =
      (float *)malloc(sizeof(float) * fewha->nLay); // allocate memory
  ret = fread(fewha->param.d_lay_, sizeof(float), fewha->nLay, file);
  fewha->param.J_lay_ =
      (int *)malloc(sizeof(int) * fewha->nLay); // allocate memory
  ret = fread(fewha->param.J_lay_, sizeof(int), fewha->nLay, file);
  ret = fread(&fewha->param.J_glcs_, sizeof(int), 1, file);
  ret = fread(&fewha->param.alpha_, sizeof(float), 1, file);
  ret = fread(&fewha->param.alpha_eta_, sizeof(float), 1, file);
  ret = fread(&fewha->param.alpha_J_, sizeof(float), 1, file);
  ret = fread(&fewha->param.gain_, sizeof(float), 1, file);
  ret = fread(&fewha->param.leaky_int_, sizeof(float), 1, file);
  ret = fread(&fewha->param.max_iter_, sizeof(int), 1, file);
  ret = fread(&fewha->param.useGLMS, sizeof(int), 1, file);
  ret = fread(&fewha->param.usePrecond, sizeof(int), 1, file);

  // parall
  ret = fread(&fewha->parall, sizeof(aao_FEWHA_Parallel), 1,
              file); // does this work?

  // pcg
  // only temporary buffer
  fewha->pcg.tmp_z = (float *)calloc(fewha->n_tot_lay, sizeof(float));
  fewha->pcg.tmp_p = (float *)calloc(fewha->n_tot_lay, sizeof(float));
  fewha->pcg.tmp_q = (float *)calloc(fewha->n_tot_lay, sizeof(float));

  // loop
  // do this extra, because this changes in each iteration and therefore
  // allocate memory only once
  fewha->loop.a_old =
      (float *)malloc(sizeof(float) * fewha->n_tot_act); // allocate memory
  fewha->loop.b =
      (float *)malloc(sizeof(float) * fewha->n_tot_lay); // allocate memory
  fewha->loop.r =
      (float *)malloc(sizeof(float) * fewha->n_tot_lay); // allocate memory
  fewha->loop.c =
      (float *)malloc(sizeof(float) * fewha->n_tot_lay); // allocate memory

  // this data is not written now !!
  /*	ret=fread(fewha->loop.a_old, sizeof(float), fewha->n_tot_act, file);
          ret=fread(fewha->loop.b, sizeof(float), fewha->n_tot_lay, file);
          ret=fread(fewha->loop.r, sizeof(float), fewha->n_tot_lay, file);
          ret=fread(fewha->loop.c, sizeof(float), fewha->n_tot_lay, file);*/
  // and temporary buffer
  fewha->loop.tmp_b = (float *)calloc(fewha->n_tot_lay, sizeof(float));
  fewha->loop.tmp_a = (float *)calloc(fewha->n_tot_act, sizeof(float));

  // A, W
  fewha->A.W.wscf_c2e =
      (float *)malloc(sizeof(float) * fewha->n_tot_lay); // allocate memory
  ret = fread(fewha->A.W.wscf_c2e, sizeof(float), fewha->n_tot_lay, file);
  // and tmp
  fewha->A.W.tmp_U = (float *)calloc(fewha->n_tot_lay, sizeof(float));

  // A, P
  fewha->A.P.I_arr = (aao_Interp2 *)malloc(sizeof(aao_Interp2) * fewha->nWFS *
                                           fewha->nLay); // allocate memory
  for (i = 0; i < fewha->nWFS; i++)
    aao_initInterp2_NULL(
        &fewha->A.P.I_arr[i * fewha->nLay]); // set initInterp2 for the first
                                             // layer of each wfs to zero
  for (i = 0; i < fewha->nWFS; i++)
    for (n = 1; n < fewha->nLay; n++) // start with 2nd layer!
    {
      index = i * fewha->nLay + n;
      fewha->A.P.I_arr[index].ref_x_indx = (unsigned short *)malloc(
          sizeof(unsigned short) * fewha->n_phi_at_wfs[i]); // allocate memory
      fewha->A.P.I_arr[index].ref_x_wght = (float *)malloc(
          sizeof(float) * fewha->n_phi_at_wfs[i]); // allocate memory
      fewha->A.P.I_arr[index].ref_y_indx = (unsigned short *)malloc(
          sizeof(unsigned short) * fewha->n_phi_at_wfs[i]); // allocate memory
      fewha->A.P.I_arr[index].ref_y_wght = (float *)malloc(
          sizeof(float) * fewha->n_phi_at_wfs[i]); // allocate memory
      ret = fread(fewha->A.P.I_arr[index].ref_x_indx, sizeof(unsigned short),
                  fewha->n_phi_at_wfs[i], file);
      ret = fread(fewha->A.P.I_arr[index].ref_x_wght, sizeof(float),
                  fewha->n_phi_at_wfs[i], file);
      ret = fread(fewha->A.P.I_arr[index].ref_y_indx, sizeof(unsigned short),
                  fewha->n_phi_at_wfs[i], file);
      ret = fread(fewha->A.P.I_arr[index].ref_y_wght, sizeof(float),
                  fewha->n_phi_at_wfs[i], file);

      // and tmp buffer
      n_tmp = fewha->n_phi_at_wfs[i] *
              ((fewha->A.P.I_arr[index].ref_y_indx[fewha->n_phi_at_wfs[i] - 1] +
                1) -
               fewha->A.P.I_arr[index].ref_y_indx[0] + 1); // <= n_phi2
      fewha->A.P.I_arr[index].tmp = (float *)calloc(
          n_tmp,
          sizeof(float)); // this is too much; can be less if LGS are used
    }

  // A, SH
  fewha->A.SH.I_sub = (const unsigned char **)malloc(
      sizeof(unsigned char *) * fewha->nWFS); // allocate memory
  for (i = 0; i < fewha->nWFS; i++) {
    i_sub = (unsigned char *)malloc(
        sizeof(unsigned char) * fewha->AOsys.wfs_[i].n_sub_ *
        fewha->AOsys.wfs_[i].n_sub_); // allocate memory
    //		fewha->A.SH.I_sub[i]=(const unsigned char *)malloc(sizeof(unsigned
    //char) * fewha->AOsys.wfs_[i].n_sub_*fewha->AOsys.wfs_[i].n_sub_); //
    //allocate memory
    ret =
        fread(i_sub, sizeof(unsigned char),
              fewha->AOsys.wfs_[i].n_sub_ * fewha->AOsys.wfs_[i].n_sub_, file);
    fewha->A.SH.I_sub[i] = i_sub;
    //		ret=fread(fewha->A.SH.I_sub[i], sizeof(unsigned char),
    //fewha->AOsys.wfs_[i].n_sub_*fewha->AOsys.wfs_[i].n_sub_, file);
  }
  fewha->A.SH.n_active_sub2 =
      (int *)malloc(sizeof(int) * fewha->nWFS); // allocate memory
  ret = fread(fewha->A.SH.n_active_sub2, sizeof(int), fewha->nWFS, file);
  fewha->A.SH.os_tmp_SH =
      (int *)malloc(sizeof(int) * fewha->nWFS); // allocate memory
  ret = fread(fewha->A.SH.os_tmp_SH, sizeof(int), fewha->nWFS, file);
  // and tmp. buffer
  // compute size of tmp_SH
  i = fewha->nWFS - 1;
  n = fewha->A.SH.os_tmp_SH[i];
  if (i < fewha->nLGS + fewha->nNGS) // is it a TTS?
    n += fewha->n_phi_at_wfs[i] * fewha->n_sub_at_wfs[i];
  else
    n += fewha->n_phi_at_wfs[i] *
         fewha->n_sub_at_wfs[0]; // use geometry of first WFS for TT
  fewha->A.SH.tmp_SH = (float *)calloc(n, sizeof(float));

  // A, iC
  fewha->A.iC.invCxx =
      (float *)malloc(sizeof(float) * fewha->n_tot_sub_lgs); // allocate memory
  ret = fread(fewha->A.iC.invCxx, sizeof(float), fewha->n_tot_sub_lgs, file);
  fewha->A.iC.invCxy =
      (float *)malloc(sizeof(float) * fewha->n_tot_sub_lgs); // allocate memory
  ret = fread(fewha->A.iC.invCxy, sizeof(float), fewha->n_tot_sub_lgs, file);
  fewha->A.iC.invCyy =
      (float *)malloc(sizeof(float) * fewha->n_tot_sub_lgs); // allocate memory
  ret = fread(fewha->A.iC.invCyy, sizeof(float), fewha->n_tot_sub_lgs, file);
  fewha->A.iC.inv_sigma2 =
      (float *)malloc(sizeof(float) * fewha->nWFS); // allocate memory
  ret = fread(fewha->A.iC.inv_sigma2, sizeof(float), fewha->nWFS, file);

  // A, D
  fewha->A.D.d_at_lay =
      (float **)malloc(sizeof(float *) * fewha->nLay); // allocate memory
  for (i = 0; i < fewha->nLay; i++) {
    fewha->A.D.d_at_lay[i] = (float *)malloc(
        sizeof(float) * fewha->param.J_lay_[i]); // allocate memory
    ret = fread(fewha->A.D.d_at_lay[i], sizeof(float), fewha->param.J_lay_[i],
                file);
  }

  // A, and tmp. buffer
  fewha->A.tmp_phi = (float *)calloc(fewha->n_tot_phi, sizeof(float));
  fewha->A.tmp_s = (float *)calloc(fewha->n_tot_meas, sizeof(float));

  // iJ
  fewha->iJ.d =
      (float *)malloc(sizeof(float) * fewha->n_tot_lay); // allocate memory
  ret = fread(fewha->iJ.d, sizeof(float), fewha->n_tot_lay, file);
  if (fewha->param.usePrecond) {
    fewha->iJ.diag_AwoD =
        (float *)malloc(sizeof(float) * fewha->n_tot_lay); // allocate memory
    ret = fread(fewha->iJ.diag_AwoD, sizeof(float), fewha->n_tot_lay, file);
  } else
    fewha->iJ.diag_AwoD = NULL;

  // H, SH, is the same as SH of A
  // H, Q
  fewha->H.Q.I_arr = (aao_Interp2 *)malloc(sizeof(aao_Interp2) * fewha->nWFS *
                                           fewha->nDM); // allocate memory
  for (i = 0; i < fewha->nWFS; i++)
    aao_initInterp2_NULL(
        &fewha->H.Q.I_arr[i * fewha->nDM]); // set initInterp2 for the first
                                            // layer of each wfs to zero
  for (i = 0; i < fewha->nWFS; i++)
    for (n = 1; n < fewha->nDM; n++) // start with 2nd layer!
    {
      index = i * fewha->nDM + n;
      fewha->H.Q.I_arr[index].ref_x_indx = (unsigned short *)malloc(
          sizeof(unsigned short) * fewha->n_phi_at_wfs[i]); // allocate memory
      fewha->H.Q.I_arr[index].ref_x_wght = (float *)malloc(
          sizeof(float) * fewha->n_phi_at_wfs[i]); // allocate memory
      fewha->H.Q.I_arr[index].ref_y_indx = (unsigned short *)malloc(
          sizeof(unsigned short) * fewha->n_phi_at_wfs[i]); // allocate memory
      fewha->H.Q.I_arr[index].ref_y_wght = (float *)malloc(
          sizeof(float) * fewha->n_phi_at_wfs[i]); // allocate memory
      ret = fread(fewha->H.Q.I_arr[index].ref_x_indx, sizeof(unsigned short),
                  fewha->n_phi_at_wfs[i], file);
      ret = fread(fewha->H.Q.I_arr[index].ref_x_wght, sizeof(float),
                  fewha->n_phi_at_wfs[i], file);
      ret = fread(fewha->H.Q.I_arr[index].ref_y_indx, sizeof(unsigned short),
                  fewha->n_phi_at_wfs[i], file);
      ret = fread(fewha->H.Q.I_arr[index].ref_y_wght, sizeof(float),
                  fewha->n_phi_at_wfs[i], file);

      // and tmp buffer
      n_tmp = fewha->n_phi_at_wfs[i] *
              ((fewha->H.Q.I_arr[index].ref_y_indx[fewha->n_phi_at_wfs[i] - 1] +
                1) -
               fewha->H.Q.I_arr[index].ref_y_indx[0] + 1); // <= n_phi2
      fewha->H.Q.I_arr[index].tmp = (float *)calloc(
          n_tmp,
          sizeof(float)); // this is too much; can be less if LGS are used
    }
  // and assignement
  fewha->H.SH = &fewha->A.SH;          // use SH operator of A
  fewha->H.tmp_phi = fewha->A.tmp_phi; // use tmp_phi of A
  fewha->H.tmp_s = fewha->A.tmp_s;     // use tmp_s of A

  // F, W, is the same as W of A
  fewha->F.W = &fewha->A.W; // use W operator of A

  // F, P

  fewha->F.P.I_arr = (aao_Interp2 *)malloc(sizeof(aao_Interp2) * fewha->nDIR *
                                           fewha->nLay); // allocate memory
  for (i = 0; i < fewha->nDIR; i++)
    aao_initInterp2_NULL(
        &fewha->F.P.I_arr[i * fewha->nLay]); // set initInterp2 for the first
                                             // layer of each wfs to zero
  for (i = 0; i < fewha->nDIR; i++)
    for (n = 1; n < fewha->nLay; n++) // start with 2nd layer!
    {
      index = i * fewha->nLay + n;
      fewha->F.P.I_arr[index].ref_x_indx = (unsigned short *)malloc(
          sizeof(unsigned short) * fewha->n_act_at_dm[0]); // allocate memory
      fewha->F.P.I_arr[index].ref_x_wght = (float *)malloc(
          sizeof(float) * fewha->n_act_at_dm[0]); // allocate memory
      fewha->F.P.I_arr[index].ref_y_indx = (unsigned short *)malloc(
          sizeof(unsigned short) * fewha->n_act_at_dm[0]); // allocate memory
      fewha->F.P.I_arr[index].ref_y_wght = (float *)malloc(
          sizeof(float) * fewha->n_act_at_dm[0]); // allocate memory
      ret = fread(fewha->F.P.I_arr[index].ref_x_indx, sizeof(unsigned short),
                  fewha->n_act_at_dm[0], file);
      ret = fread(fewha->F.P.I_arr[index].ref_x_wght, sizeof(float),
                  fewha->n_act_at_dm[0], file);
      ret = fread(fewha->F.P.I_arr[index].ref_y_indx, sizeof(unsigned short),
                  fewha->n_act_at_dm[0], file);
      ret = fread(fewha->F.P.I_arr[index].ref_y_wght, sizeof(float),
                  fewha->n_act_at_dm[0], file);

      // and tmp buffer
      n_tmp =
          fewha->n_act_at_dm[0] *
          ((fewha->F.P.I_arr[index].ref_y_indx[fewha->n_act_at_dm[0] - 1] + 1) -
           fewha->F.P.I_arr[index].ref_y_indx[0] + 1); // <= n_phi2
      fewha->F.P.I_arr[index].tmp = (float *)calloc(
          n_tmp,
          sizeof(float)); // this is too much; can be less if LGS are used
    }

  // F, Q, not handled
  fewha->F.Q.I_arr = NULL; // MCAO(nLay>nDM) case is unhandled

  // G
  if (fewha->param.useGLMS) {
    ret = fread(&fewha->G.nnz_diag_AwoD, sizeof(int), 1, file);
    fewha->G.L_nz = (float *)malloc(sizeof(float) * fewha->G.nnz_diag_AwoD *
                                    fewha->G.nnz_diag_AwoD); // allocate memory
    ret = fread(fewha->G.L_nz, sizeof(float),
                fewha->G.nnz_diag_AwoD * fewha->G.nnz_diag_AwoD, file);
    fewha->G.D_nz = (float *)malloc(sizeof(float) *
                                    fewha->G.nnz_diag_AwoD); // allocate memory
    ret = fread(fewha->G.D_nz, sizeof(float), fewha->G.nnz_diag_AwoD, file);
    fewha->G.AwoD_nz =
        (float *)malloc(sizeof(float) * fewha->G.nnz_diag_AwoD *
                        fewha->G.nnz_diag_AwoD); // allocate memory
    ret = fread(fewha->G.AwoD_nz, sizeof(float),
                fewha->G.nnz_diag_AwoD * fewha->G.nnz_diag_AwoD, file);
    fewha->G.nz2lay =
        (int *)malloc(sizeof(int) * fewha->G.nnz_diag_AwoD); // allocate memory
    ret = fread(fewha->G.nz2lay, sizeof(int), fewha->G.nnz_diag_AwoD, file);
    fewha->G.glcs2nz = (int *)malloc(sizeof(int) * fewha->n_lay_at_glcs *
                                     fewha->n_lay_at_glcs); // allocate memory
    ret = fread(fewha->G.glcs2nz, sizeof(int),
                fewha->n_lay_at_glcs * fewha->n_lay_at_glcs, file);

    // and tmp buffer
    fewha->G.tmp_r_cs = (float *)calloc(
        fewha->n_lay_at_glcs * fewha->n_lay_at_glcs, sizeof(float));
    fewha->G.tmp_v_cs = (float *)calloc(
        fewha->n_lay_at_glcs * fewha->n_lay_at_glcs, sizeof(float));
    fewha->G.tmp_q = (float *)calloc(fewha->n_tot_lay, sizeof(float));
  } else {
    fewha->G.nnz_diag_AwoD = 0;
    fewha->G.L_nz = NULL;
    fewha->G.D_nz = NULL;
    fewha->G.AwoD_nz = NULL;
    fewha->G.nz2lay = NULL;
    fewha->G.glcs2nz = NULL;

    // and tmp buffer
    fewha->G.tmp_r_cs = NULL;
    fewha->G.tmp_v_cs = NULL;
    fewha->G.tmp_q = NULL;
  }

  // log, set to zero
  aao_zeroFEWHAlog(&fewha->log);

  fclose(file);
}

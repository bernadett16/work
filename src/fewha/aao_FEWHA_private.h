/*
 * aao_FEWHA_private.h
 *
 *  Created on: Sep 5, 2014
 *      Author: misha
 */

#ifndef AAO_FEWHA_PRIVATE_H_
#define AAO_FEWHA_PRIVATE_H_

#include "aao_FEWHA.h"
#include "aao_FEWHA_params.h"
#include "aao_FEWHA_parallel.h"

// copy structs
int aao_copyFEWHAparall_(aao_FEWHA_Parallel * to, aao_FEWHA_Parallel const * from);
int aao_copyFEWHAparams_(aao_FEWHA_Params * to, aao_FEWHA_Params const * from, int nLay);

// init routine
void aao_initFEWHAparall_(aao_FEWHA_Parallel * parall, aao_FEWHA const * fewha);

// update routines
int aao_updateFEWHAparams_gain_(aao_FEWHA_Params * param, float gain);

#endif /* AAO_FEWHA_PRIVATE_H_ */

function [AOsys] = aao_initFEWHA4most(sim,iTest)


% library name
libname = 'fewha';

% path to fewha folder
path_to_fewha_folder = '/Users/misha/Documents/workspace/AAO_Reconstructors';

% list of header files
headerfiles = ...
   {'aao_FEWHA.h',...           % Main header files comes first in the list
    'aao_atmosphere.h',...      % Headers with AO system parameters
    'aao_dm.h',...
    'aao_star.h',...
    'aao_system.h',...
    'aao_wfs.h',...
    'aao_FEWHA_log.h',...       % Headers with FEWHA parameters
    'aao_FEWHA_parallel.h',...
    'aao_FEWHA_params.h'};

% try to load the fewha library
loadFEWHAlibrary(libname,path_to_fewha_folder,headerfiles);

% parse system
AOsys = initSystem(libname,sim,iTest);


end

%% Function that parses the AO system settings
function AOsys = initSystem(libname,sim,iTest)

% Init DMs
dm = libpointer('aao_DM',struct(libstruct('aao_DM')));
for iDM = 1:sim.phys.nDM
    n_act       = sim.phys.dm{iDM}.nact; 
    dm_height   = sim.phys.dm{iDM}.height;
    tmp         = sim.phys.dm{iDM}.I_act';
    i_act       = tmp(:);
    actpos_x    = sim.phys.dm{iDM}.actvec(:);
    actpos_y    = sim.phys.dm{iDM}.actvec(:);
    
    calllib(libname, 'aao_setSquareDM', dm.plus(iDM-1), n_act, dm_height, i_act, actpos_x, actpos_y);
end

% Init Stars / WFS
tmp = struct(libstruct('aao_WFS'));
tmp.wfs_type_ = 0;
wfs = libpointer('aao_WFS',tmp);
for iWFS = 1:sim.phys.nWFS
    
    % Init guide star
    pos_x = sim.phys.wfs{iWFS}.direction(1);
    pos_y = sim.phys.wfs{iWFS}.direction(2); 
    n_photons = sim.test{iTest}.phys.wfs{iWFS}.nPhotons;
    spot_fwhm = 1; warning('no non-elongated spot FWHM parameter in MOST...');
    
    tmp = struct(libstruct('aao_Star'));
    tmp.type_ = 0;
    star = libpointer('aao_Star',tmp);
    
    if strcmp(sim.phys.wfs{iWFS}.GS,'NGS')
        calllib(libname,'aao_setNGS',star, pos_x, pos_y, n_photons, spot_fwhm);
    elseif strcmp(sim.phys.wfs{iWFS}.GS,'LGS')
        gs_llt_x = sim.phys.wfs{iWFS}.LGSLaunchPosition(1);
        gs_llt_y = sim.phys.wfs{iWFS}.LGSLaunchPosition(2);
        calllib(libname, 'aao_setLGS', star, pos_x, pos_y, n_photons, spot_fwhm, gs_llt_x, gs_llt_y);
    elseif strcmp(sim.phys.wfs{iWFS}.GS,'TTS')
        error('TTS unhandled');
    end
    
    % Init WFS
    if strcmp(sim.phys.wfs{iWFS}.type,'Shack-Hartmann');
        wfs_type = 'ShackHartmann';
    else
        error('WFS Type unhandled');
    end
    n_sub = sim.phys.wfs{iWFS}.nsub;
    tmp = sim.phys.wfs{iWFS}.I_sub';
    i_sub = tmp(:);
    subap_size = sim.phys.wfs{iWFS}.subapsize;
    wavelength = sim.phys.wfs{iWFS}.lambda;
    
    calllib(libname, 'aao_setWFS', wfs.plus(iWFS-1), wfs_type, n_sub, i_sub, subap_size, wavelength, star);
    
    
    % Clean up star
    % -- do nothing, as star has no dynamic memory --
    clear star
end

% Init Atmosphere
n_layer      = sim.phys.atm.nLayers;
cn2          = zeros(n_layer,1);
height_layer = zeros(n_layer,1);
for iLay = 1:n_layer
    cn2(iLay)          = sim.phys.atm.layer{iLay}.cn2;
    height_layer(iLay) = sim.phys.atm.layer{iLay}.height;
end
r0           = sim.phys.atm.r0;
L0           = sim.phys.atm.l0;
na_height    = sim.phys.atm.sl_altitude;
na_fwhm      = sim.phys.atm.sl_FWHM;

atm = libpointer('aao_Atmosphere',struct(libstruct('aao_Atmosphere')));
calllib(libname, 'aao_setAtmosphere', atm, n_layer, cn2, height_layer, r0, L0, na_height, na_fwhm);

% Init target
tmp = struct(libstruct('aao_Star'));
tmp.type_ = 0;
target = libpointer('aao_Star',tmp);

for iTarget = 1:sim.simul.nViews
    pos_x = sim.simul.view{iTarget}.direction(1);
    pos_y = sim.simul.view{iTarget}.direction(2);
    
    calllib(libname, 'aao_setTarget', target.plus(iTarget-1), pos_x, pos_y);
end

% Init AO System 
sys_type = sim.simul.AOsystem; 
teldiam = sim.phys.telDiam;
if sim.phys.closedLoop
    loop = 'ClosedLoop';
else
    loop = 'OpenLoop';
end
time_unit = sim.simul.dt;
n_wfs = sim.phys.nWFS;
n_dm = sim.phys.nDM;
n_target = sim.simul.nViews;

tmp = struct(libstruct('aao_System'));
tmp.sys_type_ = 0;
tmp.loop_ = 0;
AOsys = libpointer('aao_System',tmp);

calllib(libname, 'aao_initSystem', AOsys, sys_type, teldiam, loop, time_unit, n_wfs, wfs, n_dm, dm, atm, n_target, target);

end

%% Function to load FEWHA library
function loadFEWHAlibrary(libname,path_to_fewha_folder,headerfiles)

% Set library name
switch computer('arch')
    case 'maci64'
        libfullname = sprintf('%s.dylib',libname);
    case 'glnxa64'
        libfullname = sprintf('lib%s.so',libname);
    case 'win64'
        error('Architecture not supported');
    case 'win32'
        error('Architecture not supported'); 
end

% if not loaded, load library
if libisloaded(libname)
    libversion = calllib(libname,'aao_printFEWHAversion',blanks(128));
    fprintf('FEWHA version: %s\n',libversion);
else
    % note: first name in headerfiles is the main header of FEWHA
    path_to_fewha_lib = fullfile(path_to_fewha_folder,'lib',libfullname);
    path_to_fewha_header = fullfile(path_to_fewha_folder,'include',headerfiles{1});
    
    % if library doesn't exist compile it
    if ~exist(path_to_fewha_lib,'file')
        
        % on mac, add path to gcc-4.2 if needed
        if (strcmp(computer('arch'),'maci64') && system('which gcc-4.2') ~= 0)
            str = sprintf('%s:/usr/local/bin',getenv('PATH'));
            setenv('PATH',str);
        end
        
        % compile library
        system(sprintf('cd %s; make clean; make library',path_to_fewha_folder));
    end
    
    % load library
    % note: first name in headerfiles is the main header of FEWHA
    str = sprintf('loadlibrary(''%s'',''%s''',path_to_fewha_lib,path_to_fewha_header);
    for iHeader = 2:numel(headerfiles)
        str = sprintf('%s,''%s'',''%s''',str,'addheader',headerfiles{iHeader});
    end
    str = sprintf('%s)',str);
    eval(str);
    
    
    % print library info if succesfully load library
    if libisloaded(libname)
        disp('FEWHA library loaded succesfully');
     
        libversion = calllib(libname,'aao_printFEWHAversion',blanks(128));
        fprintf('FEWHA version: %s\n',libversion);
        
        disp('FEWHA functions:');
        disp(libfunctions(libname,'-full'));        
    else
        error('Could not load FEWHA library');
    end
end


end
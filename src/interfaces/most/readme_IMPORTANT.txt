MOST interface readme...
Usage: load DLL into matlab, work with DLL functions directly via matlab scripts/ functions
Problem: MATLAB does not support access to arrays of structs
Date: 19.12.2014
Author: MY

!!! IMPORATNT !!!

a direct use of the library in MATLAB is impossible: 
MATLAB does not support access to arrays of structs, e.g.,:

in C:
  aao_WFS wfs[9];
  wfs[iWFS]; // is accessible for iWFS = 0, ..., 8

in MATLAB:
  tmp = struct(libstruct('aao_WFS'));
  tmp.wfs_type_ = 0;
  wfs = libpointer('aao_WFS',tmp);
  wfs.plus(iWFS); // is not accessible for iWFS = 1, ..., 8, access leads to crashes

alternative in MATLAB:
  tmp = struct(libstruct('aao_WFS'));
  tmp.wfs_type_ = 0;
  tmp = repmat(tmp, 9, 1);
  wfs = libpointer('aao_WFS',tmp); // is not supported

alternative in C:
  aao_WFS * wfs[9]; // redifine as aao_WFS**
  *wfs[iWFS] // is accessible for iWFS = 0, ..., 8
  // but this requires restructuring of the code

solution: put the MATLAB DLL loading on ice; create a MOST interface using the MEX interface





#ifndef MATRICE_UTILS_H
#define MATRICE_UTILS_H

#include <dfftw_mpi.h>
#include "flex_types.h"

typedef struct {
  int  x;
  int  y;
  int  w;
  int  h;  
} rectangle_t;


typedef struct {
  int sx[2];
  int ex[2];
  int sy[2];
  int ey[2];

  int x;
  int y;

  int pieces_x;
  int pieces_y;
  int state_x;
  int state_y;
  int ready;
} output_iterator_t;


// For MPI communication
typedef struct {
  int    local_nx;
  int    local_x_start;
  int    local_ny_after_transpose;
  int    local_y_start_after_transpose;
  int    total_local_size;
} matrix_info_t;

#define FIELDS_IN_INFO 5


typedef struct {
  int              fft_dim;

  int              local_nx;
  int              local_x_start;
  int              local_ny_after_transpose;
  int              local_y_start_after_transpose;
  int              total_local_size;

  matrix_info_t   *locations;  
} distributed_matrice_t;


int scatter_center(distributed_matrice_t  *distrm,
		   float                  *phase, 
		   int                     phase_support_x, 
		   int                     phase_support_y,
		   int                     diam_pup,		   
		   int                    *start_x,
		   int                    *start_y,
		   int                     numprocs,
		   int                     myrank,
		   int                     master_rank,
		   float                 **local_phase,
		   MPI_Datatype            pupilphase_mpi_t,
		   MPI_Comm                communicator);

int get_local_central_copy(fftw_complex           *local_data,
			   distributed_matrice_t  *dmatr,
			   rectangle_t            *output_area, // in
			   fftw_complex          **send_data,
			   int                    *sendcount,
			   rectangle_t            *output,
			   rectangle_t            *larea);

int get_central_recvcounts(distributed_matrice_t  *distrm,
			   rectangle_t            *output,
			   int                     numprocs,
			   int                    *recvcounts,
			   int                    *displs);

fftw_complex get_pixelval(distributed_matrice_t *distrm, 
			  int                    numprocs,
			  int                   *displs,
			  fftw_complex          *data,
			  int                    xi, int yi, 
			  rectangle_t           *output, 
			  int                    transpose);


int initialize_output_iterator(distributed_matrice_t      *distrm, 
			       rectangle_t        *output,
			       rectangle_t        *larea,
			       output_iterator_t  *iter);
int iterate_output(output_iterator_t  *iter);

void find_overlapping_size(distributed_matrice_t  *distrm,
			   rectangle_t    *output,
			   rectangle_t    *larea, 
			   int            *size);

void calculate_output_area(distributed_matrice_t   *distrm, 
			   rectangle_t     *output_area,
			   rectangle_t     *output);
void calculate_local_area(distributed_matrice_t   *distrm, 
			  rectangle_t     *larea);


int shifted_ind(distributed_matrice_t *distrm, int i);
int local_ind_data(output_iterator_t *iter, int xi, int yi);
int local_ind(distributed_matrice_t *distrm, int xi, int yi);
int local_ind_tr(distributed_matrice_t *distrm, int xi, int yi);

void set_matr(distributed_matrice_t *distrm, fftw_complex *matr, 
		     int xi, int yi, fftw_complex val);
fftw_complex get_matr(distributed_matrice_t *distrm, fftw_complex *matr, 


			     int xi, int yi);
void set_matr_re(distributed_matrice_t *distrm, fftw_complex *matr, 
		 int xi, int yi, double val);
void set_matr_im(distributed_matrice_t *distrm, fftw_complex *matr, 
		 int xi, int yi, double val);
double get_matr_re(distributed_matrice_t *distrm, fftw_complex *matr, 
			  int xi, int yi);
double get_matr_im(distributed_matrice_t *distrm, fftw_complex *matr, 
			  int xi, int yi);


void set_wmatrf(float *matr, int xi, int yi, float val, int size);
double get_wmatrf(float *matr, int xi, int yi, int size);


int get_pixel_owner(distributed_matrice_t *distrm, int numprocs, 
		    int xi, int yi);



void set_matrd(int diam_pup, double *matr, int xi, int yi, double val);
double get_matrd(int diam_pup, double *matr, int xi, int yi);
void set_matrf(int diam_pup, float *matr, int xi, int yi, double val);
double get_matrf(int diam_pup, float *matr, int xi, int yi);
void set_matrpup(int diam_pup, pupil_type_t *matr, int xi, int yi, double val);
pupil_type_t get_matrpup(int diam_pup, pupil_type_t *matr, int xi, int yi);


#endif

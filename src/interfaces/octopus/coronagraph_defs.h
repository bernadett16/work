
#ifndef CORONAGRAPH_H
#define CORONAGRAPH_H

#include "distributed_matrice.h"
#include <dfftw_mpi.h>


//
// Macros
//

#define MIN(x,y)  (((x) < (y)) ? (x) : (y))
#define MAX(x,y)  (((x) > (y)) ? (x) : (y))
#define SQR(x) ((x)*(x))


#define FALSE 0
#define TRUE  1

#define TRANSPOSE_NO   0
#define TRANSPOSE_YES  1


//
// Type definitions
//


typedef struct {

  fftwnd_mpi_plan  plan;
  fftwnd_mpi_plan  iplan;
  
  //fftwnd_plan  splan;
  //fftwnd_plan  siplan;

  // Contains fft_size and the local points of the distribued matrice
  distributed_matrice_t  distrm;

  int              diam_pup;  // also pupil support size
  int              diam_pup_lyot;
  int              central_obs_lyot;
  int              phase_mask_size;

  double          *pupil_mask;
  double          *phase_mask;
  double          *tilt;

  fftw_complex    *local_data;
  fftw_complex    *workp;
  fftw_complex    *sum_image;
  
  // For matrix spreading
  MPI_Datatype     fftw_complex_mpi_t;
  MPI_Datatype     pupilphase_mpi_t;
  MPI_Comm         communicator;
  int              myrank;
  int              numprocs;

} coronagraph_t;

#endif


#ifndef STRUCTURES_H
#define STRUCTURES_H

#include "coronagraph_defs.h"

/*!
 *@brief Structure that holds the SH WFS physical parameters
 *
 * This structure is used throughout the code where SH parameters are needed
 * The values for the parameters are mostly set in the file mcao_parameters.c
*/

typedef struct{
  int n_subap; 
  int n_ccd_pix_subap;
  float read_noise;
  photon_type_t dark_per_frame_pixel;
  photon_type_t background_photons;
  photon_type_t n_photons_persubap_integ;
  float integration_time;
  float threshold;
  int use_weightedcog;
  int L3_noise;
  long n_active_subaps;
  float wfs_wavelength;
  float subap_diam_m;
  float fov_per_subap;
  float fov_per_subap_copy;         /*added for speedup elongation with truncation*/
  int temporal_delay;
  int remove_tilt;
  float fiber_width_arcsec;
  float r0_for_spotelongation;     /* Spot elongation */
  float llt_x;                     /* Spot elongation */
  float llt_y;                     /* Spot elongation */
  int spot_elongation;             /* Spot elongation */
  float wcog_gaussian_width;
  float fov_of_pyr;                /* specific to pyr */
  int shape_mask;                  /* specific to pyr */
  float modulation_lambda_D;       /* specific to pyr */
  float sep_between_quad;          /* specific to pyr */
  float alpha;  /*rmc scaling of elongation numerical fudge */
} struct_sh_phys_param;


typedef struct{
  int LO_flag;                     /*pyr_LO*/ // 0 : No LO 1: LO
  int n_LO_wfs;                    /*pyr_LO*/ // number of LO WFSs
  float *alt_LO_wfs;               /*pyr_LO*/ // altitudes of the LO WFSs
  float *size_subap_LO_wfs;        /*pyr_LO*/ // size of the subaps of each wfs
  float *fraction_light_LO_wfs;    /*pyr_LO*/ // fraction of light on wfs
  int *n_stars_LO_wfs;             /*pyr_LO*/ // number of stars seen by wfs
  int **list_stars_LO_wfs;         /*pyr_LO*/ // list of stars seen by wfs
  float *fov_LO_wfs;               /*pyr_LO*/ // field of view seen by wfs
  int *int_time_LO_wfs;            /*pyr_LO*/ // integration time on wfs
  float *gain_LO_wfs;              /*pyr_LO*/ //gain of command in local LO
  int *delay_command_LO;            /*pyr_LO*/ //delay of command in local LO
} struct_LO_phys_param; 

typedef struct{
  int x0,x1,xx0;
  int flag_actif_wave;
  int x0G,x1G,x0D,x1D,xx0G,xx0D;
  int flag_actif_D,flag_actif_G;
} init_loc_pyr_t;                  /* specific to pyr */


typedef struct{
  int flag_actif_wave;
  int x0;
  int x1;
} struct_pyr_wave_t;               /* specific to pyr */



/*!
 *@brief Structure that holds the SH WFS simulation parameters
 *
 * This structure is used throughout the code where SH parameters are needed
 * The values for the parameters are mostly set in the file mcao_parameters.c
*/

typedef struct {
int local_nx , local_x_start,local_ny_after_transpose, local_y_start_after_transpose,total_local_size,X_start;
}
 init_mpi_fftw_t;                  /* specific to pyr */


typedef struct{
  int diam_pup;
  int n_pix_per_subap;
  int npix_fft_subap;
  int wfs_random_seed;
  float illum_percentage_thres;
  int wfs_initialize;
  int over_sampling;
  int use_fiber;
  float tt_overgain;
  float TT_additonalgain_inCL; // If different gain for LO WFS in CL
  float TT_stroke_reduc_inbuild;
  int LO_mode_clip;

  int wfs_type;                    /* specific to pyr */
  int pyr_mes_type;                /* specific to pyr */
  int n_steps_modulation;          /* specific to pyr */
  int npix_fft_pyr;                /* specific to pyr */
  int n_pyr_machines;              /* specific to pyr */
  int modul_split;                 /* specific to pyr */
  int pyr_wkspace_flag;            /* specific to pyr */

  //rmc for cent algs
  int centmethod; 
  int corrover;
  double corrthresh;
  int iterstartaccum;
  int iterstartwithaccum;
  int focalxshift;
  int focalyshift;
  float spotsizefudge;
  //rmc
  float power_on_covariance;
  int removeTiltMethod;
  int pupil_elong_over;
} struct_sh_simul_param;

/*!
 *@brief Structure that holds the telescope parameters
 *
 * This  structure  is  used   throughout  the  code  where  telescope
 * parameters are needed The values  for the parameters are mostly set
 * in the file mcao_parameters.c */

typedef struct{
  float Diam;
  float central_obs;
  int pupil_support;
  float vibrations_x;
  float vibrations_y;
  float vibrations_freq;
  int use_spider;


} struct_telescope;


/*!
 *@brief Possible coronagraphs.
 */
typedef enum {
  CORONAGRAPH_NO = 0,
  CORONAGRAPH_4Q = 1
  // more coronagraphs may follow...
} coronagraph_type_t;


/*!
 *@brief Structure that holds the  WFS guide star physical parameters
 *
 * This structure is used throughout  the code where GS parameters are
 * needed The  values for  the parameters are  mostly set in  the file
 * mcao_parameters.c */

typedef struct {
  coronagraph_type_t  coronagraph; // tells the used coronograph  
  double              fft_dim_faq;
  double              diam_pup_lyot_faq;
  double              central_obs_lyot_faq;
  double              phasemask_diam_faq;
  coronagraph_t      *pcor;
  MPI_Comm            comm;
  
  float          height; // if height=0 -> NGS
  float          Na_thickness; /* Spot elongation */
  float          position_x;
  float          position_y;
  float          magnitude;
  int            extended;
  float          wavelength; // Only used for PSF stars !
  int            gl_correction;
  int            nongausssodflag; //flag whether Gaussian or not
  char           *nongaussdir; //string of directory to find the subap images (this one is what references are calculated from
  char           *nongaussdir2; //string of directory to find the subap images when assuming staleness - 2 is what noisy images are calculated from
  int            index; //just a fudge so know which LGS is which



} struct_guidestar;

/*!
 *@brief Structure that holds the atmospheric parameters
 *
 * This structure is used throughout the code where atm parameters are needed
 * The values for the parameters are mostly set in the file mcao_parameters.c
*/

typedef struct{
  int number_layers ;
  float total_r0_05um;
  float L0;
  float weight_thislayer;
  float speed_thislayer;
  float height_thislayer;
  int turbulent_seed;
  int phase_screen_size;
} struct_turbulentlayer;

/*!
 *@brief Structure that holds the DM parameters
 *
 * This structure is used throughout the code where DM parameters are needed
 * The values for the parameters are mostly set in the file mcao_parameters.c
*/

typedef struct{
  float dm_height;
  int n_actu_thisdm;
  int IF_model_thisdm;//model to use for the influence functions (triangles...)
  int modes_thisdm;
  int n_dms;
  int kernel_size_pix_thisdm; // Support of the IF for this DM
  int fov_pixels_thisdm; // Size of the FOV at this DM (in pixels)
  int metapupil_support;
  float volts2microns;
  int influence_type;
  int influence_width;
  int use_virtualDM;
  int use_geomDSM;
  int actu_spacing;
  int DM_modeClip;
//additions for misregistrations
  int xregoffset;  //translation x offset
  int yregoffset;  //translation y offset
  float regratio;  //magnification ratio in subapertures
  float regtheta;  //rotation (degrees)

} struct_deformable_mirror;

/*!
 *@brief Structure that holds the global WFS parameters (related to SH)
 *
 * This structure is used throughout the code where SH parameters are needed
 * The values for the parameters are mostly set in the file mcao_parameters.c
*/

typedef struct{
  int wfs_subaps;
  int *active_subapertures;
  float *subap_illumination;
} struct_allwfs;


#endif

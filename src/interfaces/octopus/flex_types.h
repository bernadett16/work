#ifndef _FLEX_TYPES_H
#define _FLEX_TYPES_H

//
// Define the type of pupil array.
//
#if(defined(PUPIL_BYTE))

typedef unsigned char      pupil_type_t;
#define MPI_PUPIL_TYPE     MPI_BYTE

#elif(defined(PUPIL_INTEGER))

typedef int                pupil_type_t;
#define MPI_PUPIL_TYPE     MPI_INT

#elif(defined(PUPIL_DOUBLE))

typedef double             pupil_type_t;
#define MPI_PUPIL_TYPE     MPI_DOUBLE

#else

typedef float              pupil_type_t;
#define MPI_PUPIL_TYPE     MPI_FLOAT

#endif


//
// Define the type of pupil array.
//
#if(defined(PHOTON_DOUBLE))

typedef double             photon_type_t;
#define MPI_PHOTON_TYPE     MPI_DOUBLE
#else

typedef long              photon_type_t;
#define MPI_PHOTON_TYPE     MPI_LONG
// Default
#endif


#endif

/*
 * main.cpp
 *
 *  Created on: Jan 22, 2018
 *      Author: bernadett
 */

// included test framework Catch2, therefore, moved from C to C++ file
// tests can be found in tests directory

// catch confired such that own main is used
#define CATCH_CONFIG_RUNNER
#include "catch.hpp"

extern "C" {
#include "old_tests/test_initFEWHA.h"
}

int main(int argc, char* argv[]) {
  // run tests
  int result = Catch::Session().run(argc, argv);

  //    test_initCANARY_FEWHA_NTAO();
  //    test_initCANARY_FEWHA();
  //    test_initEELT_FEWHA();
  //    test_init10m_FEWHA();
  //    test_initEELTMCAO_FEWHA();

  // TODO MY 06.09.2014 make it possible for there to be a separate tip/tilt
  // mirror in that case DM shape phi(x,y) = ax + by + psi(x,y), where psi are
  // the higher- order mirror components

  return result;
}

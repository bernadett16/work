/*
 * Functions specific to the system
 * declared in system.h
 *
 * TBD: how to give parameters to initSystem and updateSystem?
 * 	one long list vs filename
 *
 *
 * Author: RW, 05/2014
 * Last change: MY 27.07.2014
 */

#include "aao_system.h"
#include "aao_print.h"
#include "aao_system_private.h"
#include <stdio.h>
#include <stdlib.h>

int aao_initSystem(aao_System *sys, aao_SystemType sys_type, float teldiam,
                   aao_LoopType loop, float time_unit, int n_wfs,
                   const aao_WFS *wfs, int n_dm, aao_DM *dm,
                   const aao_Atmosphere *atm, int n_target,
                   const aao_Star *target) {
  // local var
  int i;

  // code
  sys->wfs_ = NULL;
  sys->dm_ = NULL;
  sys->target_ = NULL;

  sys->sys_type_ = sys_type;

  if (teldiam > 0) // tel diam must be positive
    sys->teldiam_ = teldiam;
  else
    return 1;

  sys->loop_ = loop;

  if (time_unit > 0) // time unit must be positive
    sys->time_unit_ = time_unit;
  else
    return 2;

  // !!! for now just set zenith to 0 !!!
  sys->zenith_ = 0;

  if (n_wfs > 0)
    sys->n_wfs_ = n_wfs;
  else
    return 3;
  sys->wfs_ = (aao_WFS *)malloc(n_wfs * sizeof(aao_WFS));
  if (sys->wfs_ == NULL)
    return 4; // out of memory
  // don't need to process the return value (0/1) b/c a check has
  // already been performed by setWFS(&wfs[i],...);
  for (i = 0; i < n_wfs; i++)
    aao_copyWFS_(&sys->wfs_[i], &wfs[i]);

  if (n_dm > 0)
    sys->n_dm_ = n_dm;
  sys->dm_ = (aao_DM *)malloc(n_dm * sizeof(aao_DM));
  if (sys->dm_ == NULL)
    return 5; // out of memory
  for (i = 0; i < n_dm; i++)
    aao_copyDM_(&sys->dm_[i], &dm[i]);

  if (n_target > 0)
    sys->n_target_ = n_target;
  sys->target_ = (aao_Star *)malloc(n_target * sizeof(aao_Star));
  if (sys->target_ == NULL)
    return 6; // out of memory
  // !!! Copy by value only b/c Star has no dynamic fields !!!
  for (i = 0; i < n_target; i++)
    sys->target_[i] = target[i];

  aao_copyAtmosphere_(&sys->atmosphere_, atm);

  return 0;
}
int aao_copySystem_(aao_System *to, aao_System const *from) {
  return aao_initSystem(to, from->sys_type_, from->teldiam_, from->loop_,
                        from->time_unit_, from->n_wfs_, from->wfs_, from->n_dm_,
                        from->dm_, &from->atmosphere_, from->n_target_,
                        from->target_);
}

void aao_freeSystem(aao_System *sys) {
  // local var
  int i;

  // code
  if (sys != NULL) {
    if (sys->wfs_ != NULL) {
      for (i = 0; i < sys->n_wfs_; i++)
        aao_freeWFS(&sys->wfs_[i]);
      free(sys->wfs_);
      sys->wfs_ = NULL;
    }
    if (sys->dm_ != NULL) {
      for (i = 0; i < sys->n_dm_; i++)
        aao_freeDM(&sys->dm_[i]);
      free(sys->dm_);
      sys->dm_ = NULL;
    }
    if (sys->target_ != NULL) {
      // no freeStar b/c it has no dynamically allocated memory
      free(sys->target_);
      sys->target_ = NULL;
    }
    aao_freeAtmosphere(&sys->atmosphere_);
  }
}
void aao_printSystem(const aao_System *sys) {
  int i;

  printf("===== System struct ======\n");
  printf("sys_type_: ");
  switch (sys->sys_type_) {
  case SCAO:
    printf("SCAO\n");
    break;
  case LTAO:
    printf("LTAO\n");
    break;
  case MOAO:
    printf("MOAO\n");
    break;
  case MCAO:
    printf("MCAO\n");
    break;
  }
  printf("teldiam_: %f\n", sys->teldiam_);
  printf("loop_: ");
  switch (sys->loop_) {
  case ClosedLoop:
    printf("ClosedLoop\n");
    break;
  case OpenLoop:
    printf("OpenLoop\n");
    break;
  }
  printf("time_unit_: %f\n", sys->time_unit_);
  printf("zenith_: %f\n", sys->zenith_);
  printf("\n");

  printf("n_wfs_: %d\n", sys->n_wfs_);
  for (i = 0; i < sys->n_wfs_; i++) {
    printf("iWFS=%d:\n", i);
    aao_printWFS(&sys->wfs_[i]);
  }

  printf("n_dm_: %d\n", sys->n_dm_);
  for (i = 0; i < sys->n_dm_; i++) {
    printf("iDM=%d:\n", i);
    aao_printDM(&sys->dm_[i]);
  }

  aao_printAtmosphere(&sys->atmosphere_);

  printf("n_target_: %d\n", sys->n_target_);
  for (i = 0; i < sys->n_target_; i++) {
    printf("iTarget=%d:\n", i);
    aao_printStar(&sys->target_[i]);
  }
}

/*
 * atmosphere.c
 *
 *  Created on: Jun 27, 2014
 *      Author: misha
 */


#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include "aao_atmosphere.h"
#include "aao_system_private.h"
#include "aao_print.h"


int aao_setAtmosphere(aao_Atmosphere* atm, int n_layer, const float* cn2, const float* height_layer,
		float r0, float L0, float na_height, float na_fwhm)
{
	// local var
	int i;
	float tmp;

	// code
	atm->cn2_ = NULL;
	atm->height_layer_ = NULL;

	if (n_layer > 0) // number of layers must be positive
		atm->n_layer_ = n_layer;
	else
		return 1;

	// check that sum(cn2) is numerically 1
	tmp = 0;
	for(i=0; i<n_layer; i++)
		tmp += cn2[i];
	if (fabsf(tmp-1.0) > n_layer * FLT_EPSILON)
		return 2;

	atm->cn2_ = (float*)malloc(n_layer*sizeof(float));
	for(i=0; i<n_layer; i++)
		atm->cn2_[i] = cn2[i];

	// check that all layer heights are positive
	for(i=0; i<n_layer; i++)
		if(height_layer[i] < 0)
			return 3;

	atm->height_layer_ = (float*)malloc(n_layer*sizeof(float));
	for(i=0; i<n_layer; i++)
		atm->height_layer_[i] = height_layer[i];

	if(r0 > 0)
		atm->r0_ = r0;
	else
		return 4;

	if(L0 > 0)
		atm->L0_ = L0;
	else
		return 5;

	if(na_height >= 0) // na_height should be positive, but we accept if it is not set (set to 0)
		atm->na_height_ = na_height;
	else
		return 6;

	if(na_fwhm >= 0) // na_fwhm should be positive, but we accept if it is not set (set to 0)
		atm->na_fwhm_ = na_fwhm;
	else
		return 7;


	return 0;

}

// Update parameters of the atmosphere
// in/out: atmosphere
//     in: cn2, r0, L0, na_height, na_fwhm
//    ret: 1 if parameters are fine, 0 if update failed
int aao_updateAtmosphere(aao_Atmosphere* atm, const float* cn2,
		float r0, float L0, float na_height, float na_fwhm)
{
	// TODO MY 27.06.2014 - handle updating!!!
	return 1;
}

// Free memory allocated to atmosphere struct
void aao_freeAtmosphere(aao_Atmosphere* atm)
{
	if(atm!=NULL)
	{
		if(atm->cn2_ != NULL)
		{
			free(atm->cn2_);
			atm->cn2_ = NULL;
		}
		if(atm->height_layer_ != NULL)
		{
			free(atm->height_layer_);
			atm->height_layer_ = NULL;
		}
	}
}
int aao_copyAtmosphere_(aao_Atmosphere * to, aao_Atmosphere const * from)
{
	int ret = 0;

	ret = aao_setAtmosphere(to, from->n_layer_, from->cn2_, from->height_layer_, from->r0_, from->L0_, from->na_height_, from->na_fwhm_);

	return ret;
}
void aao_printAtmosphere(const aao_Atmosphere * atm)
{
	printf("===== Atmosphere struct ======\n");
	printf("n_layer_: %d\n",atm->n_layer_);
	aao_print_vec_float("cn2_",atm->cn2_,atm->n_layer_);
	aao_print_vec_float("height_layer_",atm->height_layer_,atm->n_layer_);
	printf("r0_: %f\n",atm->r0_);
	printf("L0_: %f\n",atm->L0_);
	printf("na_height_: %f\n",atm->na_height_);
	printf("na_fwhm_: %f\n",atm->na_fwhm_);
	printf("\n");
}


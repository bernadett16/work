/*
 * dm.c
 *
 *  Created on: Jun 27, 2014
 *      Author: misha
 */

#include <stdlib.h>
#include <stdio.h>
#include "aao_dm.h"
#include "aao_system_private.h"
#include "aao_print.h"

int aao_setSquareDM(aao_DM* dm, int n_act, float dm_height, const unsigned char* i_act, const float* actpos_x, const float* actpos_y)
{
	// local var
	int i;
	int n_act2;

	// local var init
	n_act2 = n_act*n_act;

	// code
	dm->i_act_ = NULL;
	dm->actpos_x_ = NULL;
	dm->actpos_y_ = NULL;

	dm->isSquare = 1;

	if (n_act > 0)	// number of actuators (in 1 dimension) must be positive
		dm->n_act_ = n_act;
	else
		return 1;

	if(dm_height >= 0)	// height must be non-negative
		dm->dm_height_ = dm_height;
	else
		return 2;

	dm->i_act_ = (unsigned char*)malloc(n_act2*sizeof(unsigned char));
	if(dm->i_act_ == NULL)
		return 3;	// ran out of memory
	for (i=0; i<n_act2; i++)
		dm->i_act_[i] = i_act[i];

	dm->actpos_x_ = (float*)malloc(n_act*sizeof(float));
	dm->actpos_y_ = (float*)malloc(n_act*sizeof(float));
	if(dm->actpos_x_ == NULL || dm->actpos_y_ == NULL)
		return 4; // ran out of memory
	for (i=0; i<n_act; i++)
	{
		dm->actpos_x_[i] = actpos_x[i];
		dm->actpos_y_[i] = actpos_y[i];
	}

	return 0;

}

int aao_setArbitraryDM(aao_DM* dm, int n_act, float dm_height, const float* actpos_x, const float* actpos_y)
{
	// local var
	int i;

	// local var init

	// code
	dm->isSquare = 0;

	if (n_act > 0)	// number of actuators (in 2 dimensions) must be positive
		dm->n_act_ = n_act;
	else
		return 1;

	if(dm_height >= 0)	// height must be non-negative
		dm->dm_height_ = dm_height;
	else
		return 2;

	// we anyways only store the active actuators
	dm->i_act_ = NULL;

	dm->actpos_x_ = (float*)malloc(n_act*sizeof(float));
	dm->actpos_y_ = (float*)malloc(n_act*sizeof(float));
	if(dm->actpos_x_ == NULL || dm->actpos_y_ == NULL)
		return 3; // ran out of memory
	for (i=0; i<n_act; i++)
	{
		dm->actpos_x_[i] = actpos_x[i];
		dm->actpos_y_[i] = actpos_y[i];
	}


	return 0;
}

void aao_freeDM(aao_DM * dm)
{
	if (dm != NULL)
	{
		if (dm->actpos_x_ != NULL)
		{
			free(dm->actpos_x_);
			dm->actpos_x_ = NULL;
		}
		if (dm->actpos_y_ != NULL)
		{
			free(dm->actpos_y_);
			dm->actpos_y_ = NULL;
		}
		if (dm->i_act_ != NULL)
		{
			free(dm->i_act_);
			dm->i_act_ = NULL;
		}
	}
}

int aao_copyDM_(aao_DM * to, aao_DM const * from)
{
	if(from->isSquare)
		return aao_setSquareDM(to, from->n_act_, from->dm_height_, from->i_act_, from->actpos_x_, from->actpos_y_);
	else
		return aao_setArbitraryDM(to, from->n_act_, from->dm_height_, from->actpos_x_, from->actpos_y_);
}

void aao_printDM(const aao_DM * dm)
{
	printf("===== DM struct ======\n");
	printf("isSquare: %d\n",dm->isSquare);
	printf("n_act_: %d\n",dm->n_act_);
	printf("dm_height_: %f\n",dm->dm_height_);
	aao_print_vec_uint8_compact("i_act_",dm->i_act_,dm->n_act_*dm->n_act_);
	aao_print_vec_float("actpos_x_",dm->actpos_x_,dm->n_act_);
	aao_print_vec_float("actpos_y_",dm->actpos_y_,dm->n_act_);
	printf("\n");
}

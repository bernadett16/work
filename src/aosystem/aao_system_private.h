/*
 * system_private.h
 *
 *  Created on: Jun 27, 2014
 *      Author: misha
 */

#ifndef AAO_SYSTEM_PRIVATE_H_
#define AAO_SYSTEM_PRIVATE_H_

#include "aao_atmosphere.h"
#include "aao_dm.h"
#include "aao_wfs.h"
#include "aao_system.h"

// copy structs
int aao_copyDM_(aao_DM * to, aao_DM const * from);
int aao_copyWFS_(aao_WFS * to, aao_WFS const * from);
int aao_copyAtmosphere_(aao_Atmosphere * to, aao_Atmosphere const * from);
int aao_copySystem_(aao_System * to, aao_System const * from);


#endif /* AAO_SYSTEM_PRIVATE_H_ */

/*
 * star.c
 *
 *  Created on: Jun 27, 2014
 *      Author: misha
 */

#include <stdlib.h>
#include <stdio.h>
#include "aao_star.h"


int aao_setNGS(aao_Star* star, float pos_x, float pos_y, int n_photons, float spot_fwhm)
{
	// code

	star->type_ = NGS;
	star->pos_x_ = pos_x;
	star->pos_y_ = pos_y;

	if (n_photons >= 0)	// photons must be non-negative
		star->n_photons_ = n_photons;
	else
		return 1;

	if(spot_fwhm > 0) // spot fwhm must be positive
		star->spot_fwhm_ = spot_fwhm;
	else
		return 2;

	star->gs_llt_x_ = 0;
	star->gs_llt_y_ = 0;
	star->fov_ = 0.;

	return 0;

}
int aao_setLGS(aao_Star* star, float pos_x, float pos_y, int n_photons, float spot_fwhm, float gs_llt_x, float gs_llt_y)
{
	// code
	star->type_ = LGS;
	star->pos_x_ = pos_x;
	star->pos_y_ = pos_y;

	if (n_photons >= 0)	// photons must be non-negative
		star->n_photons_ = n_photons;
	else
		return 1;

	if(spot_fwhm > 0) // spot fwhm must be positive
		star->spot_fwhm_ = spot_fwhm;
	else
		return 2;

	star->gs_llt_x_ = gs_llt_x;
	star->gs_llt_y_ = gs_llt_y;

	star->fov_ = 0.;

	return 0;
}

int aao_updateGS(aao_Star* star, int n_photons)
{
	// TODO MY 27.06.2014 - handle updating!!!
	return 1;
}


int aao_setTarget(aao_Star* star, float pos_x, float pos_y)
{
	// code
	star->type_ = NGS;
	star->pos_x_ = pos_x;
	star->pos_y_ = pos_y;

	star->n_photons_ = 0;
	star->spot_fwhm_ = 0.;
	star->gs_llt_x_ = 0.;
	star->gs_llt_y_ = 0.;
	star->fov_ = 0.;

	return 0;
}
int aao_setTargetMCAO(aao_Star* star, float fov)
{
	// code
	star->type_ = NGS;
	star->fov_ = fov;

	star->pos_x_ = 0.;
	star->pos_y_ = 0.;
	star->n_photons_ = 0;
	star->spot_fwhm_ = 0.;
	star->gs_llt_x_ = 0.;
	star->gs_llt_y_ = 0.;


	return 0;
}
void aao_printStar(const aao_Star* star)
{
	printf("===== Star struct ======\n");
	printf("type_: ");
	switch (star->type_)
	{
	case LGS:
		printf("LGS\n");
		break;
	case NGS:
		printf("NGS\n");
		break;
	}
	printf("pos_x_: %f\n",star->pos_x_);
	printf("pos_y_: %f\n",star->pos_y_);
	printf("n_photons_: %d\n",star->n_photons_);
	printf("spot_fwhm_: %f\n",star->spot_fwhm_);
	printf("gs_llt_x_: %f\n",star->gs_llt_x_);
	printf("gs_llt_y_: %f\n",star->gs_llt_y_);
	printf("fov_: %f\n",star->fov_);
	printf("\n");
}


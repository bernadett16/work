/*
 * wfs.c
 *
 *  Created on: Jun 27, 2014
 *      Author: misha
 */

#include <stdlib.h>
#include <stdio.h>
#include "aao_wfs.h"
#include "aao_system_private.h"
#include "aao_print.h"

int aao_setWFS(aao_WFS* wfs, aao_WFSType wfs_type, int n_sub, const unsigned char* i_sub, float subap_size, float wavelength, const aao_Star* gs)
{
	// local var
	int i;
	int n_sub2;

	// local var init
	n_sub2 = n_sub*n_sub;

	// code
	wfs->i_sub_ = NULL;

	wfs->wfs_type_ = wfs_type;

	if (n_sub > 0)	// num subap must be positive
		wfs->n_sub_ = n_sub;
	else
		return 1;

	wfs->i_sub_ = (unsigned char*)malloc(n_sub2*sizeof(unsigned char));
	if (wfs->i_sub_ == NULL)
		return 2; // out of memory

	for(i=0; i<n_sub2; i++)
		wfs->i_sub_[i] = i_sub[i];

	if(subap_size > 0)	// subaperture size must be positive
		wfs->subap_size_ = subap_size;
	else
		return 3;

	if(wavelength > 0) // wavelength must be positive
		wfs->wavelength_ = wavelength;
	else
		return 4;

	// !!!! Copy by value !!!!
	// !!!! this is OK b/c Star has no dynamic memory !!!!
	wfs->gs_ = *gs;

    wfs->shift_x_ = 0;
    wfs->shift_y_ = 0;
    wfs->scale_ = 0;
    wfs->rotation_ = 0;

	return 0;
}

void aao_freeWFS(aao_WFS* wfs)
{
	// don't need to free gs_ b/c it has no dynamically allocated memory
	if(wfs != NULL)
	{
		if(wfs->i_sub_ != NULL)
		{
			free(wfs->i_sub_);
			wfs->i_sub_ = NULL;
		}
	}
}

int aao_copyWFS_(aao_WFS * to, aao_WFS const * from)
{
	// !!! only works this way b/c Star gs_ has no dynamic memory!
	return aao_setWFS(to, from->wfs_type_, from->n_sub_, from->i_sub_, from->subap_size_, from->wavelength_, &from->gs_);
}

void aao_printWFS(const aao_WFS* wfs)
{
	printf("===== WFS struct ======\n");
	printf("wfs_type_: ");
	switch (wfs->wfs_type_)
	{
	case ShackHartmann:
		printf("ShackHartmann\n");
		break;
	case Pyramid:
		printf("Pyramid\n");
		break;
	}
	printf("n_sub_: %d\n",wfs->n_sub_);
	aao_print_vec_uint8_compact("i_sub_",wfs->i_sub_,wfs->n_sub_*wfs->n_sub_);\
	printf("subap_size_: %f\n",wfs->subap_size_);
	printf("wavelength_: %f\n",wfs->wavelength_);
	printf("\n");

	printf("shift_x_: %f\n",wfs->shift_x_);
	printf("shift_y_: %f\n",wfs->shift_y_);
	printf("scale_: %f\n",wfs->scale_);
	printf("rotation_: %f\n",wfs->rotation_);
	printf("\n");

	aao_printStar(&wfs->gs_);
	printf("\n");
}


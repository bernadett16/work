/*
 * test_initCANARY_FEWHA.h
 *
 *  Created on: Aug 8, 2014
 *      Author: misha
 */

#ifndef TEST_INIT_FEWHA_H_
#define TEST_INIT_FEWHA_H_

void test_initCANARY();
void test_initCANARY_FEWHA();
void test_initEELT_FEWHA();
void test_initEELTMCAO_FEWHA();
void test_initCANARY_FEWHA_NTAO();
void test_init10m_FEWHA();

#endif /* TEST_INIT_FEWHA_H_ */

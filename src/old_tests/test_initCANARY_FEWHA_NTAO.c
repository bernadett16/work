/*
 * test.c
 * 
 * test of set and free of all system structs (DM, WFS, Star, Atmoshere)
 * test initSystem
 *
 *  Created on: Jun 26, 2014
 *      Author: misha
 */

#include <stdlib.h>
#include <stdio.h>

#include "test_initFEWHA.h"

#include "aao_atmosphere.h"
#include "aao_dm.h"
#include "aao_star.h"
#include "aao_wfs.h"
#include "aao_system.h"

#include "aao_FEWHA.h"
#include "aao_FEWHA_params.h"
#include "aao_FEWHA_parallel.h"


#define NLGS 0
#define NNGS 3  //ignoring the on-axis truth sensor.
#define NWFS 3
#define NSUBX 7
#define NACTX 8
#define TELDIAM 4.2
#define NLAY 3
void test_initCANARY_FEWHA_NTAO()
{
	// System structs
	aao_System NTAO_on_CANARY;
	aao_DM dm;
	aao_Star star[NWFS];
	aao_WFS wfs[NWFS];
	aao_Atmosphere atm;
	aao_Star target;

	// FEWHA structs
	aao_FEWHA fewha = {0};
	aao_FEWHA_Params fewha_param = {0};
	aao_FEWHA_Parallel fewha_parall = {0};
	char *createPrecondFromFile = NULL; // use precond from a txt file, if not null (heavy computation)

	// sample measurements, sample DM
	float * slopes = NULL, * dmshape = NULL;

	// local variables
	int i;
	int status[NWFS];

	// System settings
	float actpos_x[NACTX];
	unsigned char i_act[NACTX*NACTX]={0,0,1,1,1,1,0,0,
					  0,1,1,1,1,1,1,0,
					  1,1,1,1,1,1,1,1,
					  1,1,1,1,1,1,1,1,
					  1,1,1,1,1,1,1,1,
					  1,1,1,1,1,1,1,1,
					  0,1,1,1,1,1,1,0,
					  0,0,1,1,1,1,0,0};

	unsigned char i_sub[NSUBX*NSUBX]={0,0,1,1,1,0,0,
					  0,1,1,1,1,1,0,
					  1,1,1,1,1,1,1,
					  1,1,1,0,1,1,1,
					  1,1,1,1,1,1,1,
					  0,1,1,1,1,1,0,
					  0,0,1,1,1,0,0};
	float cn2[NLAY];
	float height_layer[NLAY];

	// init settings
	for(i=0; i<NACTX; i++)
	  actpos_x[i] =i*(TELDIAM/NSUBX)-TELDIAM/2;
	cn2[0] = 0.7;
	cn2[1] = 0.2;
	cn2[2] = 0.1;
	height_layer[0] = 0;
	height_layer[1] = 4000;
	height_layer[2] = 12000;

	// FEWHA settings
	float d_lay[NLAY] = {0.6, 0.6, 0.6};	// discretization spacing on each layer, in meters
											// discretization of the *first* layer must be the same as of the ground DM!!
	int   J_lay[NLAY] = {5, 5, 5};			// number of wavelet scales on each layer
											// corresponds to 2^5x2^5 = 32x32 point grids
	float alpha     = 1;					// regularization tuning parameter
	float alpha_eta = 0.4;					// spot elongation tuning parameter (=0 NGS model, =1 full LGS model)
	float gain      = 0.1;					// loop gain
	float leaky_int = 0.99;					// leaky integrator value
	int max_iter    = 10;					// number of CG iterations

	int useGLMS = 0;						// GLMS method, turned off
	int usePrecond = 0;						// preconditioner, turned off
	float alpha_J = 0;						// preconditioner parameter, turned off for now
	int J_glcs = 0;							// GLMS (ground layer multi-scale) parameter, turned off for now

	int global_par = 0;						// global parallelization on
	int max_thr_system = 12;				// number of threads

	printf("==== Testing init CANARY ====\n");


	// Set DM
	status[0] = aao_setSquareDM(&dm, NACTX, 0, i_act, actpos_x, actpos_x);

	if (status[0]==0)
		printf("DM set\n");
	else
		printf("DM set failed \n");


	// Set guide stars
	status[0] = aao_setNGS(&star[0],  0.3333333333,  0,   			10000, 0.9);
	status[1] = aao_setNGS(&star[1], -0.1666666667,  0.28866666667, 10000, 0.9);
	status[2] = aao_setNGS(&star[2], -0.1666666667, -0.28866666667, 10000, 0.9);

	if (status[0] + status[1] + status[2] == 0)
		printf("Stars set\n");
	else
		printf("Stars set failed\n");

	// Set WFS
	for(i=0; i<NLGS; i++)
		status[i] = aao_setWFS(&wfs[i], ShackHartmann, NSUBX, i_sub, 0.6, 800., &star[i]);
	for(i=NLGS; i<NWFS; i++)
		status[i] = aao_setWFS(&wfs[i], ShackHartmann, NSUBX, i_sub, 0.6, 800, &star[i]);

	if (status[0] + status[1] + status[2] == 0)
		printf("WFS set\n");
	else
		printf("WFS set failed\n");


	// Set Atmosphere
	float r0=0.12;
	float l0=30.;
	float lgsheight=22000.;
	float lgsfwhm=1000.;
	status[0] = aao_setAtmosphere(&atm, 3, cn2, height_layer, r0,l0, lgsheight,lgsfwhm);

	if (status[0]==0)
		printf("Atmosphere set\n");
	else
		printf("Atmosphere set failed\n");

	// Set Probestar
	status[0] = aao_setTarget(&target, 0., 0.);

	if (status[0]==0)
		printf("Probestar set\n");
	else
		printf("Probestar set failed\n");


	// Init system
	status[0] = aao_initSystem(&NTAO_on_CANARY, LTAO, TELDIAM, ClosedLoop, 1/150., NWFS, wfs, 1, &dm, &atm, 1, &target);

	if (status[0]==0)
		printf("System init successful\n");
	else
		printf("System init failed\n");
	printf("\n");

	// Init FEWHA params
	status[0] = aao_setFEWHAparams(&fewha_param, NLAY, d_lay, J_lay, J_glcs, alpha, alpha_eta, alpha_J, gain, leaky_int, max_iter, useGLMS, usePrecond, createPrecondFromFile);
	if (status[0]==0)
		printf("FEWHA params init successful\n");
	else
		printf("FEWHA params init failed\n");
	printf("\n");

	// Init FEWHA Parallelization configuration
	status[0] = aao_setFEWHAparall(&fewha_parall, global_par, max_thr_system);
	if (status[0]==0)
		printf("FEWHA parallelization set successful\n");
	else
		printf("FEWHA parallelization set failed\n");
	printf("\n");

	// Init FEWHA
	aao_initFEWHA(&fewha, &NTAO_on_CANARY, &fewha_param, &fewha_parall);

	// Print FEWHA config
	aao_printFEWHAconfig(&fewha);

	// Init sample measurements
	slopes = (float*)calloc(fewha.n_tot_meas,sizeof(float));
	for(i=0; i<fewha.n_tot_meas; i++)
		slopes[i] = 1.0;
	dmshape = (float*)calloc(fewha.n_tot_act, sizeof(float));

	// Call the FEWHA reconstructor
	aao_runFEWHA(dmshape, slopes, &fewha);

	// Open loop
	aao_resetFEWHAloop(dmshape, &fewha);

	// Destroy FEWHA
	aao_freeFEWHAparams(&fewha_param);
	aao_freeFEWHA(&fewha);


	// Destroy system
	aao_freeSystem(&NTAO_on_CANARY);

	// free local variables
	free(slopes);
	slopes = NULL;
	free(dmshape);
	dmshape = NULL;

	aao_freeDM(&dm);
	for(i=0; i<NWFS; i++)
		aao_freeWFS(&wfs[i]);
	aao_freeAtmosphere(&atm);
	// no freeStar(&target) or freeStar(&star[i]) needed -> no dynamic memory in Star


}

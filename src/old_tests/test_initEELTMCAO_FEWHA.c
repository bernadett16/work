/*
 * test_initEELTMCAO_FEWHA.c
 *
 *  Created on: Aug 22, 2014
 *      Author: misha
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "test_initFEWHA.h"

#include "aao_atmosphere.h"
#include "aao_dm.h"
#include "aao_star.h"
#include "aao_system.h"
#include "aao_wfs.h"

#include "aao_FEWHA.h"
#include "aao_FEWHA_parallel.h"
#include "aao_FEWHA_params.h"
#include "aao_vec_op.h"

#include "aao_print.h"

#define NLGS 6
#define NNGS 3
#define NWFS 9
#define NSUBX 84
#define NACTX 85
#define TELDIAM 39.
#define NLAY 9
#define NDM 6
#define FOV 2.3

void test_initEELTMCAO_FEWHA() {
  // System structs
  aao_System MCAO_on_EELT;
  aao_DM dm;
  aao_Star star[NWFS];
  aao_WFS wfs[NWFS];
  aao_Atmosphere atm;
  aao_Star target;
  char *createPrecondFromFile = "jacobi.txt"; // use precond from a txt file, if not null (heavy computation)
  // FEWHA structs
  aao_FEWHA fewha = {0};
  aao_FEWHA_Params fewha_param = {0};
  aao_FEWHA_Parallel fewha_parall = {0};

  // sample measurements, sample DM
  float *slopes = NULL, *dmshape = NULL;

  // local variables
  int i;
  int status[NWFS];

  // System settings
  float actpos_x[NACTX];
  unsigned char i_act[NACTX * NACTX] = {0};
  read_vec_from_file_cast_uint8(
      "/media/sf_shared_folder/fewha_test/matlab_mex/MOAO_on_EELT_I_act.txt",
      i_act, NACTX * NACTX);

  unsigned char i_sub[NSUBX * NSUBX] = {0};
  read_vec_from_file_cast_uint8(
      "/media/sf_shared_folder/fewha_test/matlab_mex/MOAO_on_EELT_I_sub.txt",
      i_sub, NSUBX * NSUBX);
  float cn2[NLAY] = {0.5224, 0.0260, 0.0444, 0.1160, 0.0989,
                     0.0295, 0.0598, 0.0430, 0.0600};
  float height_layer[NLAY] = {0, 140, 281, 562, 1125, 2250, 4500, 9000, 18000};

  // init settings
  for (i = 0; i < NACTX; i++)
    actpos_x[i] = i * (TELDIAM / NSUBX) - TELDIAM / 2;

  // FEWHA settings
  float d_lay[NLAY] = {
      0.5, 0.5, 0.5, 0.5, 0.5,
      0.5, 0.5, 1,   1}; // discretization spacing on each layer, in meters
                         // discretization of the *first* layer must be the same
                         // as of the ground DM!!
  int J_lay[NLAY] = {7, 7, 7, 7, 7,
                     7, 7, 7, 7}; // number of wavelet scales on each layer
                                  // corresponds to 2^5x2^5 = 32x32 point grids
  float alpha = 1;                // regularization tuning parameter
  float alpha_eta =
      0.4; // spot elongation tuning parameter (=0 NGS model, =1 full LGS model)
  float gain = 0.4;      // loop gain
  float leaky_int = 1.0; // leaky integrator value
  int max_iter = 10;     // number of CG iterations

  int useGLMS = 0;      // GLMS method, turned off
  int usePrecond = 1;   // preconditioner, turned off
  float alpha_J = 10e6; // preconditioner parameter, turned off for now
  int J_glcs =
      0; // GLMS (ground layer multi-scale) parameter, turned off for now

  int global_par = 1;      // global parallelization on
  int max_thr_system = 12; // number of threads

  printf("==== Testing init E-ELT ====\n");

  // Set DM
  status[0] = aao_setSquareDM(&dm, NACTX, 0, i_act, actpos_x, actpos_x);

  // Set guide stars
  float lgssep = 3.75;
  float n_photons = 100;
  float spot_fwhm = 1.1;
  status[0] =
      aao_setLGS(&star[0], lgssep, 0, n_photons, spot_fwhm, 16.26, -16.26);
  status[1] = aao_setLGS(&star[1], lgssep * 1 / 2, lgssep * sqrt(3) / 2,
                         n_photons, spot_fwhm, 16.26, 16.26);
  status[2] = aao_setLGS(&star[2], -lgssep * 1 / 2, lgssep * sqrt(3) / 2,
                         n_photons, spot_fwhm, -16.26, 16.26);
  status[3] =
      aao_setLGS(&star[3], -lgssep, 0, n_photons, spot_fwhm, -16.26, 16.26);
  status[4] = aao_setLGS(&star[4], -lgssep * 1 / 2, -lgssep * sqrt(3) / 2,
                         n_photons, spot_fwhm, -16.26, -16.26);
  status[5] = aao_setLGS(&star[5], lgssep * 1 / 2, -lgssep * sqrt(3) / 2,
                         n_photons, spot_fwhm, 16.26, -16.26);

  lgssep = 5;
  n_photons = 100;
  spot_fwhm = 1.1;
  status[6] = aao_setNGS(&star[6], -lgssep, 0, n_photons, spot_fwhm);
  status[7] = aao_setNGS(&star[7], lgssep * 1 / 2, lgssep * sqrt(3) / 2,
                         n_photons, spot_fwhm);
  status[8] = aao_setNGS(&star[8], lgssep * 1 / 2, -lgssep * sqrt(3) / 2,
                         n_photons, spot_fwhm);

  if (status[0] + status[1] + status[2] + status[3] + status[4] + status[5] +
          status[6] + status[7] + status[8] ==
      0)
    printf("Stars set\n");
  else
    printf("Stars set failed\n");

  // Set WFS
  for (i = 0; i < NLGS; i++)
    status[i] =
        aao_setWFS(&wfs[i], ShackHartmann, NSUBX, i_sub, 0.5, 589, &star[i]);
  for (i = NLGS; i < NWFS; i++)
    status[i] =
        aao_setWFS(&wfs[i], ShackHartmann, NSUBX, i_sub, 0.5, 500, &star[i]);

  if (status[0] + status[1] + status[2] + status[3] + status[4] + status[5] +
          status[6] + status[7] + status[8] ==
      0)
    printf("WFS set\n");
  else
    printf("WFS set failed\n");

  // Set Atmosphere
  float r0 = 0.129;
  float l0 = 25.;
  float lgsheight = 90000.;
  float lgsfwhm = 11400.;
  status[0] = aao_setAtmosphere(&atm, NLAY, cn2, height_layer, r0, l0,
                                lgsheight, lgsfwhm);

  if (status[0] == 0)
    printf("Atmosphere set\n");
  else
    printf("Atmosphere set failed\n");

  // Set Probestar
  status[0] = aao_setTargetMCAO(&target, FOV);

  if (status[0] == 0)
    printf("Probestar set\n");
  else
    printf("Probestar set failed\n");

  // Init system
  status[0] = aao_initSystem(&MCAO_on_EELT, MCAO, TELDIAM, ClosedLoop, 0.002,
                             NWFS, wfs, 1, &dm, &atm, 1, &target);

  if (status[0] == 0)
    printf("System init successful\n");
  else
    printf("System init failed\n");
  printf("\n");

  // Init FEWHA params
  status[0] = aao_setFEWHAparams(&fewha_param, NLAY, d_lay, J_lay, J_glcs,
                                 alpha, alpha_eta, alpha_J, gain, leaky_int,
                                 max_iter, useGLMS, usePrecond, createPrecondFromFile);
  if (status[0] == 0)
    printf("FEWHA params init successful\n");
  else
    printf("FEWHA params init failed\n");
  printf("\n");

  // Init FEWHA Parallelization configuration
  status[0] = aao_setFEWHAparall(&fewha_parall, global_par, max_thr_system);
  if (status[0] == 0)
    printf("FEWHA parallelization set successful\n");
  else
    printf("FEWHA parallelization set failed\n");
  printf("\n");

  // Inite FEWHA
  aao_initFEWHA(&fewha, &MCAO_on_EELT, &fewha_param, &fewha_parall);

  // Init sample measurements
  slopes = (float *)calloc(fewha.n_tot_meas, sizeof(float));
  for (i = 0; i < fewha.n_tot_meas; i++)
    slopes[i] = 1.0;
  dmshape = (float *)calloc(fewha.n_tot_act, sizeof(float));

  // Call the FEWHA reconstructor
  aao_runFEWHA(dmshape, slopes, &fewha);

  // Inverse FEWHA (DM shapes to slopes)
  aao_invFEWHA(slopes, dmshape, &fewha);
  {
    int i1, i2;
    for (i1 = 0; i1 < NACTX; i1++)
      for (i2 = 0; i2 < NACTX; i2++)
        dmshape[NACTX * i1 + i2] = actpos_x[i1];

    aao_invFEWHA(slopes, dmshape, &fewha);

    aao_print_vec_float("dmshape", dmshape, fewha.n_tot_act);
    aao_print_vec_float("slopes", slopes, fewha.n_tot_meas);
  }

  // Destroy FEWHA
  aao_freeFEWHAparams(&fewha_param);
  aao_freeFEWHA(&fewha);

  // Destroy system
  aao_freeSystem(&MCAO_on_EELT);

  // free local variables
  free(slopes);
  slopes = NULL;
  free(dmshape);
  dmshape = NULL;

  aao_freeDM(&dm);
  for (i = 0; i < NWFS; i++)
    aao_freeWFS(&wfs[i]);
  aao_freeAtmosphere(&atm);
  // no freeStar(&target) or freeStar(&star[i]) needed -> no dynamic memory in
  // Star
}

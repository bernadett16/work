/*
 * test.c
 *
 * test of set and free of all system structs (DM, WFS, Star, Atmoshere)
 * test initSystem
 *
 *  Created on: Jun 26, 2014
 *      Author: misha
 */

#include <stdio.h>
#include <stdlib.h>

#include "test_initFEWHA.h"

#include "aao_atmosphere.h"
#include "aao_dm.h"
#include "aao_star.h"
#include "aao_system.h"
#include "aao_wfs.h"

#include "aao_FEWHA.h"
#include "aao_FEWHA_parallel.h"
#include "aao_FEWHA_params.h"

#include "aao_print.h"

#define NLGS 4
#define NNGS 3 // ignoring the on-axis truth sensor.
#define NWFS 7
#define NSUBX 7
#define NACTX 8
#define TELDIAM 4.2
#define NLAY 3

void test_initCANARY_FEWHA() {
  // System structs
  aao_System LTAO_on_CANARY;
  aao_DM dm;
  aao_Star star[NWFS];
  aao_WFS wfs[NWFS];
  aao_Atmosphere atm;
  aao_Star target;

  // FEWHA structs
  aao_FEWHA fewha = {0};
  aao_FEWHA_Params fewha_param = {0};
  aao_FEWHA_Parallel fewha_parall = {0};
  char *createPrecondFromFile = NULL; // use precond from a txt file, if not null (heavy computation)

  // sample measurements, sample DM
  float *slopes = NULL, *dmshape = NULL;

  // local variables
  int i;
  int status[NWFS];

  // System settings
  float actpos_x[NACTX];
  unsigned char i_act[NACTX * NACTX] = {
      0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0};

  unsigned char i_sub[NSUBX * NSUBX] = {
      0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
      1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0};
  float cn2[NLAY];
  float height_layer[NLAY];

  // init settings
  for (i = 0; i < NACTX; i++)
    actpos_x[i] = i * (TELDIAM / NSUBX) - TELDIAM / 2;
  cn2[0] = 0.7;
  cn2[1] = 0.2;
  cn2[2] = 0.1;
  height_layer[0] = 0;
  height_layer[1] = 4000;
  height_layer[2] = 12700;

  // FEWHA settings
  float d_lay[NLAY] = {0.6, 0.6,
                       0.6}; // discretization spacing on each layer, in meters
                             // discretization of the *first* layer must be the
                             // same as of the ground DM!!
  int J_lay[NLAY] = {5, 5, 5}; // number of wavelet scales on each layer
                               // corresponds to 2^5x2^5 = 32x32 point grids
  float alpha = 1;             // regularization tuning parameter
  float alpha_eta =
      0.4; // spot elongation tuning parameter (=0 NGS model, =1 full LGS model)
  float gain = 0.4;       // loop gain
  float leaky_int = 0.99; // leaky integrator value
  int max_iter = 5;       // number of CG iterations

  int useGLMS = 1;    // GLMS method, turned off
  int usePrecond = 0; // preconditioner, turned off
  float alpha_J = 0;  // preconditioner parameter, turned off for now
  int J_glcs =
      4; // GLMS (ground layer multi-scale) parameter, turned off for now

  int global_par = 0;      // global parallelization on
  int max_thr_system = 12; // number of threads

  printf("==== Testing init CANARY ====\n");

  // Set DM
  status[0] = aao_setSquareDM(&dm, NACTX, 0, i_act, actpos_x, actpos_x);

  if (status[0] == 0)
    printf("DM set\n");
  else
    printf("DM set failed \n");

  // Set guide stars
  //	float lgsdiam=0.3;
  status[0] =
      aao_setLGS(&star[0], -0.23566666667, 0.23566666667, 10000, 0.9, 0., 0.);
  status[1] =
      aao_setLGS(&star[1], -0.23566666667, -0.23566666667, 10000, 0.9, 0., 0.);
  status[2] =
      aao_setLGS(&star[2], 0.23566666667, 0.23566666667, 10000, 0.9, 0., 0.);
  status[3] =
      aao_setLGS(&star[3], 0.23566666667, -0.23566666667, 10000, 0.9, 0., 0.);
  status[4] = aao_setNGS(&star[4], 0., -1., 10000, 0.9);
  status[5] = aao_setNGS(&star[5], 0.86600000000, 0.50000000000, 10000, 0.9);
  status[6] = aao_setNGS(&star[6], -0.86600000000, 0.50000000000, 10000, 0.9);
  //	status[0] = aao_setLGS(&star[0],  lgsdiam,      0,      300, 1.5,
  //0.,0.); 	status[1] = aao_setLGS(&star[1],  0,lgsdiam, 300, 1.5, 0.,0.);
  //	status[2] = aao_setLGS(&star[2], -lgsdiam,0, 300, 1.5, 0.,0.);
  //	status[3] = aao_setLGS(&star[3], 0,-lgsdiam, 300, 1.5, 0.,0.);
  //	status[4] = aao_setNGS(&star[4], 0.5, 0.1,      300, 0.7);
  //	status[5] = aao_setNGS(&star[5], -0.3, 0.2, 300, 0.7);
  //	status[6] = aao_setNGS(&star[6], -0.1,-0.4, 300, 0.7);

  if (status[0] + status[1] + status[2] + status[3] + status[4] + status[5] +
          status[6] ==
      0)
    printf("Stars set\n");
  else
    printf("Stars set failed\n");

  // Set WFS
  for (i = 0; i < NLGS; i++)
    status[i] =
        aao_setWFS(&wfs[i], ShackHartmann, NSUBX, i_sub, 0.6, 532., &star[i]);
  for (i = NLGS; i < NWFS; i++)
    status[i] =
        aao_setWFS(&wfs[i], ShackHartmann, NSUBX, i_sub, 0.6, 800, &star[i]);

  if (status[0] + status[1] + status[2] + status[3] + status[4] + status[5] +
          status[6] ==
      0)
    printf("WFS set\n");
  else
    printf("WFS set failed\n");

  // Set Atmosphere
  float r0 = 0.12;
  float l0 = 30.;
  float lgsheight = 22000.;
  float lgsfwhm = 1000.;
  status[0] =
      aao_setAtmosphere(&atm, 3, cn2, height_layer, r0, l0, lgsheight, lgsfwhm);

  if (status[0] == 0)
    printf("Atmosphere set\n");
  else
    printf("Atmosphere set failed\n");

  // Set Probestar
  status[0] = aao_setTarget(&target, 0., 0.);

  if (status[0] == 0)
    printf("Probestar set\n");
  else
    printf("Probestar set failed\n");

  // Init system
  status[0] = aao_initSystem(&LTAO_on_CANARY, LTAO, TELDIAM, ClosedLoop,
                             1 / 150., NWFS, wfs, 1, &dm, &atm, 1, &target);

  if (status[0] == 0)
    printf("System init successful\n");
  else
    printf("System init failed\n");
  printf("\n");

  // Init FEWHA params
  status[0] = aao_setFEWHAparams(&fewha_param, NLAY, d_lay, J_lay, J_glcs,
                                 alpha, alpha_eta, alpha_J, gain, leaky_int,
                                 max_iter, useGLMS, usePrecond, createPrecondFromFile);
  if (status[0] == 0)
    printf("FEWHA params init successful\n");
  else
    printf("FEWHA params init failed\n");
  printf("\n");

  // Init FEWHA Parallelization configuration
  status[0] = aao_setFEWHAparall(&fewha_parall, global_par, max_thr_system);
  if (status[0] == 0)
    printf("FEWHA parallelization set successful\n");
  else
    printf("FEWHA parallelization set failed\n");
  printf("\n");

  // Print FEWHA version
  aao_printFEWHAversion(NULL);

  // Init FEWHA
  aao_initFEWHA(&fewha, &LTAO_on_CANARY, &fewha_param, &fewha_parall);

  // Open FEWHA log
  aao_openFEWHAlog(&fewha, ".", "LTAOonCANARY");

  // Print FEWHA config
  aao_printFEWHAconfig(&fewha);

  // Init sample measurements
  slopes = (float *)calloc(fewha.n_tot_meas, sizeof(float));
  for (i = 0; i < fewha.n_tot_meas; i++)
    slopes[i] = 1.0;
  dmshape = (float *)calloc(fewha.n_tot_act, sizeof(float));

  // Call the FEWHA reconstructor
  aao_runFEWHA(dmshape, slopes, &fewha);

  // Write FEWHA log
  aao_writeFEWHAlog(&fewha);
  aao_writeFEWHAlog(&fewha);
  aao_writeFEWHAlog(&fewha);

  // Open loop
  aao_resetFEWHAloop(dmshape, &fewha);

  // Inverse FEWHA (DM shapes to slopes)
  aao_invFEWHA(slopes, dmshape, &fewha);
  {
    int i1, i2;
    for (i1 = 0; i1 < NACTX; i1++)
      for (i2 = 0; i2 < NACTX; i2++)
        dmshape[NACTX * i1 + i2] = actpos_x[i1];

    aao_invFEWHA(slopes, dmshape, &fewha);

    aao_print_vec_float("dmshape", dmshape, fewha.n_tot_act);
    aao_print_vec_float("slopes", slopes, fewha.n_tot_meas);
  }

  // close fewha log
  aao_closeFEWHAlog(&fewha);

  // Destroy FEWHA
  aao_freeFEWHAparams(&fewha_param);
  aao_freeFEWHA(&fewha);

  // Destroy system
  aao_freeSystem(&LTAO_on_CANARY);

  // free local variables
  free(slopes);
  slopes = NULL;
  free(dmshape);
  dmshape = NULL;

  aao_freeDM(&dm);
  for (i = 0; i < NWFS; i++)
    aao_freeWFS(&wfs[i]);
  aao_freeAtmosphere(&atm);
  // no freeStar(&target) or freeStar(&star[i]) needed -> no dynamic memory in
  // Star
}

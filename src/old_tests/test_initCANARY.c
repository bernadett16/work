/*
 * test.c
 * 
 * test of set and free of all system structs (DM, WFS, Star, Atmoshere)
 * test initSystem
 *
 *  Created on: Jun 26, 2014
 *      Author: misha
 */

#include <stdlib.h>
#include <stdio.h>

#include "aao_atmosphere.h"
#include "aao_dm.h"
#include "aao_star.h"
#include "aao_wfs.h"
#include "aao_system.h"


#define NLGS 4
#define NNGS 3  //ignoring the on-axis truth sensor.
#define NWFS 7
#define NSUBX 7
#define NACTX 8
#define TELDIAM 4.2
#define NLAY 3
void test_initCANARY()
{
	// System structs
	aao_System LTAO_on_CANARY;
	aao_DM dm;
	aao_Star star[NWFS];
	aao_WFS wfs[NWFS];
	aao_Atmosphere atm;
	aao_Star target;

	// local variables
	int i;
	int status[NWFS];

	// System settings
	float actpos_x[NACTX];
	unsigned char i_act[NACTX*NACTX]={0,0,1,1,1,1,0,0,
					  0,1,1,1,1,1,1,0,
					  1,1,1,1,1,1,1,1,
					  1,1,1,1,1,1,1,1,
					  1,1,1,1,1,1,1,1,
					  1,1,1,1,1,1,1,1,
					  0,1,1,1,1,1,1,0,
					  0,0,1,1,1,1,0,0};

	unsigned char i_sub[NSUBX*NSUBX]={0,0,1,1,1,0,0,
					  0,1,1,1,1,1,0,
					  1,1,1,1,1,1,1,
					  1,1,1,0,1,1,1,
					  1,1,1,1,1,1,1,
					  0,1,1,1,1,1,0,
					  0,0,1,1,1,0,0};
	float cn2[NLAY];
	float height_layer[NLAY];

	// init settings
	for(i=0; i<NACTX; i++)
	  actpos_x[i] =i*(TELDIAM/NSUBX)-TELDIAM/2;
	cn2[0] = 0.7;
	cn2[1] = 0.2;
	cn2[2] = 0.1;
	height_layer[0] = 0;
	height_layer[1] = 4000;
	height_layer[2] = 12700;

	printf("==== Testing init CANARY ====\n");


	// Set DM
	status[0] = aao_setSquareDM(&dm, NACTX, 0, i_act, actpos_x, actpos_x);

	if (status[0]==0)
		printf("DM set\n");
	else
		printf("DM set failed \n");


	// Set guide stars
	float lgsdiam=0.3;
	status[0] = aao_setLGS(&star[0],  lgsdiam,      0,      300, 1.5, 0.,0.);
	status[1] = aao_setLGS(&star[1],  0,lgsdiam, 300, 1.5, 0.,0.);
	status[2] = aao_setLGS(&star[2], -lgsdiam,0, 300, 1.5, 0.,0.);
	status[3] = aao_setLGS(&star[3], 0,-lgsdiam, 300, 1.5, 0.,0.);
	status[4] = aao_setNGS(&star[4], 0.5, 0.1,      300, 0.7);
	status[5] = aao_setNGS(&star[5], -0.3, 0.2, 300, 0.7);
	status[6] = aao_setNGS(&star[6], -0.1,-0.4, 300, 0.7);

	if (status[0] + status[1] + status[2] + status[3] + status[4] +
	    status[5] + status[6] == 0)
		printf("Stars set\n");
	else
		printf("Stars set failed\n");

	// Set WFS
	for(i=0; i<NLGS; i++)
		status[i] = aao_setWFS(&wfs[i], ShackHartmann, NSUBX, i_sub, 0.6, 532., &star[i]);
	for(i=NLGS; i<NWFS; i++)
		status[i] = aao_setWFS(&wfs[i], ShackHartmann, NSUBX, i_sub, 0.6, 650, &star[i]);

	if (status[0] + status[1] + status[2] + status[3] + status[4] +
	    status[5] + status[6] == 0)
		printf("WFS set\n");
	else
		printf("WFS set failed\n");


	// Set Atmosphere
	float r0=0.129;
	float l0=25.;
	float lgsheight=22000.;
	float lgsfwhm=1200.;
	status[0] = aao_setAtmosphere(&atm, 3, cn2, height_layer, r0,l0, lgsheight,lgsfwhm);

	if (status[0]==0)
		printf("Atmosphere set\n");
	else
		printf("Atmosphere set failed\n");

	// Set Probestar
	status[0] = aao_setTarget(&target, 0., 0.);

	if (status[0]==0)
		printf("Probestar set\n");
	else
		printf("Probestar set failed\n");


	// Init system
	status[0] = aao_initSystem(&LTAO_on_CANARY, LTAO, TELDIAM, ClosedLoop, 1/150., NWFS, wfs, 1, &dm, &atm, 1, &target);

	if (status[0]==0)
		printf("System init successful\n");
	else
		printf("System init failed\n");
	printf("\n");

	// Destroy system
	aao_freeSystem(&LTAO_on_CANARY);

	// free local variables
	aao_freeDM(&dm);
	for(i=0; i<NWFS; i++)
		aao_freeWFS(&wfs[i]);
	aao_freeAtmosphere(&atm);
	// no freeStar(&target) or freeStar(&star[i]) needed -> no dynamic memory in Star


}

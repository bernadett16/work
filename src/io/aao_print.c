/*
 * aao_print.c
 *
 *  Created on: Oct 5, 2014
 *      Author: misha
 */

#include <stdlib.h>
#include <stdio.h>
#include <float.h>
#include <limits.h>
#include "aao_print.h"

/* ============ Local functions ============ */
float aao_min_vec_float(float const * vec, int len);
float aao_max_vec_float(float const * vec, int len);
int aao_min_vec_int    (int const * vec, int len);
int aao_max_vec_int    (int const * vec, int len);
int aao_nnz_vec_float  (float const * vec, int len);
int aao_nnz_vec_int    (int const * vec, int len);
int aao_nnz_vec_uint8  (unsigned char const * vec, int len);
int aao_nnz_vec_uint16 (unsigned short const * vec, int len);
/* ========================================= */

void aao_print_vec_float(char const * name, float const * vec, int len)
{
	// local var
	int i;

	// code
	if (vec == NULL)
	{
		printf("%s: NULL\n",name);
	}
	else if (len==0)
	{
		printf("%s: %p, size(%s)=%d, %s=[]\n",name,vec,name,len,name);
	}
	else
	{
		printf("%s: %p, size(%s)=%d, %s=[",name,vec,name,len,name);
		for(i=0; i<len-1; i++)
//			printf("%f, ",vec[i]);
//		printf("%f], ",vec[len-1]);
			printf("%0.20f, ",vec[i]);
		printf("%0.20f], ",vec[len-1]);
		printf("min(%s)=%f, max(%s)=%f, nnz(%s)=%d\n",name,aao_min_vec_float(vec,len),name,aao_max_vec_float(vec,len),name,aao_nnz_vec_float(vec,len));
	}
}
void aao_print_vec_int(char const * name, int const * vec, int len)
{
	// local var
	int i;

	// code
	if (vec == NULL)
	{
		printf("%s: NULL\n",name);
	}
	else if (len==0)
	{
		printf("%s: %p, size(%s)=%d, %s=[]\n",name,vec,name,len,name);
	}
	else
	{
		printf("%s: %p, size(%s)=%d, %s=[",name,vec,name,len,name);
		for(i=0; i<len-1; i++)
			printf("%d, ",vec[i]);
		printf("%d], ",vec[len-1]);
		printf("min(%s)=%d, max(%s)=%d, nnz(%s)=%d\n",name,aao_min_vec_int(vec,len),name,aao_max_vec_int(vec,len),name,aao_nnz_vec_int(vec,len));
	}
}
void aao_print_vec_uint16(char const * name, unsigned short const * vec, int len)
{
	// local var
	int i;

	// code
	if (vec == NULL)
	{
		printf("%s: NULL\n",name);
	}
	else if (len==0)
	{
		printf("%s: %p, size(%s)=%d, %s=[]\n",name,vec,name,len,name);
	}
	else
	{
		printf("%s: %p, size(%s)=%d, %s=[",name,vec,name,len,name);
		for(i=0; i<len-1; i++)
			printf("%d, ",vec[i]);
		printf("%d], ",vec[len-1]);
		printf("nnz(%s)=%d\n",name,aao_nnz_vec_uint16(vec,len));

	}
}
void aao_print_vec_float_compact(char const * name, float const * vec, int len)
{
	// code
	if (vec == NULL)
	{
		printf("%s: NULL\n",name);
	}
	else if (len==0)
	{
		printf("%s: %p, size(%s)=%d, %s=[]\n",name,vec,name,len,name);
	}
	else
	{
		printf("%s: %p, size(%s)=%d, %s=[%f, ..., %f], ",name,vec,name,len,name,vec[0],vec[len-1]);
		printf("min(%s)=%f, max(%s)=%f, nnz(%s)=%d\n",name,aao_min_vec_float(vec,len),name,aao_max_vec_float(vec,len),name,aao_nnz_vec_float(vec,len));

	}
}
void aao_print_vec_uint8_compact(char const * name, unsigned char const * vec, int len)
{
	// code
	if (vec == NULL)
	{
		printf("%s: NULL\n",name);
	}
	else if (len==0)
	{
		printf("%s: %p, size(%s)=%d, %s=[]\n",name,vec,name,len,name);
	}
	else
	{
		printf("%s: %p, size(%s)=%d, %s=[%d, ..., %d], ",name,vec,name,len,name,vec[0],vec[len-1]);
		printf("nnz(%s)=%d\n",name,aao_nnz_vec_uint8(vec,len));
	}
}
float aao_min_vec_float(float const * vec, int len)
{
	int i;
	float ret;

	ret = FLT_MAX;
	for(i=0; i<len; i++)
		ret = (vec[i] < ret) ? vec[i] : ret;

	return ret;
}
float aao_max_vec_float(float const * vec, int len)
{
	int i;
	float ret;

	ret = FLT_MIN;
	for(i=0; i<len; i++)
		ret = (vec[i] > ret) ? vec[i] : ret;

	return ret;
}
int aao_min_vec_int(int const * vec, int len)
{
	int i;
	int ret;

	ret = INT_MAX;
	for(i=0; i<len; i++)
		ret = (vec[i] < ret) ? vec[i] : ret;

	return ret;
}
int aao_max_vec_int(int const * vec, int len)
{
	int i;
	int ret;

	ret = INT_MIN;
	for(i=0; i<len; i++)
		ret = (vec[i] > ret) ? vec[i] : ret;

	return ret;
}
int aao_nnz_vec_float(float const * vec, int len)
{
	int i;
	int ret;

	ret = 0;
	for(i=0; i<len; i++)
		ret += (vec[i] != ret);

	return ret;
}
int aao_nnz_vec_int(int const * vec, int len)
{
	int i;
	int ret;

	ret = 0;
	for(i=0; i<len; i++)
		ret += (vec[i] != ret);

	return ret;
}
int aao_nnz_vec_uint8(unsigned char const * vec, int len)
{
	int i;
	int ret;

	ret = 0;
	for(i=0; i<len; i++)
		ret += (vec[i] != ret);

	return ret;
}
int aao_nnz_vec_uint16(unsigned short const * vec, int len)
{
	int i;
	int ret;

	ret = 0;
	for(i=0; i<len; i++)
		ret += (vec[i] != ret);

	return ret;
}

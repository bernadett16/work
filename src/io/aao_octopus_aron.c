/*
 * aaao_octopus_aron.c
 *
 *  Created on: Oct 17, 2014
 *      Author: misha
 */

#include <float.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "aao_octopus_aron.h"

// ========= Local functions ==========
// use for mask computation
// returns 1 if |x - x_i| <=smallval && |y - y_i| <= smallval for at least 1 point (x_i,y_i) in file
// else returns 0
unsigned char aao_octopusAronContains(aao_OctopusAronFile const * octfile, int indx, float x, float y, float smallval);
// ====================================

aao_OctopusAronFile aao_readOctopusAronFile(char const * communication_dir, char const * filename)
{
	// local var
	aao_OctopusAronFile octfile = {0};
	FILE * file = NULL;
	char fullpath[2048] = "";
	int c, len, i;
	float col0, col1;
	int col2, n_read;

	// local var init
	sprintf(fullpath,"%s/%s",communication_dir,filename);
	strcpy(octfile.filename, filename);
	octfile.read_status = -1; // NOK

	// code
	file = fopen(fullpath,"r");

	// if opening file failed
	if(file == NULL)
	{
		octfile.col0 = NULL;
		octfile.col1 = NULL;
		octfile.col2 = NULL;
		octfile.len = 0;
		octfile.read_status = -1;	// set status to NOK
		return octfile;
	}

	// count lines
	len = 0;
	while (EOF != (c=fgetc(file)))
	    if (c=='\n')
	        len++;
	len++; // add the last line

	// allocate arrays
	octfile.col0 = (float*)calloc(len,sizeof(float));
	octfile.col1 = (float*)calloc(len,sizeof(float));
	octfile.col2 = (int*)  calloc(len,sizeof(float));

	// rewind file to beginning
	rewind(file);

	// read values; format: "%f %f %d \n"
	i=0; // write counter
	while ( (n_read=fscanf(file, "%f %f %d", &col0, &col1, &col2)) != EOF)
	{
		if(n_read==3)
		{
			octfile.col0[i] = col0;
			octfile.col1[i] = col1;
			octfile.col2[i] = col2;
			i++;
		}
	}
	octfile.len = i; // number of entries written
	fclose(file);

	// set status to OK
	octfile.read_status = 0;

	return octfile;
}

unsigned char aao_octopusAronContains(aao_OctopusAronFile const * octfile, int indx, float x, float y, float smallval)
{
	// local var
	int i;
	unsigned int contains;

	// code
	contains = 0;

	for(i=0; i<octfile->len; i++)
	{
		// index matched
		if(octfile->col2[i] == indx)
		{
			if(fabsf(x-octfile->col0[i])<=smallval && fabsf(y-octfile->col1[i])<=smallval)
			{
				contains = 1;
				break;
			}
		}
	}

	return contains;
}

aao_OctopusAronBlock aao_deriveOctopusAronBlock(aao_OctopusAronFile const * octfile, int indx, float teldiam)
{
	// local var
	aao_OctopusAronBlock octblk = {0};
	int n_act, n_sub, n_act2, n_sub2;
	unsigned char * i_act = NULL;
	unsigned char * i_sub = NULL;
	float * actpos_x = NULL;
	float * actpos_y = NULL;
	float spacing; // need teldiam ONLY if 1x1 sensor!!

	float x, y; // help quantities
	int i, j;
	float min, max; // for searching min and max
	float smallval = 1e-6;

	// code
	// task 1: compute the spacing
	spacing = teldiam; // ONLY if 1x1 sensor
	for(i=0; i<octfile->len-1; i++)
	{
		if(octfile->col2[i] == indx && octfile->col2[i+1] == indx && octfile->col0[i] == octfile->col0[i+1])
		{
			// e.g., -2.100000e+01 -4.000000e+00 0  and  -2.100000e+01 -3.500000e+00 0
			spacing = octfile->col1[i+1] - octfile->col1[i]; // substract the y-values
			break;
		}
	}

	// task 2: compute min and max values
	min = FLT_MAX;
	max = -FLT_MAX;

	for(i=0; i<octfile->len; i++)
	{
		// index matched
		if(octfile->col2[i] == indx)
		{
			// find block's minimum value, doesn't matter if x or y
			min = (octfile->col0[i] < min) ? octfile->col0[i] : min;
			min = (octfile->col1[i] < min) ? octfile->col1[i] : min;

			// find block's maximum value, doesn't matter if x or y
			max = (octfile->col0[i] > max) ? octfile->col0[i] : max;
			max = (octfile->col1[i] > max) ? octfile->col1[i] : max;
		}
	}

	// !!! ASSUMPTION 1 !!! domain is square
	// !!! ASSUMPTION 2 !!! domain is not "shifted", i.e., the middle point *is* (0,0)
	// note: subapertures_aron.txt entries: -21, -20.5, ..., 20.5     => min = -21, max = 20.5
	//          actuators_aron.txt entries: -21, -20.5, ..., 20.5, 21 => min = -21, max = 21

	// do allow domains where -min != max
	max = (max > -min) ? max : -min;	// maximum (max, -min)
	min = -max;
	// note: in both subap and act examples above, now min = -21, max = 21

	// task 3: compute n_sub and n_act
	n_sub = (int)roundf(2.0*max/spacing);
	n_act = n_sub+1;

	n_sub2 = n_sub*n_sub;
	n_act2 = n_act*n_act;

	// task 4: derive points
	actpos_x = (float*)calloc(n_act,sizeof(float));
	actpos_y = (float*)calloc(n_act,sizeof(float));

	for(i=0; i<n_act; i++)
	{
		actpos_x[i] = min+i*spacing;
		actpos_y[i] = min+i*spacing;
	}

	// task 5: derive masks
	i_sub = (unsigned char*)calloc(n_sub2,sizeof(unsigned char));
	i_act = (unsigned char*)calloc(n_act2,sizeof(unsigned char));

	// subaperture mask (subapertures_aron.txt contains only lower left corner points, only these points are traversed)
	for(i=0; i<n_sub; i++)
	for(j=0; j<n_sub; j++)
	{
		x = actpos_x[i];
		y = actpos_y[j];
		i_sub[i*n_sub+j] = aao_octopusAronContains(octfile,indx,x,y,smallval);
	}

	// actuator mask (actuators_aron.txt contains all active actuator points)
	for(i=0; i<n_act; i++)
	for(j=0; j<n_act; j++)
	{
		x = actpos_x[i];
		y = actpos_y[j];
		i_act[i*n_act+j] = aao_octopusAronContains(octfile,indx,x,y,smallval);
	}

	// copy to struct
	octblk.indx     = indx;
	octblk.i_act    = i_act;
	octblk.n_act    = n_act;
	octblk.i_sub    = i_sub;
	octblk.actpos_x = actpos_x;
	octblk.actpos_y = actpos_y;
	octblk.spacing  = spacing;

	return octblk;
}

// Free aao_OctopusAronFile struct
//  Input: octopus_aron_file struct
void aao_freeOctopusAronFile(aao_OctopusAronFile * octfile)
{
	if(octfile->col0 != NULL)
	{
		free(octfile->col0);
		octfile->col0 = NULL;
	}
	if(octfile->col1 != NULL)
	{
		free(octfile->col1);
		octfile->col1 = NULL;
	}
	if(octfile->col2 != NULL)
	{
		free(octfile->col2);
		octfile->col2 = NULL;
	}
	strcpy(octfile->filename,"");
	octfile->len = 0;
}
void aao_freeOctopusAronBlock(aao_OctopusAronBlock * octblk)
{
	if(octblk->i_act != NULL)
	{
		free(octblk->i_act);
		octblk->i_act = NULL;
	}
	if(octblk->i_sub != NULL)
	{
		free(octblk->i_sub);
		octblk->i_sub = NULL;
	}
	if(octblk->actpos_x != NULL)
	{
		free(octblk->actpos_x);
		octblk->actpos_x = NULL;
	}
	if(octblk->actpos_y != NULL)
	{
		free(octblk->actpos_y);
		octblk->actpos_y = NULL;
	}
}


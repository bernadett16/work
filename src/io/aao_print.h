/*
 * aao_print.h
 *
 *  Created on: Oct 5, 2014
 *      Author: misha
 */

#ifndef AAO_PRINT_H_
#define AAO_PRINT_H_

// print functions: to the console
// format: name: 0x000addr, size(name)=len, name=[vec[0], vec[1], vec[2], ..., vec[len-1]]
void aao_print_vec_float (char const * name, float const * vec, int len);
void aao_print_vec_int   (char const * name, int const * vec, int len);
void aao_print_vec_uint16(char const * name, unsigned short const * vec, int len);

// format: name: 0x000addr, size(name)=len, name=[vec[0], ..., vec[len-1]]
void aao_print_vec_float_compact(char const * name, float const * vec, int len);
void aao_print_vec_uint8_compact(char const * name, unsigned char const * vec, int len);


#endif /* AAO_PRINT_H_ */

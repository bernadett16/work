/*
 * applyF.c
 *
 *  Created on: Jul 24, 2014
 *      Author: misha
 */

#include "aao_operators.h"
#include "aao_proj_op.h"
#include "aao_vec_op.h"
#include "aao_wav_op.h"
#include <stdlib.h>

/* F is the evaluation (wav coeff to DM) operator */

// tmp_lay in this call is a temporary variable of layer size (nLay*n_lay*n_lay)
// for example, a good candidate is Loop tmp_b
void aao_applyF(float *a, aao_OperatorF const *F, float *c, float *tmp_lay,
                aao_FEWHA const *fewha, aao_FEWHA_Parallel const *par) {
  // local variables
  int iLay, iDIR;
  int nLay, nDIR;
  float *tmp_U = NULL; // tmp variable for wavelet transform

  int const *n_lay = NULL, *os_lay = NULL;
  int const *n_act = NULL, *os_act = NULL;

  tmp_U = F->W->tmp_U;
  nLay = fewha->nLay;
  nDIR = fewha->nDIR;
  n_lay = fewha->n_lay_at_lay;
  os_lay = fewha->os_lay_at_lay;
  n_act = fewha->n_act_at_dm;
  os_act = fewha->os_act_at_dm;

  //#pragma omp parallel private(iLay,iDIR) num_threads(par->max_thr_applyF)
  //if(par->global)
  //{
  //	#pragma omp for
  for (iLay = 0; iLay < nLay; iLay++) {
    // =========== apply iW ==========
    // 1. wscf.c2e * c
    // q = c2e*c
    aao_vec_ti_const(tmp_lay + os_lay[iLay], F->W->wscf_c2e[iLay],
                     c + os_lay[iLay], n_lay[iLay] * n_lay[iLay]);

    // 2. DWT
    // q = W^{-1}q
    aao_waverec2_db3(tmp_lay + os_lay[iLay], n_lay[iLay], tmp_U + os_lay[iLay]);
  }

  // in case of LTAO, MOAO: nDIR >= 1, so projections are performed:
  //	#pragma omp for
  for (iDIR = 0; iDIR < nDIR; iDIR++) {
    // =========== apply P ===========
    // 1. layer 0
    // dm_iDIR = P_{iDIR,0} tmp_lay_0 = R tmp_lay_0
    aao_vec_rest_sqr(a + os_act[iDIR], tmp_lay + os_lay[0], n_act[iDIR],
                     n_lay[0]);

    // 2. layer 1,2,...
    for (iLay = 1; iLay < nLay; iLay++) {
      // dm_iDIR += P_{iDIR,iLay} tmp_lay_{iLay} for iLay>0
      aao_interp2_ac(a + os_act[iDIR], F->P.I_arr[nLay * iDIR + iLay],
                     tmp_lay + os_lay[iLay], n_act[iDIR], n_lay[iLay]);
    }
  }

  // in case of SCAO, MCAO(nLay=nDM): layer values are copied to DMs
  if (nDIR == 0) {
    // WARNING: this assumes that layers are discretized on DMs!!!
    //		#pragma omp for
    for (iLay = 0; iLay < nLay; iLay++) {
      // dm_iLay = R tmp_lay_{iLay}
      aao_vec_rest_sqr(a + os_act[iLay], tmp_lay + os_lay[iLay], n_act[iLay],
                       n_lay[iLay]);
    }
  }
  //}
}

/*
 * operators.h
 *
 *  Created on: Jul 4, 2014
 *      Author: misha
 */

#ifndef AAO_OPERATORS_H_
#define AAO_OPERATORS_H_

#include "aao_FEWHA.h"
#include "aao_FEWHA_parallel.h"

// =========== Operator structs ===========
#include "aao_operators_structs.h"


// =========== Operator actions ===========
void  aao_applyA    (float * q, aao_OperatorA const * A, float * c, aao_FEWHA const * fewha, aao_FEWHA_Parallel const * par); // q = Ac
float aao_normAwoD2 (aao_OperatorA const * A, float * c, float * tmp_s, aao_FEWHA const * fewha, aao_FEWHA_Parallel const * par); // sqrt(AwoDe,e)
void  aao_applyA_GLMS(float * q, aao_OperatorA const * A, float * v, aao_FEWHA const * fewha, aao_FEWHA_Parallel const * par); // q = A_{GLMS}v
void  aao_applyB    (float * b, aao_OperatorA const * B, float * s, aao_FEWHA const * fewha, aao_FEWHA_Parallel const * par); // b = Bs, B reuses elements in A
void  aao_applyH    (float * s, aao_OperatorH const * H, float const * a, aao_FEWHA const * fewha, aao_FEWHA_Parallel const * par); // s = Ha
void  aao_applyF    (float * a, aao_OperatorF const * F, float * c, float * tmp_lay, aao_FEWHA const * fewha, aao_FEWHA_Parallel const * par); // a = Fc


// =========== Operator init/free =========
void aao_initW(aao_OperatorW * W, aao_FEWHA const * fewha);
void aao_freeW(aao_OperatorW * W);

void aao_initP_Lay    (aao_OperatorP * P, aao_FEWHA const * fewha);
void aao_initP_DM     (aao_OperatorP * P, aao_FEWHA const * fewha);
void aao_initP_Lay_DIR(aao_OperatorP * P, aao_FEWHA const * fewha); // direction of interest
void aao_freeP_Lay    (aao_OperatorP * P, aao_FEWHA const * fewha);
void aao_freeP_DM     (aao_OperatorP * P, aao_FEWHA const * fewha);
void aao_freeP_Lay_DIR(aao_OperatorP * P, aao_FEWHA const * fewha); // direction of interest

void aao_initSH_tt(aao_OperatorSH * SH, aao_FEWHA const * fewha);
void aao_initSH(aao_OperatorSH * SH, aao_FEWHA const * fewha);
void aao_freeSH(aao_OperatorSH * SH);

void aao_initiC(aao_OperatoriC * iC, aao_FEWHA const * fewha);
void aao_freeiC(aao_OperatoriC * iC);

void aao_initD(aao_OperatorD * D, aao_FEWHA const * fewha);
void aao_freeD(aao_OperatorD * D, aao_FEWHA const * fewha);

void aao_initiJ(aao_OperatoriJ * iJ, aao_FEWHA const * fewha, aao_OperatorA const * A, aao_FEWHA_Parallel const * parall, char* createPrecondFromFile);
void aao_freeiJ(aao_OperatoriJ * iJ);

void aao_initG(aao_OperatorG * G, aao_FEWHA const * fewha, aao_OperatorA const * A, aao_FEWHA_Parallel const * parall);
void aao_freeG(aao_OperatorG * G);

void aao_initA(aao_OperatorA * A, aao_FEWHA const * fewha);
void aao_freeA(aao_OperatorA * A, aao_FEWHA const * fewha);

void aao_initH(aao_OperatorH * H, aao_FEWHA const * fewha, aao_OperatorA const * A);
void aao_freeH(aao_OperatorH * H, aao_FEWHA const * fewha);

void aao_initF(aao_OperatorF * F, aao_FEWHA const * fewha, aao_OperatorA const * A);
void aao_freeF(aao_OperatorF * F, aao_FEWHA const * fewha);

// =========== Operator print =============
void aao_printW (aao_OperatorW  const * W,  aao_FEWHA const * fewha);
void aao_printP (aao_OperatorP  const * P,  aao_FEWHA const * fewha, int isLay, int isWFS);
void aao_printSH(aao_OperatorSH const * SH, aao_FEWHA const * fewha);
void aao_printiC(aao_OperatoriC const * iC, aao_FEWHA const * fewha);
void aao_printD (aao_OperatorD  const * D,  aao_FEWHA const * fewha);
void aao_printiJ(aao_OperatoriJ const * iJ, aao_FEWHA const * fewha);
void aao_printG (aao_OperatorG  const * G,  aao_FEWHA const * fewha);

void aao_printA (aao_OperatorA  const * A,  aao_FEWHA const * fewha);
void aao_printH (aao_OperatorH  const * H,  aao_FEWHA const * fewha);
void aao_printF (aao_OperatorF  const * F,  aao_FEWHA const * fewha);


#endif /* AAO_OPERATORS_H_ */

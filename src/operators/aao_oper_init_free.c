/*
 * oper_init_free.c
 *
 *  Created on: Jul 6, 2014
 *      Author: misha
 */


#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "aao_operators.h"
#include "aao_proj_op.h"
#include "aao_sh_op.h"
#include "aao_vec_op.h"
#include "aao_wav_op.h"
#include "aao_print.h"
#include "aao_matr_op.h"

// ========= Local functions =========

void aao_initP_template(aao_OperatorP * P, aao_FEWHA const * fewha, int isLay, int isWFS);
void aao_freeP_template(aao_OperatorP * P, aao_FEWHA const * fewha, int nLay, int nWFS);

// ===================================

void aao_initW(aao_OperatorW * W, aao_FEWHA const * fewha)
{
	// local var
	int iLay;
	int nLay      = fewha->nLay;
	int n_tot_lay = fewha->n_tot_lay;
	float const * d_lay = fewha->param.d_lay_;

	// code
	// d_lay ... parameter delta_\ell in thesis
	// c2e ... parameter delta_\ell^{-1} in thesis
	// e.g., for spacing = 0.5, c2e = 1/0.5 = 2
	W->wscf_c2e = (float*) calloc(nLay, sizeof(float));
	for(iLay=0; iLay<nLay; iLay++)
		W->wscf_c2e[iLay] = 1./d_lay[iLay];

	W->tmp_U = (float*) calloc(n_tot_lay, sizeof(float));
}
void aao_freeW(aao_OperatorW * W)
{
	if(W != NULL)
	{
		if(W->wscf_c2e != NULL)
		{
			free(W->wscf_c2e);
			W->wscf_c2e = NULL;
		}
		if(W->tmp_U != NULL)
		{
			free(W->tmp_U);
			W->tmp_U = NULL;
		}
	}
}
void aao_initP_Lay(aao_OperatorP * P, aao_FEWHA const * fewha)
{
	int isLay = 1;	// or isDM?
	int isWFS = 1;	// or isDIR? (i.e., optimization direction)

	aao_initP_template(P, fewha, isLay, isWFS);
}
void aao_initP_DM (aao_OperatorP * P, aao_FEWHA const * fewha)
{
	int isLay = 0;
	int isWFS = 1;

	aao_initP_template(P, fewha, isLay, isWFS);
}
void aao_initP_Lay_DIR(aao_OperatorP * P, aao_FEWHA const * fewha)
{
	int isLay = 1;	// or isDM?
	int isWFS = 0;	// or isDIR? (i.e., optimization direction)

	aao_initP_template(P, fewha, isLay, isWFS);
}
void aao_freeP_Lay(aao_OperatorP * P, aao_FEWHA const * fewha)
{
	int isLay = 1;
	int isWFS = 1;

	aao_freeP_template(P, fewha, isLay, isWFS);
}
void aao_freeP_DM (aao_OperatorP * P, aao_FEWHA const * fewha)
{
	int isLay = 0; // isDM
	int isWFS = 1;

	aao_freeP_template(P, fewha, isLay, isWFS);
}
void aao_freeP_Lay_DIR(aao_OperatorP * P, aao_FEWHA const * fewha)
{
	int isLay = 1;
	int isWFS = 0;

	aao_freeP_template(P, fewha, isLay, isWFS);
}
void aao_initP_template(aao_OperatorP * P, aao_FEWHA const * fewha, int isLay, int isWFS)
{
	// local var
	int nLay;
	int nLGS, nNGS, nWFS;
	int iLay, iWFS, iWFS_ext = 0;
	int   const * n_lay;
	float const * d_lay;
	float         x0_lay;
	int           n_phi;
	float         d_phi;
	float h, H, pos_x, pos_y;
	int const * int2ext = fewha->wfs_indx_int2ext;	// External WFS ordering

	// local var init
	if(isLay)
	{
		nLay  = fewha->nLay;
		n_lay = fewha->n_lay_at_lay;
		d_lay = fewha->param.d_lay_;
	}
	else // isDM
	{
		nLay  = fewha->nDM;
		n_lay = fewha->n_act_at_dm;
		d_lay = fewha->d_act_at_dm;
	}

	if(isWFS)
	{
		nLGS = fewha->nLGS;
		nNGS = fewha->nNGS;
		nWFS = fewha->nWFS;
	}
	else // isDIR
	{
		nWFS = fewha->nDIR;
		nLGS=0; nNGS=0; // not used in this case
	}

	// code
	if (nWFS>0)
		P->I_arr = (aao_Interp2*) calloc(nWFS*nLay,sizeof(aao_Interp2));
	else // nWFS=0 (this could happen if isDIR, SCAO or MCAO(nLay=nDM)
		P->I_arr = NULL;

	for(iWFS=0; iWFS<nWFS; iWFS++)
	{
		if(isWFS)
			iWFS_ext = int2ext[iWFS];

		for(iLay=0; iLay<nLay; iLay++)
		{
			// TODO MY 27.07.2014 should handle if(isDM) on the ground layer by interpolating there too
			// -> also change in applyP!!
//			if(iLay == 0 && isLay)
			if(iLay == 0)
			{
				// Ground layer -> init to NULL
				aao_initInterp2_NULL(&P->I_arr[nLay*iWFS+iLay]);
			}
			else // iLay > 0 or isDM
			{
				if(isWFS)
				{
					pos_x = fewha->AOsys.wfs_[iWFS_ext].gs_.pos_x_;
					pos_y = fewha->AOsys.wfs_[iWFS_ext].gs_.pos_y_;
					n_phi = fewha->n_phi_at_wfs[iWFS];
					if(iWFS<nLGS+nNGS)
						d_phi=fewha->AOsys.wfs_[iWFS_ext].subap_size_;
					else // TTS, use geometry of first WFS (spacing)
						d_phi=fewha->AOsys.wfs_[int2ext[0]].subap_size_;
				}
				else // isDIR, SR: i.e. fitting step??
				{
					pos_x = fewha->AOsys.target_[iWFS].pos_x_;
					pos_y = fewha->AOsys.target_[iWFS].pos_y_;
					// NOTE: for SCAO and MCAO(nLay=nDM) nDIR is 0
					// for LTAO and MOAO its the spacing of the ground DM
					// for MCAO(nLay>nDM) this case is currently unhandled
					// TODO MY 24.07.2014 handle MCAO(nLay>nDM)
					// (probably will need to define an external configuration variable)
					n_phi = fewha->n_act_at_dm[0];
					d_phi = fewha->d_act_at_dm[0];
				}
				if(isLay)
				{
					h = fewha->AOsys.atmosphere_.height_layer_[iLay];
					x0_lay = fewha->x0_lay_at_lay[iLay];
				}
				else // isDM
				{
					h = fewha->AOsys.dm_[iLay].dm_height_;
					x0_lay = -n_lay[iLay]*d_lay[iLay]/2.;
				}


				if (isWFS && fewha->AOsys.wfs_[iWFS_ext].gs_.type_ == LGS)
					H = fewha->AOsys.atmosphere_.na_height_;
				else // NGS
					// NOTE: if isDIR, these projections are always towards NGS
					H = 0.;

				aao_initInterp2(&P->I_arr[nLay*iWFS+iLay], pos_x, pos_y, h, H, n_phi, d_phi, n_lay[iLay], d_lay[iLay], x0_lay);
			}
		}
	}
}
void aao_freeP_template(aao_OperatorP * P, aao_FEWHA const * fewha, int isLay, int isWFS)
{
	// local var
	int nLay;
	int nWFS;
	int i;

	// local var init
	if (isLay)
		nLay = fewha->nLay;
	else // isDM
		nLay = fewha->nDM;

	if(isWFS)
		nWFS = fewha->nWFS;
	else // is optimization direction
		nWFS = fewha->nDIR;

	// code
	if(P != NULL)
	{
		if(P->I_arr != NULL)
		{
			for(i=0; i<nLay*nWFS; i++)
				aao_freeInterp2(&P->I_arr[i]);
			free(P->I_arr);
			P->I_arr = NULL;
		}
	}
}

void aao_initSH(aao_OperatorSH * SH, aao_FEWHA const * fewha)
{
	// local var
	int nWFS = fewha->nWFS;
	int nLGS = fewha->nLGS;
	int nNGS = fewha->nNGS;
	int iWFS, iWFS_ext;
	int os;
	int n_active_sub2;

	int const * n_phi = fewha->n_phi_at_wfs;
	int const * n_sub = fewha->n_sub_at_wfs;

	int const * int2ext = fewha->wfs_indx_int2ext;	// External WFS ordering

	// code
	SH->I_sub 		  = (unsigned char const **) calloc(nWFS,sizeof(unsigned char*));
	SH->n_active_sub2 = (int*) calloc(nWFS, sizeof(int));
	SH->tmp_SH        = NULL;
	SH->os_tmp_SH 	  = (int*) calloc(nWFS, sizeof(int));

	os = 0;
	for(iWFS=0; iWFS<nWFS; iWFS++)
	{
		iWFS_ext = int2ext[iWFS];

		// !!pointer copy, not value copy!!
		SH->I_sub[iWFS] = fewha->AOsys.wfs_[iWFS_ext].i_sub_;

		// compute number of active subapertures
		n_active_sub2 = aao_numActSubap(SH->I_sub[iWFS], n_sub[iWFS]);
		SH->n_active_sub2[iWFS] = n_active_sub2;

		// compute offsets
		SH->os_tmp_SH[iWFS] = os;			// 0, 85*84, 2*85*84, ...
		if(iWFS<nLGS+nNGS)
			os += n_phi[iWFS]*n_sub[iWFS];
		else
			os += n_phi[iWFS]*n_sub[0]; // use geometry of first WFS for TT
	}

	// os is now nWFS*n_phi*n_sub, i.e., total size of tmp_SH
	SH->tmp_SH = (float*) calloc(os, sizeof(float));

}

void aao_freeSH(aao_OperatorSH * SH)
{
	if(SH != NULL)
	{
		if(SH->I_sub != NULL)
		{
			// do NOT free individual arrays I_sub[iWFS] b/c they are NOT a copy!!!
			free(SH->I_sub);
			SH->I_sub = NULL;
		}
		if(SH->n_active_sub2 != NULL)
		{
			free(SH->n_active_sub2);
			SH->n_active_sub2 = NULL;
		}
		if(SH->tmp_SH != NULL)
		{
			free(SH->tmp_SH);
			SH->tmp_SH = NULL;
		}
		if(SH->os_tmp_SH != NULL)
		{
			free(SH->os_tmp_SH);
			SH->os_tmp_SH = NULL;
		}
	}
}

void aao_initiC(aao_OperatoriC * iC, aao_FEWHA const * fewha)
{
	// local var
	int nWFS = fewha->nWFS;
	int nLGS = fewha->nLGS;
	int iWFS, iWFS_ext;

	int const * n_sub   = fewha->n_sub_at_wfs;
	int const * os_sub  = fewha->os_sub_at_wfs;
	int n_tot_sub_lgs   = fewha->n_tot_sub_lgs;
	int const * int2ext = fewha->wfs_indx_int2ext;

	unsigned char const * I_sub = NULL;
	float inv_sigma2;
	float subap_size, spot_fwhm;
	float na_height, na_fwhm;
	float gs_llt_x, gs_llt_y;
	float alpha_eta;

	int n_photons;
	double scf, scf2, lambda;
	double const nm = 1e-9;

	// init local var
	na_height = fewha->AOsys.atmosphere_.na_height_;
	na_fwhm   = fewha->AOsys.atmosphere_.na_fwhm_;
	alpha_eta = fewha->param.alpha_eta_;


	// code
	iC->invCxx = (float*) calloc(n_tot_sub_lgs, sizeof(float));
	iC->invCxy = (float*) calloc(n_tot_sub_lgs, sizeof(float));
	iC->invCyy = (float*) calloc(n_tot_sub_lgs, sizeof(float));
	iC->inv_sigma2 = (float*) calloc(nWFS, sizeof(float));

	for(iWFS=0; iWFS<nLGS; iWFS++)
	{
		iWFS_ext = int2ext[iWFS];

		// compute sigma^{-2}
		n_photons  = fewha->AOsys.wfs_[iWFS_ext].gs_.n_photons_;
		// TODO MY 08.08.2014 use the scaling factor here
//		lambda     = fewha->AOsys.wfs_[iWFS_ext].wavelength_*nm; // wavelength in meters
//		scf        = 2*M_PI/lambda;
//		scf2       = scf*scf;
//		inv_sigma2 = n_photons * scf2;
		inv_sigma2 = n_photons;

		I_sub      = fewha->AOsys.wfs_[iWFS_ext].i_sub_;
		subap_size = fewha->AOsys.wfs_[iWFS_ext].subap_size_;
		spot_fwhm  = fewha->AOsys.wfs_[iWFS_ext].gs_.spot_fwhm_;
		gs_llt_x   = fewha->AOsys.wfs_[iWFS_ext].gs_.gs_llt_x_;
		gs_llt_y   = fewha->AOsys.wfs_[iWFS_ext].gs_.gs_llt_y_;

		aao_init_invC_LGS(iC->invCxx+os_sub[iWFS], iC->invCxy+os_sub[iWFS], iC->invCyy+os_sub[iWFS], n_sub[iWFS],
				I_sub, inv_sigma2, subap_size, spot_fwhm,
				na_height, na_fwhm, gs_llt_x, gs_llt_y,
				alpha_eta);
	}

	// set LGS sigma^{-2} to 0 (convention)
	for(iWFS=0; iWFS<nLGS; iWFS++)
		iC->inv_sigma2[iWFS] = 0;

	// set NGS, TTS sigma^{-2} to nPhotons
	// using: sigma^2 = 1 / nPhotons model
	for(iWFS=nLGS; iWFS<nWFS; iWFS++)
	{
		// TODO MY 08.08.2014 use the scaling factor here
		iWFS_ext = int2ext[iWFS];
		n_photons = fewha->AOsys.wfs_[iWFS_ext].gs_.n_photons_;
		iC->inv_sigma2[iWFS] = n_photons;
	}

}
void aao_freeiC(aao_OperatoriC * iC)
{
	if(iC != NULL)
	{
		if(iC->invCxx != NULL)
		{
			free(iC->invCxx);
			iC->invCxx = NULL;
		}
		if(iC->invCxy != NULL)
		{
			free(iC->invCxy);
			iC->invCxy = NULL;
		}
		if(iC->invCyy != NULL)
		{
			free(iC->invCyy);
			iC->invCyy = NULL;
		}
		if(iC->inv_sigma2 != NULL)
		{
			free(iC->inv_sigma2);
			iC->inv_sigma2 = NULL;
		}
	}
}

void aao_initD(aao_OperatorD * D, aao_FEWHA const * fewha)
{
	// local var
	int nLay = fewha->nLay;
	int iLay;
	int const   * J_lay = fewha->param.J_lay_;
	float const * d_lay = fewha->param.d_lay_;
	int i;
	double j0, j; // wavelet scale 0 and wavelet scale
	float r0;
	float const * cn2;
	double val, cov_alpha, inv_cnst2, inv_tau2;
	double const nm = 1e-9;		// nanometer
	double const lambda = 500;  // use the default value of 500 nm wavelength
	float valf;

	// init local var
	r0  = fewha->AOsys.atmosphere_.r0_;
	cn2 = fewha->AOsys.atmosphere_.cn2_;

	//  formula:
	//	r0 = fm.dm{1}.atm.r0;
	//  tau = 0.1517*(1/r0)^(5/6)*lay{iLay}.cn2^0.5*0.5d-6/(2*pi);
	//  lay{iLay}.cov_alpha = 1 / tau^2;

	inv_cnst2 = 1/(0.1517*0.1517);
	inv_tau2  = inv_cnst2 * pow(r0, 5./3.); // * 1/cn2 is multiplied at each layer
	// TODO MY 08.08.2014 turn the scaling factor back on!
// 	inv_tau2 = inv_tau2 *= (4*M_PI*M_PI) / (lambda*nm * lambda*nm);

	// code
	D->d_at_lay = (float**) calloc(nLay,sizeof(float*));
	for(iLay=0; iLay<nLay; iLay++)
	{
		D->d_at_lay[iLay] = (float*) calloc(J_lay[iLay], sizeof(float));

		// compute j0
		j0 = -log2(d_lay[iLay])-J_lay[iLay];

		// compute cov_alpha
		cov_alpha = inv_tau2 / cn2[iLay];

		for(i=0; i<J_lay[iLay]; i++)
		{
			// compute j
			j = j0+i;

			// TODO MY 06.07.2014 [low priority] investigate!
			// changing scale j->j+1 as in MATLAB
			j = j+1;

			// compute value
			val  = cov_alpha * pow(2., 11./3.*j); // 2^(11/3 * (j+1))
			valf = (float)val;

			D->d_at_lay[iLay][i] = valf;
		}
	}

}
void aao_freeD(aao_OperatorD * D, aao_FEWHA const * fewha)
{
	// local var
	int nLay = fewha->nLay;
	int iLay;


	// code
	if(D != NULL)
	{
		if(D->d_at_lay != NULL)
		{
			for(iLay=0; iLay<nLay; iLay++)
			{
				if (D->d_at_lay[iLay] != NULL)
				{
					free(D->d_at_lay[iLay]);
					D->d_at_lay[iLay] = NULL;
				}
			}
			free(D->d_at_lay);
			D->d_at_lay = NULL;
		}
	}
}

void aao_initiJ(aao_OperatoriJ * iJ, aao_FEWHA const * fewha, aao_OperatorA const * A, aao_FEWHA_Parallel const * par, char* createPrecondFromFile)
{
	// local var
	int i, j, iLay;
	int nLay = fewha->nLay;
	int n_tot_lay      = fewha->n_tot_lay;
	int n_tot_meas     = fewha->n_tot_meas;
	int const * J_lay  = fewha->param.J_lay_;
	int const * n_lay  = fewha->n_lay_at_lay;
	int const * os_lay = fewha->os_lay_at_lay;

	float * e = NULL;			// vector e_j, where e_j = 1 at jth position
	float * d_at_lay = NULL;	// copy of D->d_at_lay[iLay]
	float * tmp_s = NULL;		// used in normAwoD computation
	float tmp;
	float norm_diag_AwoD_2;
	float min_diag_D, tau;

	float alpha, alphaJ;

	// ext var init
	iJ->d = (float*)calloc(n_tot_lay, sizeof(float));
	if(!fewha->param.usePrecond)
	{
		// set diag_AwoD variable to NULL; won't use it
		// set d to 1. (identity preconditioner)
		iJ->diag_AwoD = NULL;
		for(i=0; i<n_tot_lay; i++)
			iJ->d[i] = 1.;
	}
	else	// if(fewha->param->usePrecond)
	{
		iJ->diag_AwoD = (float*)calloc(n_tot_lay, sizeof(float));

		// local var init
		alpha  = fewha->param.alpha_;
		alphaJ = fewha->param.alpha_J_;

		e     = (float*) calloc(n_tot_lay, sizeof(float));
		tmp_s = (float*) calloc(n_tot_meas, sizeof(float));


		// Compute diag(AwoD), store in iJ->diag_AwoD
		// Heavy operation! Compute once and used stored preconditioner (in iJ->diag_AwoD)
        FILE * fp;
        if(createPrecondFromFile != NULL){
			fp = fopen(createPrecondFromFile, "r");
			char line[100];
			for(i=0; i<n_tot_lay; i++)
			{
				if(fgets(line, 20, fp) != NULL) {
					iJ->diag_AwoD[i] = strtof(line, NULL);
				}
			}
        }
        else{
			fp = fopen ("jacobi.txt", "w+");
			for(i=0; i<n_tot_lay; i++)
			{
				// e is a vector with value 1 at e[i] and 0 else
				for(j=0; j<n_tot_lay; j++)
					e[j] = 0.;
				e[i] = 1.;

				//if(i%1000==0)
				//    printf("element i = %d/%d\n",i,n_tot_lay);

				// compute diag(AwoD)
				tmp = aao_normAwoD2(A, e, tmp_s, fewha, par);

				// set value
				iJ->diag_AwoD[i] = tmp;
				fprintf(fp, "%f\n", tmp);
			}
        }
        fclose(fp);

		// Cheap operations:

		// Compute the norm(diag(AwoD))^2 and set anything below a tolerance to 0
		norm_diag_AwoD_2 = aao_vec_norm(iJ->diag_AwoD, n_tot_lay);

		j=0;	// just for debugging: j is the number of elements "forced" to 0
		for(i=0; i<n_tot_lay; i++)
		{
			// MATLAB: ind = find(diagA > 1e-15*norm(diagA));
			if(iJ->diag_AwoD[i] < 1e-15*norm_diag_AwoD_2)
			{
				iJ->diag_AwoD[i] = 0.;
				j++;
			}
		}

		// Redefine e as vector of all ones,
		// Copy values: d = diag(AwoD)
		for(i=0; i<n_tot_lay; i++)
		{
			e[i] = 1.;
			iJ->d[i] = iJ->diag_AwoD[i];
		}

		// Compute tau = alphaJ*min(diag(D))
		iLay=0;
			min_diag_D = A->D.d_at_lay[iLay][0];
		for(iLay=1; iLay<nLay; iLay++)
			min_diag_D = fminf(A->D.d_at_lay[iLay][0], min_diag_D);
			//min_diag_D = (A->D.d_at_lay[iLay][0] < min_diag_D) ? A->D.d_at_lay[iLay][0] : min_diag_D;
		tau = alphaJ*min_diag_D;

		// tildeD = max(tau,diag(D))
		// d += alpha*tildeD*e
		for(iLay=0; iLay<nLay; iLay++)
		{
			// allocate tildeD
			if(d_at_lay == NULL)
				d_at_lay = (float*)calloc(J_lay[iLay], sizeof(float));

			// tildeD = max(tau,diag(D))
			for(i=0; i<J_lay[iLay]; i++)
				d_at_lay[i] = fmaxf(tau, A->D.d_at_lay[iLay][i]);

			// d += alpha*tildeD*e
			aao_wavemult_ac(iJ->d+os_lay[iLay], alpha, d_at_lay, e+os_lay[iLay], n_lay[iLay]);

			// deallocate tildeD
			if(d_at_lay != NULL)
			{
				free(d_at_lay);
				d_at_lay = NULL;
			}
		}

		// Invert J
		for(i=0; i<n_tot_lay; i++)
		{
			// invert only the elements that aren't in nullspace of the operator,
			// set others to zero. I.e., in MATLAB:
			// ind = (diagA > 1e-15*norm(diagA));
			// d(ind) = 1./d(ind); d(~ind) = 0.;
			tmp = 1./(iJ->d[i]);
			iJ->d[i] = (iJ->diag_AwoD[i] == 0) ? 0. : tmp;
		}

	} // end if(fewha->param->usePrecond)


	// free local var
	if(e != NULL)
	{
		free(e);
		e = NULL;
	}
	if(d_at_lay != NULL)
	{
		free(d_at_lay);
		d_at_lay = NULL;
	}
	if(tmp_s != NULL)
	{
		free(tmp_s);
		tmp_s = NULL;
	}
}

// TODO save preconditioner once and use files from text file than (faster testing purposes)
void aao_initJSave(aao_OperatoriJ * iJ, aao_FEWHA const * fewha, aao_OperatorA const * A, aao_FEWHA_Parallel const * par)
{
    // local var
    int i, j, iLay;
    int nLay = fewha->nLay;
    int n_tot_lay      = fewha->n_tot_lay;
    int n_tot_meas     = fewha->n_tot_meas;
    int const * J_lay  = fewha->param.J_lay_;
    int const * n_lay  = fewha->n_lay_at_lay;
    int const * os_lay = fewha->os_lay_at_lay;

    float * e = NULL;			// vector e_j, where e_j = 1 at jth position
    float * d_at_lay = NULL;	// copy of D->d_at_lay[iLay]
    float * tmp_s = NULL;		// used in normAwoD computation
    float tmp;
    float norm_diag_AwoD_2;
    float min_diag_D, tau;

    float alpha, alphaJ;

    // ext var init
    iJ->d = (float*)calloc(n_tot_lay, sizeof(float));
    if(!fewha->param.usePrecond)
    {
        // set diag_AwoD variable to NULL; won't use it
        // set d to 1. (identity preconditioner)
        iJ->diag_AwoD = NULL;
        for(i=0; i<n_tot_lay; i++)
            iJ->d[i] = 1.;
    }
    else	// if(fewha->param->usePrecond)
    {
        iJ->diag_AwoD = (float*)calloc(n_tot_lay, sizeof(float));

        // local var init
        alpha  = fewha->param.alpha_;
        alphaJ = fewha->param.alpha_J_;

        e     = (float*) calloc(n_tot_lay, sizeof(float));
        tmp_s = (float*) calloc(n_tot_meas, sizeof(float));

        // code


        // Compute diag(AwoD), store in iJ->diag_AwoD
        // Heavy operation! Compute once and store (in iJ->diag_AwoD)
        FILE * fp;
        fp = fopen ("jacobi.txt", "w+");
        printf("Computing preconditioner\n");
        for(i=0; i<n_tot_lay; i++)
        {
            // e is a vector with value 1 at e[i] and 0 else
            for(j=0; j<n_tot_lay; j++)
                e[j] = 0.;
            e[i] = 1.;

            //if(i%1000==0)
            //    printf("element i = %d/%d\n",i,n_tot_lay);

            // compute diag(AwoD)
            tmp = aao_normAwoD2(A, e, tmp_s, fewha, par);

            // set value
            iJ->diag_AwoD[i] = tmp;
            fprintf(fp, "%f\n", tmp);
        }
        fclose(fp);

        // Cheap operations:

        // Compute the norm(diag(AwoD))^2 and set anything below a tolerance to 0
        norm_diag_AwoD_2 = aao_vec_norm(iJ->diag_AwoD, n_tot_lay);

        j=0;	// just for debugging: j is the number of elements "forced" to 0
        for(i=0; i<n_tot_lay; i++)
        {
            // MATLAB: ind = find(diagA > 1e-15*norm(diagA));
            if(iJ->diag_AwoD[i] < 1e-15*norm_diag_AwoD_2)
            {
                iJ->diag_AwoD[i] = 0.;
                j++;
            }
        }

        // Redefine e as vector of all ones,
        // Copy values: d = diag(AwoD)
        for(i=0; i<n_tot_lay; i++)
        {
            e[i] = 1.;
            iJ->d[i] = iJ->diag_AwoD[i];
        }

        // Compute tau = alphaJ*min(diag(D))
        iLay=0;
        min_diag_D = A->D.d_at_lay[iLay][0];
        for(iLay=1; iLay<nLay; iLay++)
            min_diag_D = fminf(A->D.d_at_lay[iLay][0], min_diag_D);
        //min_diag_D = (A->D.d_at_lay[iLay][0] < min_diag_D) ? A->D.d_at_lay[iLay][0] : min_diag_D;
        tau = alphaJ*min_diag_D;

        // tildeD = max(tau,diag(D))
        // d += alpha*tildeD*e
        for(iLay=0; iLay<nLay; iLay++)
        {
            // allocate tildeD
            if(d_at_lay == NULL)
                d_at_lay = (float*)calloc(J_lay[iLay], sizeof(float));

            // tildeD = max(tau,diag(D))
            for(i=0; i<J_lay[iLay]; i++)
                d_at_lay[i] = fmaxf(tau, A->D.d_at_lay[iLay][i]);

            // d += alpha*tildeD*e
            aao_wavemult_ac(iJ->d+os_lay[iLay], alpha, d_at_lay, e+os_lay[iLay], n_lay[iLay]);

            // deallocate tildeD
            if(d_at_lay != NULL)
            {
                free(d_at_lay);
                d_at_lay = NULL;
            }
        }

        // Invert J
        for(i=0; i<n_tot_lay; i++)
        {
            // invert only the elements that aren't in nullspace of the operator,
            // set others to zero. I.e., in MATLAB:
            // ind = (diagA > 1e-15*norm(diagA));
            // d(ind) = 1./d(ind); d(~ind) = 0.;
            tmp = 1./(iJ->d[i]);
            iJ->d[i] = (iJ->diag_AwoD[i] == 0) ? 0. : tmp;
        }

    } // end if(fewha->param->usePrecond)


    // free local var
    if(e != NULL)
    {
        free(e);
        e = NULL;
    }
    if(d_at_lay != NULL)
    {
        free(d_at_lay);
        d_at_lay = NULL;
    }
    if(tmp_s != NULL)
    {
        free(tmp_s);
        tmp_s = NULL;
    }
}



void aao_freeiJ(aao_OperatoriJ * iJ)
{
	if(iJ != NULL)
	{
		if(iJ->d != NULL)
		{
			free(iJ->d);
			iJ->d = NULL;
		}
		if(iJ->diag_AwoD != NULL)
		{
			free(iJ->diag_AwoD);
			iJ->diag_AwoD = NULL;
		}
	}
}

void aao_initG(aao_OperatorG * G, aao_FEWHA const * fewha, aao_OperatorA const * A, aao_FEWHA_Parallel const * par)
{
	// local var
	int n_tot_lay;
	int n_lay_glcs, n_lay_glcs2;
	int i, ics, i1, i2, inz, j, jcs, j1, j2;	// rule: i = i1*n_lay[0]+i2; (global); ics = i1*n_lay_glcs+i2; (local)
	float * e = NULL;
	float * q = NULL;
	float * A_GLMS = NULL; 		// AwoD+alpha*D
	float * AwoD   = NULL;
	float * diag_alphaD = NULL; // alpha*diag(D)
	float * diag_AwoD   = NULL; // diag(AwoD)
	float norm_diag_AwoD; 	    // norm(diag(AwoD))
	int nnz_diag_AwoD;    		// nnz(diag(AwoD))
	float alpha;
	float const * d_at_lay0 = NULL;
	int const * n_lay = NULL;

	// local var init
	n_tot_lay   = fewha->n_tot_lay;
	n_lay_glcs  = fewha->n_lay_at_glcs;
	n_lay_glcs2 = n_lay_glcs*n_lay_glcs;
	alpha = fewha->param.alpha_;
	d_at_lay0 = A->D.d_at_lay[0];
	n_lay = fewha->n_lay_at_lay;

	// code
	if(!fewha->param.useGLMS)
	{
		G->L_nz   		 = NULL;
		G->D_nz   		 = NULL;
		G->AwoD_nz 		 = NULL;
		G->tmp_r_cs  	 = NULL;
		G->tmp_v_cs  	 = NULL;
		G->tmp_q     	 = NULL;
		G->nnz_diag_AwoD = 0;
		G->nz2lay		 = NULL;
		G->glcs2nz		 = NULL;
	}
	else
	{
		G->L_nz   		 = NULL;
		G->D_nz   		 = NULL;
		G->AwoD_nz 		 = NULL;
		// TODO MY 12.10.2014 in the algorithm i can actually reduce the size of tmp_v_cs to nnz_diag_AwoD,
		// but the difference is minimal: 256 vs 177...
		G->tmp_r_cs  	 = (float*)calloc(n_lay_glcs2,sizeof(float));
		G->tmp_v_cs  	 = (float*)calloc(n_lay_glcs2,sizeof(float));
		G->tmp_q     	 = (float*)calloc(n_tot_lay,sizeof(float));
		G->nz2lay		 = NULL;
		G->glcs2nz		 = NULL;

		AwoD 		= (float*)calloc(n_lay_glcs2*n_lay_glcs2,sizeof(float));
		A_GLMS 		= (float*)calloc(n_lay_glcs2*n_lay_glcs2,sizeof(float));
		diag_alphaD = (float*)calloc(n_lay_glcs2,sizeof(float));
		diag_AwoD   = (float*)calloc(n_lay_glcs2,sizeof(float));
		e 			= (float*)calloc(n_tot_lay,sizeof(float));
		q 			= (float*)calloc(n_tot_lay,sizeof(float));

		// task: compute alpha*diag(D)
		// e = zeros(n_tot_lay,1);
		// q = zeros(n_tot_lay,1);
		for(i=0; i<n_tot_lay; i++)
		{
			e[i] = 0.;
			q[i] = 0.;
		}
		// e(ind) = 1; where ind corresponds to coarse scales of ground layer
		for(i1=0; i1<n_lay_glcs; i1++)
		for(i2=0; i2<n_lay_glcs; i2++)
		{
			i = n_lay[0]*i1+i2; // the "global" index
			e[i] = 1.; // a vector of ones, but only at where the coarse scales are
		}

		// diag_alphaD += alpha*D*e
		aao_wavemult_ac(q, alpha, d_at_lay0, e, n_lay[0]);

		// copy results of q to diag_alphaD
		for(i1=0; i1<n_lay_glcs; i1++)
		for(i2=0; i2<n_lay_glcs; i2++)
		{
			i = n_lay[0]*i1+i2; 	// the "global" index
			ics = n_lay_glcs*i1+i2; // the "local" index
			diag_alphaD[ics] = q[i];
		}

		// task: compute A, AwoD, and diagAwoD = diag(A-alpha*D)
		for(j1=0; j1<n_lay_glcs; j1++)
		for(j2=0; j2<n_lay_glcs; j2++)
		{
			// e is a vector with value 1 at e[j] and 0 else
			// e = zeros(n_tot_lay,1);
			for(i=0; i<n_tot_lay; i++)
				e[i] = 0.;
			// e(j) = 1;
			j = n_lay[0]*j1+j2;		// the "global" index
			jcs = n_lay_glcs*j1+j2; // the "local" index
			e[j] = 1.;

			// computing A on coarse scales of ground layer
			// MY 10.10.2014 probably can be applyA_GLMS too
			aao_applyA(q, A, e, fewha, par); // computes the whole A = AwoD+alpha*D

			// set value
			for(i1=0; i1<n_lay_glcs; i1++)
			for(i2=0; i2<n_lay_glcs; i2++)
			{
				i = n_lay[0]*i1+i2;
				ics = n_lay_glcs*i1+i2;
				A_GLMS[ics*n_lay_glcs2+jcs] = q[i];	// A
				AwoD  [ics*n_lay_glcs2+jcs] = q[i]; // AwoD, where D is removed below

				// remove the alpha*diag(D) element;
				// save diagonal element AwoD_{ii} in a vector
				if(ics == jcs)
				{
					AwoD[ics*n_lay_glcs2+jcs] -= diag_alphaD[ics];
					diag_AwoD[ics] = AwoD[ics*n_lay_glcs2+jcs];
				}
			}
		}

		// Compute the norm(diag(AwoD)) and set anything below a tolerance to 0
		norm_diag_AwoD = aao_vec_norm(diag_AwoD, n_lay_glcs2);

		nnz_diag_AwoD=n_lay_glcs2;	// the number of elements "forced" to 0
		for(i=0; i<n_lay_glcs2; i++)
		{
			// MATLAB: ind = find(diagA > 1e-15*norm(diagA));
			if(diag_AwoD[i] < 1e-15*norm_diag_AwoD)
			{
				diag_AwoD[i] = 0.;
				nnz_diag_AwoD--;
			}
		}

		// save nnz_diag_AwoD ("compressed" size)
		G->nnz_diag_AwoD = nnz_diag_AwoD;

		// initialize the remaining variables
		G->L_nz   	 = (float*)calloc(nnz_diag_AwoD*nnz_diag_AwoD,sizeof(float));
		G->D_nz   	 = (float*)calloc(nnz_diag_AwoD,sizeof(float));
		G->AwoD_nz 	 = (float*)calloc(nnz_diag_AwoD*nnz_diag_AwoD,sizeof(float));
		G->nz2lay	 = (int*)  calloc(nnz_diag_AwoD,sizeof(int));
		G->glcs2nz	 = (int*)  calloc(n_lay_glcs2,sizeof(int));

		// crop A_GLMS dimensions: size(A_GLMS) = n_lay_glcs2 x n_lay_glcs2 -> size(A_GLMS) = nnz_diag_AwoD x nnz_diag_AwoD
		// A_GLMS = R*A_GLMS*R' where R is a restriction matrix size(R) = [nnz_diag_AwoD n_lay_glcs2];
		// use i1,j1 for reading; i2,j2 for writing
		for(i1=0, i2=0; i1<n_lay_glcs2; i1++)
		{
			if(diag_AwoD[i1] == 0) // the whole row is bad, skip it, go to next row
				continue;
			for(j1=0, j2=0; j1<n_lay_glcs2; j1++)
			{
				if(diag_AwoD[j1] == 0) // this element is bad, skip it, go to next column
					continue;
				// else: all is good, write the element
				A_GLMS    [nnz_diag_AwoD*i2+j2] = A_GLMS[n_lay_glcs2*i1+j1];
				AwoD      [nnz_diag_AwoD*i2+j2] = AwoD  [n_lay_glcs2*i1+j1];
				G->AwoD_nz[nnz_diag_AwoD*i2+j2] = AwoD  [nnz_diag_AwoD*i2+j2]; // store this result for later
				// increment writing index
				j2++;
			}
			// increment writing index
			i2++;
		}
		// from this point on, A_GLMS, AwoD is of size nnz_diag_AwoD x nnz_diag_AwoD;
		// set other elements to 0 (don't have to do this but why not)
		for(i=nnz_diag_AwoD*nnz_diag_AwoD; i<n_lay_glcs2*n_lay_glcs2; i++)
		{
			A_GLMS[i] = 0.;
			AwoD  [i] = 0.;
		}

		// cholesky decompose the matrix A_GLMS
		chol_decomp(G->L_nz, G->D_nz, A_GLMS, nnz_diag_AwoD);

		// store the indices
		// (i1,i2) for reading, innz for writing
		inz = 0;
		for(i1=0; i1<n_lay_glcs; i1++)
		for(i2=0; i2<n_lay_glcs; i2++)
		{
			i = n_lay[0]*i1+i2; 	// the "global" index
			ics = n_lay_glcs*i1+i2; // the "local" index
			if(diag_AwoD[ics] == 0)
			{
				// bad index: store -1
				G->glcs2nz[ics] = -1;
			}
			else
			{
				// good index: store the corresponding indices
				G->glcs2nz[ics] = inz;
				G->nz2lay [inz] = i;
				// increment writing index
				inz++;
			}
		}
	}
	if(AwoD != NULL)
	{
		free(AwoD);
		AwoD = NULL;
	}
	if(A_GLMS != NULL)
	{
		free(A_GLMS);
		A_GLMS = NULL;
	}
	if(diag_alphaD != NULL)
	{
		free(diag_alphaD);
		diag_alphaD = NULL;
	}
	if(diag_AwoD != NULL)
	{
		free(diag_AwoD);
		diag_AwoD = NULL;
	}
	if(e != NULL)
	{
		free(e);
		e = NULL;
	}
	if(q != NULL)
	{
		free(q);
		q = NULL;
	}
}
void aao_freeG(aao_OperatorG * G)
{
	if(G->L_nz != NULL)
	{
		free(G->L_nz);
		G->L_nz = NULL;
	}
	if(G->D_nz != NULL)
	{
		free(G->D_nz);
		G->D_nz = NULL;
	}
	if(G->AwoD_nz != NULL)
	{
		free(G->AwoD_nz);
		G->AwoD_nz = NULL;
	}
	if(G->tmp_r_cs != NULL)
	{
		free(G->tmp_r_cs);
		G->tmp_r_cs = NULL;
	}
	if(G->tmp_v_cs != NULL)
	{
		free(G->tmp_v_cs);
		G->tmp_v_cs = NULL;
	}
	if(G->tmp_q != NULL)
	{
		free(G->tmp_q);
		G->tmp_q = NULL;
	}
	if(G->nz2lay != NULL)
	{
		free(G->nz2lay);
		G->nz2lay = NULL;
	}
	if(G->glcs2nz != NULL)
	{
		free(G->glcs2nz);
		G->glcs2nz = NULL;
	}
}

void aao_initA(aao_OperatorA * A, aao_FEWHA const * fewha)
{
	// local var
	int n_tot_phi = fewha->n_tot_phi;
	int n_tot_meas = fewha->n_tot_meas;


	// code
	aao_initW    (&A->W,  fewha);
	aao_initP_Lay(&A->P,  fewha);
	aao_initSH   (&A->SH, fewha);
	aao_initiC   (&A->iC, fewha);
	aao_initD    (&A->D,  fewha);

	A->tmp_phi = (float*) calloc(n_tot_phi, sizeof(float));
	A->tmp_s   = (float*) calloc(n_tot_meas, sizeof(float));
}
void aao_freeA(aao_OperatorA * A, aao_FEWHA const * fewha)
{
	if(A != NULL)
	{
		// Free operators
		aao_freeW    (&A->W);
		aao_freeP_Lay(&A->P, fewha);
		aao_freeSH   (&A->SH);
		aao_freeiC   (&A->iC);
		aao_freeD    (&A->D,fewha);

		// free temporary memory
		if(A->tmp_phi != NULL)
		{
			free(A->tmp_phi);
			A->tmp_phi = NULL;
		}
		if(A->tmp_s != NULL)
		{
			free(A->tmp_s);
			A->tmp_s = NULL;
		}
	}
}

void aao_initH(aao_OperatorH * H, aao_FEWHA const * fewha, aao_OperatorA const * A)
{

	H->SH = &A->SH;				// use SH operator of A
	aao_initP_DM(&H->Q, fewha);		// init Q
	H->tmp_phi = A->tmp_phi;	// use tmp_phi of A
	H->tmp_s   = A->tmp_s;		// use tmp_s of A
}
void aao_freeH(aao_OperatorH * H, aao_FEWHA const * fewha)
{
	if(H != NULL)
	{
		H->SH = NULL;			// free SH via OperatorA
		aao_freeP_DM(&H->Q, fewha);	// free Q locally
		H->tmp_phi = NULL;		// free tmp_phi via OperatorA
		H->tmp_s   = NULL;		// free tmp_s via OperatorA
	}
}
void aao_initF(aao_OperatorF * F, aao_FEWHA const * fewha, aao_OperatorA const * A)
{
	F->W = &A->W;				// use W operator of A
	aao_initP_Lay_DIR(&F->P, fewha);// init P
	// TODO MY 24.07.2014 handle MCAO(nLay>nDM) case
	F->Q.I_arr = NULL;			// MCAO(nLay>nDM) case is unhandled

}
void aao_freeF(aao_OperatorF * F, aao_FEWHA const * fewha)
{
	if(F != NULL)
	{
		aao_freeP_Lay_DIR(&F->P, fewha);
		// TODO MY 24.07.2014 handle MCAO(nLay>nDM) case
		F->Q.I_arr = NULL;		// MCAO(nLay>nDM) case is unhandled
	}
}
void aao_printW(aao_OperatorW const * W, aao_FEWHA const * fewha)
{
	printf("===== FEWHA OperatorW ======\n");
	aao_print_vec_float("wscf_c2e",W->wscf_c2e,fewha->nLay);
	aao_print_vec_float_compact("tmp_U",W->tmp_U,fewha->n_tot_lay);
	printf("\n");
}
void aao_printP(aao_OperatorP const * P, aao_FEWHA const * fewha, int isLay, int isWFS)
{
	int iLay, iWFS, i;
	int nLay, nWFS;
	int len, n_phi;
	aao_Interp2 const * I = NULL;

	if(isLay)
		nLay = fewha->nLay;
	else
		nLay = fewha->nDM;
	if(isWFS)
		nWFS = fewha->nWFS;
	else
		nWFS = fewha->nDIR;

	printf("===== FEWHA OperatorP ======\n");
	printf("nWFS: %d, isWFS: %d\n",nWFS,isWFS);
	printf("nLay: %d, isLay: %d\n",nLay,isLay);
	printf("size(I_arr) = %d\n",nWFS*nLay);
	if (P->I_arr == NULL)
		printf("I_arr = NULL\n");
	else
	{
		for (iWFS=0; iWFS<nWFS; iWFS++)
		{
			if(isWFS)
				n_phi = fewha->n_phi_at_wfs[iWFS];
			else
				n_phi = fewha->n_act_at_dm[0];
				// TODO MY 05.10.2014 handle MCAO nLay>nDM

			for(iLay=0; iLay<nLay; iLay++)
			{
				i = nLay*iWFS+iLay;
				I = &P->I_arr[i];

				printf("(iWFS,iLay)=(%d,%d), nLay*iWFS+iLay=%d:\n",iWFS,iLay,i);
				printf("  ");aao_print_vec_uint16("ref_x_indx",I->ref_x_indx,n_phi);
				printf("  ");aao_print_vec_float ("ref_x_wght",I->ref_x_wght,n_phi);
				printf("  ");aao_print_vec_uint16("ref_y_indx",I->ref_y_indx,n_phi);
				printf("  ");aao_print_vec_float ("ref_y_wght",I->ref_y_wght,n_phi);
				if(I->tmp == NULL)
					len = 0;
				else
					len = n_phi*(I->ref_y_indx[n_phi-1]+1 - I->ref_y_indx[0] + 1);
				printf("  ");aao_print_vec_float_compact("tmp",I->tmp,len);
			}
		}
	}
	printf("\n");
}
void aao_printSH(aao_OperatorSH const * SH, aao_FEWHA const * fewha)
{
	int iWFS, n;
	printf("===== FEWHA OperatorSH ======\n");

	for(iWFS=0; iWFS<fewha->nWFS; iWFS++)
	{
		printf("iWFS=%d: ",iWFS);
		aao_print_vec_uint8_compact("I_sub",SH->I_sub[iWFS],fewha->n_sub_at_wfs[iWFS]*fewha->n_sub_at_wfs[iWFS]);
	}

	n = 0;
	for(iWFS=0; iWFS<fewha->nWFS; iWFS++)
		n += fewha->n_phi_at_wfs[iWFS]*fewha->n_sub_at_wfs[iWFS];

	aao_print_vec_int("n_active_sub2",SH->n_active_sub2,fewha->nWFS);
	aao_print_vec_float_compact("tmp_SH",SH->tmp_SH,n);
	aao_print_vec_int("os_tmp_SH",SH->os_tmp_SH,fewha->nWFS);
	printf("\n");
}
void aao_printiC(aao_OperatoriC const * iC, aao_FEWHA const * fewha)
{
	printf("===== FEWHA OperatoriC ======\n");
	aao_print_vec_float_compact("invCxx",iC->invCxx,fewha->n_tot_sub_lgs);
	aao_print_vec_float_compact("invCxy",iC->invCxy,fewha->n_tot_sub_lgs);
	aao_print_vec_float_compact("invCyy",iC->invCyy,fewha->n_tot_sub_lgs);
	aao_print_vec_float("inv_sigma2",iC->inv_sigma2,fewha->nWFS);
	printf("\n");
}
void aao_printD(aao_OperatorD const * D,  aao_FEWHA const * fewha)
{
	int iLay;

	printf("===== FEWHA OperatorD ======\n");
	for(iLay=0; iLay<fewha->nLay; iLay++)
	{
		printf("iLay=%d: ",iLay);
		aao_print_vec_float("d_at_lay",D->d_at_lay[iLay],fewha->param.J_lay_[iLay]);
	}
	printf("\n");
}
void aao_printiJ(aao_OperatoriJ const * iJ, aao_FEWHA const * fewha)
{
	printf("===== FEWHA OperatoriJ ======\n");
	aao_print_vec_float_compact("d",iJ->d,fewha->n_tot_lay);
	aao_print_vec_float_compact("diag_AwoD",iJ->diag_AwoD,fewha->n_tot_lay);
	printf("\n");
}
void aao_printG(aao_OperatorG const * G, aao_FEWHA const * fewha)
{
	printf("===== FEWHA OperatorG ======\n");
	aao_print_vec_float_compact("L",G->L_nz,G->nnz_diag_AwoD*G->nnz_diag_AwoD);
	aao_print_vec_float_compact("D",G->D_nz,G->nnz_diag_AwoD);
	aao_print_vec_float_compact("AwoD_nz",G->AwoD_nz,G->nnz_diag_AwoD*G->nnz_diag_AwoD);
	aao_print_vec_float_compact("tmp_r_cs",G->tmp_r_cs,fewha->n_lay_at_glcs*fewha->n_lay_at_glcs);
	aao_print_vec_float_compact("tmp_v_cs",G->tmp_v_cs,fewha->n_lay_at_glcs*fewha->n_lay_at_glcs);
	aao_print_vec_float_compact("tmp_q",G->tmp_q,fewha->n_tot_lay);
	printf("nnz_diag_AwoD: %d\n",G->nnz_diag_AwoD);
	aao_print_vec_int("nz2lay",G->nz2lay,G->nnz_diag_AwoD);
	aao_print_vec_int("glcs2nz",G->glcs2nz,fewha->n_lay_at_glcs*fewha->n_lay_at_glcs);
	printf("\n");
}
void aao_printA(aao_OperatorA const * A, aao_FEWHA const * fewha)
{
	int isLay = 1;
	int isWFS = 1;

	printf("===== FEWHA OperatorA ======\n");
	aao_printW (&A->W, fewha);
	aao_printP (&A->P, fewha, isLay, isWFS);
	aao_printSH(&A->SH, fewha);
	aao_printiC(&A->iC, fewha);
	aao_printD (&A->D, fewha);
	aao_print_vec_float_compact("tmp_phi",A->tmp_phi,fewha->n_tot_phi);
	aao_print_vec_float_compact("tmp_s",A->tmp_s,fewha->n_tot_meas);
	printf("\n");
}
void aao_printH(aao_OperatorH const * H,  aao_FEWHA const * fewha)
{
	int isLay = 0;
	int isWFS = 1;

	printf("===== FEWHA OperatorH ======\n");
	aao_printSH(H->SH, fewha);
	aao_printP (&H->Q, fewha, isLay, isWFS);
	aao_print_vec_float_compact("tmp_phi",H->tmp_phi,fewha->n_tot_phi);
	aao_print_vec_float_compact("tmp_s",H->tmp_s,fewha->n_tot_meas);
	printf("\n");
}
void aao_printF (aao_OperatorF const * F,  aao_FEWHA const * fewha)
{
	int isLay = 0;
	int isWFS = 0;

	printf("===== FEWHA OperatorF ======\n");
	aao_printW(F->W, fewha);
	isLay = 1;
	aao_printP(&F->P, fewha, isLay, isWFS);
	isLay = 0;
	aao_printP(&F->Q, fewha, isLay, isWFS);
	printf("\n");
}



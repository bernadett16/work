/*
 * applyA.c
 *
 *  Created on: Jul 4, 2014
 *      Author: misha
 */

#include "aao_FEWHA_parallel.h"
#include "aao_operators.h"
#include "aao_proj_op.h"
#include "aao_sh_op.h"
#include "aao_vec_op.h"
#include "aao_wav_op.h"
#include <omp.h>
#include <stdlib.h>

// ==== Local Definitions ====

// Operator type; determines how the template will be processed
typedef enum aao_ApplyOperatorType {
  ApplyA,
  ApplyA_GLMS,
  ApplyB
} aao_ApplyOperatorType;

// A general template of the left-hand side operator A.
void aao_applyA_template(float *out, aao_OperatorA const *A, float *in,
                         aao_FEWHA const *fewha, aao_FEWHA_Parallel const *par,
                         aao_ApplyOperatorType oper);

// ===========================

// q = Ac
void aao_applyA(float *q, aao_OperatorA const *A, float *c,
                aao_FEWHA const *fewha, aao_FEWHA_Parallel const *par) {
  // Operator (the most general form):
  // A = W P* SH* (I-T) C^{-1} (I_T) SH P W^{-1} + alpha D
  aao_applyA_template(q, A, c, fewha, par, ApplyA);
}

// q = A_{GLMS}v
void aao_applyA_GLMS(float *q, aao_OperatorA const *A, float *v,
                     aao_FEWHA const *fewha, aao_FEWHA_Parallel const *par) {
  // Operator (special structure of input vector):
  // A = W P* SH* (I-T) C^{-1} (I_T) SH P_1 W^{-1}_1 + alpha D_1
  aao_applyA_template(q, A, v, fewha, par, ApplyA_GLMS);
}

// b = Bs, B reuses elements in A
void aao_applyB(float *b, aao_OperatorA const *B, float *s,
                aao_FEWHA const *fewha, aao_FEWHA_Parallel const *par) {
  // Operator (no penalty term, no SH P W^{-1}):
  // B = W P* SH* (I-T) C^{-1} (I_T)
  aao_applyA_template(b, B, s, fewha, par, ApplyB);
}

void aao_applyA_template(float *out, aao_OperatorA const *A, float *in,
                         aao_FEWHA const *fewha, aao_FEWHA_Parallel const *par,
                         aao_ApplyOperatorType oper) {
  // TODO MY 06.07.2014 work in the Tip/tilt stars in here!!

  // local variables
  int iLay, iWFS;
  int nLay, nWFS, nLGS, nNGS; //, nTTS;
  float *phi = NULL;          // all incoming wavefronts
  float *s = NULL;            // all measurements
  float *tmp_U = NULL;        // tmp variable for wavelet transform
  float *tmp_SH = NULL;       // tmp variable to compute SH

  int const *n_lay = NULL, *os_lay = NULL;
  int n_glcs;
  int const *n_phi = NULL, *os_phi = NULL;
  int const *n_sub = NULL, *os_sub = NULL, *os_meas = NULL;
  int const *os_tmp_SH = NULL;

  float const *invCxx = NULL, *invCxy = NULL, *invCyy = NULL;

  float alpha;

  // in/out variables
  float *q = NULL;       // out of applyA, applyB
  float const *c = NULL; // in of applyA

  int n_sub_virtual;
  unsigned char *I_sub_virtual;

  // local variable init
  if (oper == ApplyA || oper == ApplyA_GLMS) {
    q = out;
    c = in;
    s = A->tmp_s;
  } else if (oper == ApplyB) {
    q = out;
    s = in;
  }

  alpha = fewha->param.alpha_;
  phi = A->tmp_phi;
  tmp_U = A->W.tmp_U;
  tmp_SH = A->SH.tmp_SH;
  nWFS = fewha->nWFS;
  nLGS = fewha->nLGS;
  //	nTTS    = fewha->nTTS;
  nNGS = fewha->nNGS;
  nLay = fewha->nLay;
  n_lay = fewha->n_lay_at_lay;
  n_glcs = fewha->n_lay_at_glcs;
  os_lay = fewha->os_lay_at_lay;
  n_phi = fewha->n_phi_at_wfs;
  os_phi = fewha->os_phi_at_wfs;
  n_sub = fewha->n_sub_at_wfs;
  os_sub = fewha->os_sub_at_wfs;
  os_meas = fewha->os_meas_at_wfs;
  os_tmp_SH = A->SH.os_tmp_SH;
  invCxx = A->iC.invCxx;
  invCxy = A->iC.invCxy;
  invCyy = A->iC.invCyy;

  n_sub_virtual = fewha->AOsys.wfs_[fewha->wfs_indx_int2ext[0]]
                      .n_sub_; // use geometry of first WFS for TT
  I_sub_virtual = fewha->AOsys.wfs_[fewha->wfs_indx_int2ext[0]].i_sub_;

  // code
  if (oper == ApplyA_GLMS) {
    iLay = 0;
    {
      // =========== apply iW ==========
      // 1. wscf.c2e * c
      // q = c2e*c
      aao_vec_ti_const_prol_sqr(q + os_lay[iLay], A->W.wscf_c2e[iLay],
                                c + os_lay[iLay], n_glcs, n_lay[iLay]);

      // 2. DWT
      // q = W^{-1}q
      aao_waverec2_db3_coarse_scales(q + os_lay[iLay], n_lay[iLay],
                                     tmp_U + os_lay[iLay], n_glcs);
    }
  } // endif ApplyA_GLMS
    //#pragma omp parallel private(iLay,iWFS) num_threads(par->max_thr_applyA)
    //if(par->global)
    //{
  if (oper == ApplyA) {
    //		#pragma omp for
    for (iLay = 0; iLay < nLay; iLay++) {
      // =========== apply iW ==========
      // 1. wscf.c2e * c
      // q = c2e*c
      aao_vec_ti_const(q + os_lay[iLay], A->W.wscf_c2e[iLay], c + os_lay[iLay],
                       n_lay[iLay] * n_lay[iLay]);

      // 2. DWT
      // q = W^{-1}q
      aao_waverec2_db3(q + os_lay[iLay], n_lay[iLay], tmp_U + os_lay[iLay]);
    }
  } // endif ApplyA

  // q is readable for all threads at this point

  //	#pragma omp for
  for (iWFS = 0; iWFS < nWFS; iWFS++) {
    if (oper == ApplyA || oper == ApplyA_GLMS) {
      // Wavefront Sensors
      // =========== apply P ===========
      // 1. layer 0
      // phi_iWFS = P_{iWFS,0} q_0 = R q_0
      aao_vec_rest_sqr(phi + os_phi[iWFS], q + os_lay[0], n_phi[iWFS],
                       n_lay[0]);

      if (oper == ApplyA) {
        // 2. layer 1,2,...
        for (iLay = 1; iLay < nLay; iLay++) {
          // phi_iWFS += P_{iWFS,iLay} q_{iLay} for iLay>0
          aao_interp2_ac(phi + os_phi[iWFS], A->P.I_arr[nLay * iWFS + iLay],
                         q + os_lay[iLay], n_phi[iWFS], n_lay[iLay]);
        }
      } // endif ApplyA

      // =========== apply SH ==========
      if (iWFS < nLGS + nNGS)
        aao_sh(s + os_meas[iWFS], phi + os_phi[iWFS], n_sub[iWFS],
               tmp_SH + os_tmp_SH[iWFS]);
      else // TT
        aao_sh_tt(s + os_meas[iWFS], phi + os_phi[iWFS], n_sub[iWFS],
                  n_sub_virtual, I_sub_virtual, tmp_SH + os_tmp_SH[iWFS]);

    } // endif ApplyA || ApplyA_GLMS

    if (iWFS < nLGS) {
      // LGS
      // =========== apply I-T =========
      aao_mask(s + os_meas[iWFS], A->SH.I_sub[iWFS], n_sub[iWFS]);
      aao_removeTT(s + os_meas[iWFS], n_sub[iWFS], A->SH.n_active_sub2[iWFS]);

      // =========== apply invC ========
      aao_invC_LGS(s + os_meas[iWFS], invCxx + os_sub[iWFS],
                   invCxy + os_sub[iWFS], invCyy + os_sub[iWFS], n_sub[iWFS]);

      // =========== apply I-T =========
      // mask    (tmp_s0+(iWFS*n_sub0_at_wfs), m, n.phi0_sqrt_at_wfs); // data
      // already masked via invC_LGS operation
      aao_removeTT(s + os_meas[iWFS], n_sub[iWFS], A->SH.n_active_sub2[iWFS]);
      aao_mask(s + os_meas[iWFS], A->SH.I_sub[iWFS], n_sub[iWFS]);
    } else {
      // NGS
      // =========== apply mask ========
      aao_mask(s + os_meas[iWFS], A->SH.I_sub[iWFS], n_sub[iWFS]);

      // =========== apply invC ========
      aao_vec_ti_const_ac(s + os_meas[iWFS], A->iC.inv_sigma2[iWFS],
                          2 * n_sub[iWFS] * n_sub[iWFS]);
    }

    // at this point vector tmp_s is masked (for both LGS and NGS)

    // =========== apply SH' =========
    if (iWFS < nLGS + nNGS)
      aao_sh_tr(phi + os_phi[iWFS], s + os_meas[iWFS], n_sub[iWFS]);
    else // TT
      aao_sh_tt_tr(phi + os_phi[iWFS], s + os_meas[iWFS], n_sub[iWFS],
                   n_sub_virtual,
                   I_sub_virtual); // use geometry of first WFS for TT (use mask
                                   // of WFS as virtual mask for TT)
  }

  // at this point tmp_phi is readable

  //	#pragma omp for
  for (iLay = 0; iLay < nLay; iLay++) {
    // =========== apply P' ==========

    if (iLay == 0) // layer 0
    {
      iWFS = 0;
      aao_vec_prol_sqr(q + os_lay[0], phi + os_phi[iWFS], n_phi[iWFS],
                       n_lay[0]);

      for (iWFS = 1; iWFS < nWFS; iWFS++)
        aao_vec_prol_sqr_ac(q + os_lay[0], phi + os_phi[iWFS], n_phi[iWFS],
                            n_lay[0]);
    } else // layer 1,2,...
    {
      iWFS = 0;
      aao_interp2_tr(q + os_lay[iLay], A->P.I_arr[nLay * iWFS + iLay],
                     phi + os_phi[iWFS], n_phi[iWFS], n_lay[iLay]);

      for (iWFS = 1; iWFS < nWFS; iWFS++)
        aao_interp2_tr_ac(q + os_lay[iLay], A->P.I_arr[nLay * iWFS + iLay],
                          phi + os_phi[iWFS], n_phi[iWFS], n_lay[iLay]);
    }

    // =========== apply iW' =========
    // 1. DWT
    aao_wavedec2_db3(q + os_lay[iLay], n_lay[iLay], tmp_U + os_lay[iLay]);

    // 2. wscf.c2e * c   It is again c2e for iW transpose!!!
    aao_vec_ti_const_ac(q + os_lay[iLay], A->W.wscf_c2e[iLay],
                        n_lay[iLay] * n_lay[iLay]);

    // =========== apply D ===========
    if (oper == ApplyA) {
      // q += alpha*D*c
      aao_wavemult_ac(q + os_lay[iLay], alpha, A->D.d_at_lay[iLay],
                      c + os_lay[iLay], n_lay[iLay]);
    } // endif ApplyA
  }
  //} // end #pragma omp parallel

  if (oper == ApplyA_GLMS) {
    iLay = 0;
    {
      // q += alpha*D*c at iLay=0
      aao_wavemult_ac_coarse_scales(q + os_lay[iLay], alpha,
                                    A->D.d_at_lay[iLay], c + os_lay[iLay],
                                    n_lay[iLay], n_glcs);
    }
  } // endif ApplyA_GLMS

  // free local variables
}

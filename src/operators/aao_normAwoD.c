/*
 * normAwoD.c
 *
 *  Created on: Jul 24, 2014
 *      Author: misha
 */


#include <stdlib.h>
#include <math.h>
#include "aao_operators.h"
#include "aao_proj_op.h"
#include "aao_sh_op.h"
#include "aao_vec_op.h"
#include "aao_wav_op.h"

// (AwoDe,e)
// tmp_s is a temporary variable defined outside
// TODO improvement possible??
float aao_normAwoD2(aao_OperatorA const * A, float * c, float * tmp_s, aao_FEWHA const * fewha, aao_FEWHA_Parallel const * par)
{
	// Compute: || SH P W^{-1} e ||_{C^{-1}}^2 = (C^{-1} SH P W^{-1} e, SH P W^{-1} e) = (C^{-1} s, s)
	// where s = SH P W^{-1} e

	// TODO MY 24.07.2014 work in the Tip/tilt stars in here!!

	// local variables
	int iLay, iWFS;
	int nLay, nWFS, nLGS, nNGS, nTTS;
	float * phi  	= NULL;			// all incoming wavefronts
	float * s    	= NULL;			// all measurements
	float * tmp_U   = NULL;			// tmp variable for wavelet transform
	float * tmp_SH  = NULL;			// tmp variable to compute SH

	int const * n_lay = NULL, * os_lay = NULL;
	int const * n_phi = NULL, * os_phi = NULL;
	int const * n_sub = NULL, * os_sub = NULL, * os_meas = NULL;
	int const * os_tmp_SH = NULL;

	float const * invCxx = NULL, * invCxy = NULL, * invCyy = NULL;

	float ret;	// returns variable

	ret     = 0.;
	phi     = A->tmp_phi;
	s       = A->tmp_s;
	tmp_U   = A->W.tmp_U;
	tmp_SH  = A->SH.tmp_SH;
	nWFS    = fewha->nWFS;
	nLGS    = fewha->nLGS;
	nNGS    = fewha->nNGS;
	nTTS    = fewha->nTTS;
	nLay    = fewha->nLay;
	n_lay   = fewha->n_lay_at_lay;
	os_lay  = fewha->os_lay_at_lay;
	n_phi   = fewha->n_phi_at_wfs;
	os_phi  = fewha->os_phi_at_wfs;
	n_sub   = fewha->n_sub_at_wfs;
	os_sub  = fewha->os_sub_at_wfs;
	os_meas = fewha->os_meas_at_wfs;
	os_tmp_SH = A->SH.os_tmp_SH;
	invCxx  = A->iC.invCxx;
	invCxy  = A->iC.invCxy;
	invCyy  = A->iC.invCyy;


	// code
	#pragma omp parallel private(iLay,iWFS) num_threads(par->max_thr_applyA) if(par->global)
	{
		#pragma omp for
		for (iLay=0; iLay<nLay; iLay++)
		{
			// =========== apply iW ==========
			// 1. wscf.c2e * c
			// c = c2e*c		// NOTE: this is different than in applyA
			aao_vec_ti_const_ac(c+os_lay[iLay], A->W.wscf_c2e[iLay], n_lay[iLay]*n_lay[iLay]);

			// 2. DWT
			// c = W^{-1}c
			aao_waverec2_db3(c+os_lay[iLay], n_lay[iLay], tmp_U+os_lay[iLay]);
		}

		// q is readable for all threads at this point

		#pragma omp for
		for (iWFS=0; iWFS<nWFS; iWFS++)
		{
			// Wavefront Sensors
			// =========== apply P ===========
			// 1. layer 0
			// phi_iWFS = P_{iWFS,0} c_0 = R c_0
			aao_vec_rest_sqr(phi+os_phi[iWFS], c+os_lay[0], n_phi[iWFS], n_lay[0]);

			// 2. layer 1,2,...
			for(iLay=1; iLay<nLay; iLay++)
			{
				// phi_iWFS += P_{iWFS,iLay} q_{iLay} for iLay>0
				aao_interp2_ac(phi+os_phi[iWFS], A->P.I_arr[nLay*iWFS+iLay], c+os_lay[iLay], n_phi[iWFS], n_lay[iLay]);
			}

			// =========== apply SH ==========
			aao_sh(s+os_meas[iWFS], phi+os_phi[iWFS], n_sub[iWFS], tmp_SH+os_tmp_SH[iWFS]);

			// ============ copy s ===========
			aao_vec_copy(tmp_s+os_meas[iWFS], s+os_meas[iWFS], 2*n_sub[iWFS]*n_sub[iWFS]);

			if (iWFS<nLGS)
			{
				// LGS
				// =========== apply I-T =========
				aao_mask    (s+os_meas[iWFS], A->SH.I_sub[iWFS], n_sub[iWFS]);
				aao_removeTT(s+os_meas[iWFS], n_sub[iWFS], A->SH.n_active_sub2[iWFS]);

				// =========== apply invC ========
				aao_invC_LGS(s+os_meas[iWFS], invCxx+os_sub[iWFS], invCxy+os_sub[iWFS], invCyy+os_sub[iWFS], n_sub[iWFS]);

			}
			else
			{
				// NGS
				// =========== apply mask ========
				aao_mask    (s+os_meas[iWFS], A->SH.I_sub[iWFS], n_sub[iWFS]);

				// =========== apply invC ========
				aao_vec_ti_const_ac(s+os_meas[iWFS], A->iC.inv_sigma2[iWFS], n_sub[iWFS]*n_sub[iWFS]);
			}

			// at this point vector tmp_s is masked (for both LGS and NGS)
		}

		// ===== compute (invC*s,s) ======
		#pragma omp for reduction(+:ret)
		for(iWFS=0; iWFS<nWFS; iWFS++)
		{
			ret += aao_vec_scal_prod(s+os_meas[iWFS], tmp_s+os_meas[iWFS], 2*n_sub[iWFS]*n_sub[iWFS]);
		}
	}

	// MY 28.08.2014 no sqrt b/c: (Me,e) = ||M^{1/2}e||^2  is what i actually want, not ||M^{1/2}e||
//	ret = sqrtf(ret);

	return ret;
}

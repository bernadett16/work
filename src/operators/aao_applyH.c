/*
 * applyH.c
 *
 *  Created on: Jul 24, 2014
 *      Author: misha
 */

#include "aao_operators.h"
#include "aao_proj_op.h"
#include "aao_sh_op.h"
#include "aao_vec_op.h"
#include <stdlib.h>

/* H is the pseudo-open loop operator (DM, closed loop s to pseudo open loop s)
 */

void aao_applyH(float *s, aao_OperatorH const *H, float const *a,
                aao_FEWHA const *fewha, aao_FEWHA_Parallel const *par) {
  // TODO MY 24.07.2014 work in the Tip/tilt stars in here!!

  // local variables
  int iDM, iWFS;
  int nDM, nWFS, nLGS, nNGS;
  int n_sub_virtual;
  unsigned char *I_sub_virtual;
  float *tmp_phi = NULL; // all incoming wavefronts
  float *tmp_s = NULL;   // all measurements (SH applied to WF thru DMs)
  float *tmp_SH = NULL;  // tmp variable to compute SH

  int const *n_act = NULL, *os_act = NULL;
  int const *n_phi = NULL, *os_phi = NULL;
  int const *n_sub = NULL, *os_meas = NULL;
  int const *os_tmp_SH = NULL;

  tmp_phi = H->tmp_phi;   // reused from A
  tmp_s = H->tmp_s;       // reused from A
  tmp_SH = H->SH->tmp_SH; // reused from A
  nDM = fewha->nDM;
  nWFS = fewha->nWFS;
  nLGS = fewha->nLGS;
  nNGS = fewha->nNGS;
  n_act = fewha->n_act_at_dm;
  os_act = fewha->os_act_at_dm;
  n_phi = fewha->n_phi_at_wfs;
  os_phi = fewha->os_phi_at_wfs;
  n_sub = fewha->n_sub_at_wfs;
  os_meas = fewha->os_meas_at_wfs;
  os_tmp_SH = H->SH->os_tmp_SH;
  n_sub_virtual = fewha->AOsys.wfs_[fewha->wfs_indx_int2ext[0]]
                      .n_sub_; // use geometry of first WFS for TT
  I_sub_virtual = fewha->AOsys.wfs_[fewha->wfs_indx_int2ext[0]].i_sub_;

  //#pragma omp parallel private(iDM,iWFS) num_threads(par->max_thr_applyH)
  //if(par->global)
  //{
  //	#pragma omp for
  for (iWFS = 0; iWFS < nWFS; iWFS++) {
    // =========== apply P ===========
    // 1. ground DM
    // phi_iWFS = P_{iWFS,0} a_0 = a_0 (copy operation)
    // WARNING!!! this assumes wavefront and ground DM have the same geometry!!!
    aao_vec_copy(tmp_phi + os_phi[iWFS], a + os_act[0],
                 n_phi[iWFS] * n_phi[iWFS]);

    // 2. altitude DMs
    for (iDM = 1; iDM < nDM; iDM++) {
      // phi_iWFS += P_{iWFS,iDM} a_{iDM} for iLay>0
      aao_interp2_ac(tmp_phi + os_phi[iWFS], H->Q.I_arr[nDM * iWFS + iDM],
                     a + os_act[iDM], n_phi[iWFS], n_act[iDM]);
    }

    // =========== apply SH ==========
    //			aao_sh(tmp_s+os_meas[iWFS], tmp_phi+os_phi[iWFS], n_sub[iWFS],
    //tmp_SH+os_tmp_SH[iWFS]);
    if (iWFS < nLGS + nNGS)
      aao_sh(tmp_s + os_meas[iWFS], tmp_phi + os_phi[iWFS], n_sub[iWFS],
             tmp_SH + os_tmp_SH[iWFS]);
    else // TT, use geometry of first WFS, n_sub_virtual as the size of mask
         // I_sub[0]
      aao_sh_tt(tmp_s + os_meas[iWFS], tmp_phi + os_phi[iWFS], n_sub[iWFS],
                n_sub_virtual, I_sub_virtual, tmp_SH + os_tmp_SH[iWFS]);

    // === accumulate measurements ===
    aao_vec_pl_vec_ac(s + os_meas[iWFS], tmp_s + os_meas[iWFS],
                      2 * n_sub[iWFS] * n_sub[iWFS]);
  }
  //}
}

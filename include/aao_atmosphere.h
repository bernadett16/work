/*
 * Definition of a struct for the atmosphere 
 * used for the Austrian reconstructors
 * one instance contains:
 * 	number of layers of the atmosphere
 * 	Cn^2 profile
 * 	height of atmospheric layers
 * 	r0, L0 (outer scale)
 * 	sodium layer profile (height, fwhm)
 * 
 * functions:
 * setAtmosphere: initializes an atmosphere for given input parameters
 * updateAtmosphere: changes variable parameters for an atmosphere during run time
 * 
 * 
 * TBD: resolution of the Cn^2 profile and sodium layer profile
 * 	how should a layer be adressed? by its number or by its height?
 * 		what if on a height is no layer in case 2?
 * 
 * 
 * Author: RW, 05/2014
 * Last change: MY 27.06.2014
 */

#ifndef AAO_ATMOSPHERE_H_
#define AAO_ATMOSPHERE_H_

#ifdef __cplusplus
extern "C" {
#endif


typedef struct aao_Atmosphere
{	
	int n_layer_;         	// number of layers; fixed
	float* cn2_;          	// C_n^2 profile; variable; units: fractions or 1, sum(cn2_)=1; size=n_layer_
	float* height_layer_; 	// layer altitudes; fixed; units: meters; size=n_layer_
	float r0_;            	// Fried parameter; variable; units: meters
	float L0_;				// outer scale; variable; units: meters
	// Sodium layer profile is simulated as Gaussian random variable:
	float na_height_;		// mean altitude of the Sodium layer; variable; units: meters
	float na_fwhm_;        	// full width at half maximum of the Sodium layer; variable; units: meters
} aao_Atmosphere;

// Example settings:
// n_layer_ = 3;
// cn2_ = [0.6 0.2 0.2];
// height_layer_ = [0 4000 12700];
// r0_ = 0.129;
// L0_ = 25;
// na_height_ = 90000;
// na_fwhm_ = 11000;

// Set parameters of the atmosphere
// allocates memory when needed
// out: atmosphere
//  in: n_layer, cn2, height_layer, r0, L0, na_height, na_fwhm
// ret: 0 on ok
int aao_setAtmosphere(aao_Atmosphere* atm, int n_layer, const float* cn2, const float* height_layer,
		float r0, float L0, float na_height, float na_fwhm);

// Update parameters of the atmosphere
// in/out: atmosphere
//     in: cn2, r0, L0, na_height, na_fwhm
//    ret: 0 on ok
int aao_updateAtmosphere(aao_Atmosphere* atm, const float* cn2,
		float r0, float L0, float na_height, float na_fwhm);

// Free memory allocated to atmosphere struct
void aao_freeAtmosphere(aao_Atmosphere* atm);

// Display atmosphere configuration in the standard output (using printf)
// useful for debugging
//  Input: atm struct
void aao_printAtmosphere(const aao_Atmosphere * atm);

#ifdef __cplusplus
}
#endif


#endif /* AAO_ATMOSPHERE_H_ */

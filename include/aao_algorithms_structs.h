/*
 * algorithms_structs.h
 *
 *  Created on: Jul 27, 2014
 *      Author: misha
 */

#ifndef AAO_ALGORITHMS_STRUCTS_H_
#define AAO_ALGORITHMS_STRUCTS_H_

#include "aao_system.h"

#ifdef __cplusplus
extern "C" {
#endif

// ======== Structs ========

typedef struct aao_PCG
{
	// Temporary variables
	float * tmp_z;		// size = nLay*n_lay*n_lay
	float * tmp_p;		// size = nLay*n_lay*n_lay
	float * tmp_q;		// size = nLay*n_lay*n_lay
} aao_PCG;


typedef struct aao_Loop{
	// MY 05.09.2014 loop type should stay in fewha->AOsys->loop_!
//	aao_LoopType loop_type;

	float * a_old;		// size = nDM*n_act*n_act

	// For warm-restart:
	float * b;			// size = nLay*n_lay*n_lay
	float * r;			// size = nLay*n_lay*n_lay
	float * c;			// size = nLay*n_lay*n_lay


	float * tmp_b;		// size = nLay*n_lay*n_lay
	float * tmp_a;		// size = nDM*n_act*n_act
} aao_Loop;

#ifdef __cplusplus
}
#endif




#endif /* AAO_ALGORITHMS_STRUCTS_H_ */

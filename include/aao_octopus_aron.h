/*
 * aao_octopus_aron.h
 *
 *  Created on: Oct 17, 2014
 *      Author: misha
 */

#ifndef AAO_OCTOPUS_ARON_H_
#define AAO_OCTOPUS_ARON_H_

// ========== Octopus Aron structs ============
// Contents of an aron file
typedef struct aao_OctopusAronFile
{
	float * col0;		 	// size=len, x-values
	float * col1;		 	// size=len, y-values
	int   * col2;			// size=len, associated to WFS/DM index
	int     len;		 	// number of lines with values
	char    filename[128];	// either "subapertures_aron.txt" or "actuators_aron.txt", c-string
	int     read_status; 	// file read successfully? 0 on OK, -1 on NOK
} aao_OctopusAronFile;

// Derived quantities of one block (either one WFS or one DM)
// NOTE: expecting a square geometry!!!
typedef struct aao_OctopusAronBlock
{
	int indx;				// index of the WFS or DM (iWFS or iDM)
	int n_act;				// number of actuators (!!!! i.e., corner points)
	unsigned char * i_act;	// size=n_act*n_act; actuator mask
	unsigned char * i_sub;  // size=(n_act-1)*(n_act_1); subaperture mask
	float * actpos_x;		// size=n_act; x-positions
	float * actpos_y;		// size=n_act; y-positions
	float spacing;			// spacing
} aao_OctopusAronBlock;


// ========== Octopus Aron functions ==========
// Reads an actuators_aron.txt or a subapertures_aron.txt file in communication_dir
//  Input: communcation_dir, filename
// Output: aao_OctopusAron (see above); here memory is allocated dynamically, so run aao_freeOctopusAronFile after you're done with the struct
aao_OctopusAronFile aao_readOctopusAronFile(char const * communication_dir, char const * filename);

// Computes quantities associated to an index from an octopus aron file
//  Input: octopus_aron_file, index (iWFS or iDM), teldiam (only if 1x1 sensor)
// Output: aao_OctopusAronBlock (see above); here memory is allocated dynamically, so run aao_freeOctopusAronBlock after you're done with the struct
// WARNING: assuming a square DM/WFS
aao_OctopusAronBlock aao_deriveOctopusAronBlock(aao_OctopusAronFile const * octopus_aron_file, int indx, float teldiam);

// Free aao_OctopusAronFile struct
//  Input: octopus_aron_file struct
void aao_freeOctopusAronFile(aao_OctopusAronFile * octopus_aron_file);

// Free aao_OctopusAronBlock struct
//  Input: octopus_aron_file struct
void aao_freeOctopusAronBlock(aao_OctopusAronBlock * octopus_aron_block);



#endif /* AAO_OCTOPUS_ARON_H_ */

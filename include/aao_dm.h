/*
 * Definition of a struct for deformable mirrors
 * used for the Austrian reconstructors
 * one dm contains:
 * 	number of actuators
 * 	active actuator mask
 * 	positions of the actuators
 * 	spacing of the actuators
 * 
 * functions for this struct:
 * setDM: setting the properties of the deformable mirror from a given list of parameters
 * 
 * Author: RW, 05/2014
 * Last change: MY 27.06.2014
 */

#ifndef AAO_DM_H_
#define AAO_DM_H_

#ifdef __cplusplus
extern "C" {
#endif


typedef struct aao_DM
{
	int isSquare; 			// set to 1 if actuator positions set on a square;
							// set to 0 if arbitrary actuator positions; fixed
							// !!! NOTE: isSquare==0 has NOT been implemented !!!
	int n_act_; 			// number of actuators in 1 dimension if (isSquare==1);
							// total number of *active* actuators if (isSquare==0); fixed
	float dm_height_; 		// height of DM; fixed; units: meters
	unsigned char* i_act_;	// actuator mask; set to 0 or 1 if (isSquare==1), size=n_act_*n_act_;
							// set to NULL if (isSquare==0); fixed;
	float* actpos_x_; 		// x-coordinates of the actuator positions relative to center of aperture (0,0);
							// if (isSquare==1) in 1 dimension, size=n_act_;
							// if (isSquare==0) the whole list of x-values, size=n_act_;
							// fixed; units: meters
	float* actpos_y_; 		// y-coordinates of the actuator positions, see actpos_x_
} aao_DM;

// Example settings:
// == square DM: ==
// isSquare = 1;
// n_act_ = 3;
// dm_height_ = 4000;
// i_act_ = [0 1 0 1 1 1 0 1 0]
// actpos_x_ = [-1 0 1];
// actpos_y_ = NULL;
//
// == arbitrary DM: ==
// isSquare = 0;
// n_act_ = 5;
// dm_height_ = 4000;
// i_act_ = NULL;
// actpos_x_ = [-1  0 0 0 1];
// actpos_y_ = [ 0 -1 0 1 0];



// Sets square DM parameters
// allocates memory when needed
// out: dm
//  in: n_act, dm_height, i_act, actpos_x, actpos_y
// ret: 0 on ok
int aao_setSquareDM(aao_DM* dm, int n_act, float dm_height, const unsigned char* i_act, const float* actpos_x, const float* actpos_y);

// Sets DM with variable actuator positions parameters
// allocates memory when needed
// out: dm
//  in: n_act, dm_height, actpos_x, actpos_y
// ret: 0 on ok
int aao_setArbitraryDM(aao_DM* dm, int n_act, float dm_height, const float* actpos_x, const float* actpos_y);

// Free memory allocated to DM struct
void aao_freeDM(aao_DM * dm);

// Display DM configuration in the standard output (using printf)
// useful for debugging
//  Input: dm struct
void aao_printDM(const aao_DM * dm);


#ifdef __cplusplus
}
#endif



#endif /* AAO_DM_H_ */

/*
 * parallelization.h
 *
 *  Created on: Feb 14, 2013
 *      Author: misha
 */

#ifndef AAO_FEWHA_PARALLEL_H_
#define AAO_FEWHA_PARALLEL_H_

#include "aao_system.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct aao_FEWHA_Parallel
{
	// need to set by user
	int global;				// true or false (0 or 1)
	int max_thr_system;		// maximum number of threads on this particular system

	// set automatically
	int max_thr_applyA;		// e.g., min(max(nLay, nWFS),max_thr_system)
	int max_thr_applyF;		// e.g., min(max(nLay, nDIR),max_thr_system)
	int max_thr_applyH;		// e.g., min(nWFS,max_thr_system)
	int max_thr_lay; 		// layer operations, e.g., min(nLay,max_thr_system); used in e.g., pcg or control algorithms
	int max_thr_DM;			// DM operations, e.g., min(nDM,max_thr_system), used in control algorithm

	int n_dwt_pa;			// wavedec2_db3_pa, waverec2_db3_pa on grids >= n_dwt_pa will be carried out
							// in parallel; on grids < n_dwt_pa on single core, e.g., 64
} aao_FEWHA_Parallel;

// Set parallelization parameter, only need to set global = 0 or 1
// parameters will be initialized automatically after in aao_initFEWHA
// out: fewha_parall
//  in: global, max_thr_system
// ret: 0 on ok
int aao_setFEWHAparall(aao_FEWHA_Parallel * fewha_parall, int global, int max_thr_system);

// Free memory allocated to parall struct
void aao_freeFEWHAparall(aao_FEWHA_Parallel * fewha_parall);

// Display configuration in the standard output (using printf)
// useful for debugging
//  Input: fewha_parall
void aao_printFEWHAparall(aao_FEWHA_Parallel const * fewha_parall);

#ifdef __cplusplus
}
#endif

#endif /* AAO_FEWHA_PARALLEL_H_ */

/*
 * Definition of the struct holding the (AO) system specific parameters
 * used for the Austrian reconstructors
 * one instance contains:
 * 	type of the system
 * 	diameter of the telescope
 * 	control
 * 	wavefront sensors (including number)
 * 	deformable mirrors (including number)
 * 	atmosphere
 * 	targets (depending on the system type)
 *
 * function specific to the struct
 * initSystem: set all parameters needed to start the reconstruction
 * updateSystem: change parameters at the run time of the system, e.g., Cn^2
 *proifle call updateAtmosphere, updateWFS destroySystem: should free all
 *allocated memory!
 *
 * TBD: how are the params given?
 * 	long list vs paramfile?
 *	are there default system with certain settings available? i.e., SCAO,
 *MCAO, MOAO, LTAO
 *
 * Author: RW, 05/2014
 * Last change: MY 27.06.2014
 */

#ifndef AAO_SYSTEM_H_
#define AAO_SYSTEM_H_

#include "aao_atmosphere.h"
#include "aao_dm.h"
#include "aao_star.h"
#include "aao_wfs.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum { SCAO, LTAO, MOAO, MCAO } aao_SystemType;
typedef enum { OpenLoop, ClosedLoop } aao_LoopType;

typedef struct aao_System {
  aao_SystemType sys_type_;
  float teldiam_;     // telescope diameter; units: meters; fixed
  aao_LoopType loop_; // either open or closed loop; control (e.g., POLC, gain,
                      // etc.) is a setting of the reconstructor
  float time_unit_;   // time step of one frame; units: seconds; fixed
  float zenith_;      // zenith; fixed
                 // !!! NOTE: at the moment only zenith=0 is implemented !!!

  // WFS of the AO system
  int n_wfs_;    // number of WFS; fixed
  aao_WFS *wfs_; // array of WFS; size = n_wfs_

  // DMs of the AO system
  int n_dm_;   // number of DMs; fixed
  aao_DM *dm_; // array of DMs; size = n_dm_

  aao_Atmosphere atmosphere_; // atmosphere

  int n_target_; // number of targets; fixed
  aao_Star
      *target_; // array of targets; fixed; size = n_target_
                // for SCAO, set n_target_ = 1;
                // for LTAO, set n_target_ = 1;
                // for MOAO, set n_target_ = variable;
                // for MCAO, set n_target_ = 1; set target_[0].fov_ = variable
} aao_System;

// Example:
// sys_type_ = LTAO;
// teldiam_ = 39.3;
// control_ = ClosedLoop;
// time_unit_ = 0.002;
// zenith_ = 0;
// n_wfs_ = 3;
// wfs_ = new WFS[3]; // ... set WFS
// n_dm_ = 1;
// dm_ = new DM; // ... set DMs
// atmosphere_.n_layer_ = 9 // ... set atmosphere
// n_target_ = 1;
// target_ = new Star; // set target

// Initialize the AO system
// parameters are copied
// out: sys
//  in: sys_type, teldiam, loop, n_wfs, wfs array, n_dm, dm array, atm struct,
//  n_target, target array
// ret: 0 on ok
int aao_initSystem(aao_System *sys, aao_SystemType sys_type, float teldiam,
                   aao_LoopType loop, float time_unit, int n_wfs,
                   const aao_WFS *wfs, int n_dm, aao_DM *dm,
                   const aao_Atmosphere *atm, int n_target,
                   const aao_Star *target);

// updates with bit-status?
// udpates with callback? e.g., :
// - reconstructor offers a function void updatePhotons(int new_val, int iWFS);
// - this function is called within int updateGS(Star* star, int n_photons);

// free all memory!
void aao_freeSystem(aao_System *sys);

// Display system configuration in the standard output (using printf)
// useful for debugging
//  Input: system struct
void aao_printSystem(const aao_System *sys);

#ifdef __cplusplus
}
#endif

#endif /* AAO_SYSTEM_H_ */

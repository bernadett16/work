/* 
 * Definition of a struct for wavefront sensors
 * used for the Austrian reconstructors
 * each instance contains:
 * 	type of the wfs (sh or pyr)
 * 	number of subapertures
 * 	subaperture mask
 * 	wavelength
 * 	star belonging to this sensor (separate struct)
 * 
 * functions belonging to wfs:
 * setWFS: initializes a wfs with given data
 * updateWFS: calls updateStar and changes parameters
 * 
 * Author: RW, 05/2014
 * Last change: MY 27.06.2014
 */


#ifndef AAO_WFS_H_
#define AAO_WFS_H_

#include "aao_star.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum  {ShackHartmann, Pyramid} aao_WFSType;

typedef struct aao_WFS
{
	aao_WFSType wfs_type_;
	int n_sub_; 			// number of subapertures of a square sensors in 1 dimension; fixed
    unsigned char * i_sub_; // subaperture mask; values = 0 (inactive) or 1 (active);
    						// fixed; size = n_sub_*n_sub_;
    float subap_size_;		// subaperture size projected to the telescope aperture; units: meters; fixed;
    float wavelength_; 		// fixed; units: nanometers
    aao_Star gs_; 				// Guide star assigned to the WFS; fixed;

    // shift, scale and rotation of the WFS w.r.t. (0,0), the center of telescope aperture
    // !!! NOTE: not implemented yet !!!
    float shift_x_;
    float shift_y_;
    float scale_;
    float rotation_;
} aao_WFS;

// Example settings:
// wfs_type_ = ShackHartmann;
// n_sub_ = 4;
// i_sub_ = [0 1 1 0  1 1 1 1  1 1 1 1   0 1 1 0];
// size_ = 0.5;
// wavelength_ = 589;
// gs_.type_ = NGS
// gs_.pos_x_ = ...


// Set WFS parameters
// out: wfs
//  in: wfs_type, n_sub, i_sub, subap_size, wavelength, guide star
// ret: 0 on ok
int aao_setWFS(aao_WFS* wfs, aao_WFSType wfs_type, int n_sub, const unsigned char* i_sub, float subap_size, float wavelength, const aao_Star* gs);

// Free memory allocated to WFS struct
void aao_freeWFS(aao_WFS* wfs);

// Display wfs configuration in the standard output (using printf)
// useful for debugging
//  Input: wfs struct
void aao_printWFS(const aao_WFS* wfs);

#ifdef __cplusplus
}
#endif


#endif /* AAO_WFS_H_ */


/*
 * aao_FEWHA_log.h
 *
 *  Created on: Nov 28, 2014
 *      Author: misha
 */

#ifndef AAO_FEWHA_LOG_H_
#define AAO_FEWHA_LOG_H_

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct aao_FEWHA_Log
{
	// log counter
	unsigned int counter;

	// pointers to files
	FILE * wav_coef;		// pointer to file where wavelet coefficients are written fewha.loop.c
	FILE * res_coef;		// pointer to file where residual coefficients are written fewha.loop.r
	FILE * lay_coef;		// pointer to file where bilinear layer coefficients are written
	FILE * dmshapes;		// pointer to file where dm shapes are written

} aao_FEWHA_Log;

// set elements of the log file to 0 / NULL
// out: fewha_log
void aao_zeroFEWHAlog(aao_FEWHA_Log * fewha_log);




#ifdef __cplusplus
}
#endif



#endif /* AAO_FEWHA_LOG_H_ */

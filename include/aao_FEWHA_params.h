/*
 * FEWHA_params.h
 *
 *  Created on: Jul 24, 2014
 *      Author: misha
 */

#ifndef AAO_FEWHA_PARAMS_H_
#define AAO_FEWHA_PARAMS_H_

#ifdef __cplusplus
extern "C" {
#endif

// Parameters of the FEWHA method
typedef struct aao_FEWHA_Params
{
	// Reconstruction layer settings for FEWHA
	// use in compliment to atmosphere.h
	float * d_lay_;		// size = nLay (n_layer_), equidistant spacing; units: meters; fixed
	int   * J_lay_; 	// size = nLay (n_layer_), number of wavelet scales on layer; fixed
	int     J_glcs_;	// ground layer coarse scales, J_glcs_ = 0 means GLMS turned off; fixed

	// Regularization tuning parameters and gain
	float alpha_;		// regularization tuning, range: [0,infty), typical value: 1; variable
	float alpha_eta_;	// elongation tuning, range: [0,1], 0...NGS model, 1...LGS model; variable
	float alpha_J_;		// preconditioner thresholding; range: [0,infty), typical value: 10e7; variable
	float gain_;		// controller gain, range: (0,1], typical value: 0.4; variable
	float leaky_int_;	// leaky integrator value, range: (0,1], typical value: 0.99; variable

	// Number of PCG iterations
	int max_iter_;		// number of iterations of PCG; range: |N; variable

	// Options switches
	int useGLMS;		// set to 1 to use GLMS, set to skip GLMS
	int usePrecond;		// set to 1 to use PCG, set to 0 to use CG
	char *createPrecondFromFile;	// avoid heavy computation and use a precond from a txt file (depends on input parameters!)

} aao_FEWHA_Params;

//Example settings:
// d_lay_  = [0.5, 1, 1]
// J_lay_  = [7, 6, 6] (corresponds to n_lay = [128, 64, 64] in one dimension; 2^J = n_lay)
// J_glcs_ = 4	(J_glcs_ = 0 means GLMS turned off)
//
// alpha_     = 1
// alpha_eta_ = 0.4
// alpha_J_   = 10e7
// gain_      = 0.4
//
// max_iter_ = 4

// Set parameters of the layers
// allocates memory when needed
// out: fewha_par
//  in: n_layer, d_lay, J_lay, J_glcs, alpha, alpha_eta, alpha_J, gain, leaky_int, max_iter, useGLMS, usePrecond
// ret: 0 on ok
int aao_setFEWHAparams(aao_FEWHA_Params * fewha_par, int n_layer, float const * d_lay, int const * J_lay, int J_glcs,
		float alpha, float alpha_eta, float alpha_J, float gain, float leaky_int, int max_iter, int useGLMS, int usePrecond, char *createPrecondFromFile);

// Free memory allocated to params struct
void aao_freeFEWHAparams(aao_FEWHA_Params * fewha_par);

// Display configuration in the standard output (using printf)
// useful for debugging
//  Input: fewha_par
void aao_printFEWHAparams(aao_FEWHA_Params const * fewha_par, int n_layer);

#ifdef __cplusplus
}
#endif

#endif /* FEWHA_PARAMS_H_ */

/*
 * aao_proj_op_structs.h
 *
 *  Created on: Aug 29, 2014
 *      Author: misha
 */

#ifndef AAO_PROJ_OP_STRUCTS_H_
#define AAO_PROJ_OP_STRUCTS_H_

#ifdef __cplusplus
extern "C" {
#endif

// Interpolation struct
typedef struct aao_Interp2{
	unsigned short  * ref_x_indx; 	// size = n_phi, (e.g., 84)
	float 			* ref_x_wght; 	// size = n_phi, (e.g., 84)
	unsigned short  * ref_y_indx; 	// size = n_phi, (e.g., 84)
	float 			* ref_y_wght; 	// size = n_phi, (e.g., 84)

	// tmp
	// size = n_phi*(I.ref_y_indx[n_phi0-1]+1 - I.ref_y_indx[0] + 1);
	// size has at most n_phi^2 elements (might as well allocate n_phi^2)
	float * tmp;
} aao_Interp2;

#ifdef __cplusplus
}
#endif


#endif /* AAO_PROJ_OP_STRUCTS_H_ */

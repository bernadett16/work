/*
 * Definition of a struct for a star
 * used for the Austrian reconstructors
 * one star might be as well a guide star (and thus ngs or lgs) as a target
 * one instance contains:
 * 	type of the star: ngs, lgs or target
 * 	position (posx_, posy_) of the star on sky
 * 	photon flux (only for gs needed, 0 for target stars)
 * 	some lgs only parameters that are set to 0 for ngs or target:
 * 		gs height
 * 		spot fwhm
 * 		laser launch position (wrt to the wfs)
 * 	target specific (MCAO only) and 0 else:
 * 		fov
 * 
 * Functions for this struct are:
 * setGuideStar: set parameters of a guide star. last parameters might be omitted as they are lgs only
 * getStarType: get the type of a star: lgs, ngs, target
 * updateStar: change parameters of a star that might be changed during run time
 * setTarget: set parameters of a target star
 * 
 * Author: RW, 05/2014 
 * Last change: MY 27.06.2014
 */


#ifndef AAO_STAR_H_
#define AAO_STAR_H_

#ifdef __cplusplus
extern "C" {
#endif


typedef enum {NGS, LGS} aao_StarType;

typedef struct aao_Star
{
	aao_StarType type_;
	float pos_x_; 		// star position in x-direction; units: arcmin; fixed
	float pos_y_; 		// star position in y-direction; units: arcmin; fixed
	int n_photons_; 	// number of photons units: photons / subaperture / frame; variable
	float spot_fwhm_; 	// spotsize fudge of the non-elongated spot; units = arsec; fixed
	float gs_llt_x_;   	// x-coordinate laser launch position away from center of telescope aperture (0,0);
						// fixed; units: meters
	float gs_llt_y_;   	// y-coordinate
	float fov_;			// corrected FoV radius for MCAO; units: arcmin; fixed
} aao_Star;

// Example:
// type_ = LGS;
// pos_x_ = 1;
// pos_y_ = 0;
// n_photons = 300; // 600000 photons / second / meter^2, frame = 0.002s, subap = 0.5^2 m
// spot_fwhm_ = 1.5;
// gs_llt_x_ = -16.16; // E-ELT side launch
// gs_llt_y_ =  16.16;
// fov_ = 1;



// Sets parameters of a Natural/Laser Guide Star
// out: star
//  in: pos_x, pos_y, n_photons, spot_fwhm (gs_llt_x, gs_llt_y if LGS)
// ret: 0 on ok
int aao_setNGS(aao_Star* star, float pos_x, float pos_y, int n_photons, float spot_fwhm);
int aao_setLGS(aao_Star* star, float pos_x, float pos_y, int n_photons, float spot_fwhm, float gs_llt_x, float gs_llt_y);

// Update parameters of a Natural/Laser Guide Star
// in/out: star
//     in: n_photons
// ret: 0 on ok
int aao_updateGS(aao_Star* star, int n_photons);

// Sets parameters of a Probe star
// out: star
//  in: pos_x, pos_y or fov
// ret: 0 on ok
int aao_setTarget    (aao_Star* star, float pos_x, float pos_y);
int aao_setTargetMCAO(aao_Star* star, float fov);

// Display star configuration in the standard output (using printf)
// useful for debugging
//  Input: star struct
void aao_printStar(const aao_Star* star);

#ifdef __cplusplus
}
#endif



#endif /* STAR_H_ */

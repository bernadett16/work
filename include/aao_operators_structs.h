/*
 * operators_structs.h
 *
 *  Created on: Jul 27, 2014
 *      Author: misha
 */

#ifndef AAO_OPERATORS_STRUCTS_H_
#define AAO_OPERATORS_STRUCTS_H_

// =========== Interpolation structs ===========
#include "aao_proj_op_structs.h"

#ifdef __cplusplus
extern "C" {
#endif

// =========== Operator structs ===========
// =========== Single operators ===========
// Operators: W, P, SH, C^{-1}, D, J^{-1}, G

typedef struct aao_OperatorW	/* === Wavelet transform operator === */
{
	float * wscf_c2e;			// size = nLay; wavelet scaling factor

	// tmp
	float * tmp_U;				// size = nLay*n_lay^2, variable for temporary storage of the wavelet transform
} aao_OperatorW;

typedef struct aao_OperatorP	/* === Projection operator === */
{
	aao_Interp2 * I_arr;		// size = nLay*nWFS, (buf if iLay==0, NULL-operator)
} aao_OperatorP;

typedef struct aao_OperatorSH	/* === Shack-Hartmann operator === */
{
	unsigned char const ** I_sub;
								// size = nWFS; size(I_sub[iWFS]) = n_sub*n_sub;
								// this is a different way of storage: b/c here we link I_sub directly to i_sub_ in wfs.h
								// NOT a copy!!!
	int * n_active_sub2;		// size = nWFS


	// tmp
	float * tmp_SH;				// size = nWFS*n_phi*n_sub;
	int * os_tmp_SH;			// size = nWFS; offsets for tmp_SH sub-vectors
} aao_OperatorSH;

typedef struct aao_OperatoriC	/* === Inverse noise covariance operator === */
{
	// LGS
	float * invCxx;				// size = nLGS*n_sub*n_sub
	float * invCxy;				// size = nLGS*n_sub*n_sub
	float * invCyy;				// size = nLGS*n_sub*n_sub
// TODO DELETE MY 06.07.2014 want to generalize, have invC for each LGS
// Compromise - more storage used: size(invCxx) = nLGS*n_sub*n_sub >= nLLP*n_sub*n_sub
// Reasoning: if the sensors have the same LLP but different dimensions or are rotated, they will need a different invC anyhow...
//	int   * os_invC_at_lgs;		// size = nLGS, e.g., 0, 84^2, 2*84^2, 2*84^2, 3*84^2, 0
//								// corresponds to 0 1 2 2 3 0 laser launch pos configuration

	// NGS
	float * inv_sigma2;			// size = nWFS; set to 0 for 0<=i<nLGS
} aao_OperatoriC;

typedef struct aao_OperatorD	/* === Wavelet penalty === */
{
	float ** d_at_lay;			// size = nLay; size d_at_lay[iLay] = number of scales at layer iLay
} aao_OperatorD;

typedef struct aao_OperatoriJ	/* === Frequency-dependent preconditioner === */
{
	float * d;					// size = nLay*n_lay*n_lay; 1./[diag(AwoD)+alpha*max(alpha_J,diag(D))]
	float * diag_AwoD;			// size = nLay*n_lay*n_lay; diag(AwoD) = diag(W P* SH* iC SH P W^{-1})
} aao_OperatoriJ;

typedef struct aao_OperatorG	/* === GLMS operator === */
{
	// A = AwoD + alpha*D = LDL'
	float * L_nz;				// cholesky L, size = nnz_diag_AwoD*nnz_diag_AwoD (<=n_lay_at_glcs*n_lay_at_glcs*n_lay_at_glcs*n_lay_at_glcs)
	float * D_nz;				// cholesky D, size = nnz_diag_AwoD 			  (<=n_lay_at_glcs*n_lay_at_glcs)
	float * AwoD_nz;			// size = nnz_diag_AwoD*nnz_diag_AwoD 			  (<=n_lay_at_glcs*n_lay_at_glcs*n_lay_at_glcs*n_lay_at_glcs)
	float * tmp_r_cs;			// size = n_lay_at_glcs*n_lay_at_glcs
	float * tmp_v_cs;			// size = n_lay_at_glcs*n_lay_at_glcs
	float * tmp_q;				// size = nLay*n_lay*n_lay;
	int nnz_diag_AwoD;			// compressed size of LDL' (<=n_lay_glcs2)
	int * nz2lay;				// size = nnz_diag_AwoD; index map: nnz_diag_AwoD indices to n_lay[0]*n_lay_[0]
	int * glcs2nz;				// size = n_lay_at_glcs*n_lay_at_glcs; index map: n_lay_at_glcs*n_lay_at_glcs to nnz_diag_AwoD
	// !!! WARNING !!! glcs2nz contians values -1 if index is "bad" (diag_AwoD[index] == 0)

} aao_OperatorG;


// =========== Composite operators ===========
// Operators: A (B, A_GLMS), H, F

typedef struct aao_OperatorA	/* === Forward operator === */
{
	aao_OperatorW  W;			// wavelet transform
	aao_OperatorP  P;			// projection through layers in GS directions
	aao_OperatorSH SH;			// Shack-Hartmann
	aao_OperatoriC iC;			// noise C_eta^{-1}
	aao_OperatorD  D;			// penalty term

	// tmp
	float * tmp_phi;			// size =   n_phi^2 * nWFS
	float * tmp_s;				// size = 2*n_sub^2 * nWFS
} aao_OperatorA;


typedef struct aao_OperatorH	/* === Pseudo-open loop operator === */
{
	aao_OperatorSH const * SH;	// reuse OperatarA's Shack-Hartmann
	aao_OperatorP Q;			// project through mirrors in GS directions

	// tmp
	float * tmp_phi;			// size =   n_phi^2 * nWFS, reused memory from A
	float * tmp_s;				// size = 2*n_sub^2 * nWFS, reused memory from A
} aao_OperatorH;

typedef struct aao_OperatorF	/* === Fitting operator === */
{
	aao_OperatorW const * W; 	// reuse OperatarA's wavelet transform
	aao_OperatorP P;			// project through layers  in probe star directions
	aao_OperatorP Q;			// project through mirrors in probe star directions
} aao_OperatorF;

#ifdef __cplusplus
}
#endif


#endif /* AAO_OPERATORS_STRUCTS_H_ */

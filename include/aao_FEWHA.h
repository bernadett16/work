/*
 * FEWHA.h
 *
 *  Created on: Jul 4, 2014
 *      Author: misha
 */

#ifndef AAO_FEWHA_H_
#define AAO_FEWHA_H_

#include "aao_FEWHA_params.h"
#include "aao_FEWHA_parallel.h"
#include "aao_FEWHA_log.h"
#include "aao_system.h"
#include "aao_algorithms_structs.h"
#include "aao_operators_structs.h"

#ifdef __cplusplus
extern "C" {
#endif

// =========== Version ================================
static const char aao_FEWHA_VERSION[128] = "1.1.7beta";
static const char aao_FEWHA_DATE   [128] = "05.06.2015"; // european format

typedef struct aao_FEWHA
{
	// =========== System Specs: dimensions ===============
	int nWFS;				// number of wavefront sensors / number of GS
	int nLGS;				// number of LGS; convention: 0, ... nLGS-1, nLGS, ..., nLGS+nNGS-1, ..., nLGS+nNGS+nTTS-1 = nWFS-1
	int nNGS;				// number of NGS
	int nTTS;				// number of tip/tilt sensors (low-order NGS)
// TODO DELETE MY 06.07.2014 want to generalize, have invC for each LGS, see operators.h
//	int nLLP;				// number of laser launch positions: nLLP <= nLGS
	int nLay;				// number of reconstruction layers
	int nDM;				// number of deformable mirrors
	int nDIR;				// number of optimization directions:
							// in SCAO and MCAO (nLay=nDM) = 0 (F = Id W^{-1})
							// in LTAO = 1 (F = P W^{-1})
							// in MOAO = number of DMs (F = P W^{-1})
							// in MCAO (nLay>nDM) = n optimization over the field of view [this case is for now unhandled!!!]

	// =========== Vector sizes ===========================
	int n_tot_meas;			// total number of measurements (twice the number of all subapertures)
	int n_tot_phi;			// total number of wavefront coefficients of the system (in bilinear domain)
	int n_tot_sub_lgs;		// total number of sub-apertures of all LGS
	int n_tot_lay;			// total number of coefficients to represent all layers (in bilinear & wavelet domains)
	int n_tot_act;			// total number of coefficients to represent all actuators (in bilinear domain)


	int * n_sub_at_wfs;		// size = nWFS, number of subapertures at WFS (in 1 dim), e.g, 84, 84, ...
	int * os_sub_at_wfs;	// size = nWFS, offset of subaperture vector at WFS, e.g., 0, 84^2, 2*84^2, ...
	int * os_meas_at_wfs;	// size = nWFS, offset of measurement vector at WFS, e.g., 0, 2*84^2, 2*(2*84^2), ...

	int * n_phi_at_wfs;		// size = nWFS, number of wavefront coeffs at WFS (in 1 dim), e.g., 85, 85, ...
	int * os_phi_at_wfs;	// size = nWFS, offset of wavefront vector at WFS, e.g., 0, 85^2, 2*85^2, ...

	int * n_lay_at_lay;		// size = nLay, number of layer coeffs at layer (in 1 dim), e.g., 128, 64, 64, ...
	int * os_lay_at_lay;	// size = nLay, offset of layer coeff vector at layer, e.g., 0, 128^2, 128^2+64^2, ...
	float * x0_lay_at_lay;	// size = nLay, coordinates of corner point (x0,x0) of indices ix=0,iy=0:
							// \Omega_{\ell} = [0,(2^J_lay-1)*d_lay]^2 + (x0,x0)
	int   n_lay_at_glcs;	// size = nLay, number of layer coeffs at ground layer coarse scale (in 1 dim),
							// e.g., 16 (corresponds to 16^2=256 vector)

	int * n_act_at_dm;		// size = nDM, number of actuators at DM (in 1 dim), e.g., 85, 85, ...
	int * os_act_at_dm;		// size = nDM, offset of actuator vector at DM, e.g., 0, 85^2, 2*85^2, ...
	float * d_act_at_dm;	// size = nDM, spacing of actuators in meters (only for square DM), e.g., 0.5, 1, 1,

	// External and intenal ordering of the WFS may be different
	// Internally, we use the convention: 0, ... nLGS-1, nLGS, ..., nLGS+nNGS-1, ..., nLGS+nNGS+nTTS-1 = nWFS-1
	// Externally, arrangement can be arbitrary
	// E.g., external indexing lgs0, lgs1, tts0, ngs0, lgs2, lgs3, internal indexing lgs0, lgs1, lgs2, lgs3, ngs0, tts0
	int * wfs_indx_int2ext;	// size = nWFS, based on example above: [0 1 4 5 3 2]
	int * wfs_indx_ext2int;	// size = nWFS, based on example above: [0 1 5 4 2 3]


	// =========== Reconstructor configuration ============
	// Settings: full AO system
	aao_System AOsys;

	// FEWHA configurable settings
	aao_FEWHA_Params param;

	// Parallelization configuration
	aao_FEWHA_Parallel parall;


	// =========== FEWHA reconstructor components =========
	// Algorithm components
	aao_PCG   pcg;		// for PCG
	aao_Loop  loop;		// for control

	// Composite operators
	aao_OperatorA  A;	// Tomography: LHS operator A, RHS operator B, A_GLMS
	aao_OperatoriJ iJ;	// Frequency-dependent preconditioner for A
	aao_OperatorH  H;	// POL DM-to-Meas operator
	aao_OperatorF  F;	// Fitting operator
	aao_OperatorG  G;   // GLMS operator

	// =========== FEWHA log files ========================
	aao_FEWHA_Log log;


} aao_FEWHA;

// =========== FEWHA init/free =========
// Initializes the FEWHA struct:
// 1) copies settings: AOsys, params, parall (by value)
// 2) computes vector dimiensions
// 3) pre-computes all matrices / operators
// 4) allocates temporary vectors
// 5) defines parallelization config
//  Input: AOsys, params, parall
// Output: fewha
void aao_initFEWHA(aao_FEWHA * fewha, aao_System const * AOsys, aao_FEWHA_Params const * params, aao_FEWHA_Parallel const * parall);

// Frees the FEWHA struct
// all memory is dealloacted
// In/Out: fewha
void aao_freeFEWHA(aao_FEWHA * fewha);

// =========== FEWHA run ===============
// Performs a full computation of the finite element hybrid algorithm
// control algorithm (see algorithms/algorithms.h and algorithms/control.c) is applied
// In/Out: dmshapes (this vector will be modiefied!!!!)
//           as an input vector, dmshapes are the current shapes, as an output vector
//           dmshapes are the new shapes
//  Input: slopes (this vector will be modified!!!!)
//         fewha struct (this struct will be modified)
void aao_runFEWHA(float * dmshapes, float * slopes, aao_FEWHA * fewha);

// An inverse operator: direct mapping of DM shapes to slopes (uses the POLC's applyH operator)
// Output: slopes (this vector will be modified!!!!)
//  Input: dmshapes (this vector will not be modified)
//         fewha struct (this struct (some temporary variables) will be modified)
void aao_invFEWHA(float * slopes, float const * dmshapes, aao_FEWHA * fewha);

// ====== FEWHA update at runtime ======
// Resets (opens) the FEWHA loop: current and old DM shapes are set to 0;
// warm restart is set to zero
// In/Out: dmshapes (current shapes)
//         fewha struct (loop variable of the struct will be modified)
void aao_resetFEWHAloop(float * dmshapes, aao_FEWHA * fewha);

// Update gain parameter
// In/Out: fewha struct (current shapes)
//  Input: gain (new gain value)
// Return: 0 on success
int aao_updateFEWHAgain(aao_FEWHA * fewha, float gain);

// =========== FEWHA print ==============
// Display FEWHA version
// In/Out: ver, as input:
//          NULL - version and release date is printed to standard output (via printf)
//         !NULL - version string is copied to ver (must be at least 128 chars long), nothing is printed
void aao_printFEWHAversion(char * ver);

// Display FEWHA configuration in the standard output (using printf)
// useful for debugging
//  Input: fewha struct
void aao_printFEWHAconfig(aao_FEWHA const * fewha);

// Display FEWHA configuration in the standard output (using printf)
// useful for debugging
//  Input: fewha struct
void aao_printFEWHAconfig(aao_FEWHA const * fewha);

// Display FEWHA configuration in the standard output (using printf)
// useful for debugging
//  Input: fewha struct
void aao_printFEWHAconfig(aao_FEWHA const * fewha);

// Write FEWHA output to log files. Following quantities are logged:
// - wavelet coefficients vector
// - residual coefficients vector
// - layer shapes (bilinear coefficients)
// - dm shapes
// useful for debugging
// In/Out: fewha struct
//  Input: directory, prefix
//         e.g., directory = "/home/data"; prefix = "LTAOonCANARY" will produce files like
//         /home/data/LTAOonCANARY_2014_11_28-10_23_14_wav_coef.txt
// call aao_openFEWHAlog before the loop to open files (returns 0 if succsfully opened all files and -1 if not)
// call aao_writeFEWHAlog during the loop to write log
// call aao_closeFEWHAlog after the loop to close files (also automatically called via aao_freeFEWHA)
int  aao_openFEWHAlog (aao_FEWHA * fewha, const char * dir, const char * prefix);
void aao_writeFEWHAlog(aao_FEWHA * fewha);
void aao_closeFEWHAlog(aao_FEWHA * fewha);

void aao_saveFEWHAdata(aao_FEWHA *fewha);
void aao_save_runtimeFEWHAdata(aao_FEWHA *fewha, float *dmshapes, float *slopes, int after_calculation);
void aao_read_runtimeFEWHAdata(aao_FEWHA *fewha, float *dmshapes, float *slopes, int block, int after_calculation);
void aao_readFEWHAdata(aao_FEWHA *fewha, float **dmshapes, float **slopes);


#ifdef __cplusplus
}
#endif




#endif /* AAO_FEWHA_H_ */

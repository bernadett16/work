FEWHA - Finite Element-Wavelet Hybrid Algorithm for atmospheric tomography

A SCAO/LTAO/MOAO/MCAO reconstructor that utilizes compactly supported orthogonal wavelet
and finite-element bases to descritize turbulence layers.  The solution algorithm is based 
on the conjugate gradient iteration.

FEWHA v1.1.7
Date: 21.05.2015
Author: Misha Yudytskiy

1) Contents:
   FEWHA is located in the following directories:
   include ... interfece files for AO system and the reconstructor
   src/algorithm ... PCG & Control algorithms 
                     (see src/algorithm/aao_algorithms.h)
   src/aosystem ... files that describe the AO system  
                    (see include/aao_system.h)
   src/fewha ... FEWHA reconstructor interface: configuration, init/run/free 
                 (see include/aao_FEWHA.h)
   src/interfaces ... interfaces to: octopus
   src/io ... console I/O, file I/O
   src/operators ... operations associated to the full tomographic problem 
                     (see src/operators/aao_operators.h)
   src/suboperators ... operations associated to as single layer/guide star direction 
                        (see src/suboperators/*.h)
   
   an example AO aystem initializaiton routine: src/test_initCANARY.c
   an example AO system initialization + FEWHA reconstrutor routine: src/test_initCANARY_FEWHA.c


2) Features:
   SCAO/LTAO/MOAO/MCAO Reconstructor
   Input: Slopes, Output: (piecewise bilinear) DM shapes
   Conjugate Gradient algorithm
   No preconditioner (in this version)
   Ground layer multi-scale (GLMS)
   Global parallelization scheme
   Can handle LGS and NGS (but low-order NGS (TTS) not in this version)
   An interface to octopus is available
   
   
3) Procedure/flow:

   1. Initialization (off-line)
      - Define the AO system: 
        set single or multiple object structs: DM, (Guide-)Star, WFS, Atmosphere, (Probe-)Star
        init System struct using those parameters
      - Set FEWHA parameters & parallelization configuration
        output is a fully initialized FEWHA struct

   2. Evaluation (on-line)
      - Call runFEWHA, input: slopes, FEWHA struct; output: DM shapes
  
   3. Free resources (off-line)
   
   See src/test_initCANARY_FEWHA.c for an example.


4) Ordering of the slopes, dmshapes:
   In short: Two-dimensional (x,y)-Cartesian system.
      Points (x_i, x_j) in R^2, where x_0 < x_1 < ... < x_n.
      Continuous piecewise bilinear function f(x_i,x_j) is given by (n+1)^2 values f_{ij}.
      Values are stored in vector form: f(x_i,x_j) = f_{ij} = f[i*(n+1)+j], i.e., as vector
      [f(x_0,x_0), f(x_0,x_1), f(x_0,x_2), ..., f(x_0,x_n), f(x_1, x_0), ..., f(x_n,x_n)]
      
      Shack-Hartmann slopes are given over squares [x_i,x_{i+1}] x [x_j,x_{j+1}], for i,j=0, ..., n-1.
      Slopes are given by 2n^2 scalar values sx_{ij}, sy_{ij}.
      Values are stored in vector form: sx_{ij} = s[i*n+j], sy_{ij} = s[n*n+i*n+j], i.e., as vector
      [sx_{00},sx_{01},...,sx_{0(n-1)},sx_{10},...sx{(n-1)(n-1)},sy_{00},sy_{01},...,sy{(n-1)(n-1)}]
      
      Mirror shapes are concatenated in a single long vector:
      [DM0, DM1, ..., DM(N-1)]
      All slopes are concatenated in a long vector:
      [sx0, sy0, sx1, sy1, ... sx(M-1), sy(M-1)]
      Here: N mirrors, M WFSs.
      
      !!!!IMPORTANT - NOTE!!!! 
      1. Ordering w.r.t. LGS/NGS: first all LGS, then all NGS
      2. A slope vector of a single WFS has 2n^2 values, i.e., also inactive apertures are elements 
      of the vector (their value may be arbitrary, e.g., 0., as they are not used for calculation)
      
   Long version: see doc/rec_notation.pdf
   
   
5) Compilation:
   A makefile is available.
   Include directories:
     -I"include" -I"src/algorithms" -I"src/aosystem" -I"src/operators" -I"src/suboperators" -I"src/fewha" -I"src/io"
   Compile with -fopenmp flag for parallelization
   
